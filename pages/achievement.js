/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-22 10:10:50
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';
import Achievement from 'src/containers/Achievement';
import LoginRequire from 'src/layout/LoginRequire';

import Title from 'src/components/Layout/Title';

@withRoot
export default class AchievementPage extends PureComponent {
	static async getInitialProps(ctx) {
		return { categoryId: ctx.query.categoryId };
	}

	render() {
		return (
			<MainLayout>
				<Title>Achievement</Title>
				<LoginRequire>
					<Achievement />
				</LoginRequire>
			</MainLayout>
		);
	}
}
