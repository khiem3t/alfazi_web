/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-10 23:32:12
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import withRoot from 'src/root';

// import AuthStorage from 'src/utils/AuthStorage';

import MainLayout from 'src/layout/Main';

import SignUpForm from 'src/components/Form/SignUp';

import Title from 'src/components/Layout/Title';

@withRoot
export default class SignUpPage extends PureComponent {
	static async getInitialProps(ctx) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		return { ...ctx.query };
	}

	render() {
		return (
			<MainLayout>
				<Title>Đăng ký</Title>
				<div style={{ margin: '50px 0', textAlign: 'center' }}>
					<SignUpForm isLoginPage referralId={this.props.referralId} />
				</div>
			</MainLayout>
		);
	}
}
