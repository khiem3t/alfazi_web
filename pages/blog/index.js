/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-10 23:32:12
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withRoot from 'src/root';
import Title from 'src/components/Layout/Title';

import MainLayout from 'src/layout/Main';
import Blog from 'src/containers/Blog/List';

@withRoot
export default class BlogPage extends PureComponent {
	static async getInitialProps(ctx) {
		return { categoryId: ctx.query.categoryId };
	}

	static propTypes = {
		categoryId: PropTypes.string,
	}

	static defaultProps = {
		categoryId: undefined,
	}

	render() {
		return (
			<MainLayout>
				<Title>Blog</Title>
				<Blog categoryId={this.props.categoryId} />
			</MainLayout>
		);
	}
}
