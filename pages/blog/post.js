/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-10 23:32:12
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withRoot from 'src/root';
import Title from 'src/components/Layout/Title';

import MainLayout from 'src/layout/Main';
import BlogPostDetail from 'src/containers/Blog/PostDetail';

import { getPostData } from 'src/redux/actions/blogPost';

@withRoot
export default class BlogPostPage extends PureComponent {
	static async getInitialProps(ctx) {
		if (ctx.isServer && ctx.query.slug) {
			await ctx.store.dispatch(getPostData({ filter: { where: { slug: ctx.query.slug }, include: 'category' } }));
		}
		return { slug: ctx.query.slug };
	}

	static propTypes = {
		slug: PropTypes.string.isRequired,
	}

	static defaultProps = {}

	render() {
		return (
			<MainLayout>
				<Title>Blog</Title>
				<BlogPostDetail slug={this.props.slug} />
			</MainLayout>
		);
	}
}
