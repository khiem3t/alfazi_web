/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 08:19:32
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withRoot from 'src/root';

import Title from 'src/components/Layout/Title';

import MainLayout from 'src/layout/Main';
import Profile from 'src/containers/Profile';

import { getUserData } from 'src/redux/actions/user';

@withRoot
export default class ProfilePage extends PureComponent {
	static async getInitialProps(ctx) {
		if (ctx.isServer && ctx.query.id) {
			await ctx.store.dispatch(getUserData({
				id: ctx.query.id,
				filter: {
					where: {
						status: 'active',
					},
				},
			}));
		}
		return { userId: ctx.query.id };
	}

	static propTypes = {
		userId: PropTypes.string.isRequired,
	}

	static defaultProps = {}

	render() {
		return (
			<MainLayout>
				<Title>Hồ sơ người dùng</Title>
				<Profile userId={this.props.userId} />
			</MainLayout>
		);
	}
}
