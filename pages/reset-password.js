/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-10 15:08:14
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';

import SetPassword from 'src/components/Form/SetPassword';

import Title from 'src/components/Layout/Title';

@withRoot
export default class ResetPasswordPage extends PureComponent {
	static propTypes = {
		url: PropTypes.object.isRequired,
	}

	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		const { url } = this.props;
		return (
			<MainLayout>
				<Title>Đặt lại mật khẩu</Title>
				<div style={{ margin: '50px 0', textAlign: 'center' }}>
					<SetPassword token={url.query && url.query.access_token} />
				</div>
			</MainLayout>
		);
	}
}
