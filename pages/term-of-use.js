/**
 * Zelo team
 * @author Tran Trung
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';
import Container from 'src/components/Layout/Container';

import Title from 'src/components/Layout/Title';

import withStyles from 'src/theme/jss/withStyles';

const styleSheet = (/* theme */) => ({
	root: {
		padding: '45px',
		background: '#fff',
	},
});

@withRoot
@withStyles(styleSheet)
export default class TermOfUsePage extends PureComponent {
	static propTypes = {
		// url: PropTypes.object.isRequired,
		classes: PropTypes.object.isRequired,
	}

	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		const { classes } = this.props;

		return (
			<MainLayout>
				<Title>Điều khoản sử dụng</Title>
				<div style={{ margin: '50px 0' }}>
					<Container className={classes.root}>
						<Row type="flex" justify="center" align="top">
							<Col span={24}>
								<h2>Điều khoản sử dụng</h2>

								<p><strong>1. QUAN HỆ HỢP ĐỒNG</strong></p>

								<p>Các Điều khoản Sử dụng này (<em>Điều khoản</em>) điều chỉnh việc bạn, một cá nhân, từ bất kỳ quốc gia nào trên thế giới, truy cập hoặc sử dụng các ứng dụng, trang web, nội dung, sản phẩm và dịch vụ (<em>Dịch vụ</em>) của Alfazi, &nbsp;một công ty cổ phần thành lập ở Việt Nam, có văn phòng tại 2/5A&nbsp;Quang Trung, phường 14, quận Gò Vấp,&nbsp;thành phố Hồ Chí Minh, Việt Nam, Giấy chứng nhận đăng ký kinh doanh số 0313610652. Do Phòng đăng ký kinh doanh – Sở Kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp ngày 11/01/2016.</p>

								<p><strong>VUI LÒNG ĐỌC KỸ CÁC ĐIỀU KHOẢN NÀY TRƯỚC KHI TRUY CẬP HOẶC SỬ DỤNG DỊCH VỤ.</strong></p>

								<p>Việc tiếp cận và sử dụng các Dịch vụ của bạn đã cấu thành sự chấp nhận chịu sự ràng buộc của các Điều khoản này, sự ràng buộc này sẽ thiết lập mối quan hệ hợp đồng giữa bạn và Alfazi. Nếu bạn không đồng ý với các Điều khoản này, bạn sẽ không có quyền truy cập hoặc sử dụng các dịch vụ. Các Điều khoản này sẽ thay thế cho các thỏa thuận hoặc hợp đồng trước đây của bạn. Alfazi có thể ngay lập tức chấm dứt các Điều khoản này hoặc bất kỳ Dịch vụ nào liên quan đến bạn, hoặc nói chung là ngừng cung cấp hoặc từ chối quyền truy cập đến các Dịch vụ hoặc bất kỳ phần nào của dịch vụ, vào bất kỳ lúc nào vì bất kỳ lý do gì.</p>

								<p>Các điều khoản bổ sung có thể được áp dụng cho các Dịch vụ nhất định, chẳng hạn như các chính sách cho một sự kiện, hoạt động hoặc khuyến mãi cụ thể, và các điều khoản bổ sung này sẽ được tiết lộ cho bạn liên quan đến các Dịch vụ được áp dụng. Các điều khoản bổ sung được thêm vào, và sẽ được coi là một phần của, các Điều khoản dành cho các mục đích của Dịch vụ được áp dụng. Các điều khoản bổ sung sẽ được ưu tiên áp dụng hơn các Điều khoản này trong trường hợp có xung đột liên quan đến các Dịch vụ được áp dụng.</p>

								<p>Alfazi có thể sửa đổi các Điều khoản liên quan đến các Dịch vụ tùy từng thời điểm. Các sửa đổi sẽ có hiệu lực khi Alfazi đăng các Điều khoản được cập nhật tại vị trí này hoặc các chính sách sửa đổi hoặc các điều khoản bổ sung về Dịch vụ áp dụng. Nếu bạn tiếp tục truy cập hoặc sử dụng các Dịch vụ sau các bài đăng này, nghĩa là bạn đồng ý chịu sự ràng buộc của các Điều khoản đã được sửa đổi.</p>

								<p>Việc thu thập và sử dụng thông tin cá nhân liên quan đến các Dịch vụ của chúng tôi được quy định trong Chính sách Bảo mật của Alfazi tại <a href="http://alfazi.com/Terms-of-service.html">http://alfazi.com/chinh-sach-bao-mat</a> .</p>

								<p><strong>2. CÁC DỊCH VỤ</strong></p>

								<p>Các Dịch vụ tạo thành một nền tảng công nghệ cho phép người dùng sử dụng các ứng dụng di động của Alfazi hoặc các trang web được Alfazi cung cấp như một phần của Dịch vụ (gọi chung là <em>Ứng dụng</em>) để sắp xếp và lên lịch giảng dạy và học tập với bên thứ ba độc lập với Alfazi gồm: các trung tâm gia sư, các gia sư là các cá nhân tự do không thuộc bất kỳ trung tâm nào. Trừ khi Alfazi có sự chấp thuận khác trong một thỏa thuận bằng văn bản riêng với bạn, các Dịch vụ này chỉ được cung cấp cho mục đích sử dụng cá nhân, phi thương mại. BẠN CÔNG NHẬN RẰNG ALFAZI KHÔNG CUNG CẤP DỊCH VỤ GIA SƯ VÀ GIẢNG DẠY HOẶC CÓ CHỨC NĂNG GIA SƯ VÀ GIẢNG DẠY. TẤT CẢ DỊCH VỤ GIA SƯ VÀ GIẢNG DẠY ĐƯỢC CUNG CẤP BỞI NHÀ THẦU ĐỘC LẬP BÊN THỨ BA KHÔNG LÀM VIỆC CHO ALFAZI HOẶC BẤT KỲ CHI NHÁNH NÀO CỦA ALFAZI.</p>

								<p><strong>GIẤY PHÉP.</strong></p>

								<p>Tùy vào việc tuân thủ theo các Điều khoản này của bạn, Alfazi sẽ cấp cho bạn giấy phép hữu hạn, không độc quyền, không thể cấp giấy phép con, có thể hủy bỏ và không thể chuyển nhượng để: (i) truy cập và sử dụng các Ứng dụng trên các thiết bị cá nhân chỉ cho mục đích sử dụng Dịch vụ của bạn; và (ii) truy cập và sử dụng bất kỳ nội dung, thông tin và các tài liệu nào liên quan có thể sẵn có thông qua các Dịch vụ, chỉ cho mục đích sử dụng cá nhân, phi thương mại của bạn trong mỗi trường hợp. Mọi quyền lợi không được công nhận rõ ràng bằng văn bản này đều được Alfazi và tổ chức cấp phép của Alfazi bảo lưu.</p>

								<p><strong>CÁC GIỚI HẠN.</strong></p>

								<p>Bạn không thể:
                                    (i) xóa bỏ bất kỳ bản quyền, thương hiệu hoặc thông tin độc quyền khỏi bất kỳ phần nào của Dịch vụ;
                                    (ii) xuất bản lại, sửa đổi, tạo ra tác phẩm phái sinh dựa trên, phân phối, cấp phép, cho thuê, bán, bán lại, chuyển nhượng, trưng bày công khai, thực hiện công khai, truyền tải, truyền phát, phát sóng hoặc khai thác các Dịch vụ trừ khi được Alfazi cho phép;
                                    (iii) dịch ngược, đảo ngược hoặc phân tách Dịch vụ trừ khi được luật pháp cho phép;
                                    (iv) liên kết đến, ánh xạ hoặc điều chỉnh bất kỳ phần nào của Dịch vụ;
                                    (v) gây ra hoặc khởi chạy bất kỳ chương trình hoặc câu lệnh nào với mục đích rút trích, lập chỉ mục, khảo sát, hoặc khai thác dữ liệu trong bất kỳ phần nào của Dịch vụ hoặc khai thác quá tải hoặc cản trở các hoạt động và/hoặc chức năng của bất kỳ phần nào của Dịch vụ; hoặc
                                    (vi) cố gắng nắm được quyền truy cập trái phép hoặc gây tổn hại bất kỳ phần nào của Dịch vụ hoặc hệ thống hoặc mạng lưới liên quan đến Dịch vụ.
								</p>
								<p><strong>CUNG CẤP DỊCH VỤ.</strong></p>

								<p>Bạn công nhận rằng các phần của Dịch vụ có thể được cung cấp dưới các nhãn hiệu được gọi là Alfazi. Bạn cũng công nhận rằng các Dịch vụ có thể được cung cấp dưới các nhãn hiệu đó hoặc tùy chọn theo yêu cầu bởi hoặc liên quan đến: (i) một số các công ty con và chi nhánh của Alfazi; hoặc (ii) Bên Thứ ba độc lập(các trung tâm gia sư, các gia sư là các cá nhân tự do không thuộc bất kỳ trung tâm nào)</p>

								<p><strong>CÁC DỊCH VỤ VÀ NỘI DUNG CỦA BÊN THỨ BA.</strong></p>

								<p>Các Dịch vụ có thể được cung cấp hoặc truy cập liên quan đến các dịch vụ và nội dung của bên thứ ba (bao gồm cả quảng cáo) mà Alfazi không kiểm soát. Bạn công nhận rằng các điều khoản khác nhau về sử dụng và chính sách về quyền riêng tư có thể áp dụng đối với việc sử dụng dịch vụ và nội dung của bên thứ ba. Alfazi không xác nhận các dịch vụ và nội dung của bên thứ ba và trong mọi trường hợp Alfazi sẽ không chịu trách nhiệm hoặc nghĩa vụ về bất kỳ sản phẩm hoặc dịch vụ nào của các nhà cung cấp bên thứ ba đó. Ngoài ra, Apple Inc., Google, Inc., Microsoft Corporation hoặc BlackBerry Limited và/hoặc các công ty con và các chi nhánh quốc tế áp dụng của họ sẽ là bên thứ ba được hưởng lợi từ hợp đồng này nếu bạn truy cập vào các Dịch vụ này bằng cách sử dụng các Ứng dụng được phát triển cho các thiết bị di động tương ứng của Apple iOS, Android, Microsoft Windows hoặc Blackberry. Bên thụ hưởng thứ ba này không phải là các bên tham gia hợp đồng này và không chịu trách nhiệm về việc cung cấp hoặc hỗ trợ các Dịch vụ theo bất kỳ cách nào. Việc bạn truy cập các Dịch vụ bằng cách sử dụng các thiết bị này tuân theo các điều khoản được quy định trong các điều khoản dịch vụ của bên thụ hưởng thứ ba.</p>

								<p><strong>QUYỀN SỞ HỮU.</strong></p>

								<p>Các Dịch vụ và tất cả các quyền trong văn bản này là tài sản của Alfazi hoặc tài sản của tổ chức cấp phép của Alfazi. Các Điều khoản này và việc bạn sử dụng các Dịch vụ này không thể hiện hoặc cấp cho bạn các quyền: (i) về hoặc liên quan đến các Dịch vụ này, ngoại trừ giấy phép hữu hạn đã được cấp ở trên; hoặc (ii) sử dụng hoặc tham chiếu tên công ty, logo, tên sản phẩm và dịch vụ, thương hiệu hoặc nhãn dịch vụ của Alfazi hoặc của các tổ chức cấp phép của Alfazi dưới bất kỳ hình thức nào.</p>

								<p><strong>3. SỬ DỤNG DỊCH VỤ ALFAZI</strong></p>

								<p><strong>TÀI KHOẢN NGƯỜI DÙNG.</strong></p>

								<p>Để sử dụng hầu hết các khía cạnh của Dịch vụ, bạn&nbsp;phải đăng ký và duy trì hoạt động một tài khoản Dịch vụ người dùng cá nhân (<em>Tài khoản</em>). Việc đăng ký tài khoản yêu cầu bạn phải gửi cho Alfazi các thông tin cá nhân nhất định, chẳng hạn như tên, địa chỉ, số điện thoại di động của bạn và tuổi tác, cũng như tối thiểu một phương thức thanh toán hợp lệ (hoặc thẻ tín dụng hoặc đối tác thanh toán được chấp thuận). Bạn đồng ý duy trì thông tin chính xác, đầy đủ và cập nhật trong Tài khoản của bạn. Việc bạn không duy trì thông tin Tài khoản chính xác, đầy đủ và cập nhật, bao gồm đưa phương thức thanh toán không hợp lệ hoặc hết hạn sử dụng vào trong hồ sơ, có thể dẫn đến việc bạn sẽ không có quyền truy cập và sử dụng Dịch vụ hoặc chấm dứt Thỏa thuận này giữa bạn và Alfazi. Bạn chịu trách nhiệm đối với tất cả các hoạt động diễn ra trong Tài khoản của bạn, và bạn đồng ý duy trì tính an ninh và bảo mật của tên người dùng và mật khẩu Tài khoản của bạn tại mọi thời điểm. Trừ trường hợp được Alfazi cho phép bằng văn bản, bạn chỉ có thể sở hữu một Tài khoản.</p>

								<p><strong>YÊU CẦU VÀ ỨNG XỬ CỦA NGƯỜI DÙNG.</strong></p>

								<p>Bạn không thể uỷ quyền cho bên thứ ba sử dụng Tài khoản của bạn, Bạn không được chuyển nhượng hoặc chuyển giao Tài khoản của mình cho bất kỳ cá nhân hoặc thực thể pháp lý nào khác. Bạn đồng ý tuân thủ tất cả các luật áp dụng khi sử dụng các Dịch vụ, và bạn chỉ có thể sử dụng Dịch vụ cho các mục đích hợp pháp (<em>ví dụ như</em>, không tuyên truyền văn hóa phẩm đồi trụy, các thông tin liên quan đến chính trị, tôn giáo). Khi sử dụng các dịch vụ, bạn không được gây phiền toái, khó chịu, bất tiện, hoặc thiệt hại tài sản, cho các Nhà cung cấp Bên Thứ ba hoặc bất kỳ bên nào khác. Trong một số trường hợp bạn có thể được yêu cầu cung cấp giấy tờ chứng minh quyền truy cập hoặc sử dụng dịch vụ, và bạn đồng ý rằng bạn có thể bị từ chối truy cập hoặc sử dụng Dịch vụ nếu bạn từ chối cung cấp giấy tờ chứng minh.</p>

								<p><strong>TIN NHẮN VĂN BẢN.</strong></p>

								<p>Bằng cách tạo một Tài khoản, bạn đồng ý rằng các Dịch vụ có thể gửi cho bạn tin nhắn văn bản thông báo (SMS) như một phần của hoạt động kinh doanh thông thường của việc sử dụng các Dịch vụ. Bạn có thể chọn không nhận tin nhắn văn bản (SMS) từ Alfazi bất kỳ lúc nào bằng cách gửi email tới&nbsp; <a href="mailto:contact@alfazi.edu.vn">contact@alfazi.edu.vn</a> &nbsp;nêu rõ rằng bạn không còn muốn nhận các tin nhắn như vậy, cùng với số điện thoại của thiết bị di động nhận tin nhắn. Bạn công nhận rằng việc chọn không nhận các tin nhắn văn bản (SMS) có thể ảnh hưởng đến việc bạn sử dụng Dịch vụ.</p>

								<p><strong>MÃ KHUYẾN MÃI.</strong></p>

								<p>Alfazi có thể tuỳ ý tạo ra các mã khuyến mãi để đổi điểm tín dụng Tài khoản, hoặc các tính năng hoặc lợi ích khác liên quan đến các Dịch vụ này và/hoặc các dịch vụ của một Nhà cung cấp Bên Thứ ba, tùy thuộc vào bất kỳ điều khoản bổ sung nào mà Alfazi thiết lập trên cơ sở từng mã khuyến mãi (“<em>Mã khuyến mãi</em>”). Bạn đồng ý rằng Mã khuyến mãi: (i) phải được sử dụng cho các đối tượng và mục đích dự định, theo cách phù hợp với pháp luật; (ii) không được sao chép, bán hoặc chuyển giao theo bất kỳ cách nào, hoặc cung cấp công khai (cho dù được đăng lên theo hình thức công khai hoặc bất kỳ hình thức nào khác), trừ khi được sự cho phép của Alfazi; (iii) có thể bị Alfazi vô hiệu hóa bất kỳ lúc nào vì bất kỳ lý do gì mà Alfazi không phải chịu trách nhiệm; (iv) chỉ có thể được sử dụng theo các điều khoản cụ thể mà Alfazi thiết lập cho Mã khuyến mãi đó; (v) không có giá trị quy đổi thành tiền mặt; và (vi) có thể hết hạn trước khi bạn sử dụng. Alfazi có quyền giữ lại hoặc khấu trừ các khoản tín dụng, các tính năng hoặc lợi ích khác thu được thông qua việc sử dụng các Mã khuyến mãi của bạn hoặc bất kỳ người dùng nào khác trong trường hợp Alfazi xác định được hoặc tin rằng việc sử dụng hoặc đổi thưởng Mã khuyến mãi có lỗi, gian lận, bất hợp pháp, hoặc vi phạm các điều khoản về Mã khuyến mãi được áp dụng hoặc các Điều khoản này.</p>

								<p><strong>NỘI DUNG NGƯỜI DÙNG CUNG CẤP.</strong></p>

								<p>Alfazi có thể tùy ý cho phép bạn gửi, tải lên, công khai hoặc cung cấp cho Alfazi, thông qua các tính năng văn bản, âm thanh và/hoặc hình ảnh của Dịch vụ, các nội dung và thông tin, bao gồm các nhận xét và phản hồi liên quan đến các Dịch vụ, các yêu cầu hỗ trợ, và nộp các thông tin cho các cuộc thi và chương trình khuyến mãi (“<em>Nội dung Người dùng</em>”) tùy từng thời điểm. Mọi Nội dung Người dùng do bạn cung cấp vẫn là tài sản của bạn. Tuy nhiên, bằng cách cung cấp Nội dung Người dùng cho Alfazi, bạn cấp cho Alfazi giấy phép không có bản quyền, không thể thu hồi, chuyển nhượng, sử dụng vĩnh viễn, trên toàn thế giới, với quyền cấp giấy phép con, để sử dụng, sao chép, sửa đổi, tạo ra tác phẩm phái sinh, phân phối, công khai, thực hiện công khai, và khai thác bằng bất kỳ cách nào Nội dung Người dùng trong tất cả các định dạng và các kênh phân phối hiện tại hoặc sau đây được đưa ra (bao gồm liên quan đến các Dịch vụ và hoạt động kinh doanh của Alfazi và trên các trang web và các dịch vụ của bên thứ ba), mà không cần thông báo thêm cho hoặc có được sự đồng ý của bạn, và không yêu cầu bạn hoặc bất kỳ người nào hoặc tổ chức nào khác phải thanh toán.</p>

								<p>Bạn tuyên bố và đảm bảo rằng: (i) bạn là chủ sở hữu duy nhất và độc quyền tất cả các Nội dung Người dùng hoặc bạn có tất cả các quyền, giấy phép, sự chấp thuận và phát hành cần thiết để cấp cho Alfazi giấy phép Nội dung Người dùng như đã nêu trên; và (ii) Nội dung Người dùng hoặc thông tin bạn nộp, tải lên, công bố hoặc cung cấp Nội dung Người dùng hoặc việc Alfazi sử dụng Nội dung Người dùng như được cho phép bằng văn bản này sẽ không vi phạm, chiếm đoạt, xâm phạm tài sản trí tuệ hoặc các quyền sở hữu hoặc quyền công khai hoặc riêng tư của một bên thứ ba, hoặc dẫn đến hành vi vi phạm pháp luật hoặc quy định áp dụng.</p>

								<p>Bạn đồng ý không cung cấp Nội dung Người dùng mang tính phỉ báng, bôi nhọ, hận thù, bạo lực, khiêu dâm, tình dục, bất hợp pháp, hoặc gây khó chịu, như được Alfazi tùy ý xác định, bất kể các tài liệu này có thể được pháp luật bảo vệ hay không. Alfazi có thể, nhưng không có nghĩa vụ, rà soát, theo dõi, hoặc xóa Nội dung Người dùng, theo toàn quyền quyết định của Alfazi và vào bất kỳ lúc nào vì bất kỳ lý do nào mà không cần thông báo trước.</p>

								<p><strong>TRUY CẬP MẠNG VÀ THIẾT BỊ.</strong></p>

								<p>Bạn chịu trách nhiệm về việc truy cập mạng dữ liệu cần thiết để sử dụng các Dịch vụ. Mạng dữ liệu di động của bạn và mức phí nhắn tin và có thể áp dụng nếu bạn truy cập hoặc sử dụng các Dịch vụ từ một thiết bị không dây được kích hoạt và bạn phải chịu trách nhiệm về mức giá và phí này. Bạn chịu trách nhiệm thu thập và cập nhật phần cứng tương thích hoặc các thiết bị cần thiết để truy cập và sử dụng các Dịch vụ và Ứng dụng này và bất kỳ bản cập nhật nào. Alfazi không đảm bảo rằng các Dịch vụ, hoặc bất kỳ phần nào trong đó, sẽ hoạt động trên bất kỳ phần cứng hoặc các thiết bị cụ thể nào. Ngoài ra, các Dịch vụ có thể bị trục trặc và chậm trễ vốn thường gặp trong việc sử dụng Internet và các phương tiện liên lạc điện tử.</p>

								<p><strong>4. THANH TOÁN</strong></p>

								<p>Bạn hiểu rằng việc sử dụng các Dịch vụ có thể phát sinh chi phí cho các dịch vụ hoặc hàng hóa mà bạn nhận được từ một Bên Thứ ba (các trung tâm gia sư, các gia sư là các cá nhân tự do không thuộc bất kỳ trung tâm nào) (“<em>Phí</em>”). Sau khi bạn nhận được dịch vụ hoặc hàng hóa thông qua việc sử dụng Dịch vụ, Alfazi sẽ tạo điều kiện thanh toán các khoản Phí được áp dụng thay mặt Bên Thứ ba (các trung tâm gia sư, các gia sư là các cá nhân tự do không thuộc bất kỳ trung tâm nào) như đại lý thu nhận thanh toán của Bên Thứ ba (các trung tâm gia sư, các gia sư là các cá nhân tự do không thuộc bất kỳ trung tâm nào). Thanh toán các khoản Phí theo cách này sẽ được coi như thanh toán được bạn thực hiện trực tiếp cho Bên Thứ ba. Bên thứ ba (các trung tâm gia sư, các gia sư là các cá nhân tự do không thuộc bất kỳ trung tâm nào) phải thanh toán các khoản phí kết nối, phí dịch vụ, phí ngân hàng cho việc thanh toán tiền cho Bên thứ ba cho Alfazi. Các khoản Phí sẽ bao gồm thuế được áp dụng nếu có yêu cầu của pháp luật. Các khoản Phí mà bạn trả là cuối cùng và không được hoàn lại, trừ trường hợp được Alfazi quyết định.</p>

								<p>Bên thứ ba (các trung tâm gia sư, các gia sư là các cá nhân tự do không thuộc bất kỳ trung tâm nào) yêu cầu Alfazi thanh toán các khoản tiền thu được và được Alfazi thanh toán chậm nhất 3 tuần sau khi nhận được yêu cầu của Bên thứ ba.</p>

								<p>Việc thanh toán sẽ được tạo điều kiện bởi Alfazi bằng cách sử dụng phương thức thanh toán ưa thích được chỉ định trong Tài khoản của bạn, sau đó Alfazi sẽ gửi cho bạn một biên nhận qua email. Nếu phương thức thanh toán Tài khoản chính của bạn được xác định là đã hết hạn, không hợp lệ hoặc không thể tính phí, bạn đồng ý rằng Alfazi có thể, trong vai trò là đại lý thu nhận thanh toán của Nhà cung cấp Bên Thứ ba, sử dụng phương thức thanh toán thứ hai trong Tài khoản của bạn, nếu có.</p>

								<p>Alfazi có quyền tùy ý thiết lập, xóa và/hoặc sửa đổi Phí đối với bất kỳ hoặc tất cả các dịch vụ hoặc hàng hóa nhận được thông qua việc sử dụng Dịch vụ tại bất kỳ thời điểm nào. Hơn nữa, bạn công nhận và đồng ý rằng Phí áp dụng tại các khu vực địa lý nhất định có thể tăng đáng kể tùy thuộc vào mức độ khó của bài tập và yêu cầu của bạn. Alfazi sẽ nỗ lực thông báo cho bạn về những khoản phí có thể được áp dụng, với điều kiện là bạn sẽ chịu trách nhiệm cho các khoản Phí phát sinh theo Tài khoản của bạn bất kể bạn có biết về các khoản Phí hoặc các khoản tiền đó hay không. Tùy từng thời điểm, Alfazi có thể cung cấp cho người dùng nhất định các chương trình khuyến mãi và giảm giá có thể dẫn đến việc tính phí các khoản tiền khác nhau cho cùng dịch vụ hoặc hàng hóa hay các dịch vụ hoặc hàng hóa tương tự nhận được thông qua việc sử dụng các Dịch vụ này, và bạn đồng ý rằng các chương trình khuyến mãi giảm giá như vậy sẽ không liên quan đến việc bạn sử dụng các Dịch vụ hoặc các khoản Phí áp dụng đối với bạn, trừ khi cũng được cung cấp cho bạn. Bạn có thể chọn hủy bỏ yêu cầu về các dịch vụ hoặc hàng hóa từ một Bên Thứ ba bất kỳ lúc nào trước 10 giây sau khi Bên Thứ ba thực hiện dịch vụ, trong trường hợp này bạn KHÔNG phải trả phí hủy bỏ dịch vụ.</p>

								<p>Cơ cấu thanh toán này được thiết kế để bù đắp đầy đủ cho các Nhà cung cấp Bên Sau khi bạn nhận được các dịch vụ hoặc hàng hóa qua Dịch vụ này, bạn sẽ có cơ hội đánh giá trải nghiệm của bạn và để lại phản hồi thêm về Nhà cung cấp Bên Thứ ba.</p>

								<p><strong>5. TUYÊN BỐ MIỄN TRÁCH; GIỚI HẠN TRÁCH NHIỆM PHÁP LÝ; BỒI THƯỜNG.</strong></p>

								<p><strong>TUYÊN BỐ MIỄN TRÁCH.</strong></p>

								<p>CÁC DỊCH VỤ ĐƯỢC CUNG CẤP “NGUYÊN TRẠNG” VÀ “NHƯ SẴN CÓ”. ALFAZI KHƯỚC TỪ MỌI TUYÊN BỐ VÀ BẢO ĐẢM, RÕ RÀNG, NGỤ Ý HOẶC THEO LUẬT ĐỊNH, KHÔNG ĐƯỢC NÊU RÕ TRONG NHỮNG ĐIỀU KHOẢN NÀY, BAO GỒM CÁC BẢO ĐẢM VỀ KHẢ NĂNG THƯƠNG MẠI, TÍNH PHÙ HỢP CHO MỘT MỤC ĐÍCH CỤ THỂ VÀ KHÔNG VI PHẠM PHÁP LUẬT. NGOÀI RA, ALFAZI KHÔNG ĐƯA RA TUYÊN BỐ, BẢO HÀNH, BẢO ĐẢM VỀ TÍNH TIN CẬY, TÍNH KỊP THỜI, CHẤT LƯỢNG, TÍNH PHÙ HỢP HOẶC SẴN CÓ CỦA CÁC DỊCH VỤ NÀY, CÁC DỊCH VỤ HOẶC HÀNG HÓA ĐƯỢC YÊU CẦU QUA VIỆC SỬ DỤNG DỊCH VỤ NÀY, HOẶC CÁC DỊCH VỤ NÀY SẼ KHÔNG BỊ GIÁN ĐOẠN HOẶC KHÔNG CÓ LỖI. ALFAZI KHÔNG ĐẢM BẢO CHẤT LƯỢNG, TÍNH PHÙ HỢP, AN TOÀN HOẶC NĂNG LỰC CỦA NHÀ CUNG CẤP BÊN THỨ BA. BẠN ĐỒNG Ý CHỊU TRÁCH NHIỆM VỀ TOÀN BỘ RỦI RO PHÁT SINH BÊN NGOÀI VIỆC SỬ DỤNG CÁC DỊCH VỤ NÀY VÀ BẤT KỲ DỊCH VỤ HOẶC HÀNG HÓA ĐƯỢC YÊU CẦU NÀO CÓ LIÊN QUAN, TRONG PHẠM VI TỐI ĐA PHÁP LUẬT ÁP DỤNG CHO PHÉP.</p>

								<p><strong>GIỚI HẠN TRÁCH NHIỆM PHÁP LÝ.</strong></p>

								<p>ALFAZI SẼ KHÔNG CHỊU TRÁCH NHIỆM VỀ CÁC THIỆT HẠI MANG TÍNH GIÁN TIẾP, NGẪU NHIÊN, ĐẶC BIỆT, CẢNH CÁO, TRỪNG TRỊ HOẶC HẬU QUẢ, BAO GỒM TỔN THẤT VỀ LỢI NHUẬN, TỔN THẤT VỀ DỮ LIỆU, THƯƠNG TÍCH CÁ NHÂN HOẶC THIỆT HẠI VỀ TÀI SẢN, LIÊN QUAN, HOẶC PHÁT SINH TỪ VIỆC SỬ DỤNG CÁC DỊCH VỤ, NGAY CẢ NẾU ALFAZI ĐÃ ĐƯỢC CẢNH BÁO VỀ NGUY CƠ CỦA CÁC THIỆT HẠI NHƯ VẬY. ALFAZI SẼ KHÔNG CHỊU TRÁCH NHIỆM CHO BẤT KỲ THIỆT HẠI, TRÁCH NHIỆM HOẶC TỔN THẤT NÀO PHÁT SINH DO: (i) VIỆC BẠN SỬ DỤNG HOẶC DỰA TRÊN CÁC DỊCH VỤ HOẶC BẠN KHÔNG CÓ QUYỀN TRUY CẬP HOẶC SỬ DỤNG DỊCH VỤ; HOẶC (ii) MỌI GIAO DỊCH HOẶC QUAN HỆ GIỮA BẠN VÀ NHÀ CUNG CẤP BÊN THỨ BA, NGAY CẢ NẾU ALFAZI ĐÃ ĐƯỢC CẢNH BÁO VỀ NGUY CƠ CỦA CÁC THIỆT HẠI NHƯ VẬY. ALFAZI SẼ KHÔNG CHỊU TRÁCH NHIỆM VỀ VIỆC TRÌ HOÃN HOẶC KHÔNG HOẠT ĐỘNG DO CÁC NGUYÊN NHÂN NẰM NGOÀI KHẢ NĂNG KIỂM SOÁT CỦA ALFAZI. BẠN CÔNG NHẬN RẰNG NHÀ CUNG CẤP DỊCH VỤ BÊN THỨ BA CUNG CẤP CÁC DỊCH VỤ GIA SƯ, GIẢNG DẠY CÓ THỂ KHÔNG ĐƯỢC CẤP PHÉP HOẶC CHO PHÉP HOẠT ĐỘNG MỘT CÁCH CHUYÊN NGHIỆP.</p>

								<p>BẠN CÓ THỂ SỬ DỤNG DỊCH VỤ CỦA ALFAZI ĐỂ YÊU CẦU VÀ LÊN LỊCH GIA SƯ, GIẢNG DẠY VỚI BÊN THỨ BA, NHƯNG BẠN ĐỒNG Ý RẰNG ALFAZI KHÔNG CÓ TRÁCH NHIỆM HOẶC NGHĨA VỤ VỚI BẠN LIÊN QUAN ĐẾN BẤT KỲ DỊCH VỤ GIA SƯ, GIẢNG DẠY NÀO ĐƯỢC BÊN THỨ BA CUNG CẤP CHO BẠN TRỪ CÁC TRÁCH NHIỆM ĐƯỢC QUY ĐỊNH RÕ TRONG CÁC ĐIỀU KHOẢN NÀY.</p>

								<p>CÁC GIỚI HẠN VÀ TUYÊN BỐ MIỄN NHIỆM TRONG PHẦN 5 NÀY KHÔNG NHẰM GIỚI HẠN TRÁCH NHIỆM HOẶC THAY THẾ CÁC QUYỀN CỦA NGƯỜI TIÊU DÙNG KHÔNG THỂ LOẠI TRỪ THEO PHÁP LUẬT.</p>

								<p><strong>BỒI THƯỜNG.</strong></p>

								<p>Bạn đồng ý bồi thường và giữ cho Alfazi và các cán bộ, giám đốc, nhân viên và đại lý vô hại khỏi bất kỳ và tất cả các yêu cầu bồi thường, yêu cầu, trách nhiệm pháp lý, và các chi phí (bao gồm chi phí luật sư) phát sinh từ hoặc liên quan đến: (i) bạn sử dụng các Dịch vụ này, dịch vụ hoặc các sản phẩm nhận được thông qua việc sử dụng các Dịch vụ này; (ii) bạn phá vỡ hoặc vi phạm các Điều khoản này; (iii) Alfazi sử dụng Nội dung Người dùng; hoặc (iv) bạn vi phạm các quyền của bất kỳ bên thứ ba nào, bao gồm Nhà cung cấp Bên Thứ ba.</p>

								<p><strong>6. LUẬT ĐIỀU CHỈNH; TRỌNG TÀI.</strong></p>

								<p>Trừ những điều được nêu trong những Điều khoản này, những Điều khoản này sẽ được điều chỉnh một cách riêng biệt và được hiểu theo luật pháp của Việt Nam, ngoại trừ các quy định về xung đột luật pháp. Tuy nhiên, bất kỳ tranh chấp, xung đột hoặc tranh cãi nào phát sinh từ hoặc nhìn chung có mối liên hệ hoặc liên quan đến các Dịch vụ hoặc những Điều khoản này, bao gồm những điều liên quan tới tính hiệu lực, việc xây dựng hoặc khả năng thực hiện các Dịch vụ hoặc Thỏa thuận (bất kỳ “<em>Tranh chấp</em>&nbsp;nào”), trước hết sẽ buộc phải được đệ trình để tiến hành thủ tục hòa giải. Nếu Tranh chấp nói trên không được giải quyết trong vòng sáu mươi (60) ngày sau khi yêu cầu hòa giải. Tranh chấp đó có thể được giải quyết riêng và cuối cùng giải quyết bằng hình thức xét xử trọng tài. Tranh chấp sẽ được giải quyết bởi một (1) trọng tài được chỉ định. Địa điểm cơ quan hòa giải và trọng tài sẽ là Thành phố, Hồ Chí Minh, Việt Nam. Sự tồn tại và nội dung của thủ tục trọng tài và hòa giải, bao gồm các văn bản và tóm tắt được các bên đệ trình, thư gửi từ phía hòa giải, và thư, yêu cầu cũng như quyết định chỉ từ phía trọng tài, sẽ tuyệt đối được bảo mật và sẽ không được tiết lộ cho bất kỳ bên thứ ba nào mà không có sự đồng ý rõ ràng bằng văn bản từ bên còn lại, trừ khi: (i) việc tiết lộ cho bên thứ ba được yêu cầu hợp lý trong trường hợp tiến hành thủ tục trọng tài hoặc hòa giải; và (ii) bên thứ ba đồng ý vô điều kiện bằng văn bản sẽ chịu ràng buộc theo nghĩa vụ bảo mật được quy định trong tài liệu này.</p>

								<p><strong>7. CÁC QUY ĐỊNH KHÁC</strong><br />
									<strong>THÔNG BÁO.</strong>
								</p>

								<p>Alfazi có thể đưa ra thông báo bằng phương thức thông báo chung về các Dịch vụ, gửi thư điện tử đến địa chỉ email trong Tài khoản của bạn, hoặc thông qua văn bản liên lạc được gửi qua đường bưu điện đến địa chỉ được ghi trong Tài khoản của bạn. Bạn có thể thông báo cho Alfazi bằng văn bản liên lạc đến địa chỉ của Alfazi tại số 2/5A, đường Quang Trung, phường 14, quận Gò Vấp, thành phố Hồ Chí Minh, Việt Nam.</p>

								<p><strong>ĐIỀU KHOẢN CHUNG.</strong></p>

								<p>Bạn không thể chuyển nhượng hoặc chuyển giao các Điều khoản này, toàn bộ hoặc một phần, mà không có sự chấp thuận trước bằng văn bản của Alfazi. Bạn chấp thuận cho Alfazi quyền chuyển nhượng hoặc chuyển giao các Điều khoản này, toàn bộ hoặc một phần, cho: (i) một công ty con hoặc chi nhánh; (ii) ngân hàng thanh toán vốn chủ sở hữu, doanh nghiệp hoặc tài sản của Alfazi; hoặc (iii) bên kế thừa do sáp nhập. Không có mối quan hệ liên doanh, hợp tác, làm việc hoặc đại lý tồn tại giữa bạn, Alfazi hoặc bất kỳ Nhà cung cấp Bên Thứ ba nào như là kết quả của hợp đồng giữa bạn và Alfazi hoặc việc sử dụng các Dịch vụ.</p>

								<p>Nếu bất kỳ điều khoản nào trong các Điều khoản này được coi là phi pháp, không có hiệu lực hoặc không thể thực hiện, toàn bộ hoặc một phần, thì điều khoản hoặc phần đó, theo bất kỳ quy định nào của pháp luật, trong phạm vi đó sẽ không được coi là một phần của Điều khoản này nhưng tính hợp pháp, tính hiệu lực và khả năng thực thi của các điều khoản còn lại trong các Điều khoản này sẽ không bị ảnh hưởng. Trong trường hợp đó, các bên sẽ thay thế điều khoản hoặc phần của điều khoản phi pháp, không có hiệu lực hoặc không thể thực hiện được bằng một điều khoản hoặc phần điều khoản hợp pháp, có hiệu lực và có thể thực thi ở mức tối đa nhất có thể, có hiệu lực tương tự như điều khoản hoặc phần của điều khoản phi pháp, không có hiệu lực hoặc không thể thực thi, bất kể nội dung và mục đích của các Điều khoản này là gì. Các Điều khoản này tạo thành toàn bộ thỏa thuận và biên bản ghi nhớ của các bên liên quan đến nội dung chính và các sửa đổi, thay thế đối với các thỏa thuận hoặc các cam kết hiện hành hoặc trước đó liên quan tới nội dung chính. Trong các Điều khoản này, từ bao gồm nghĩa là bao gồm, nhưng không giới hạn.”</p>

								<p>&nbsp;</p>

							</Col>
						</Row>


					</Container>
				</div>
			</MainLayout>
		);
	}
}
