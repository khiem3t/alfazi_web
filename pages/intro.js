/**
 * Zelo team
 * @author Tran Trung
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';
import Container from 'src/components/Layout/Container';

import Title from 'src/components/Layout/Title';

import withStyles from 'src/theme/jss/withStyles';


const styleSheet = (/* theme */) => ({
	root: {
		padding: '45px',
		background: '#fff',
	},
});

@withRoot
@withStyles(styleSheet)
export default class IntroPage extends PureComponent {
	static propTypes = {
		// url: PropTypes.object.isRequired,
		classes: PropTypes.object.isRequired,
	}

	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		const { classes } = this.props;

		return (
			<MainLayout>
				<Title>Giới thiệu</Title>
				<div style={{ margin: '50px 0' }}>
					<Container className={classes.root}>
						<Row type="flex" justify="center" align="top">
							<Col span={24}>
								<h2>Giới thiệu về Alfazi</h2>
								<p>
									Alfazi được tạo ra nhằm mang lại môi trường trực tuyến cho việc học tập và giải quyết các vấn đề, bài tập, phương pháp giải đặc biệt ở các môn khoa học tự nhiên và ngoại ngữ, từ đó xây dựng một cộng đồng cùng học hỏi, giúp đỡ nhau cùng tiến bộ.

									Alfazi cung cấp một lớp học “ảo” giúp các học sinh có thể tiếp cận và học hỏi các gia sư thông qua các công cụ tương tác như bảng trắng, cuộc gọi thoại và đặc biệt là hiển thị thời gian thực trao đổi của cả 2 bên trên màn hình điện thoại.
								</p>

								<p>Sử dụng Alfazi, người dùng có thể tiếp cận gia sư online dưới 2 dạng:</p>
								<ul>
									<li>Giải nhanh: Đăng bài tập và chọn một trong những gia sư đang online để được giải đáp ngay trong thời gian ngắn</li>
									<li>Dạy kèm: đặt lịch học tập và nhờ gia sư hướng dẫn theo thời gian định sẵn, gia sư có giáo trình sơ bộ nhất định để có thể định hướng và giảng dạy cho học sinh</li>
								</ul>
								<p>Gia sư online là những người chia sẻ kiến thức, có thể truyền đạt phương pháp học tập, kiến thức và hướng dẫn các bài tập cho học sinh. Các gia sư phải đăng ký tại Alfazi và phải vượt qua những bài kiểm tra của Alfazi và đáp ứng một số yêu cầu về kỹ thuật của Alfazi.</p>

								<h2>Sứ mệnh</h2>
								<ul>
									<li>Sứ mệnh của Alfazi là giúp việc học trở nên dễ dàng và hiệu quả hơn.</li>
									<li>Nhờ Alfazi, bạn có thể học ở bất kỳ ở đâu, bấy kỳ thời gian nào.</li>
								</ul>
								<h2>Tầm nhìn</h2>
								<p>
									Alfazi cung cấp nền tảng, phương pháp giảng dạy, học tập tương tác hiện đại chất lượng quốc tế cho mọi đối tượng muốn học tập và chia sẻ kiến thức.<br />
									Cải cách, đổi mới phương pháp gia sư truyền thống bằng công nghệ để người học có thể học nhiều hơn, tốt hơn, hiệu quả hơn.<br />
									Chúng tôi rất hy vọng nhận được phản hồi từ phía các bạn để có thể hoàn thiện hơn ứng dụng và giúp cộng đồng học tập phát triển tốt hơn.<br />
									<br />
									<br />
									<strong>Hãy liên hệ với chúng tôi tại: <a href="mailto:contact@alfazi.edu.vn">contact@alfazi.edu.vn</a></strong>
								</p>
							</Col>
						</Row>


					</Container>
				</div>
			</MainLayout>
		);
	}
}
