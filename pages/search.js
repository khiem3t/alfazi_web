/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 08:19:32
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';
import Search from 'src/containers/Search';

import Title from 'src/components/Layout/Title';

@withRoot
export default class SearchPage extends PureComponent {
	static async getInitialProps(ctx) {
		return { searchText: ctx.query.q };
	}


	static propTypes = {
		searchText: PropTypes.string.isRequired,
	}

	static defaultProps = {}

	render() {
		return (
			<MainLayout>
				<Title>{this.props.searchText} - Tìm kiếm trên Alfazi</Title>
				<Search searchText={this.props.searchText} />
			</MainLayout>
		);
	}
}
