/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-18 17:01:16
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import withRoot from 'src/root';

import Title from 'src/components/Layout/Title';

import { Row, Col } from 'antd';

import MainLayout from 'src/layout/Main';
import TopQuestion from 'src/components/Widget/TopQuestion';

@withRoot
export default class QuestionTopPage extends PureComponent {
	static async getInitialProps(ctx) {
		return { searchText: ctx.query.q };
	}

	render() {
		return (
			<MainLayout>
				<Title>Top câu hỏi</Title>
				<Row>
					<Col sm={{ span: 24 }} lg={{ span: 12, offset: 6 }}>
						<TopQuestion isTopPage />
					</Col>
				</Row>
			</MainLayout>
		);
	}
}
