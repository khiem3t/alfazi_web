/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-10 23:32:12
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Head from 'next/head';
import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';
import Index from 'src/containers/Index';

// import { getQuestionList } from 'src/redux/actions/question';
import { initGA, logPageView } from 'src/utils/analytics';

import Title from 'src/components/Layout/Title';

@withRoot
export default class IndexPage extends PureComponent {
	static async getInitialProps({ /* isServer, store, */ query }) {
		// if (isServer) {
		// 	await store.dispatch(getQuestionList({
		// 		filter: {
		// 			limit: 12,
		// 			skip: 0,
		// 			order: 'createdAt DESC',
		// 			include: {
		// 				relation: 'creator',
		// 				scope: {
		// 					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
		// 				},
		// 			},
		// 			where: {
		// 				status: 'active',
		// 			},
		// 		},
		// 	}));
		// }
		return { query };
	}

	static propTypes = {
		query: PropTypes.object.isRequired,
		url: PropTypes.object.isRequired,
	}

	static defaultProps = {}

	componentDidMount() {
		if (!window.GA_INITIALIZED) {
			initGA();
			window.GA_INITIALIZED = true;
		}
		logPageView();
	}

	render() {
		return (
			<MainLayout>
				<Title />
				<Head>
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.css" integrity="sha384-TEMocfGvRuD1rIAacqrknm5BQZ7W7uWitoih+jMNFXQIbNl16bO8OZmylH/Vi/Ei" crossOrigin="anonymous" />
				</Head>
				<Index query={this.props.query} asPath={this.props.url.asPath} />

			</MainLayout>
		);
	}
}
