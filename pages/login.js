/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-10 23:32:12
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import withRoot from 'src/root';

// import AuthStorage from 'src/utils/AuthStorage';

import Title from 'src/components/Layout/Title';

import MainLayout from 'src/layout/Main';

import LoginForm from 'src/components/Form/Login';

@withRoot
export default class LoginPage extends PureComponent {
	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		return (
			<MainLayout>
				<Title>Đăng nhập</Title>
				<div style={{ margin: '50px 0', textAlign: 'center' }}>
					<LoginForm isLoginPage />
				</div>
			</MainLayout>
		);
	}
}
