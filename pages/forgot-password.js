/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-10 14:39:40
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';

import ForgotPassword from 'src/components/Form/ForgotPassword';

import Title from 'src/components/Layout/Title';

@withRoot
export default class ForgotPasswordPage extends PureComponent {
	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		return (
			<MainLayout>
				<Title>Quên mật khẩu</Title>
				<div style={{ margin: '50px 0', textAlign: 'center' }}>
					<ForgotPassword />
				</div>
			</MainLayout>
		);
	}
}
