/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-18 17:01:16
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import { Row, Col } from 'antd';

import Head from 'next/head';
import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';

import Ranking from 'src/components/Widget/Ranking';

import Title from 'src/components/Layout/Title';

@withRoot
export default class RankingPage extends PureComponent {
	static async getInitialProps(ctx) {
		return { searchText: ctx.query.q };
	}

	render() {
		return (
			<MainLayout>
				<Title>Bảng xếp hạng</Title>
				<Row>
					<Col sm={{ span: 24 }} lg={{ span: 12, offset: 6 }}>
						<Ranking isRankingPage />
					</Col>
				</Row>
			</MainLayout>
		);
	}
}
