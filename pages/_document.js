/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-04 15:43:52
*------------------------------------------------------- */

import React from 'react';
// import ReactGA from 'react-ga';

import Document, { Head, Main, NextScript } from 'next/document';
import HeadShare from 'next/head';

import JssProvider from 'react-jss/lib/JssProvider';

import getPageContext from 'src/theme/jss/getPageContext';

import stylesheet from 'src/theme/antd/theme.less';
import { initGA, logPageView } from 'src/utils/analytics';

import LINK from 'src/constants/url';

export default class MyDocument extends Document {
	static async getInitialProps(ctx) {
		// Resolution order
		//
		// On the server:
		// 1. page.getInitialProps
		// 2. document.getInitialProps
		// 3. page.render
		// 4. document.render
		//
		// On the server with error:
		// 2. document.getInitialProps
		// 3. page.render
		// 4. document.render
		//
		// On the client
		// 1. page.getInitialProps
		// 3. page.render

		// Get the context of the page to collected side effects.
		const pageContext = getPageContext();
		const page = ctx.renderPage(Component => props => (
			<JssProvider
				registry={pageContext.sheetsRegistry}
				generateClassName={pageContext.generateClassName}
			>
				<Component pageContext={pageContext} {...props} />
			</JssProvider>
		));

		const css = pageContext.sheetsRegistry.toString();

		return {
			...page,
			pageContext,
			styles: (
				<style
					id="jss-server-side"
					// eslint-disable-next-line react/no-danger
					dangerouslySetInnerHTML={{ __html: css }}
				/>
			),
		};
	}

	componentDidMount() {
		if (!window.GA_INITIALIZED) {
			initGA();
			window.GA_INITIALIZED = true;
		}
		logPageView();

		if (typeof window !== 'undefined' && window.Tawk_API) {
			window.Tawk_LoadStart = new Date();
			window.Tawk_API.onLoad = () => {};
		}
		/* eslint-enable */
	}

	render() {
		const { canonical, pageContext } = this.props;

		return (
			<html lang="en" dir="ltr">
				<Head>
					<meta charSet="utf-8" />
					{/* PWA */}
					<link rel="manifest" href="/static/assets/manifest.json" />

					<meta name="mobile-web-app-capable" content="yes" />
					<meta name="apple-mobile-web-app-capable" content="yes" />

					<meta name="application-name" content="Alfazi" />
					<meta name="apple-mobile-web-app-title" content="Alfazi" />

					<meta name="apple-mobile-web-app-status-bar-style" content={'white-' + pageContext.theme.palette.primary[900]} />
					<meta name="msapplication-starturl" content="/" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, height=device-height, user-scalable=0" />

					<meta name="msapplication-navbutton-color" content={pageContext.theme.palette.primary[900]} />
					<meta name="theme-color" content={pageContext.theme.palette.primary[900]} />

					<link rel="shortcut icon" type="image/x-icon" sizes="512x512" href="/static/assets/favicon.ico" />
					<link rel="shortcut icon" href="/static/assets/favicon.ico" />

					<link rel="icon" type="image/png" sizes="512x512" href="/static/assets/icon/512x512.png" />
					<link rel="apple-touch-icon" type="image/png" sizes="512x512" href="/static/assets/icon/512x512.png" />

					<link rel="icon" type="image/png" sizes="192x192" href="/static/assets/icon/192x192.png" />
					<link rel="apple-touch-icon" type="image/png" sizes="192x192" href="/static/assets/icon/192x192.png" />

					<link rel="icon" type="image/png" sizes="144x144" href="/static/assets/icon/144x144.png" />
					<link rel="apple-touch-icon" type="image/png" sizes="144x144" href="/static/assets/icon/144x144.png" />

					<link rel="icon" type="image/png" sizes="96x96" href="/static/assets/icon/96x96.png" />
					<link rel="apple-touch-icon" type="image/png" sizes="96x96" href="/static/assets/icon/96x96.png" />

					<link rel="icon" type="image/png" sizes="72x72" href="/static/assets/icon/72x72.png" />
					<link rel="apple-touch-icon" type="image/png" sizes="72x72" href="/static/assets/icon/72x72.png" />

					<link rel="icon" type="image/png" sizes="48x48" href="/static/assets/icon/48x48.png" />
					<link rel="apple-touch-icon" type="image/png" sizes="48x48" href="/static/assets/icon/48x48.png" />

					{/* END PWA */}

					<style id="insertion-point-jss" />
					<link rel="canonical" href={canonical} />
					<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" />
					<script
						type="text/javascript"
						src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"
					/>
					<script async id="embed-tawk" src="https://embed.tawk.to/5aa4160f4b401e45400d99fd/default" charSet="UTF-8" crossOrigin="*" />
					<link rel="shortcut icon" href="/static/mathquill/mathquill.css" />
					<script type="text/javascript" src="/static/mathquill/mathquill.js " />
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.css" integrity="sha384-TEMocfGvRuD1rIAacqrknm5BQZ7W7uWitoih+jMNFXQIbNl16bO8OZmylH/Vi/Ei" crossOrigin="anonymous" />
					<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.js" integrity="sha384-jmxIlussZWB7qCuB+PgKG1uLjjxbVVIayPJwi6cG6Zb4YKq0JIw+OMnkkEC7kYCq" crossOrigin="anonymous" />
					<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116431783-1" />

					<style dangerouslySetInnerHTML={{ __html: stylesheet }} />
				</Head>
				<body>
					<HeadShare>
						<title>Alfazi</title>
						<meta
							name="description"
							content="Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam."
						/>
						<meta name="keywords" content="học tập, học, chia sẻ, hỏi đáp" />
						{/* Twitter */}
						<meta name="twitter:card" content="summary" />
						<meta name="twitter:site" content="@alfazi" />
						<meta name="twitter:title" content="Học tập và chia sẻ | Alfazi" />
						<meta
							name="twitter:description"
							content="Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam."
						/>
						<meta name="twitter:image" content={LINK.WEB_URL + '/static/assets/images/logo/256x256.png'} />
						{/* Facebook */}
						<meta property="fb:app_id" content="144879436203981" />
						<meta property="og:type" content="website" />
						<meta property="og:title" content="Học tập và chia sẻ | Alfazi" />
						<meta
							property="og:description"
							content="Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam."
						/>
						<meta property="og:image" content={LINK.WEB_URL + '/static/assets/images/logo/256x256.png'} />
						<meta property="og:image:width" content="200" />
						<meta property="og:image:height" content="200" />
						<meta property="og:locale" content="vi_VN" />
						<meta property="og:url" content={LINK.WEB_URL} />
					</HeadShare>
					<Main />
					<NextScript />
				</body>
			</html>
		);
	}
}
