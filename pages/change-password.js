/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-10 23:32:12
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';
import LoginRequire from 'src/layout/LoginRequire';

import ChangePass from 'src/components/Form/ChangePass';

import Title from 'src/components/Layout/Title';

@withRoot
export default class IndexPage extends PureComponent {
	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		return (
			<MainLayout>
				<Title>Đổi mật khẩu</Title>
				<div style={{ margin: '50px 0', textAlign: 'center' }}>
					<LoginRequire>
						<ChangePass />
					</LoginRequire>
				</div>
			</MainLayout>
		);
	}
}
