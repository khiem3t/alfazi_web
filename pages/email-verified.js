/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-10 14:39:40
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import Link from 'next/link';
import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';

import Title from 'src/components/Layout/Title';

@withRoot
export default class EmailVerified extends PureComponent {
	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		return (
			<MainLayout>
				<Title>Xác nhận mật khẩu</Title>
				<div style={{ margin: '50px 0', textAlign: 'center' }}>
					<div style={{ margin: '0 auto 30px', textAlign: 'center', background: '#fff', width: 300, padding: 50 }}>
						Chúc mừng bạn đã xác nhận email thành công.
					</div>
					<Link href="/login">
						<a>Login</a>
					</Link>
				</div>
			</MainLayout>
		);
	}
}
