/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 01:32:58
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Head from 'next/head';
import withRoot from 'src/root';

import Title from 'src/components/Layout/Title';

import MainLayout from 'src/layout/Main';
import QuestionDetail from 'src/containers/QuestionDetail';

import { getQuestionData } from 'src/redux/actions/question';

@withRoot
export default class QuestionPage extends PureComponent {
	static async getInitialProps(ctx) {
		if (ctx.isServer && ctx.query.id) {
			await ctx.store.dispatch(getQuestionData({
				id: ctx.query.id,
				filter: {
					include: [{
						relation: 'creator',
						scope: {
							fields: ['id', 'username', 'avatar', 'fullName', 'role'],
						},
					}],
					where: {
						status: 'active',
					},
				},
			}));
		}
		return { questionId: ctx.query.id };
	}

	static propTypes = {
		questionId: PropTypes.string.isRequired,
	}

	static defaultProps = {}

	render() {
		return (
			<MainLayout>
				<Title>Chi tiết câu hỏi</Title>
				<Head>
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.css" integrity="sha384-TEMocfGvRuD1rIAacqrknm5BQZ7W7uWitoih+jMNFXQIbNl16bO8OZmylH/Vi/Ei" crossOrigin="anonymous" />
					<script
						type="text/javascript"
						src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"
					/>
					<link rel="shortcut icon" href="/static/mathquill/mathquill.css" />
					<script type="text/javascript" src="/static/mathquill/mathquill.js " />
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.css" integrity="sha384-TEMocfGvRuD1rIAacqrknm5BQZ7W7uWitoih+jMNFXQIbNl16bO8OZmylH/Vi/Ei" crossOrigin="anonymous" />
					<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.js" integrity="sha384-jmxIlussZWB7qCuB+PgKG1uLjjxbVVIayPJwi6cG6Zb4YKq0JIw+OMnkkEC7kYCq" crossOrigin="anonymous" />
				</Head>
				<QuestionDetail questionId={this.props.questionId} />
			</MainLayout>
		);
	}
}
