/**
 * Zelo team
 * @author Tran Trung
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';
import Container from 'src/components/Layout/Container';

import Title from 'src/components/Layout/Title';

import withStyles from 'src/theme/jss/withStyles';


const styleSheet = (/* theme */) => ({
	root: {
		padding: '45px',
		background: '#fff',
	},
});

@withRoot
@withStyles(styleSheet)
export default class PolicyPage extends PureComponent {
	static propTypes = {
		// url: PropTypes.object.isRequired,
		classes: PropTypes.object.isRequired,
	}

	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		const { classes } = this.props;

		return (
			<MainLayout>
				<Title>Giới thiệu</Title>
				<div style={{ margin: '50px 0' }}>
					<Container className={classes.root}>
						<Row type="flex" justify="center" align="top">
							<Col span={24}>
								<div className="col-md-12 col-xs-12">
									<h2>Chính sách bảo mật</h2>
									<p>&nbsp;</p>

									<p><strong>TUYÊN BỐ VỀ QUYỀN RIÊNG TƯ CỦA NGƯỜI DÙNG</strong></p>

									<p>Alfazi thu thập thông tin về bạn khi bạn sử dụng các ứng dụng trên điện thoại di động, trang web và các sản phẩm và dịch vụ trực tuyến khác của chúng tôi (gọi chung là Dịch Vụ) và thông qua các mối tương tác và thông tin liên lạc khác mà bạn có với chúng tôi.&nbsp; Dịch Vụ sẽ được Alfazi cung cấp và Tuyên Bố về Quyền Riêng Tư này áp dụng cho thông tin được Alfazi Việt Nam thu thập và sử dụng.</p>

									<p><strong>PHẠM VI VÀ ỨNG DỤNG</strong></p>

									<p>Tuyên Bố về Quyền Riêng Tư này (Tuyên Bố) áp dụng cho những người ở mọi nơi trên thế giới sử dụng các ứng dụng cũng như Dịch Vụ của chúng tôi để yêu cầu gia sư và giảng dạy hoặc đăng ký làm gia sư.</p>

									<p><strong>THU THẬP THÔNG TIN</strong></p>

									<p><strong>THÔNG TIN MÀ BẠN CUNG CẤP CHO CHÚNG TÔI</strong></p>

									<p>Chúng tôi thu thập thông tin bạn cung cấp trực tiếp cho chúng tôi như khi bạn tạo hoặc sửa đổi tài khoản của bạn, đề nghị các dịch vụ theo yêu cầu, liên lạc với bộ phận hỗ trợ khách hàng hoặc liên lạc với chúng tôi theo cách khác. Thông tin này có thể bao gồm: tên, email, số điện thoại, địa chỉ bưu điện, ảnh hồ sơ, phương thức thanh toán và thông tin khác bạn chọn cung cấp.</p>

									<p><strong>THÔNG TIN CHÚNG TÔI THU THẬP THÔNG QUA VIỆC SỬ DỤNG CÁC DỊCH VỤ CỦA BẠN</strong></p>

									<p>Khi bạn sử dụng Dịch Vụ của chúng tôi, chúng tôi thu thập thông tin về bạn theo các danh mục nói chung sau đây:</p>

									<p><strong>- Thông Tin Giao Dịch:&nbsp;</strong>Chúng tôi thu thập chi tiết giao dịch liên quan đến việc bạn sử dụng Dịch Vụ, bao gồm loại dịch vụ được yêu cầu, ngày giờ cung cấp dịch vụ, phí được tính, thời gian đã học và các chi tiết giao dịch khác có liên quan.</p>

									<p><strong>- Thông Tin về Cách Sử Dụng và Lựa Chọn Ưu Tiên:</strong>&nbsp;Chúng tôi thu thập thông tin về cách thức bạn và khách truy cập trang web tương tác với các Dịch Vụ của chúng tôi, các lựa chọn ưu tiên được thể hiện và các thiết lập được lựa chọn. Trong một số trường hợp chúng tôi làm điều này thông qua việc sử dụng cookie, thẻ pixel và các công nghệ tương tự tạo ra và duy trì mã định danh duy nhất.</p>

									<p><strong>- Thông Tin về Thiết Bị:&nbsp;</strong>Chúng tôi có thể thu thập thông tin về thiết bị di động của bạn bao gồm, ví dụ, model phần cứng, hệ điều hành và phiên bản, phần mềm và tên tập tin và các phiên bản, ngôn ngữ ưa thích, mã nhận dạng thiết bị riêng, mã nhận dạng quảng cáo, số seri, thông tin chuyển động của thiết bị và thông tin mạng điện thoại di động.</p>

									<p><strong>- Thông Tin về Nhật Ký:</strong>&nbsp;Khi bạn tương tác với Dịch Vụ, chúng tôi thu thập các nhật ký ghi chép của máy chủ, trong đó có thể bao gồm các thông tin như địa chỉ IP của thiết bị, ngày và thời gian truy cập, các tính năng của ứng dụng hoặc các trang đã xem, sự cố của ứng dụng và hoạt động khác của hệ thống, loại trình duyệt và trang web hoặc dịch vụ của bên thứ ba mà bạn đang sử dụng trước khi tương tác với Dịch Vụ của chúng tôi.</p>

									<p><strong>THÔNG TIN QUAN TRỌNG VỀ QUYỀN HẠN TRUY CẬP NỀN TẢNG</strong></p>

									<p>Hầu hết các nền tảng di động (iOS, Android, v.v.) đã xác định một số loại dữ liệu thiết bị mà các ứng dụng không thể truy cập mà không có sự chấp thuận của bạn. Và những nền tảng này có hệ thống xin phép truy cập khác nhau để có được sự chấp thuận của bạn. Nền tảng iOS sẽ cảnh báo bạn trong lần đầu tiên ứng dụng Alfazi muốn xin phép truy cập vào một số loại dữ liệu và sẽ để cho bạn chấp thuận (hoặc không chấp thuận) yêu cầu đó. Các thiết bị Android sẽ thông báo cho bạn về những quyền hạn truy cập mà ứng dụng Alfazi tìm kiếm trước khi bạn sử dụng ứng dụng này lần đầu tiên, và việc bạn sử dụng ứng dụng này có nghĩa là bạn chấp thuận.</p>

									<p><strong>THÔNG TIN CHÚNG TÔI THU THẬP TỪ CÁC NGUỒN KHÁC</strong></p>

									<p>Chúng tôi cũng có thể nhận thông tin từ các nguồn khác và kết hợp chúng với các thông tin mà chúng tôi thu thập được thông qua Dịch Vụ của chúng tôi. Ví dụ:</p>

									<p>- Nếu bạn chọn liên kết, tạo hoặc đăng nhập vào tài khoản Alfazi của bạn với một nhà cung cấp thanh toán (ví dụ như, Google Wallet) hoặc dịch vụ truyền thông xã hội (ví dụ như, Facebook) hoặc nếu bạn tham gia với một ứng dụng hoặc trang web riêng biệt có sử dụng API của chúng tôi (hay có API mà chúng tôi sử dụng), chúng tôi có thể nhận thông tin về bạn hoặc các kết nối của bạn từ trang web hoặc ứng dụng đó.</p>

									<p>- Nếu chủ sử dụng lao động của bạn sử dụng một trong các giải pháp cho doanh nghiệp của chúng tôi như Alfazi for Business (Alfazi cho Doanh Nghiệp), chúng tôi có thể nhận thông tin về bạn từ chủ sử dụng lao động của bạn.</p>

									<p><strong>SỬ DỤNG THÔNG TIN</strong></p>

									<p>Chúng tôi có thể sử dụng các thông tin chúng tôi thu thập về bạn để:</p>

									<p>- Cung cấp, duy trì và cải thiện Dịch Vụ của chúng tôi, bao gồm, ví dụ như, tạo thuận lợi cho việc thanh toán, gửi biên lai, cung cấp các sản phẩm và dịch vụ bạn yêu cầu (và gửi thông tin liên quan), phát triển các tính năng mới, cung cấp hỗ trợ khách hàng cho Người học và Người dạy, phát triển các tính năng an toàn, xác thực người dùng và gửi cập nhật về sản phẩm và thông báo quản trị;</p>

									<p>- Thực hiện các hoạt động nội bộ, bao gồm, ví dụ như, để ngăn chặn gian lận và lạm dụng Dịch Vụ của chúng tôi; để khắc phục lỗi phần mềm và các vấn đề về hoạt động; để tiến hành phân tích dữ liệu, kiểm tra và nghiên cứu; và để theo dõi và phân tích việc sử dụng và các xu hướng hoạt động;</p>

									<p>- Gửi hoặc tạo thuận lợi cho các liên lạc (i) Người học và Người dạy;</p>

									<p>- Gửi cho bạn những thông tin liên lạc mà chúng tôi nghĩ là sẽ có ích cho bạn, bao gồm những thông tin về sản phẩm, dịch vụ, chương trình khuyến mại, tin tức và các sự kiện của Alfazi và các công ty khác, trong trường hợp cho phép và theo luật pháp hiện hành của địa phương; và để xử lý cuộc thi, rút thăm trúng thưởng hoặc các mục khuyến mãi khác và thực hiện bất kỳ giải thưởng nào liên quan;</p>

									<p>- Cá nhân hóa và cải thiện Dịch Vụ, bao gồm cung cấp hoặc khuyến nghị các tính năng, nội dung, kết nối xã hội, giới thiệu và quảng cáo.</p>

									<p>Chúng tôi có thể chuyển các thông tin được mô tả trong bản Tuyên Bố này cho, và xử lý cũng như lưu trữ các thông tin đó ở các quốc gia khác, một vài quốc gia trong đó có thể có luật bảo vệ dữ liệu ít an toàn hơn so với khu vực mà bạn cư trú. Trong trường hợp này, chúng tôi sẽ có biện pháp thích hợp để bảo vệ thông tin cá nhân của bạn phù hợp với bản Tuyên Bố này.</p>

									<p><strong>CHIA SẺ THÔNG TIN</strong></p>

									<p>Chúng tôi có thể chia sẻ những thông tin chúng tôi thu thập được về bạn như được mô tả trong bản Tuyên Bố này hay như đã được mô tả tại thời điểm thu thập hoặc chia sẻ, bao gồm các thông tin sau:</p>

									<p><strong>THÔNG QUA DỊCH VỤ CỦA CHÚNG TÔI</strong></p>

									<p>Chúng tôi có thể chia sẻ thông tin của bạn:</p>

									<p>- Với Gia sư để cho phép họ cung cấp Dịch Vụ bạn yêu cầu.</p>

									<p>- Với các bên thứ ba để cung cấp cho bạn dịch vụ bạn đã yêu cầu qua sự hợp tác hoặc dịch vụ khuyến mãi do bên thứ ba hoặc chúng tôi thực hiện;</p>

									<p>- Với công chúng nói chung nếu bạn gửi nội dung trong một diễn đàn công cộng, chẳng hạn như bình luận trên blog, các bài đăng trên phương tiện truyền thông xã hội hoặc các tính năng khác của Dịch Vụ của chúng tôi mà công chúng có thể xem;</p>

									<p>- Với các bên thứ ba mà bạn chọn để cho phép chúng tôi chia sẻ thông tin, ví dụ như các ứng dụng hoặc các trang web khác tích hợp với API hoặc Dịch Vụ của chúng tôi hoặc những người có API hoặc Dịch Vụ mà chúng tôi tích hợp;</p>

									<p><strong>CHIA SẺ QUAN TRỌNG KHÁC</strong></p>

									<p>Chúng tôi có thể chia sẻ thông tin của bạn:</p>

									<p>- Với các công ty con và các đơn vị trực thuộc của Alfazi cung cấp các dịch vụ hoặc tiến hành xử lý dữ liệu thay mặt chúng tôi hoặc nhằm mục đích tập trung dữ liệu và / hoặc hậu cần;</p>

									<p>- Với các nhà cung cấp, tư vấn, đối tác tiếp thị và các nhà cung cấp dịch vụ khác cần truy cập các thông tin đó để thực hiện công việc thay mặt cho chúng tôi;</p>

									<p>- Để phản hồi yêu cầu về thông tin của cơ quan có thẩm quyền nếu chúng tôi tin rằng việc tiết lộ đó là phù hợp với, hoặc được yêu cầu bởi bất kỳ luật, quy định, hoặc quy trình pháp lý hiện hành nào;</p>

									<p>- Với các viên chức thực thi pháp luật, các cơ quan chính quyền hoặc các bên thứ ba khác nếu chúng tôi tin rằng các hành động của bạn phù hợp với thỏa thuận Người Dùng, Điều Khoản Dịch Vụ hoặc chính sách hoặc để bảo vệ quyền, tài sản hoặc sự an toàn của Alfazi hoặc các bên khác;</p>

									<p>- Liên quan đến, hoặc trong quá trình thương lượng, bất kỳ giao dịch sáp nhập, bán tài sản công ty, hợp nhất hay tái cơ cấu, hỗ trợ tài chính hoặc mua lại toàn bộ hoặc một phần doanh nghiệp của chúng tôi bởi hoặc vào một công ty khác;</p>

									<p>- Theo cách khác nếu chúng tôi thông báo cho bạn và bạn chấp thuận với việc chia sẻ thông tin đó; và</p>

									<p>- Dưới hình thức tổng hợp và/hoặc ẩn danh mà có thể không được sử dụng hợp lý để nhận dạng bạn.</p>

									<p><strong>TÍNH NĂNG CHIA SẼ XÃ HỘI</strong></p>

									<p>Dịch Vụ có thể tích hợp với các tính năng chia sẻ xã hội và các công cụ liên quan khác mà cho phép bạn chia sẻ hành động của mình về Dịch Vụ của chúng tôi với các ứng dụng, trang web hoặc phương tiện truyền thông khác, và ngược lại. Việc sử dụng những tính năng đó cho phép bạn có thể chia sẻ thông tin với bạn bè của bạn hoặc cộng chúng, tùy thuộc vào các cài đặt mà bạn thiết lập với dịch vụ chia sẻ xã hội. Vui lòng tham khảo các chính sách về quyền riêng tư của các dịch vụ chia sẻ xã hội đó để biết thêm thông tin về cách thức các dịch vụ này xử lý dữ liệu mà bạn cung cấp cho các dịch vụ đó hoặc chia sẻ thông qua đó.</p>

									<p><strong>CÁC DỊCH VỤ PHÂN TÍCH HỌC VÀ QUẢNG CÁO ĐƯỢC CUNG CẤP BỞI CÁC BÊN KHÁC</strong></p>

									<p>Chúng tôi có thể cho phép những bên khác cung cấp số liệu đo lường độc giả và các dịch vụ phân tích cho chúng tôi để đảm nhiệm quảng cáo thay mặt cho chúng tôi trên Internet, và để theo dõi và báo cáo về việc thực hiện các quảng cáo đó. Những cơ quan này có thể sử dụng các tập tin cookie, đèn hiệu web, SDK, và các công nghệ khác để nhận dạng thiết bị của bạn khi bạn truy cập trang web của chúng tôi và sử dụng Dịch Vụ của chúng tôi, cũng như khi bạn truy cập các trang web và các dịch vụ trực tuyến khác.</p>

									<p><strong>LỰA CHỌN CỦA BẠN</strong></p>

									<p><strong>THÔNG TIN TÀI KHOẢN</strong></p>

									<p>Bạn có thể sửa thông tin tài khoản của bạn bất cứ lúc nào bằng cách đăng nhập vào tài khoản của bạn trực tuyến hoặc trong ứng dụng. Nếu bạn muốn hủy bỏ tài khoản của mình, vui lòng gửi email cho chúng tôi đến&nbsp;<a href="mailto:contact@alfazi.edu.vn">contact@alfazi.edu.vn</a>. Xin lưu ý rằng trong một số trường hợp chúng tôi có thể giữ lại một số thông tin về bạn theo yêu cầu của pháp luật, hoặc nhằm các mục đích kinh doanh hợp pháp trong phạm vi pháp luật cho phép. Ví dụ như, nếu bạn có một tín dụng dự phòng hoặc khoản nợ trong tài khoản của bạn, hoặc nếu chúng tôi tin rằng bạn đã vi phạm gian lận hoặc vi phạm các Điều Khoản của chúng tôi, chúng tôi có thể tìm cách giải quyết vấn đề trước khi xóa thông tin của bạn.</p>

									<p><strong>QUYỀN TRUY CẬP</strong></p>

									<p>Alfazi sẽ tuân thủ các yêu cầu của cá nhân liên quan đến việc truy cập, chỉnh sửa và/hoặc xóa dữ liệu cá nhân mà Alfazi lưu trữ theo quy định của pháp luật hiện hành.</p>

									<p><strong>THÔNG TIN LIÊN LẠC</strong></p>

									<p>Chúng tôi cũng có thể xin phép cho việc thu thập dữ liệu trên ứng dụng của chúng tôi và đồng bộ các thông tin liên lạc từ thiết bị của bạn theo hệ thống cho phép được sử dụng bởi hệ điều hành di động của bạn. Nếu ban đầu bạn cho phép thu thập những thông tin này, người dùng iOS sau này có thể hủy cho phép bằng cách thay đổi các thiết lập liên lạc trên thiết bị di động của bạn. Nền tảng Android không cung cấp thiết lập như vậy.</p>

									<p><strong>THÔNG TIN QUẢNG CÁO</strong></p>

									<p>Bạn có thể lựa chọn không nhận tin nhắn quảng cáo từ chúng tôi bằng cách làm theo các hướng dẫn trong những tin nhắn đó. Nếu bạn chọn không nhận tin nhắn, chúng tôi vẫn có thể gửi cho bạn những thông tin không phải quảng cáo, chẳng hạn như những thông tin về tài khoản của bạn, về Dịch Vụ mà bạn yêu cầu, hoặc các mối quan hệ kinh doanh liên tục của chúng tôi.</p>

									<p><strong>NHỮNG THAY ĐỔI VỀ TUYÊN BỐ</strong></p>

									<p>Đôi khi chúng tôi có thể thay đổi Tuyên Bố này. Nếu chúng tôi thực hiện những thay đổi đáng kể theo cách thức chúng tôi xử lý thông tin cá nhân của bạn hoặc đối với Tuyên Bố, chúng tôi sẽ thông báo cho bạn thông qua Dịch Vụ hoặc bằng một số phương tiện khác, chẳng hạn như email. Việc bạn tiếp tục sử dụng Dịch Vụ sau khi nhận được những thông báo đó có nghĩa là bạn chấp thuận với những thay đổi đó. Chúng tôi khuyến khích bạn thường xuyên xem lại Tuyên Bố này để biết thông tin mới nhất về các thực hành quyền riêng tư của chúng tôi.</p>

									<p><strong>LIÊN LẠC VỚI CHÚNG TÔI</strong></p>

									<p>Nếu bạn có bất kỳ thắc mắc nào về Chính Sách về Quyền Riêng Tư này, vui lòng liên lạc với chúng tôi theo địa chỉ&nbsp;<a href="mailto:contact@alfazi.edu.vn">contact@alfazi.edu.vn</a>, hoặc viết thư cho chúng tôi đến Công ty Cổ phần Alfazi, 2/5A Quang Trung, phường 14, quận Gò Vấp,&nbsp;thành phố Hồ Chí Minh.</p>

									<p>&nbsp;</p>

								</div>
							</Col>
						</Row>


					</Container>
				</div>
			</MainLayout>
		);
	}
}
