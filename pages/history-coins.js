/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-05 16:08:38
*------------------------------------------------------- */

import React, { PureComponent } from 'react';

import withRoot from 'src/root';

import MainLayout from 'src/layout/Main';
import LoginRequire from 'src/layout/LoginRequire';

import HistoryCoins from 'src/containers/HistoryCoins';

import Title from 'src/components/Layout/Title';

@withRoot
export default class HistoryCoinsPage extends PureComponent {
	static async getInitialProps(/* ctx */) {
		// if (AuthStorage.loggedIn) {
		// 	ctx.store.dispatch(getUserAuth());
		// }
		// return { auth: ctx.store.getState().auth };
	}

	render() {
		return (
			<MainLayout>
				<Title>Lịch sử dùng xu</Title>
				<LoginRequire>
					<HistoryCoins />
				</LoginRequire>
			</MainLayout>
		);
	}
}
