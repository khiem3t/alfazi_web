/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-03-23 11:15:00
 *-------------------------------------------------------*/

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
	view: {
	},
	countNotReceived: 0,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'RECEIVE_NOTI': {
			if (action.payload.type === 'reachedAchievement') {
				return {
					...state,
					countNotReceived: state.countNotReceived + 1,
				};
			}
			return state;
		}

		case 'GET_ACHIEVEMENT_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_ACHIEVEMENT_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		case 'GET_ACHIEVEMENT_DATA_REQUEST':
			return { ...state, view: { ...initialState.view } };

		case 'GET_ACHIEVEMENT_DATA_SUCCESS':
			return { ...state, view: { ...action.payload } };

		case 'COUNT_ACHIEVEMENT_NOT_RECEIVED_SUCCESS':
			return { ...state, countNotReceived: action.payload.count || 0 };

		case 'RECEIVED_ACHIEVEMENT_SUCCESS': {
			const { id } = action.payload;
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.achievementsUnlock && row.achievementsUnlock[0] && row.achievementsUnlock[0].id === id;
			});

			// achievementsUnlock
			list.data[index] = { ...list.data[index], achievementsUnlock: [{ ...action.payload }] };

			return {
				...state,
				list: { ...list },
				countNotReceived: state.countNotReceived - 1,
			};
		}

		default:
			return state;
	}
};

