/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-01-10 23:20:59
 *-------------------------------------------------------*/
import AuthStorage from 'src/utils/AuthStorage';

import { QUESTION_FEE_REFUND, QUESTION_FEE, ANSWER_LIKE_REWARD_EXP, ANSWER_PICK_REWARD_EXP, ANSWER_PICK_REWARD_COIN, QUESTION_CREATE_REWARD_EXP } from 'src/constants/parameters';

export const initialState = {

};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'RECEIVE_NOTI': {
			if (action.payload.type === 'answerLikeCreated') {
				let { pointsCount = 0 } = state;

				pointsCount += ANSWER_LIKE_REWARD_EXP;

				return {
					...state,
					pointsCount,
					level: Math.floor((Math.sqrt((pointsCount * 20) + 25) - 5) / 10),
				};
			}
			if (action.payload.type === 'answerPickCreated') {
				let { coinsCount = 0, pointsCount = 0 } = state;

				if (action.payload.question && action.payload.question.type === 'coin') {
					coinsCount += ANSWER_PICK_REWARD_COIN;
				}

				pointsCount += ANSWER_PICK_REWARD_EXP;

				return {
					...state,
					coinsCount,
					pointsCount,
					level: Math.floor((Math.sqrt((pointsCount * 20) + 25) - 5) / 10),
				};
			}

			if (action.payload.type === 'receivedBadge') {
				const badge = action.payload.data && action.payload.data.badgeUnlock && action.payload.data.badge ? { ...action.payload.data.badgeUnlock, badge: action.payload.data.badge } : null;

				return {
					...state,
					badgesUnlock: [badge, ...state.badgesUnlock],
				};
			}
			if (action.payload.type === 'receivedAchievement') {
				const { attendance = {} } = state;
				let { coinsCount = 0 } = state;

				const achievement = action.payload.data && action.payload.data.achievement && action.payload.data.achievement ? action.payload.data.achievement : { amount: 0 };

				coinsCount += achievement.amount;

				if (achievement.type === 'attendance') {
					attendance.loggedIn = true;
				}

				return {
					...state,
					coinsCount,
					attendance,
				};
			}
			return state;
		}
		case 'LOGIN_SUCCESS':
			/* eslint-disable no-undef */
			return action.payload;
		case 'LOGIN_FAILED':
			return { error: action.payload.message || action.payload };
		case 'LOGOUT_SUCCESS':
			AuthStorage.destroy();
			return {};
		case 'GET_USER_AUTH_SUCCESS':
			return action.payload;
		case 'EDIT_PROFILE_SUCCESS':
			return { ...state, ...action.payload };

		case 'CREATE_QUESTION_SUCCESS': {
			let { coinsCount = 0, pointsCount = 0 } = state;

			if (action.payload.type === 'coin') {
				coinsCount -= QUESTION_FEE;
			}

			pointsCount += QUESTION_CREATE_REWARD_EXP;


			return {
				...state,
				coinsCount,
				pointsCount,
				level: Math.floor((Math.sqrt((pointsCount * 20) + 25) - 5) / 10),
			};
		}

		case 'CHANGE_QUESTION_TYPE_SUCCESS': {
			let { coinsCount = 0 } = state;

			if (action.payload.type === 'coin') {
				coinsCount -= QUESTION_FEE;
			}

			return {
				...state,
				coinsCount,
			};
		}

		case 'PICK_ANSWER_SUCCESS': {
			let { coinsCount = 0 } = state;

			if (action.payload.questionType === 'coin') {
				coinsCount += QUESTION_FEE_REFUND;
			}

			return {
				...state,
				coinsCount,
			};
		}

		case 'DELETE_QUESTION_SUCCESS':
			return { ...state, questionsCount: state.questionsCount - 1 };

		default:
			return state;
	}
};

