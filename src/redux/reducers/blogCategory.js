/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-03-20 14:55:55
 *-------------------------------------------------------*/

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
	view: {
	},
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'GET_BLOG_CATEGORY_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_BLOG_CATEGORY_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		case 'GET_BLOG_CATEGORY_DATA_REQUEST':
			return { ...state, view: { ...initialState.view } };

		case 'GET_BLOG_CATEGORY_DATA_SUCCESS':
			return { ...state, view: { ...action.payload } };

		default:
			return state;
	}
};

