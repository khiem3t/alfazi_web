export const initialState = {
	rankList: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'GET_RANKING_DATA_REQUEST':
			return { ...state,
				rankList: { ...initialState.rankList } };
		case 'GET_RANKING_DATA_SUCCESS':
			return {
				...state,
				rankList: { ...action.payload },
			};
		default:
			return state;
	}
};

