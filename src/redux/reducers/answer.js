/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-03-01 14:57:13
 *-------------------------------------------------------*/

import { spliceOne } from 'src/utils';

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
	view: {
	},
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'GET_ANSWER_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_ANSWER_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		case 'GET_ANSWER_DATA_REQUEST':
			return { ...state, view: { ...initialState.view } };

		case 'GET_ANSWER_DATA_SUCCESS':
			return { ...state, view: { ...action.payload } };

		case 'UPDATE_ANSWER_SUCCESS': {
			const { id, status } = action.payload;
			const { list, view } = state;

			const index = list.data.findIndex((row) => {
				return row.id === id;
			});

			if (status === 'active') {
				list.data[index] = { ...list.data[index], ...action.payload };
			} else {
				spliceOne(list.data, index);
			}

			if (view.id) {
				return { ...state, view: { ...action.payload } };
			}

			return { ...state };
		}

		case 'DELETE_ANSWER_SUCCESS': {
			const { id } = action.payload;
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.id === id;
			});

			spliceOne(list.data, index);

			return { ...state, list: { ...list, total: list.total - 1, skip: list.skip - 1 } };
		}

		case 'GET_REPLY_LIST_SUCCESS': {
			const { answerId } = action.payload && action.payload.data && action.payload.data[0];
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.id === answerId;
			});

			if (index >= 0) {
				list.data[index] = { ...list.data[index], replies: action.payload.data };
			}

			return { ...state, list };
		}

		case 'CREATE_REPLY_LIST_SUCCESS': {
			const { answerId } = action.payload;
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.id === answerId;
			});

			if (index >= 0) {
				const replies = list.data[index] && list.data[index].replies ? list.data[index].replies : [];

				list.data[index] = { ...list.data[index], replies: [...replies, action.payload], repliesCount: list.data[index].repliesCount + 1 };
			}

			return { ...state, list };
		}

		case 'UPDATE_REPLY_SUCCESS': {
			const { answerId, id: relyId } = action.payload;
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.id === answerId;
			});

			if (index >= 0) {
				const replies = list.data[index] && list.data[index].replies ? list.data[index].replies : [];

				const indexReply = replies.findIndex((row) => {
					return row.id === relyId;
				});

				replies[indexReply] = { ...replies[indexReply], ...action.payload };

				list.data[index] = { ...list.data[index], replies: [...replies] };
			}

			return { ...state, list };
		}

		case 'DELETE_REPLY_SUCCESS': {
			const { answerId, id: relyId } = action.payload.instance;
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.id === answerId;
			});

			if (index >= 0) {
				const replies = list.data[index] && list.data[index].replies ? list.data[index].replies : [];

				const indexReply = replies.findIndex((row) => {
					return row.id === relyId;
				});

				spliceOne(replies, indexReply);

				list.data[index] = { ...list.data[index], replies: [...replies], repliesCount: list.data[index].repliesCount - 1 };
			}

			return { ...state, list };
		}

		default:
			return state;
	}
};

