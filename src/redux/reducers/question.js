/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-03-01 14:57:13
 *-------------------------------------------------------*/
import { spliceOne } from 'src/utils';

export const initialState = {
	questionList: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
	topQuestionList: {
		total: 0,
		skip: 0,
		limit: 4,
		data: [],
		loading: true,
	},
	pinList: {
		total: 0,
		skip: 0,
		limit: 4,
		data: [],
		loading: true,
	},
	relativeQuestionList: {
		total: 0,
		skip: 0,
		limit: 4,
		data: [],
		loading: true,
	},
	questionView: {
	},
	newQuestion: 0,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'QUESTION_CREATED_NOTI':
			return {
				...state,
				newQuestion: state.newQuestion + 1,
			};

		case 'CREATE_QUESTION_SUCCESS':
			return {
				...state,
				questionList: {
					...state.questionList,
					data: [{ ...action.payload, answers: [], saves: [] }, ...state.questionList.data],
					total: state.questionList.total + 1,
					skip: state.questionList.skip + 1,
				},
			};

		case 'GET_QUESTION_LIST_REQUEST':
			return { ...state, questionList: { ...initialState.questionList }, newQuestion: 0 };

		case 'GET_QUESTION_LIST_SUCCESS': {
			const data = [...state.questionList.data, ...action.payload.data];
			return {
				...state,
				questionList: {
					...action.payload,
					data,
					loading: false,
				},
				newQuestion: 0,
			};
		}

		case 'GET_QUESTION_DATA_REQUEST':
			return { ...state, questionView: { ...initialState.questionView } };

		case 'GET_QUESTION_DATA_SUCCESS':
			return { ...state, questionView: { ...action.payload } };

		case 'CHANGE_QUESTION_TYPE_SUCCESS':
		case 'UPDATE_QUESTION_SUCCESS': {
			const { id, status, oldData = {} } = action.payload;
			const { questionList, questionView } = state;

			// thay doi pin cu
			if (action.payload.oldData && action.payload.oldData.id) {
				const i = questionList.data.findIndex((row) => {
					return row.id === oldData.id;
				});
				if (i > -1) {
					questionList.data[i] = { ...questionList.data[i], ...oldData };
				}
			}

			const index = questionList.data.findIndex((row) => {
				return row.id === id;
			});


			if (status === 'active') {
				questionList.data[index] = { ...questionList.data[index], ...action.payload };
			} else {
				questionList.data.splice(index, 1);
			}

			if (questionView.id) {
				return { ...state, questionView: { ...state.questionView, ...action.payload } };
			}

			return { ...state };
		}

		case 'DELETE_QUESTION_SUCCESS': {
			const { id } = action.payload;
			const { questionList } = state;

			const index = questionList.data.findIndex((row) => {
				return row.id === id;
			});

			spliceOne(questionList.data, index);

			return { ...state };
		}

		case 'GET_TOP_QUESTION_LIST_REQUEST':
			return { ...state, topQuestionList: { ...initialState.topQuestionList, loading: true } };

		case 'GET_TOP_QUESTION_LIST_SUCCESS': {
			return { ...state, topQuestionList: { ...action.payload, loading: false } };
		}

		case 'GET_RELATIVE_QUESTION_LIST_REQUEST':
			return { ...state, relativeQuestionList: { ...initialState.relativeQuestionList, loading: true } };

		case 'GET_RELATIVE_QUESTION_LIST_SUCCESS': {
			return {
				...state,
				relativeQuestionList: {
					...action.payload,
					data: [...state.relativeQuestionList.data, ...action.payload.data],
					loading: false,
				},
			};
		}

		case 'PICK_ANSWER_SUCCESS':
			return { ...state, questionView: { ...state.questionView, pick: { ...action.payload } } };
		case 'GET_PIN_LIST_REQUEST':
			return { ...state, pinList: { ...initialState.pinList, loading: true } };
		case 'GET_PIN_LIST_SUCCESS': {
			return { ...state, pinList: { ...action.payload, loading: false } };
		}
		case 'PIN_QUESTION_SUCCESS': {
			const { id, oldData } = action.payload;
			const { questionList, pinList } = state;

			if (oldData) {
				// tim index oldData trong pinList
				const i = pinList.data.findIndex((row) => {
					return row.id === oldData;
				});

				// neu ton tai oldData trong pinList
				if (i > -1) {
					// Xoa oldData trong pinList
					pinList.data.splice(i, 1);
				}

				// tim index id cua oldData trong questionList
				const ii = questionList.data.findIndex((row) => {
					return row.id === oldData;
				});

				// Neu ton tai id oldData trong questionList
				if (ii > -1) {
					// Cap nhat nhat oldData trong questionList
					questionList.data[ii].pin = false;
				}
			}

			if (id) {
				// tim index id new trong questionList
				const index = questionList.data.findIndex((row) => {
					return row.id === id;
				});

				// Neu ton tai id new trong questionList
				if (index > -1) {
				// Cap nhat question trong questionList
					questionList.data[index].pin = true;
					// Them item trong pinList
					pinList.data.push(questionList.data[index]);
				}
			} else {
				// tim index id new trong questionList
				const iii = questionList.data.findIndex((row) => {
					return row.id === pinList.data[0].id;
				});
				if (iii > -1) {
					questionList.data[iii].pin = false;
				}
				pinList.data.splice(0, 1);
			}


			return { ...state };
		}
		default:
			return state;
	}
};

