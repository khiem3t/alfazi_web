/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-03-01 14:57:13
 *-------------------------------------------------------*/

import { spliceOne } from 'src/utils';

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
		loading: true,
	},
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'GET_SAVED_QUESTION_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_SAVED_QUESTION_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		default:
			return state;
	}
};

