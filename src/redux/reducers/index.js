/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2017-12-16 00:42:57
*------------------------------------------------------- */

import auth, { initialState as initialAuth } from './auth';
import loading, { initialState as initialLoading } from './loading';
import modal, { initialState as initialModal } from './modal';

import user, { initialState as initialUser } from './user';
import question, { initialState as initialQuestion } from './question';
import questionSave, { initialState as initialQuestionSave } from './questionSave';
import answer, { initialState as initialAnswer } from './answer';
import notification, { initialState as initialNotification } from './notification';
import relationship, { initialState as initialRelationship } from './relationship';
import tracking, { initialState as initialTracking } from './tracking';
import blogCategory, { initialState as initialBlogCategory } from './blogCategory';
import blogPost, { initialState as initialBlogPost } from './blogPost';
import achievement, { initialState as initialAchievement } from './achievement';
import transactionCoins, { initialState as initialTransactionCoins } from './transactionCoins';
import ranking, { initialState as initialRanking } from './ranking';

export const initialState = {
	auth: initialAuth,
	loading: initialLoading,
	modal: initialModal,
	user: initialUser,
	question: initialQuestion,
	questionSave: initialQuestionSave,
	answer: initialAnswer,
	notification: initialNotification,
	relationship: initialRelationship,
	tracking: initialTracking,
	blogCategory: initialBlogCategory,
	blogPost: initialBlogPost,
	achievement: initialAchievement,
	transactionCoins: initialTransactionCoins,
	ranking: initialRanking,
};

export default {
	auth,
	loading,
	modal,
	user,
	question,
	questionSave,
	answer,
	notification,
	relationship,
	tracking,
	blogCategory,
	blogPost,
	achievement,
	transactionCoins,
	ranking,
};
