/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-01-10 23:20:59
 *-------------------------------------------------------*/
// import AuthStorage from 'src/utils/AuthStorage';

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
	countUnread: 0,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'RECEIVE_NOTI': {
			if (action.payload.type === 'relationshipCreated') {
				const { data: { creator = {}, relationship = {} } } = action.payload;
				const relation = { ...relationship, creator };

				return {
					...state,
					countUnread: state.countUnread + 1,
					list: {
						...state.list,
						data: [relation, ...state.list.data],
						total: state.list.total + 1,
						skip: state.list.skip + 1,
					},
				};
			}
			return state;
		}

		case 'GET_FRIEND_REQ_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_FRIEND_REQ_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		case 'COUNT_FRIEND_REQ_UNREAD':
			return { ...state, countUnread: action.payload.count || 0 };

		case 'UPDATE_RELATIONSHIP_SUCCESS':
		case 'DELETE_RELATIONSHIP_SUCCESS': {
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.id === action.payload.id;
			});

			list.data.splice(index, 1);

			return {
				...state,
				countUnread: state.countUnread - 1,
				list: {
					...list,
					total: state.list.total - 1,
					skip: state.list.skip - 1,
				},
			};
		}

		case 'GET_RELATIONSHIP_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_RELATIONSHIP_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		default:
			return state;
	}
};

