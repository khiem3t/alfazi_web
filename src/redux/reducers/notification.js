/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-01-10 23:20:59
 *-------------------------------------------------------*/
// import AuthStorage from 'src/utils/AuthStorage';

import React from 'react';

import { notification } from 'antd';

import NotiPusher from 'src/components/Notification/Pusher';

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
	countUnread: 0,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'RECEIVE_NOTI': {
			const audio = document.getElementById('noti-sound');
			audio.play();
			notification.open({
				// message: 'Notification Title',
				description: (
					<div style={{ marginTop: -20 }}>
						<NotiPusher
							notiData={action.payload}
						/>
					</div>
				),
				// duration: 0,
			});
			if (action.payload.type === 'relationshipCreated' || action.payload.type === 'reachedAchievement') {
				return state;
			}
			return {
				...state,
				countUnread: state.countUnread + 1,
				list: {
					...state.list,
					data: [action.payload, ...state.list.data],
					total: state.list.total + 1,
					skip: state.list.skip + 1,
				},
			};
		}

		case 'GET_NOTI_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_NOTI_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		case 'COUNT_NOTI_UNREAD':
			return { ...state, countUnread: action.payload.count || 0 };

		case 'UPDATE_NOTI_SUCCESS': {
			let { countUnread } = state;
			const { list } = state;

			if (action.payload.type === 'relationshipCreated') {
				return state;
			}

			const i = list.data.findIndex((el) => {
				return el.id === action.payload.id;
			});

			list.data[i] = { ...list.data[i], ...action.payload }; // eslint-disable-line
			countUnread = countUnread - 1; // eslint-disable-line

			return {
				...state,
				countUnread,
				list: { ...list },
			};
		}

		case 'UPDATE_ALL_NOTI_SUCCESS':
			return {
				...state,
				countUnread: 0,
				list: {
					...state.list,
					data: state.list.data.map((el) => {
						return { ...el, read: true };
					}),
				},
			};

		case 'DELETE_BY_ID_NOTI_SUCCESS': {
			const { instance = {} } = action.payload;
			let { countUnread } = state;
			const { list } = state;

			if (instance.type === 'relationshipCreated') {
				return state;
			}

			const i = list.data.findIndex((el) => {
				return el.id === action.payload.id;
			});

			list.data.splice(i, 1);
			list.total = list.total - 1; // eslint-disable-line
			list.skip = list.skip - 1; // eslint-disable-line

			if (!instance.read) {
				countUnread = countUnread - 1; // eslint-disable-line
			}

			return {
				...state,
				countUnread,
				list: { ...list },
			};
		}
		default:
			return state;
	}
};

