/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-03-20 14:55:55
 *-------------------------------------------------------*/

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
	listLatest: {
		total: 0,
		skip: 0,
		limit: 5,
		data: [],
	},
	listRelative: {
		total: 0,
		skip: 0,
		limit: 4,
		data: [],
	},
	view: {
		loading: true,
	},
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'GET_POST_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_POST_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		case 'GET_POST_LIST_LATEST_SUCCESS': {
			return {
				...state,
				listLatest: {
					...action.payload,
					loading: false,
				},
			};
		}

		case 'GET_POST_LIST_RELATIVE_SUCCESS': {
			return {
				...state,
				listRelative: {
					...action.payload,
					loading: false,
				},
			};
		}

		case 'GET_POST_DATA_REQUEST':
			return { ...state, view: { ...initialState.view } };

		case 'GET_POST_DATA_SUCCESS':
			return { ...state, view: { ...action.payload, loading: false } };

		default:
			return state;
	}
};

