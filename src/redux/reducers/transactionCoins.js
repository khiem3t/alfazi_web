/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-03-01 14:57:13
 *-------------------------------------------------------*/

import { spliceOne } from 'src/utils';

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
	view: {
	},
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'GET_TRANSACTION_COINS_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_TRANSACTION_COINS_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		case 'GET_TRANSACTION_COINS_DATA_REQUEST':
			return { ...state, view: { ...initialState.view } };

		case 'GET_TRANSACTION_COINS_DATA_SUCCESS':
			return { ...state, view: { ...action.payload } };

		case 'UPDATE_TRANSACTION_COINS_SUCCESS': {
			const { id, status } = action.payload;
			const { list, view } = state;

			const index = list.data.findIndex((row) => {
				return row.id === id;
			});

			if (status === 'active') {
				list.data[index] = { ...list.data[index], ...action.payload };
			} else {
				spliceOne(list.data, index);
			}

			if (view.id) {
				return { ...state, view: { ...action.payload } };
			}

			return { ...state };
		}

		case 'DELETE_TRANSACTION_COINS_SUCCESS': {
			const { id } = action.payload;
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.id === id;
			});

			spliceOne(list.data, index);

			return { ...state, list: { ...list, total: list.total - 1, skip: list.skip - 1 } };
		}

		default:
			return state;
	}
};

