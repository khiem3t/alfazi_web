/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-03-11 00:12:46
 *-------------------------------------------------------*/
// import AuthStorage from 'src/utils/AuthStorage';

export const initialState = {
	list: {
		total: 0,
		skip: 0,
		limit: 12,
		data: [],
	},
};

export default (state = initialState, action) => {
	switch (action.type) {
		case 'DELETE_TRACKING_SUCCESS': {
			const { list } = state;

			const index = list.data.findIndex((row) => {
				return row.id === action.payload.id;
			});

			list.data.splice(index, 1);

			return {
				...state,
				countUnread: state.countUnread - 1,
				list: {
					...list,
					total: state.list.total - 1,
					skip: state.list.skip - 1,
				},
			};
		}

		case 'GET_TRACKING_LIST_REQUEST':
			return { ...state, list: { ...initialState.list } };

		case 'GET_TRACKING_LIST_SUCCESS': {
			const data = [...state.list.data, ...action.payload.data];
			return {
				...state,
				list: {
					...action.payload,
					data,
					loading: false,
				},
			};
		}

		default:
			return state;
	}
};

