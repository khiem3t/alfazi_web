/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-06 01:03:01
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

import { applyURIFilter } from 'src/utils';

export const createTransactionCoins = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/transaction-coins',
			params: payload,
			opt: { method: 'POST' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const updateTransactionCoins = (payload, next, nextError) => {
	const { id, ...data } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/transaction-coins/' + id,
			params: data,
			opt: { method: 'PATCH' },
			successType: 'UPDATE_TRANSACTION_COINS_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getTransactionCoinsData = (payload = {}, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/transaction-coins/${id}${applyURIFilter(filter)}`,
			beforeCallType: 'GET_TRANSACTION_COINS_DATA_REQUEST',
			successType: 'GET_TRANSACTION_COINS_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getTransactionCoinsList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/transaction-coins${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_TRANSACTION_COINS_LIST_REQUEST' : '',
			successType: 'GET_TRANSACTION_COINS_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const deleteTransactionCoins = (payload, next) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/transaction-coins/' + id,
			params: id,
			opt: { method: 'DELETE' },
			successType: 'DELETE_TRANSACTION_COINS_SUCCESS',
			afterSuccess: next,
		},
	};
};
