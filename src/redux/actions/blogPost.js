/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-20 14:52:35
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

import { applyURIFilter } from 'src/utils';

export const getPostData = (payload = {}, next, nextError) => {
	const { filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/blog-posts/findOne${applyURIFilter(filter)}`,
			beforeCallType: 'GET_POST_DATA_REQUEST',
			successType: 'GET_POST_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getPostList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/blog-posts${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_POST_LIST_REQUEST' : '',
			successType: 'GET_POST_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getPostListLatest = (payload = {}, next, nextError) => {
	const {
		filter = {
			limit: 5,
			skip: 0,
			order: 'publishedDate DESC',
			fields: { content: false },
			where: {
				state: 'published',
				publishedDate: { lte: new Date() },
				id: { nin: [payload.postCurrentId] },
			},
		},
	} = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/blog-posts${applyURIFilter(filter)}`,
			successType: 'GET_POST_LIST_LATEST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getPostListRelative = (payload = {}, next, nextError) => {
	const {
		filter = {
			limit: 5,
			skip: 0,
			order: 'publishedDate DESC',
			include: 'category',
			fields: { content: false },
			where: {
				state: 'published',
				publishedDate: { lte: new Date() },
				categoryId: payload.categoryId,
				id: { nin: [payload.postCurrentId] },
			},
		},
	} = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/blog-posts${applyURIFilter(filter)}`,
			successType: 'GET_POST_LIST_RELATIVE_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};
