/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2017-10-22 10:34:17
*------------------------------------------------------- */

import AuthStorage from 'src/utils/AuthStorage';

import { SINGLE_API } from 'src/redux/actions/type';

import { Router } from 'src/routes';

import URL from 'src/constants/url';
import { applyURIFilter } from 'src/utils';

const { API_URL } = URL;

let connected = false;

export const addNotiListener = (dispatch) => {
	if (!connected) {
		if (typeof (EventSource) !== 'undefined') {
			const urlToChangeStream = API_URL + '/notifications/change-stream?_format=event-stream';
			const src = new EventSource(urlToChangeStream); // eslint-disable-line

			src.addEventListener('data', (msg) => {
				const data = JSON.parse(msg.data);
				// console.log('data', data);

				if (data.type === 'create' && data.data.receiverId === AuthStorage.userId && data.data.creatorId !== AuthStorage.userId) {
					const noti = data.data;

					dispatch({
						type: 'RECEIVE_NOTI',
						payload: noti,
					});
				}

				if (Router.pathname === '/' && data.type === 'create' && data.data.type === 'questionCreated' && data.data.creatorId !== AuthStorage.userId) {
					const noti = data.data;

					dispatch({
						type: 'QUESTION_CREATED_NOTI',
						payload: noti,
					});
				}
			});

			connected = true;
		} else {
			alert('Sorry, your browser does not support server-sent events...'); // eslint-disable-line
		}
	}
};

export const getNotiList = (payload, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/notifications${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_NOTI_LIST_REQUEST' : '',
			successType: 'GET_NOTI_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const countNotiUnread = (payload = {}, next) => {
	const { where = {
		receiverId: AuthStorage.userId,
		creatorId: { neq: AuthStorage.userId },
		type: { nin: ['relationshipCreated', 'reachedAchievement'] },
		read: false,
	} } = payload;
	return {
		type: SINGLE_API,
		payload: {
			uri: `/notifications/count?where=${JSON.stringify(where)}`,
			successType: 'COUNT_NOTI_UNREAD',
			afterSuccess: next,
		},
	};
};

export const updateNoti = (payload, next) => {
	const { id, ...params } = payload;
	return {
		type: SINGLE_API,
		payload: {
			uri: `/notifications/${id}`,
			opt: { method: 'PATCH' },
			params,
			successType: 'UPDATE_NOTI_SUCCESS',
			afterSuccess: next,
		},
	};
};

export const updateAllNoti = (payload, next) => {
	const {
		where = {
			receiverId: AuthStorage.userId,
			type: { nin: ['relationshipCreated', 'reachedAchievement'] },
			read: false,
		},
		data,
	} = payload;
	return {
		type: SINGLE_API,
		payload: {
			method: 'POST',
			uri: `/notifications/update?where=${JSON.stringify(where)}`,
			opt: { method: 'POST' },
			params: data,
			successType: 'UPDATE_ALL_NOTI_SUCCESS',
			afterSuccess: next,
		},
	};
};

export const deleteNoti = (payload, next) => {
	const { id } = payload;
	return {
		type: SINGLE_API,
		payload: {
			method: 'DELETE',
			uri: `/notifications/${id}`,
			opt: { method: 'DELETE' },
			params: payload,
			successType: 'DELETE_BY_ID_NOTI_SUCCESS',
			afterSuccess: next,
		},
	};
};
