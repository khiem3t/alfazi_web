/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-06 01:03:01
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

import { applyURIFilter } from 'src/utils';

export const createAnswer = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/answers',
			params: payload,
			opt: { method: 'POST' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const updateAnswer = (payload, next, nextError) => {
	const { id, ...answer } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/answers/' + id,
			params: answer,
			opt: { method: 'PATCH' },
			successType: 'UPDATE_ANSWER_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getAnswerData = (payload = {}, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/answers/${id}${applyURIFilter(filter)}`,
			beforeCallType: 'GET_ANSWER_DATA_REQUEST',
			successType: 'GET_ANSWER_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getAnswerList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/answers${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_ANSWER_LIST_REQUEST' : '',
			successType: 'GET_ANSWER_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const deleteAnswer = (payload, next) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/answers/' + id,
			params: id,
			opt: { method: 'DELETE' },
			successType: 'DELETE_ANSWER_SUCCESS',
			afterSuccess: next,
		},
	};
};
