/*--------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * LastModified: 2018-02-10 09:52:14
 *-------------------------------------------------------*/

import { SINGLE_API } from 'src/redux/actions/type';

import { applyURIFilter } from 'src/utils';

export const getUserList = (payload, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/users${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_USER_LIST_REQUEST' : '',
			successType: 'GET_USER_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getTopUserList = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: `/users${applyURIFilter(payload.filter)}`,
			beforeCallType: payload.firstLoad ? 'GET_TOP_USER_LIST_REQUEST' : '',
			successType: 'GET_TOP_USER_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getUserData = (payload, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/users/${id}${applyURIFilter(filter)}`,
			beforeCallType: 'GET_USER_DATA_REQUEST',
			successType: 'GET_USER_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

