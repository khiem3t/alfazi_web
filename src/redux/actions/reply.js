/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-04 21:31:11
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

import { applyURIFilter } from 'src/utils';

export const createReply = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/replies',
			params: payload,
			opt: { method: 'POST' },
			successType: 'CREATE_REPLY_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const updateReply = (payload, next, nextError) => {
	const { id, ...Reply } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/replies/' + id,
			params: Reply,
			opt: { method: 'PATCH' },
			successType: 'UPDATE_REPLY_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getReplyData = (payload = {}, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/replies/${id}${applyURIFilter(filter)}`,
			beforeCallType: 'GET_REPLY_DATA_REQUEST',
			successType: 'GET_REPLY_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getReplyList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/replies${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_REPLY_LIST_REQUEST' : '',
			successType: 'GET_REPLY_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const deleteReply = (payload, next) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/replies/' + id,
			params: id,
			opt: { method: 'DELETE' },
			successType: 'DELETE_REPLY_SUCCESS',
			afterSuccess: next,
		},
	};
};
