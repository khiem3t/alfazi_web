/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-27 16:04:04
*------------------------------------------------------- */
import { SINGLE_API } from 'src/redux/actions/type';

import { fetching } from 'src/utils/FetchApi';

export const getHistoryList = (payload) => {
	const { filter } = payload;
	return fetching(`/histories-search?filter=${JSON.stringify(filter)}`);
};

export const searchUsers = (payload) => {
	const { filter } = payload;
	return fetching(`/users?filter=${JSON.stringify(filter)}`);
};

export const searchBlogPosts = (payload) => {
	const { filter } = payload;
	return fetching(`/blog-posts?filter=${JSON.stringify(filter)}`);
};

export const searchQuestions = (payload) => {
	const { filter } = payload;
	return fetching(`/questions?filter=${JSON.stringify(filter)}`);
};

export const upsertWithWhereHistorySearch = (payload, next, nextError) => {
	const { userId, type, value } = payload;

	return {
		type: SINGLE_API,
		payload: {
			opt: { method: 'POST' },
			uri: `/histories-search/upsertWithWhere?where=${JSON.stringify({ userId, type, value })}`,
			params: { ...payload, updatedAt: new Date() },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};
