/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-10 23:44:31
*------------------------------------------------------- */
// import AuthStorage from 'src/utils/AuthStorage';

import { SINGLE_API } from 'src/redux/actions/type';

import { applyURIFilter } from 'src/utils';

export const createTracking = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/trackings',
			params: payload,
			opt: { method: 'POST' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const updateTracking = (payload, next, nextError) => {
	const { id, ...tracking } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/trackings/' + id,
			params: tracking,
			opt: { method: 'PATCH' },
			successType: 'UPDATE_TRACKING_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getTrackingData = (payload = {}, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/trackings/${id}${applyURIFilter(filter)}`,
			beforeCallType: 'GET_TRACKING_DATA_REQUEST',
			successType: 'GET_TRACKING_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getTrackingList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/trackings${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_TRACKING_LIST_REQUEST' : '',
			successType: 'GET_TRACKING_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const deleteTracking = (payload, next) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/trackings/' + id,
			params: id,
			opt: { method: 'DELETE' },
			successType: 'DELETE_TRACKING_SUCCESS',
			afterSuccess: next,
		},
	};
};
