/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-06 01:03:01
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

export const createAnswerLike = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/answer-likes',
			params: payload,
			opt: { method: 'POST' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const deleteAnswerLike = (payload, next, nextError) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/answer-likes/' + id,
			params: id,
			opt: { method: 'DELETE' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};
