/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-23 11:12:53
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
import AuthStorage from 'src/utils/AuthStorage';

import { applyURIFilter } from 'src/utils';

export const getAchievementData = (payload = {}, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/achievements/${id}${applyURIFilter(filter)}`,
			beforeCallType: 'GET_ACHIEVEMENT_DATA_REQUEST',
			successType: 'GET_ACHIEVEMENT_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getAchievementList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/achievements${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_ACHIEVEMENT_LIST_REQUEST' : '',
			successType: 'GET_ACHIEVEMENT_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const countAchievementNotReceived = (payload = {}, next) => {
	const { where = {
		userId: AuthStorage.userId,
		status: 'notReceived',
	} } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/achievements-unlock/count?where=${JSON.stringify(where)}`,
			successType: 'COUNT_ACHIEVEMENT_NOT_RECEIVED_SUCCESS',
			afterSuccess: next,
		},
	};
};

export const receivedAchievement = (payload, next, nextError) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/achievements-unlock/' + id,
			params: { status: 'received' },
			opt: { method: 'PATCH' },
			successType: 'RECEIVED_ACHIEVEMENT_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

