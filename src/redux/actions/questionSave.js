/* --------------------------------------------------------
* Author Nguyễn Đăng Anh Thi
* Email nguyendanganhthi247@gmail.com
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
import { applyURIFilter } from 'src/utils';

export const saveQuestion = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/question-saves',
			params: payload,
			opt: { method: 'POST' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const unsaveQuestion = (payload, next, nextError) => {
	const { savedQuestionId } = payload;
	return {
		type: SINGLE_API,
		payload: {
			uri: '/question-saves/' + savedQuestionId,
			opt: { method: 'DELETE' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getSavedQuestionList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/question-saves/' + applyURIFilter(filter),
			opt: { method: 'GET' },
			beforeCallType: firstLoad ? 'GET_SAVED_QUESTION_LIST_REQUEST' : '',
			successType: 'GET_SAVED_QUESTION_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

