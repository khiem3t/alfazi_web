/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-20 14:52:35
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

import { applyURIFilter } from 'src/utils';

export const getCategoryData = (payload = {}, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/blog-categories/${id}${applyURIFilter(filter)}`,
			beforeCallType: 'GET_BLOG_CATEGORY_DATA_REQUEST',
			successType: 'GET_BLOG_CATEGORY_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getCategoryList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/blog-categories${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_BLOG_CATEGORY_LIST_REQUEST' : '',
			successType: 'GET_BLOG_CATEGORY_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};
