/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 14:50:54
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

export const createQuestion = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions',
			params: payload,
			opt: { method: 'POST' },
			successType: 'CREATE_QUESTION_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const updateQuestion = (payload, next, nextError) => {
	const { id, ...question } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions/' + id,
			params: question,
			opt: { method: 'PATCH' },
			successType: 'UPDATE_QUESTION_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const changeQuestionType = (payload, next, nextError) => {
	const { id, type } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions/' + id,
			params: { type },
			opt: { method: 'PATCH' },
			successType: 'CHANGE_QUESTION_TYPE_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getQuestionData = (payload = {}, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions/' + id + (filter ? `?filter=${JSON.stringify(filter)}` : ''),
			beforeCallType: 'GET_QUESTION_DATA_REQUEST',
			successType: 'GET_QUESTION_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getQuestionList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions' + (filter ? `?filter=${JSON.stringify(filter)}` : ''),
			beforeCallType: firstLoad ? 'GET_QUESTION_LIST_REQUEST' : '',
			successType: 'GET_QUESTION_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const deleteQuestion = (payload, next) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions/' + id,
			params: id,
			opt: { method: 'DELETE' },
			successType: 'DELETE_QUESTION_SUCCESS',
			afterSuccess: next,
		},
	};
};

export const getTopQuestionList = (payload = {}, next, nextError) => {
	const { firstLoad, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions' + (filter ? `?filter=${JSON.stringify(filter)}` : ''),
			beforeCallType: firstLoad ? 'GET_TOP_QUESTION_LIST_REQUEST' : '',
			successType: 'GET_TOP_QUESTION_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getRelativeQuestionList = (payload = {}, next, nextError) => {
	const { firstLoad, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions' + (filter ? `?filter=${JSON.stringify(filter)}` : ''),
			beforeCallType: firstLoad ? 'GET_RELATIVE_QUESTION_LIST_REQUEST' : '',
			successType: 'GET_RELATIVE_QUESTION_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};


export const getPinList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions' + (filter ? `?filter=${JSON.stringify(filter)}` : ''),
			beforeCallType: firstLoad ? 'GET_PIN_LIST_REQUEST' : '',
			successType: 'GET_PIN_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const pinQuestion = (payload, next, nextError) => {
	const { ...question } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/questions/pin-to-question',
			params: question,
			opt: { method: 'POST' },
			successType: 'PIN_QUESTION_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

