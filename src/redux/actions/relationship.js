/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-08 23:47:17
*------------------------------------------------------- */
import AuthStorage from 'src/utils/AuthStorage';

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

import { applyURIFilter } from 'src/utils';

export const createRelationship = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/relationships',
			params: payload,
			opt: { method: 'POST' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const updateRelationship = (payload, next, nextError) => {
	const { id, ...relationship } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/relationships/' + id,
			params: relationship,
			opt: { method: 'PATCH' },
			successType: 'UPDATE_RELATIONSHIP_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getFriendReqList = (payload, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/relationships${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_FRIEND_REQ_LIST_REQUEST' : '',
			successType: 'GET_FRIEND_REQ_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getOneRelationship = (payload, next, nextError) => {
	const { filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/relationships/findOne${applyURIFilter(filter)}`,
			beforeCallType: 'GET_RELATIONSHIP_DATA_REQUEST',
			successType: 'GET_RELATIONSHIP_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};


export const countFriendReqUnread = (payload = {}, next) => {
	const { where = {
		friendId: AuthStorage.userId,
		status: 'pending',
	} } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/relationships/count?where=${JSON.stringify(where)}`,
			successType: 'COUNT_FRIEND_REQ_UNREAD',
			afterSuccess: next,
		},
	};
};


export const getRelationshipData = (payload = {}, next, nextError) => {
	const { id, filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/relationships/${id}${applyURIFilter(filter)}`,
			beforeCallType: 'GET_RELATIONSHIP_DATA_REQUEST',
			successType: 'GET_RELATIONSHIP_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const getRelationshipList = (payload = {}, next, nextError) => {
	const { filter, firstLoad } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/relationships${applyURIFilter(filter)}`,
			beforeCallType: firstLoad ? 'GET_RELATIONSHIP_LIST_REQUEST' : '',
			successType: 'GET_RELATIONSHIP_LIST_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const deleteRelationship = (payload, next) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/relationships/' + id,
			params: id,
			opt: { method: 'DELETE' },
			successType: 'DELETE_RELATIONSHIP_SUCCESS',
			afterSuccess: next,
		},
	};
};
