/* --------------------------------------------------------
* Author Nguyễn Đăng Anh Thi
* Email nguyendanganhthi247@gmail.com
*------------------------------------------------------- */

import { SINGLE_API } from 'src/redux/actions/type';
// import { applyURIFilter } from 'src/utils';

export const createFollowQuestion = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/question-follows',
			params: payload,
			opt: { method: 'POST' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

export const deleteFollowQuestion = (payload, next, nextError) => {
	const { id } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: '/question-follows/' + id,
			params: id,
			opt: { method: 'DELETE' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

