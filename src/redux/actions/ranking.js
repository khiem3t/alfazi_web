/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-08 23:47:17
*------------------------------------------------------- */
// import AuthStorage from 'src/utils/AuthStorage';

import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

import { applyURIFilter } from 'src/utils';

export const getRankingList = (payload = {}, next, nextError) => {
	const { filter } = payload;

	return {
		type: SINGLE_API,
		payload: {
			uri: `/rankings${applyURIFilter(filter)}`,
			beforeCallType: 'GET_RANKING_DATA_REQUEST',
			successType: 'GET_RANKING_DATA_SUCCESS',
			afterSuccess: next,
			afterError: nextError,
		},
	};
};

