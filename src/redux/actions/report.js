import { SINGLE_API } from 'src/redux/actions/type';
// import AuthStorage from 'src/utils/AuthStorage';

export const createReport = (payload, next, nextError) => {
	return {
		type: SINGLE_API,
		payload: {
			uri: '/reports',
			params: payload,
			opt: { method: 'POST' },
			afterSuccess: next,
			afterError: nextError,
		},
	};
};
