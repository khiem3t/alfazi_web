/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-10 22:17:21
*------------------------------------------------------- */
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import cookie from 'react-cookies';
import NProgress from 'nprogress';

import Router from 'next/router';

import { Button } from 'antd';

import withReduxSaga from 'src/redux/store';
import withJss from 'src/theme/jss';

import AuthStorage from 'src/utils/AuthStorage';
import { getUserAuth } from 'src/redux/actions/auth';
import { addNotiListener, countNotiUnread } from 'src/redux/actions/notification';
import { countFriendReqUnread } from 'src/redux/actions/relationship';
import { countAchievementNotReceived } from 'src/redux/actions/achievement';

// import LoaderGlobal from 'src/components/LoaderGlobal';
import LoginModal from 'src/components/Modals/Login';
import SignUpModal from 'src/components/Modals/SignUp';
import BtnAddQuestion from 'src/components/Question/BtnAdd';

Router.onRouteChangeStart = (/* url */) => {
	NProgress.start();
};
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

const withRoot = (Child) => {
	@withJss
	@withReduxSaga
	class WrappedComponent extends PureComponent {
		static propTypes = {
			url: PropTypes.object.isRequired,
		}

		static getInitialProps(ctx) {
			if (!process.browser) {
				cookie.plugToRequest(ctx.req, ctx.res);
			}

			if (AuthStorage.loggedIn && !ctx.store.getState().auth.email) {
				ctx.store.dispatch(getUserAuth());
			}

			if (AuthStorage.loggedIn) {
				ctx.store.dispatch(countNotiUnread());
				ctx.store.dispatch(countFriendReqUnread());
				ctx.store.dispatch(countAchievementNotReceived());
			}

			if (Child.getInitialProps) {
				return Child.getInitialProps(ctx);
			}

			return {};
		}

		componentDidMount() {
			if (AuthStorage.loggedIn) {
				addNotiListener(this.props.dispatch); // eslint-disable-line
			}
		}

		render() {
			return (
				<Fragment>
					<Child {...this.props} />
					<LoginModal />
					<SignUpModal />
					<audio id="noti-sound">
						<track kind="captions" />
						<source src="/static/assets/media/noti-sound/light.ogg" type="audio/ogg" />
						<source src="/static/assets/media/noti-sound/light.mp3" type="audio/mpeg" />
					</audio>
					{
						AuthStorage.loggedIn &&
						<BtnAddQuestion
							node={
								<Button
									type="primary"
									shape="circle"
									icon="plus"
									size="large"
									style={{
										position: 'fixed',
										bottom: '15px',
										right: '15px',
										width: '50px',
										height: '50px',
										zIndex: '1',
									}}
									className="hidden-lg-up"
								/>
							}
						/>
					}
				</Fragment>
			);
		}
	}

	return WrappedComponent;
};

export default withRoot;
