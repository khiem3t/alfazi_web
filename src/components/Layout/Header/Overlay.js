/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-13 21:47:38
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

const styleSheet = (/* theme */) => ({
	root: {
		position: 'fixed',
		top: '0',
		bottom: '0',
		width: '100%',
		background: 'rgba(0, 0, 0, 0.46)',
		left: '0',
		right: 0,
		zIndex: '9999',
		transition: 'all .3s',
	},
	'@global body': {
		overflow: 'hidden',
	},
});

const Overlay = (props) => {
	const { classes, onClick } = props;

	return (
		<div className={classes.root} onClick={onClick} />
	);
};

Overlay.propTypes = {
	classes: PropTypes.object.isRequired,
	onClick: PropTypes.func,
};

Overlay.defaultProps = {
	onClick: f => f,
};

export default withStyles(styleSheet)(Overlay);
