/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-13 22:06:04
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import { Icon } from 'antd';

import FormSearch from 'src/components/Form/Search';

const styleSheet = (/* theme */) => ({
	root: {
		display: 'flex',
	},
	bar: {
		position: 'fixed',
		background: '#fff',
		height: '50px',
		top: '0',
		left: '50%',
		width: 0,
		transition: 'all .3s',
		zIndex: '1',
	},
	barInner: {
		display: 'none',
		alignItems: 'center',
	},
	close: {
		position: 'absolute',
		zIndex: '1',
		right: '0',
		background: '#fff',
		padding: '10px',
		display: 'flex',
		alignItems: 'center',
		height: '50px',
	},
	barOpen: {
		width: '100%',
		left: 0,
		'& $barInner': {
			display: 'flex',
		},
	},
	inner: {
		padding: '10px',
		display: 'flex',
		alignItems: 'center',
	},
	btn: {
		fontSize: '18px',
	},
	input: {
		height: '50px',
		flex: '1',
		'& input': {
			width: '100%',
			height: '50px',
			border: '0 !important',
			'&:hover': {
				background: '#fff',
			},
			'&:focus': {
				outline: 'none',
			},
		},
		'& .list-drop': {
			top: 51,
		},
	},
});

@withStyles(styleSheet)
export default class SearchMobile extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}

	static defaultProps = {}

	state = {
		open: false,
	}

	handleToggle = () => {
		this.setState({
			open: !this.state.open,
		});
	}

	render() {
		const { classes } = this.props;

		return (
			<div className={classes.root}>
				{
					!this.state.open &&
					<a className={classes.inner} onClick={this.handleToggle}>
						<Icon className={classes.btn} type="search" />
					</a>
				}
				<div className={classes.bar + ' ' + (this.state.open && classes.barOpen)}>
					<div className={classes.barInner}>
						<FormSearch className={classes.input} />
						<a className={classes.close} onClick={this.handleToggle}>
							<Icon className={classes.btn} type="close" />
						</a>
					</div>
				</div>
			</div>
		);
	}
}
