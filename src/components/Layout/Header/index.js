/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-09 17:44:33
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Link from 'next/link';

import withStyles from 'src/theme/jss/withStyles';

import Container from 'src/components/Layout/Container';
import FormSearch from 'src/components/Form/Search';

import Menu from './Menu';
import HeaderMobile from './Mobile';

const styles = (theme) => ({
	root: {
		height: '50px',
	},
	wrapper: {
		position: 'fixed',
		width: '100%',
		height: '50px',
		zIndex: '9',
		background: '#fff',
		boxShadow: '1px 1px 3px rgba(0, 0, 0, 0.23)',
	},
	content: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '0 15px',
		[theme.breakpoints.up.md]: {
			padding: 0,
		},
	},
	left: {
		display: 'flex',
		alignItems: 'center',
	},
	right: {
		display: 'flex',
		alignContent: 'center',
	},
	logoImg: {
		height: 50,
	},
});

@withStyles(styles)
export default class Header extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}

	render() {
		const { classes } = this.props;

		return (
			<header className={classes.root}>
				<div className={classes.wrapper}>
					<Container className={classes.content + ' hidden-md-up'}>
						<HeaderMobile />
					</Container>
					<Container className={classes.content + ' hidden-sm-down'}>
						<div className={classes.left}>
							<Link href="/">
								<a>
									<img className={classes.logoImg} src="/static/assets/images/logo/64x64.png" alt="alfazi" />
								</a>
							</Link>
							<div style={{ paddingLeft: 40 }}>
								<FormSearch />
							</div>
						</div>
						<div className={classes.right}>
							<Menu />
						</div>
					</Container>
				</div>
			</header>
		);
	}
}
