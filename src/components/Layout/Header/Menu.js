/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-13 14:39:16
*------------------------------------------------------- */

import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Link from 'next/link';

import { Badge, Icon } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import NotiWidget from 'src/components/Notification/NotiWidget';
import FriendReqWidget from 'src/components/Notification/FriendReqWidget';

import { toggleLoginModal } from 'src/redux/actions/modal';

import AvatarBtn from './AvatarBtn';

const styleSheet = (theme) => ({
	root: {
		display: 'flex',
		alignItems: 'center',
	},
	menu: {
		'& ul': {
			display: 'flex',
			padding: '0',
			listStyle: 'none',
			margin: 0,
			marginRight: '20px',
			'& li': {
				'& a': {
					color: theme.palette.primary[900],
					padding: '0 15px',
					display: 'block',
					textDecoration: 'none',
					textTransform: 'uppercase',
					fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
					fontWeight: '500',
					fontSize: '0.85rem',
					height: '45px',
					lineHeight: '48px',
				},
			},
		},
	},
	button: {
		fontSize: '1.4rem',
		// color: Color(theme.palette.primary.main).darken(0.5).hex(),
		color: '#fff',
	},
	btnAdd: {
		margin: '0 15px 0 10px',
		textTransform: 'uppercase',
		fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
		fontWeight: '500',
	},
	inner: {
		padding: '10px',
		marginRight: 5,
	},
	btnIcon: {
		border: '0',
		fontSize: '20px',
		color: theme.palette.primary[900],
		cursor: 'pointer',
	},
	badge: {
		'& sup': {
			height: '14px',
			fontSize: '9px',
			minWidth: '0',
			lineHeight: '12px',
			borderRadius: '2px',
			padding: '1px 3px',
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			auth: state.auth,
			countAchNotReceived: state.achievement.countNotReceived,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			toggleLoginModal,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class Menu extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
			countAchNotReceived: PropTypes.number.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			toggleLoginModal: PropTypes.func.isRequired,
		}).isRequired,
	}

	handleOpenLogin = (e) => {
		e.preventDefault();
		this.props.action.toggleLoginModal({ open: true });
	}

	render() {
		const { classes, store: { auth, countAchNotReceived } } = this.props;

		return (
			<div className={classes.root}>
				<nav className={classes.menu}>
					<ul>
						<li>
							<Link href="/">
								<a>Home</a>
							</Link>
						</li>
						<li>
							<Link href="/blog">
								<a>Blog</a>
							</Link>
						</li>
						{
							!AuthStorage.loggedIn && !auth.id &&
							<li>
								<a href="/login" onClick={this.handleOpenLogin}>Đăng nhập</a>
							</li>
						}
					</ul>
				</nav>
				{/* <BtnAddQuestion node={<Button className={classes.btnAdd} type="primary" ghost>Tạo câu hỏi</Button>} /> */}
				{
					AuthStorage.loggedIn &&
					<Fragment>
						<div className={classes.inner}>
							<Badge className={classes.badge} count={countAchNotReceived}>
								<Link href="/achievement">
									<a>
										<Icon type="trophy" className={classes.btnIcon} />
									</a>
								</Link>
							</Badge>
						</div>
						<FriendReqWidget />
						<NotiWidget />
						<AvatarBtn />
					</Fragment>
				}
			</div>
		);
	}
}
