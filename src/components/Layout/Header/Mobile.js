/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-13 14:53:10
*------------------------------------------------------- */

import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import { Link, Router } from 'src/routes';

import { Icon } from 'antd';

import AuthStorage from 'src/utils/AuthStorage';

import NotiWidget from 'src/components/Notification/NotiWidget';
import FriendReqWidget from 'src/components/Notification/FriendReqWidget';
import Avatar from 'src/components/Stuff/Avatar';

import { logoutRequest } from 'src/redux/actions/auth';

import Overlay from './Overlay';
import SearchMobile from './Search';

const styleSheet = (theme) => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
		width: '100%',
		height: '50px',
	},
	btn: {
		fontSize: '18px',
	},
	logoImg: {
		height: '30px',
	},
	menu: {
		display: 'flex',
		alignContent: 'center',
		justifyContent: 'center',
	},
	inner: {
		padding: '10px',
		display: 'flex',
		alignItems: 'center',
	},
	sidebarInner: {
		position: 'fixed',
		top: '0',
		bottom: '0',
		left: '-800px',
		zIndex: '99999999',
		transition: 'all .3s',
		width: 250,
		height: '100%',
		background: '#fff',
	},
	sidebarOpen: {
		left: '0 !important',
	},
	sidebarHeader: {
		background: '#fff',
		position: 'relative',
		height: '200px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	cover: {
		backgroundColor: theme.palette.primary[900] + ' !important',
		height: '200px',
		borderTopLeftRadius: 'inherit',
		borderTopRightRadius: 'inherit',
		backgroundPosition: '0 50%',
		width: '100%',
		padding: '0',
		marginTop: '-1px',
		backgroundImage: 'url("/static/assets/images/cover/cover-default.jpg")',
		backgroundSize: 'cover !important',
		position: 'absolute',
		'&:before': {
			content: '""',
			position: 'absolute',
			background: 'rgba(20, 97, 89, 0.7)',
			top: '0',
			right: '0',
			left: '0',
			bottom: '0',
			width: '100%',
			height: '100%',
		},
	},
	body: {
		width: '100%',
	},
	avatar: {
		verticalAlign: 'bottom',
		zIndex: '1',
		position: 'relative',
		color: '#fff',
		textAlign: 'center',
		'& span': {
			color: '#fff',
			fontSize: '12px',
			// fontStyle: 'italic',
		},
		'& h3': {
			fontSize: '16px',
			color: '#fff',
			margin: '0',
			lineHeight: '1',
			marginTop: '5px',
			'& a': {
				color: '#fff',
			},
		},
	},
	stats: {
		display: 'flex',
		zIndex: '1',
		marginTop: '20px',
		position: 'relative',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	statsItem: {
		color: '#fff',
		display: 'flex',
		fontSize: '20px',
		alignItems: 'center',
		flex: '1',
		textAlign: 'center',
		justifyContent: 'center',
		'& img': {
			width: 20,
			marginLeft: 5,
		},
	},
	sidebarMenu: {
		overflowY: 'scroll',
		padding: '5px 0',
		height: 'calc(100vh - 200px)',
		'& ul': {
			padding: 0,
			margin: 0,
			'& li': {
				padding: 0,
				'& a': {
					display: 'flex',
					alignItems: 'center',
					padding: '10px 20px',
					'& i': {
						marginRight: 10,
					},
				},
			},
		},
	},
	badge: {
		flex: 1,
		textAlign: 'right',
		lineHeight: 1,
		'& span': {
			color: '#fff',
			textAlign: 'center',
			boxShadow: '0 0 0 1px #fff',
			background: '#f5222d',
			whiteSpace: 'nowrap',
			height: '14px',
			padding: '1px 3px',
			fontSize: '9px',
			minWidth: '0',
			lineHeight: '12px',
			borderRadius: '2px',
		},
	},
	line: {
		borderTop: '1px solid #e8e8e8',
		marginTop: '10px',
		paddingTop: '20px !important',
	},
	notLoginControl: {
		paddingTop: '20px',
		'& a': {
			color: '#fff',
		},
		'& span': {
			margin: '0 10px',
			fontStyle: 'initial',
		},
	},
	right: {
		display: 'flex',
		alignItems: 'center',
	},
});


function mapStateToProps(state) {
	return {
		store: {
			auth: state.auth,
			countAchNotReceived: state.achievement.countNotReceived,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			logoutRequest,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class HeaderMobile extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
			countAchNotReceived: PropTypes.number.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			logoutRequest: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		openSidebar: false,
	}

	handleToggleSidebar = () => {
		this.setState({
			openSidebar: !this.state.openSidebar,
		});
	}

	handleLogout = () => {
		this.props.action.logoutRequest(() => {
			Router.pushRoute('/');
		});
	}

	render() {
		const { classes, store: { auth, countAchNotReceived } } = this.props;

		return (
			<div className={classes.root}>
				<div className={classes.right}>
					<a onClick={this.handleToggleSidebar} style={{ lineHeight: 1, marginRight: 20 }}>
						<Icon className={classes.btn} type="menu-unfold" />
					</a>
					<Link href="/">
						<a>
							<img className={classes.logoImg} src="/static/assets/images/logo/64x64.png" alt="alfazi" />
						</a>
					</Link>
				</div>
				<div className={classes.menu}>
					<SearchMobile />
					{
						AuthStorage.loggedIn &&
						<Fragment>
							<FriendReqWidget style={{ margin: 0 }} />
							<NotiWidget style={{ margin: 0 }} />
						</Fragment>
					}
				</div>
				{
					this.state.openSidebar &&
					<Overlay onClick={this.handleToggleSidebar} />
				}
				<div className={classes.sidebarInner + ' ' + (this.state.openSidebar && classes.sidebarOpen)}>
					<div className={classes.sidebarHeader}>
						<div className={classes.cover} style={{ backgroundImage: `url("${auth.cover ? auth.cover : '/static/assets/images/cover/cover-default.jpg'}")` }} />
						<div className={classes.body}>
							<div className={classes.avatar}>
								<Avatar src={auth.avatar} name={auth.fullName} size={60} />
								{
									AuthStorage.loggedIn ?
										<Fragment>
											<h3>{auth.fullName}</h3>
											<span>{auth.level === 0 ? 'Thành viên mới' : 'Thành viên cấp ' + auth.level}</span>
										</Fragment> :
										<div className={classes.notLoginControl}>
											<Link route="/login">
												<a>
													Đăng nhập
												</a>
											</Link>
											<span>|</span>
											<Link route="/sign-up">
												<a>
													Đăng ký
												</a>
											</Link>
										</div>
								}

							</div>
							{
								AuthStorage.loggedIn &&
								<div className={classes.stats}>
									<div className={classes.statsItem}>
										{auth.coinsCount || 0}
										<img src="/static/assets/images/coins.png" alt="coin" />
									</div>
									<div className={classes.statsItem}>
										{auth.pointsCount || 0}
										<img src="/static/assets/images/point.png" alt="coin" />
									</div>
									<div className={classes.statsItem}>
										{auth.questionsCount || 0}
										<img src="/static/assets/images/question.png" alt="coin" />
									</div>
								</div>
							}

						</div>
					</div>
					<div className={classes.sidebarMenu}>
						<ul>
							<li>
								<Link route="/">
									<a>
										<Icon type="home" /> Home
									</a>
								</Link>
							</li>
							<li>
								<Link route="/blog">
									<a>
										<Icon type="tags-o" /> Blog
									</a>
								</Link>
							</li>
							<li>
								<Link route="/ranking">
									<a>
										<Icon type="area-chart" /> Bảng xếp hạng
									</a>
								</Link>
							</li>
							<li>
								<Link route="/question-top">
									<a>
										<Icon type="bar-chart" /> Top câu hỏi
									</a>
								</Link>
							</li>
							{
								AuthStorage.loggedIn &&
								<li>
									<Link route={'/profile/' + auth.id}>
										<a>
											<Icon type="idcard" /> Hồ sơ cá nhân
										</a>
									</Link>
								</li>
							}
							{
								AuthStorage.loggedIn &&
								<li>
									<Link route="/achievement">
										<a>
											<Icon type="trophy" /> Phần thưởng
											{
												countAchNotReceived > 0 &&
												<div className={classes.badge}>
													<span>{countAchNotReceived || 0}</span>
												</div>
											}
										</a>
									</Link>
								</li>
							}
							{
								AuthStorage.loggedIn &&
								<li>
									<Link route="/history-coins">
										<a>
											<Icon type="line-chart" /> Lịch sử dùng xu
										</a>
									</Link>
								</li>
							}
							<li>
								<Link route="/blog/post/lam-the-nao-de-tang-tai-khoan-xu">
									<a>
										<Icon type="shopping-cart" /> Cách nhận xu
									</a>
								</Link>
							</li>
							{
								AuthStorage.loggedIn && auth.loginType === 'email' &&
								<li>
									<Link route="/change-password">
										<a>
											<Icon type="setting" /> Đổi mật khẩu
										</a>
									</Link>
								</li>
							}
							{
								AuthStorage.loggedIn &&
								<li>
									<a className={classes.line} onClick={this.handleLogout}>
										<Icon type="logout" /> Đăng xuất
									</a>
								</li>
							}
						</ul>
					</div>
				</div>
			</div>
		);
	}
}
