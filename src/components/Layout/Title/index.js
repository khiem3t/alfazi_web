/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-27 08:50:56
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Head from 'next/head';

function mapStateToProps(state) {
	return {
		store: {
			countUnread: state.notification.countUnread,
			newQuestion: state.question.newQuestion,
		},
	};
}

const mapDispatchToProps = (/* dispatch */) => {
	return {
		// action: bindActionCreators({
		// toggleLoginModal,
		// }, dispatch),
	};
};

const Title = (props) => {
	const { children, store: { countUnread, newQuestion } } = props;

	let count = 0;

	if (countUnread > 0) {
		count = countUnread;
	}

	if (newQuestion > 0) {
		count = newQuestion;
	}

	return (
		<Head>
			<title>{count > 0 ? `(${count}) ` : ''}{children && children + ' | '}Alfazi</title>
		</Head>
	);
};

Title.propTypes = {
	children: PropTypes.string,
	// store
	store: PropTypes.shape({
		countUnread: PropTypes.number.isRequired,
		newQuestion: PropTypes.number.isRequired,
	}).isRequired,
	// // action
	// action: PropTypes.shape({
	// 	toggleLoginModal: PropTypes.func.isRequired,
	// }).isRequired,
};

Title.defaultProps = {
	children: '',
};

export default connect(mapStateToProps, mapDispatchToProps)(Title);
