/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-12 12:45:09
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import Link from 'next/link';

import withStyles from 'src/theme/jss/withStyles';

import { Row, Col } from 'antd';

import Container from 'src/components/Layout/Container';

import FaFacebookSquare from 'react-icons/lib/fa/facebook-square';
import FaGooglePlusSquare from 'react-icons/lib/fa/google-plus-square';
import FaYoutubeSquare from 'react-icons/lib/fa/youtube-square';

const styles = (/* theme */) => ({
	root: {
		color: '#fff',
		padding: '40px 0 0 !important',
		marginTop: '24px',
		background: '#333c44',
		boxShadow: '0px -1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px -3px 1px -2px rgba(0, 0, 0, 0.12)',
	},
	copyright: {
		textAlign: 'center',
		fontSize: '12px',
		marginTop: '40px',
		padding: '15px 0px',
		borderTop: '1px solid rgba(0, 0, 0, 0.27)',
	},
	title: {
		color: '#fff',
		fontSize: '20px',
		marginBottom: '20px',
		marginTop: '20px',
	},
	list: {
		listStyle: 'none',
		padding: '0',
		margin: '0',
		'& li': {
			padding: '7px 0',
			'& a': {
				color: 'rgba(255, 255, 255, 0.8)',
				'& svg': {
					fontSize: '26px',
					marginRight: '10px',
				},
				'&:hover': {
					color: 'rgba(255, 255, 255, 0.3)',
				},
			},
		},
	},
});

const Footer = (props) => {
	const { classes = {} } = props;

	return (
		<footer className={classes.root + ' text-center-xs-down'}>
			<Container>
				<Row type="flex">
					<Col sm={6} xs={24}>
						<h4 className={classes.title}>Bản di động</h4>
						<div style={{ marginBottom: 10, paddingTop: 7 }}>
							<img src="/static/assets/images/appstore.svg" alt="appstore" />
						</div>
						<div>
							<img src="/static/assets/images/google-play.svg" alt="google-play" />
						</div>
					</Col>
					<Col sm={6} xs={24}>
						<h4 className={classes.title}>Liên kết</h4>
						<ul className={classes.list}>
							<li>
								<a href="https://www.facebook.com/alfaziapp" target="_blank" rel="noopener noreferrer">
									<FaFacebookSquare />
									Facebook
								</a>
							</li>
							<li>
								<a href="https://plus.google.com/+Alfazi " target="_blank" rel="noopener noreferrer">
									<FaGooglePlusSquare />
									Google+
								</a>
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCxEcRXCxjSIRjO2DKbsPB8g" target="_blank" rel="noopener noreferrer">
									<FaYoutubeSquare />
									Youtobe
								</a>
							</li>
						</ul>
					</Col>
					<Col sm={6} xs={24}>
						<h4 className={classes.title}>Hỗ trợ khách hàng</h4>
						<ul className={classes.list}>

							<li>
								<Link href="/privacy-policy">
									<a>
										Chính sách bảo mật
									</a>
								</Link>
							</li>
							<li>
								<Link href="/term-of-use">
									<a>
										Điều khoản sử dụng
									</a>
								</Link>
							</li>
							<li>
								<a href="https://tawk.to/chat/5aa4160f4b401e45400d99fd/default/?$_tawk_popout=true&$_tawk_sk=5aa806b4f44b912eb818a625&$_tawk_tk=0d667bc01d42e9f90ca6a57d5fa0148a&v=573" target="_blank" rel="noopener noreferrer">
									Liên hệ hỗ trợ
								</a>
							</li>
						</ul>
					</Col>
					<Col sm={6} xs={24}>
						<h4 className={classes.title}>Thông tin</h4>
						<ul className={classes.list}>
							<li>
								<Link href="/intro">
									<a>Giới thiệu</a>
								</Link>
							</li>
							<li>
								<a href="/blog/post/noi-quy-dien-dan-cong-dong-h-i-dap-alfazi" target="_blank" rel="noopener noreferrer">
									Nội quy
								</a>
							</li>
							<li>
								<a href="/blog" target="_blank" rel="noopener noreferrer">
									Blog
								</a>
							</li>
						</ul>
					</Col>
				</Row>
			</Container>
			<div className={classes.copyright}>
				Copyright ©  <strong>Alfazi</strong> 2018
			</div>
		</footer>
	);
};

Footer.propTypes = {
	classes: PropTypes.object.isRequired,
};

Footer.defaultProps = {
	// classes: {},
};

export default withStyles(styles)(Footer);
