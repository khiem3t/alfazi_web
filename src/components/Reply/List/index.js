/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-04 15:45:43
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import ReplyItem from 'src/components/Reply/Item';

import { getReplyList } from 'src/redux/actions/reply';

const styleSheet = (/* theme */) => ({
	root: {
		padding: '10px 0',
	},
});

function mapStateToProps(/* state */) {
	return {
		// store: {
		// 	replyList: state.reply.list,
		// },
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getReplyList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class ReplyList extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		answerData: PropTypes.object.isRequired,
		// store
		// store: PropTypes.shape({
		// 	replyList: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			getReplyList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		loading: true,
	}

	componentDidMount() {
		// if (this.props.answerData.repliesCount > 0) {
		this.props.action.getReplyList({
			filter: this.filter,
			firstLoad: true,
		}, () => {
			this.setState({
				loading: false,
			});
		});
		// }
	}

	filter = {
		limit: 1000,
		skip: 0,
		page: 1,
		order: 'createdAt ASC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
		],
		counts: ['likes', 'replies'],
		where: {
			status: 'active',
			answerId: this.props.answerData.id,
		},
	}

	render() {
		const { classes, answerData: { replies = [], repliesCount = 0 } } = this.props;
		if (repliesCount === 0) {
			return null;
		}
		return (
			<div className={classes.root}>
				{
					this.state.loading ?
						[0, 0].map((item, i) => {
							return <ReplyItem key={i} loading />;
						}) :
						replies.map((item) => {
							return <ReplyItem key={item.id} replyData={item} />;
						})
				}
			</div>
		);
	}
}
