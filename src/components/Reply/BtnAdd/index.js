/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 16:51:49
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Input, Icon, Tabs, Button } from 'antd';
import Latex from 'react-latex';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import Avatar from 'src/components/Stuff/Avatar';
import CheckLogin from 'src/components/Form/CheckLogin';
import Cropper from 'src/components/Stuff/ImageCropper';
import LightBox from 'src/components/LightBox/ImageLightBoxContainer';
import MathEditor from 'src/components/Answer/MathEditor';

import FaSubscript from 'react-icons/lib/fa/subscript';
// import FaPaperPlane from 'react-icons/lib/fa/paper-plane';

import { createReply } from 'src/redux/actions/reply';
import { uploadFiles } from 'src/redux/actions/upload';

const styleSheet = (/* theme */) => ({
	root: {
		margin: '15px 0 5px',
		padding: '15px',
		backgroundColor: '#f5f5f5',
		// borderRadius: '5px',
		minHeight: '162px',
	},

	textBox: {
		padding: '4px 10px !important',
		minHeight: 'auto  !important',
		flex: '1',
		cursor: 'text',
	},
	tabs: {
		width: '100%',
	},
	avatar: {
		padding: '20px',
	},
	form: {
		flex: 1,
		display: 'flex',
		'& $textArea': {
			padding: '8px 10px',
			minHeight: 40,
			flex: 1,
		},
	},
	btnAction: {
		overflow: 'hidden',
		padding: '5px 0',
		fontSize: '20px',
	},
	submit: {
		display: 'inline-block',
		float: 'right',
	},
	action: {
		position: 'relative',
		display: 'flex',
		justifyContent: 'space-between',
		marginLeft: '15px',
		'& .ant-upload-list': {
			top: '0',
			// width: '100%',
			// position: 'relative',
		},
	},
	itemImg: {
		marginTop: '15px',
		position: 'relative',
		display: 'inline-block',
		paddingLeft: 50,
		'& img': {
			height: 100,
		},
	},
	removeImg: {
		position: 'absolute',
		top: 0,
		color: '#e05e5e',
		fontSize: '20px',
		right: '5px',
	},
	btnList: {
		display: 'flex',
		alignItems: 'center',
		'& a': {
			display: 'block',
			marginRight: '15px',
			fontSize: '22px',
			'& svg': {
				verticalAlign: '0 !important',
			},
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			auth: state.auth,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			createReply,
			uploadFiles,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnAddReply extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		answerData: PropTypes.object.isRequired,
		onRefresh: PropTypes.func,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			createReply: PropTypes.func.isRequired,
			uploadFiles: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		onRefresh: f => f,
	}

	state = {
		file: null,
		urlPreview: '',
		content: '',
		loading: false,
		mathEditorVisible: false,
		editor1: 'editor1',
		showCroper: false,
		showLightBox: false,
	}

	onChangeTab = () => {
		const { content } = this.state;
		this.setState({ mathcontent: content });
	}

	handleFocus = (e) => {
		const { editor1 } = this.state;
		e.target.setAttribute('id', editor1);
	}

	handleRemoveFile = () => {
		this.setState({
			urlPreview: '',
			file: null,
			showLightBox: false,
		});
	}

	handleChangeFile = (event) => {
		event.preventDefault();
		const reader = new FileReader(); // eslint-disable-line
		const file = event.target.files[0];
		reader.onloadend = () => {
			this.setState({
				showCroper: true,
				urlPreview: reader.result,
				file,
			});
		};
		reader.readAsDataURL(file);
	}

	handleChangeText = (e) => {
		this.setState({
			content: e.target.value,
		});
	}

	showMathEditorModal = () => {
		const { mathEditorVisible } = this.state;
		this.setState({
			mathEditorVisible: !mathEditorVisible,
		});
	}
	handleMathEditorModalSubmit = () => {
		this.setState({
			mathEditorVisible: false,
		});
	}
	handleMathEditorModalCancel = () => {
		this.setState({
			mathEditorVisible: false,
		});
	}

	updateContentWithMathExpressions = (expression) => {
		this.setState(prevState => {
			return {
				content: prevState.content + expression,
			};
		});
	}

	insertContent = (content) => {
		this.setState(prevState => {
			return {
				content: prevState.content + (prevState.content ? '\n' : '') + content,
			};
		});
	}

	handleSubmit = () => {
		if (this.props.answerData.questionId) {
			const { file, content } = this.state;

			if (file || content) {
				this.setState({
					loading: true,
				});

				this.props.action.uploadFiles({ files: [{ status: 'error', originFileObj: file }] }, (imageList = []) => {
					this.props.action.createReply({
						content,
						image: imageList[0],
						creatorId: AuthStorage.userId,
						questionId: this.props.answerData.questionId,
						receiverId: this.props.answerData.creatorId,
						answerId: this.props.answerData.id,
					}, () => {
						this.setState({
							loading: false,
							file: null,
							content: '',
							urlPreview: '',
							editor1: '',
						});
						this.props.onRefresh();
					});
				});
			}
		}
	}

	handleCrop = (promise) => {
		promise.then(file => {
			const reader = new FileReader(); // eslint-disable-line
			reader.onload = () => {
				this.setState({ file, urlPreview: reader.result, showCroper: false, showLightBox: true });
			};

			reader.readAsDataURL(file);
		});
	}

	handleCropDefault = () => {
		this.setState({ showCroper: false, showLightBox: true });
	}

	handelCancel = () => {
		this.setState({ mathEditorVisible: false });
	}


	render() {
		const { classes, store: { auth }, answerData } = this.props;

		const { mathEditorVisible } = this.state;

		return (
			<div className={classes.root}>
				<div className={classes.form}>
					{
						auth.id &&
						<Avatar className={classes.avatar} size={30} src={auth.avatar} name={auth.fullName} style={{ marginRight: 10 }} />
					}
					<Tabs className={classes.tabs} onChange={this.onChangeTab}>
						<Tabs.TabPane tab="Chỉnh sửa" key="1">
							<Input.TextArea
								id="text-result"
								autosize
								autoFocus
								placeholder="Thêm câu trả lời"
								onFocus={this.handleFocus}
								onChange={this.handleChangeText}
								value={this.state.content}
								className={classes.textBox}
							/>
							<div className={classes.btnAction}>
								<a onClick={() => this.showMathEditorModal()}>
									<FaSubscript />
								</a>
								<div className={classes.submit}>
									<CheckLogin onClick={this.handleSubmit}>
										<a>
											{
												this.state.loading ?
													<Icon type="loading" style={{ fontSize: '18px' }} /> :
													<Button size="small">Trả lời</Button>
											}
										</a>
									</CheckLogin>
								</div>
							</div>

						</Tabs.TabPane>
						<Tabs.TabPane tab="Xem trước" key="2">
							<div className=" pre-wrap">
								<Latex >
									{this.state.mathcontent}
								</Latex>
							</div>
						</Tabs.TabPane>
					</Tabs>

					{	this.state.showCroper &&
						ReactDOM.createPortal(
							<Cropper
								src={this.state.urlPreview}
								onCrop={this.handleCrop}
								onDefault={this.handleCropDefault}
								name={this.state.file.name}
								type={this.state.file.type}
							/>,
							document.getElementById('question-cropper'),
						)
					}

				</div>
				{mathEditorVisible ? (
					<div>
						<MathEditor
							style={{ marginTop: 10 }}
							visible={this.state.mathEditorVisible}
							onOk={this.handleMathEditorModalSubmit}
							onCancel={this.handelCancel}
							onChange={this.handleChangeText}
							onSubmit={this.insertContent}
							updateContentWithMathExpressions={this.updateContentWithMathExpressions}
						/>
					</div>
				) : ''}
				{
					this.state.showLightBox && this.state.urlPreview &&
					<div className={classes.itemImg}>
						<a className={classes.removeImg} onClick={this.handleRemoveFile}>
							<Icon type="close-circle" />
						</a>
						<LightBox img={this.state.urlPreview} alt="preview" />
					</div>
				}
			</div>
		);
	}
}
