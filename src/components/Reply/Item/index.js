/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-04 15:41:06
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import AvatarBlock from 'src/components/User/AvatarBlock';
import Latex from 'react-latex';
import ImageLightBox from 'src/components/LightBox/ImageLightBoxContainer';
import BtnSetting from 'src/components/Reply/BtnSetting';

const styleSheet = (/* theme */) => ({
	root: {
		margin: '10px 0',
		position: 'relative',
		'&:hover $btnSetting': {
			opacity: 1,
		},
	},
	top: {
		display: 'flex',
		alignItems: 'flex-start',
	},
	btnSetting: {
		opacity: 0,
		transition: '.3s opacity',
	},
	itemContent: {
		marginLeft: '40px',
		flex: 1,
	},
	itemText: {
		marginTop: 5,
	},
	itemImg: {
		marginTop: '20px',
		'& img': {
			height: 100,
			maxWidth: '100%',
		},
	},
	avatar: {
		'& h5 > a': {
			fontSize: '13px',
			lineHeight: '1',
		},
		'& a': {
			fontSize: '7px',
			lineHeight: '1',
		},
		'& .info': {
			lineHeight: '1',
		},
	},
});

const ReplyItem = (props) => {
	const { classes, replyData = {}, loading } = props;
	const { creator } = replyData;

	if (loading) {
		return (
			<div className={classes.root} style={{ display: 'flex' }}>
				<div className="loading-block" style={{ width: 30, height: 30, borderRadius: 50 }} />
				<div className={classes.itemContent} style={{ marginLeft: '5px' }}>
					<div>
						<div className="loading-block" style={{ width: 200 }} />
					</div>
					<div className={classes.itemText}>
						<div className="loading-block" />
						<div className="loading-block" style={{ width: '50%' }} />
					</div>
				</div>
			</div>
		);
	}

	return (
		<div className={classes.root}>
			<div className={classes.top}>
				<AvatarBlock
					userData={creator}
					date={replyData.createdAt}
					size={30}
					className={classes.avatar}
				/>
				<BtnSetting
					className={classes.btnSetting}
					replyData={replyData}
				/>
			</div>
			<div className={classes.itemContent}>

				<div className={classes.itemText + ' pre-wrap'}>
					<Latex>
						{replyData.content}
					</Latex>

				</div>
				{
					replyData.image &&
					<div className={classes.itemImg}>
						<ImageLightBox img={replyData.image} alt={replyData.content} />
					</div>
				}
			</div>
		</div>
	);
};

ReplyItem.propTypes = {
	classes: PropTypes.object.isRequired,
	replyData: PropTypes.object,
	loading: PropTypes.bool,
};

ReplyItem.defaultProps = {
	replyData: {},
	loading: false,
};

export default withStyles(styleSheet)(ReplyItem);
