/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-02 09:32:05
*------------------------------------------------------- */

import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import { Button, Menu, Dropdown, Spin, Icon, notification, Modal } from 'antd';

import MdKeyboardControl from 'react-icons/lib/md/keyboard-control';
import FaFlag from 'react-icons/lib/fa/flag';

import ModalEdit from 'src/components/Reply/ModalEdit';
import ModalReport from 'src/components/Modals/Report';

import { updateReply, deleteReply } from 'src/redux/actions/reply';

const styleSheet = (/* theme */) => ({
	root: {
		height: 32,
	},
	btn: {
		background: 'transparent',
		border: 0,
		fontSize: 20,
		'& svg': {
			marginTop: '-5px',
		},
		'&:hover, &:active': {
			background: 'transparent',
		},
	},
	icon: {
		marginRight: 15,
	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			updateReply,
			deleteReply,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnSetting extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		replyData: PropTypes.object.isRequired,
		node: PropTypes.node,
		className: PropTypes.string,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			updateReply: PropTypes.func.isRequired,
			deleteReply: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		node: undefined,
		className: '',
	}

	state = {
		loading: false,
		openModalEdit: false,
		openModalReport: false,
	}

	handleDelete = () => {
		Modal.confirm({
			title: 'Bạn có chắc chắn muốn xóa câu trả lời này không?',
			onOk: () => {
				this.setState({
					loading: true,
				});
				this.props.action.deleteReply({
					id: this.props.replyData.id,
				}, () => {
					this.setState({
						loading: false,
					}, () => {
						notification.success({
							message: 'Chúc mừng!',
							description: 'Bạn đã xóa phản hồi thành công.',
						});
					});
				}, () => {
					this.setState({
						loading: false,
					});
				});
			},
		});
	}

	handleSelectMenu = ({ key }) => {
		if (key === 'edit') {
			this.setState({
				openModalEdit: true,
			});
		}
		if (key === 'report') {
			this.setState({
				openModalReport: true,
			});
		}
		if (key === 'delete') {
			this.handleDelete();
		}
	}

	handleCloseModal = () => {
		this.setState({
			openModalEdit: false,
			openModalReport: false,
		});
	}

	render() {
		const { classes, node, replyData = {}, className } = this.props;

		const menu = (
			<Menu onClick={this.handleSelectMenu}>
				{
					AuthStorage.userId !== replyData.creatorId &&
					<Menu.Item key="report">
						<FaFlag className={classes.icon} /> Báo cáo vi phạm
					</Menu.Item>
				}
				{
					AuthStorage.userId === replyData.creatorId && replyData.status === 'active' &&
					<Menu.Item key="edit">
						<Icon type="edit" className={classes.icon} /> Chỉnh sửa câu trả lời
					</Menu.Item>
				}
				{
					AuthStorage.userId === replyData.creatorId && replyData.status === 'active' &&
					<Menu.Item key="delete">
						<Icon type="delete" className={classes.icon} /> Xóa câu trả lời
					</Menu.Item>
				}
			</Menu>
		);

		return (
			<Fragment>
				<Dropdown overlay={menu} trigger={['click']} className={classes.root}>
					{
						this.state.loading ?
							<Spin /> :
							<a className={className + ' ant-dropdown-link'}>
								{
									node ||
									<Button shape="circle" className={classes.btn}>
										<MdKeyboardControl />
									</Button>
								}
							</a>
					}
				</Dropdown>
				{
					this.state.openModalEdit &&
					<ModalEdit
						visible={this.state.openModalEdit}
						onCancel={this.handleCloseModal}
						destroyOnClose
						replyData={replyData}
					/>
				}
				{
					this.state.openModalReport &&
					<ModalReport
						visible={this.state.openModalReport}
						onCancel={this.handleCloseModal}
						destroyOnClose
						type="reply"
						replyData={replyData}
					/>
				}
			</Fragment>
		);
	}
}
