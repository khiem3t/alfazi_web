/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-26 16:17:01
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import { Tooltip } from 'antd';

const styleSheet = (/* theme */) => ({
	root: {
		width: 30,
		height: 30,
		cursor: 'pointer',
		margin: 2,
		'& img': {
			width: '100%',
			height: '100%',
		},
	},
});

const BadgeItem = (props) => {
	const { classes, badgeData: { badge = {} } } = props;

	return (
		<Tooltip title={badge.name}>
			<div className={classes.root}>
				<img src={badge.image} alt={badge.name} />
			</div>
		</Tooltip>
	);
};

BadgeItem.propTypes = {
	classes: PropTypes.object.isRequired,
	badgeData: PropTypes.object,
};

BadgeItem.defaultProps = {
	badgeData: {},
};

export default withStyles(styleSheet)(BadgeItem);
