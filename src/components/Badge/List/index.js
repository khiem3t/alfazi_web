/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-26 16:18:21
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import Item from 'src/components/Badge/Item';

const styleSheet = (/* theme */) => ({
	root: {
		marginTop: 20,
		'& h4': {
			marginBottom: 10,
		},
	},
	content: {
		display: 'flex',
		flexWrap: 'wrap',
	},
});

const BadgeList = (props) => {
	const { classes, userData: { badgesUnlock = [] } } = props;

	return (
		<div className={classes.root}>
			<h4>Huy hiệu:</h4>
			{
				badgesUnlock.length === 0 && <div className="text-center">Chưa có huy hiệu nào</div>
			}

			<div className={classes.content}>
				{
					badgesUnlock.map((badge) => {
						return <Item key={badge.id} badgeData={badge} />;
					})
				}
			</div>
		</div>
	);
};

BadgeList.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
};

BadgeList.defaultProps = {
	userData: {},
};

export default withStyles(styleSheet)(BadgeList);
