/* --------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 *
 * Created: 2018-01-12 15:38:42
 *------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FacebookLogin from 'react-facebook-login';
import { bindActionCreators } from 'redux';

import { notification } from 'antd';

import Router from 'next/router';

import withStyles from 'src/theme/jss/withStyles';

import FacebookIcon from 'react-icons/lib/fa/facebook-official';

import AuthStorage from 'src/utils/AuthStorage';

import { loginFacebook } from 'src/redux/actions/auth';
import { toggleSignUpModal, toggleLoginModal } from 'src/redux/actions/modal';

const styleSheet = (/* theme */) => ({
	buttonFb: {
		backgroundColor: '#4c69ba',
		marginBottom: 20,
		width: '100%',
		boxSizing: 'border-box',
		position: 'relative',
		border: 'none',
		lineHeight: '36px',
		whiteSpace: 'nowrap',
		borderRadius: 2,
		fontSize: '16px',
		color: '#FFF',
		cursor: 'pointer',
		padding: '5px',
		textAlign: 'center',
		'& svg': {
			marginRight: 20,
			verticalAlign: 'middle',
			width: '25px',
			height: '25px',
		},
	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
			// auth: state.auth,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			loginFacebook,
			toggleLoginModal,
			toggleSignUpModal,
		}, dispatch),
	};
};

@connect(mapStateToProps, mapDispatchToProps)
@withStyles(styleSheet)
export default class FbBtnLogin extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		isLoginPage: PropTypes.bool,
		referralId: PropTypes.string,
		action: PropTypes.shape({
			loginFacebook: PropTypes.func.isRequired,
			toggleLoginModal: PropTypes.func.isRequired,
			toggleSignUpModal: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		isLoginPage: false,
		referralId: undefined,
	}

	handleLoginFbStart = () => {
		// console.log('sss');
	}

	handleResponseFacebook = (resultUser) => {
		const { accessToken } = resultUser;
		const { referralId } = this.props;

		if (resultUser.status === 'not_authorized' || resultUser.status === 'unknown') {
			return;
		}

		if (accessToken) {
			this.props.action.loginFacebook({ accessToken, referralId }, () => {
				if (AuthStorage.loggedIn) {
					if (this.props.isLoginPage) {
						Router.push('/');
					} else {
						this.props.action.toggleLoginModal({ open: false });
						this.props.action.toggleSignUpModal({ open: false });
					}
				}
			});
		} else {
			notification.error({
				message: 'Error message',
				description: 'AccessToken not found!',
			});
		}
	}

	render() {
		const { classes } = this.props;

		return (
			<FacebookLogin
				appId="144879436203981"
				fields="email"
				onClick={this.handleLoginFbStart}
				callback={this.handleResponseFacebook}
				icon={<FacebookIcon />}
				cssClass={classes.buttonFb}
			/>
		);
	}
}
