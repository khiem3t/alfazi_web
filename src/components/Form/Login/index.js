/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-06 15:52:03
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Router from 'next/router';
import Link from 'next/link';

import { Form, Icon, Input, Button } from 'antd';

import BtnFbLogin from 'src/components/Form/BtnFbLogin';
import BtnGgLogin from 'src/components/Form/BtnGgLogin';

import AuthStorage from 'src/utils/AuthStorage';

import withStyles from 'src/theme/jss/withStyles';

import { loginRequest } from 'src/redux/actions/auth';
import { toggleSignUpModal, toggleLoginModal } from 'src/redux/actions/modal';

const styleSheet = (theme) => ({
	root: {
		padding: '25px 20px',
		width: '95%',
		[theme.breakpoints.up.sm]: {
			padding: '25px 40px',
			width: '400px',
		},
		position: 'relative',
		display: 'inline-block',
		background: '#fff',
		textAlign: 'left',
	},
	btn: {
		width: '100%',
	},
	logo: {
		textAlign: 'center',
		marginBottom: '20px',
	},
	buttonLogin: {
		marginBottom: 50,
		marginTop: 20,
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	dividend: {
		textAlign: 'center',
		color: '#bdbdbd',
		fontStyle: 'initial',
		position: 'relative',
		marginBottom: 20,
		'&:before': {
			content: '""',
			background: 'rgb(224, 224, 224)',
			width: '100%',
			height: 1,
			position: 'absolute',
			right: 0,
			left: 0,
			top: '50%',
		},
		'& span': {
			background: '#fff',
			display: 'inline-block',
			position: 'relative',

			zIndex: '1',
			padding: '0 10px',
		},
	},
	signUp: {
		marginBottom: 15,
		textAlign: 'center',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			auth: state.auth,
			modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			loginRequest,
			toggleSignUpModal,
			toggleLoginModal,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
@Form.create()
export default class LoginForm extends Component {
	static propTypes = {
		form: PropTypes.object.isRequired,
		classes: PropTypes.object.isRequired,
		isLoginPage: PropTypes.bool,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
			modal: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			loginRequest: PropTypes.func.isRequired,
			toggleLoginModal: PropTypes.func.isRequired,
			toggleSignUpModal: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		isLoginPage: false,
	}

	state = {
		loading: false,
	}

	componentDidMount() {
		if (AuthStorage.loggedIn) {
			Router.push('/');
		}
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this.setState({
					loading: true,
				});
				this.props.action.loginRequest(values, () => {
					if (AuthStorage.loggedIn && this.props.store.auth.id) {
						if (this.props.isLoginPage) {
							Router.push('/');
						} else {
							this.props.action.toggleLoginModal({ open: false });
						}
					}
				}, () => {
					this.setState({
						loading: false,
					});
				});
			}
		});
	}

	handleOpenSignUpDialog = (e) => {
		e.preventDefault();
		this.props.action.toggleLoginModal({ open: false });
		setTimeout(() => {
			this.props.action.toggleSignUpModal({ open: true, closable: this.props.store.modal.login.closable });
		}, 100);
	}

	render() {
		const { form: { getFieldDecorator }, classes, isLoginPage } = this.props;

		return (
			<div className={classes.root}>
				<Form onSubmit={this.handleSubmit} className={classes.form}>
					<div className={classes.logo}>
						<img src="/static/assets/images/logo/64x64.png" alt="ipp" />
					</div>

					<Form.Item>
						{getFieldDecorator('email', {
							rules: [{ required: true, message: 'Làm ơn nhập email của bạn!' }, { pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, message: 'Email không hợp lệ!' }],
						})(
							<Input size="large" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />,
						)}
					</Form.Item>
					<Form.Item>
						{getFieldDecorator('password', {
							rules: [{ required: true, message: 'Làm ơn nhập mật khẩu!' }, { min: 5 }],
						})(
							<Input size="large" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Mật khẩu" />,
						)}
					</Form.Item>
					<Form.Item>
						<div className={classes.buttonLogin}>
							<Link href="/forgot-password">
								<a className="login-form-forgot">Quên mật khẩu?</a>
							</Link>
							<Button type="primary" htmlType="submit" size="large" loading={this.state.loading}>
								Đăng nhập
							</Button>
						</div>
					</Form.Item>
				</Form>
				<div className={classes.dividend}>
					<span>Hoặc</span>
				</div>

				<BtnFbLogin isLoginPage={isLoginPage} />
				<BtnGgLogin isLoginPage={isLoginPage} />

				<div className={classes.signUp}>
					Bạn chưa có tài khoản?
					{' '}
					<a href="/sign-up" onClick={this.handleOpenSignUpDialog}>Đăng ký</a>
				</div>
			</div>
		);
	}
}
