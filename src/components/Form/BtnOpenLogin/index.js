/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-30 14:52:31
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Button } from 'antd';

import AuthStorage from 'src/utils/AuthStorage';

import { toggleLoginModal } from 'src/redux/actions/modal';

function mapStateToProps(/* state */) {
	return {};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			toggleLoginModal,
		}, dispatch),
	};
};

const BtnOpenLogin = (props) => {
	const { children } = props;

	const handleClick = () => {
		if (!AuthStorage.loggedIn) {
			props.action.toggleLoginModal({ open: true });
		}
	};

	return (
		<div onClick={handleClick} style={{ display: 'inline-block' }}>
			{
				children ||
				<Button type="primary">Đăng nhập</Button>
			}
		</div>
	);
};

BtnOpenLogin.propTypes = {
	children: PropTypes.node,
	// action
	action: PropTypes.shape({
		toggleLoginModal: PropTypes.func.isRequired,
	}).isRequired,
};

BtnOpenLogin.defaultProps = {
	children: undefined,
};

export default connect(mapStateToProps, mapDispatchToProps)(BtnOpenLogin);
