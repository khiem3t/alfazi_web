/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-27 15:57:53
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Highlighter from 'react-highlight-words';
import { DebounceInput } from 'react-debounce-input';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'src/theme/jss/withStyles';

import { Icon } from 'antd';

import { Router } from 'src/routes';

import IconSearch from 'react-icons/lib/md/search';

import AuthStorage from 'src/utils/AuthStorage';

import Avatar from 'src/components/Stuff/Avatar';

import { upsertWithWhereHistorySearch, getHistoryList, searchUsers, searchBlogPosts, searchQuestions } from 'src/redux/actions/search';

const styles = (theme) => ({
	root: {
		height: '32px',
		display: 'flex',
		position: 'relative',
		verticalAlign: 'middle',
	},
	input: {
		border: `1px solid ${theme.palette.primary[900]} !important`,
		// background: theme.palette.primary[50],
		font: 'inherit',
		width: 280,
		[theme.breakpoints.up.lg]: {
			width: 400,
		},
		height: '32px',
		margin: '0',
		padding: '6px 8px 6px 10px',
		display: 'block',
		whiteSpace: 'normal',
		verticalAlign: 'middle',
		transition: 'all .5s',
		'&:hover': {
			background: theme.palette.primary[50],
		},
	},
	icon: {
		top: '0',
		color: theme.palette.primary[900],
		width: '32px',
		padding: '5px 5px',
		position: 'absolute',
		height: '100%',
		verticalAlign: 'middle',
		display: 'flex',
		alignItems: 'center',
		right: '0',
		'& svg': {
			verticalAlign: 'middle',
			width: '20px',
			height: '20px',
		},
	},
	list: {
		position: 'absolute',
		width: '100%',
		zIndex: '99999',
		top: '33px',
		background: '#fff',
		boxShadow: '1px 1px 3px rgba(0, 0, 0, 0.23)',
		maxHeight: '90vh',
		overflowY: 'auto',
	},
	item: {
		padding: '10px 20px 10px 30px',
		fontSize: '14px',
		cursor: 'pointer',
		color: '#777',
		display: 'flex',
		alignItems: 'center',
		'& strong': {
			color: 'initial',
			// marginLeft: 5,
		},
		'& i': {
			marginLeft: 5,
		},
		'&:hover': {
			background: theme.palette.primary[50],
			// color: '#fff',
			transition: 'all .2s',
		},
		'& svg': {
			fontSize: '20px',
			marginRight: 10,
		},
	},
	itemActive: {
		background: theme.palette.primary[50],
		// color: '#fff',
		transition: 'all .2s',
	},
	itemTitle: {
		padding: '10px 20px',
		fontSize: '14px',
		borderTop: '1px solid #e8e8e8',
		fontWeight: 'bold',
		color: 'gray',
	},
	loading: {
		top: '0',
		bottom: '0',
		width: '30px',
		right: '5px',
		zIndex: '999',
		position: 'absolute',
		fontSize: '16px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		color: theme.palette.primary[900],
		background: '#fff',
	},
	avatar: {
		marginRight: '10px',
	},
	hightLight: {
		fontWeight: 'bolder',
		color: 'initial',
		backgroundColor: 'transparent',
	},
	image: {
		width: '50px',
		marginRight: '10px',
		height: '37px',
	},
	imageQuestion: {
		width: '40px',
		marginRight: '10px',
		height: '40px',
	},
	text: {
		height: '43px',
		display: '-webkit-box',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		WebkitLineClamp: '2',
		WebkitBoxOrient: 'vertical',
		flex: 1,
	},
	block: {
		paddingBottom: 10,
	},
});

function mapStateToProps(/* state */) {
	return {
		// store: {
		// 	auth: state.auth,
		// },
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			upsertWithWhereHistorySearch,
		}, dispatch),
	};
};

@connect(mapStateToProps, mapDispatchToProps)
@withStyles(styles)
export default class FormSearch extends Component {
	static propTypes = {
		className: PropTypes.string,
		classes: PropTypes.object.isRequired,

		// action
		action: PropTypes.shape({
			upsertWithWhereHistorySearch: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		className: '',
	}

	state = {
		itemFocus: -1,
		focus: false,
		loading: false,
		text: '',
	}

	dataUser = []
	dataBlogPost = []
	dataQuestion = []
	dataHistory = []

	handleFocus = () => {
		this.setState({
			focus: true,
		});
		if (!this.state.text && AuthStorage.loggedIn) {
			this.setState({
				loading: true,
			});
			getHistoryList({
				filter: {
					order: 'updatedAt DESC',
					where: {
						userId: AuthStorage.userId,
					},
					limit: 10,
				},
			}).then((res) => {
				this.dataHistory = res.data;
				this.setState({
					loading: false,
				});
			});
		}
	}

	handleBlur = () => {
		// this.dataUser = [];
		setTimeout(() => {
			this.setState({
				focus: false,
				itemFocus: -1,
			});
		}, 500);
	}

	handleKeyDown = (/* e */) => {
		// const totalItem = this.state.text ? (this.dataQuestion.length + this.dataBlogPost.length + this.dataUser.length) : this.dataHistory.length - 1;
		// let { itemFocus } = this.state;
		// const { text } = this.state;

		// switch (e.keyCode) {
		// 	case 13: { // enter
		// 		e.target.blur();
		// 		if (text) {
		// 			if (itemFocus === -1) {
		// 				this.handleClickItem('service', text);
		// 				break;
		// 			} else if (itemFocus < this.dataBlogPost.length) {
		// 				const tag = this.dataBlogPost[itemFocus];
		// 				this.handleClickItem('service', tag.value);
		// 				break;
		// 			} else if (itemFocus === this.dataBlogPost.length) {
		// 				this.handleClickItem('searchUser', text);
		// 				break;
		// 			} else if (itemFocus > this.dataBlogPost.length) {
		// 				const user = this.dataUser[itemFocus - this.dataBlogPost.length - 1];
		// 				this.handleClickItem('user', user.username, user);
		// 				break;
		// 			}
		// 			break;
		// 		} else {
		// 			if (itemFocus === -1) {
		// 				break;
		// 			} else {
		// 				const history = this.dataHistory[itemFocus];
		// 				this.handleClickItem('history', history.value, history.type);
		// 				break;
		// 			}
		// 		}
		// 	}
		// 	case 9: // tab
		// 		console.log('tab');
		// 		break;
		// 	case 27: // escape

		// 		break;
		// 	case 38: { // up
		// 		itemFocus = itemFocus > 0 ? itemFocus - 1 : totalItem;
		// 		this.setState({
		// 			itemFocus,
		// 		});
		// 		break;
		// 	}
		// 	case 40: { // down
		// 		itemFocus = totalItem > itemFocus ? itemFocus + 1 : 0;
		// 		this.setState({
		// 			itemFocus,
		// 		});
		// 		break;
		// 	}
		// 	default:
		// 		break;
		// }
	}

	handleChange = (e) => {
		this.setState({
			loading: true,
			text: e.target.value,
			itemFocus: -1,
		}, () => {
			if (this.state.text) {
				const regex = '/' + this.state.text + '/i';

				const p1 = new Promise((revolve, reject) => {
					searchBlogPosts({
						filter: {
							where: {
								state: 'published',
								publishedDate: { lte: new Date() },
								or: [
									{ title: { regexp: regex } },
									{ brief: { regexp: regex } },
									{ content: { regexp: regex } },
									{ tags: { regexp: regex } },
								],
							},
							limit: 5,
							fields: ['id', 'title', 'image', 'slug'],
						},
					}).then((res) => {
						this.dataBlogPost = res.data;
						revolve(res.data);
					}).catch((err) => {
						reject(err);
					});
				});

				const p2 = new Promise((revolve, reject) => {
					searchUsers({
						filter: {
							where: {
								status: 'active',
								id: { nin: [AuthStorage.userId] },
								or: [
									{ fullName: { regexp: regex } },
									{ address: { regexp: regex } },
									{ province: { regexp: regex } },
									{ class: { regexp: regex } },
									{ school: { regexp: regex } },
								],
							},
							limit: 5,
							fields: ['avatar', 'id', 'fullName'],
						},
					}).then((res) => {
						this.dataUser = res.data;
						revolve(res.data);
					}).catch((err) => {
						reject(err);
					});
				});

				const p3 = new Promise((revolve, reject) => {
					searchQuestions({
						filter: {
							where: {
								status: 'active',
								or: [
									{ content: { regexp: regex } },
									{ hashTag: { regexp: regex } },
									{ subject: { regexp: regex } },
									{ class: { regexp: regex } },
								],
							},
							limit: 5,
							fields: ['id', 'content'],
						},
					}).then((res) => {
						this.dataQuestion = res.data;
						revolve(res.data);
					}).catch((err) => {
						reject(err);
					});
				});

				Promise.all([p1, p2, p3]).then((/* value */) => {
					this.setState({
						loading: false,
					});
				}).catch((/* err */) => {
					this.setState({
						loading: false,
					});
				});
			} else {
				this.setState({
					loading: false,
				});
			}
		});
	}

	handleSaveHistory = (data) => {
		if (AuthStorage.loggedIn) {
			this.props.action.upsertWithWhereHistorySearch(data);
		}
	}

	handleClickItem = (type, value, data) => {
		if (type === 'history') {
			this.handleSaveHistory({ type: data, value: value.toString(), userId: AuthStorage.userId });
			if (data === 'user') {
				Router.pushRoute('/profile/' + value);
			} else if (data === 'question') {
				Router.pushRoute('/question/' + value);
			} else if (data === 'blog') {
				Router.pushRoute('/blog/post/' + value);
			} else {
				Router.pushRoute('/search?q=' + value);
			}
		} else {
			this.handleSaveHistory({ type, value: value.toString(), data, userId: AuthStorage.userId });
			if (type === 'user') {
				Router.pushRoute('/profile/' + value);
			} else if (type === 'question') {
				// this.handleSaveHistory({ type, value, userId: AuthStorage.userId });
				Router.pushRoute('/question/' + value);
			} else if (type === 'blog') {
				// this.handleSaveHistory({ type, value, userId: AuthStorage.userId });
				Router.pushRoute('/blog/post/' + value);
			} else if (type === 'searchMore') {
				// this.handleSaveHistory({ type, value, userId: AuthStorage.userId });
				Router.pushRoute('/search?q=' + value);
			}
		}
	}

	render() {
		const { classes, className } = this.props;
		const { itemFocus, focus, loading, text } = this.state;

		return (
			<div className={`${classes.root} ${className}`} >
				<DebounceInput
					minLength={3}
					debounceTimeout={500}
					className={classes.input}
					placeholder="Tìm kiếm bạn bè, câu hỏi, blog..."
					onFocus={this.handleFocus}
					onBlur={this.handleBlur}
					onKeyDown={this.handleKeyDown}
					onChange={this.handleChange}
				/>
				{
					loading ?
						<div className={classes.loading}>
							<Icon type="loading" />
						</div> :
						<span className={classes.icon}>
							<IconSearch />
						</span>
				}

				{
					!loading && focus && !text && this.dataHistory.length > 0 &&
					<div className={classes.list + ' list-drop'}>
						<div className={classes.block}>
							<div className={classes.itemTitle}>Tìm kiếm gần đây</div>
							{
								this.dataHistory.map((el, i) => {
									if (el.type !== 'searchMore' && !el.data) {
										return null;
									}
									if (el.type === 'user') {
										return (
											<div onClick={() => this.handleClickItem('history', el.value, el.type)} key={'history' + i} className={itemFocus === i ? classes.item + ' ' + classes.itemActive : classes.item}>
												<Avatar className={classes.avatar} src={el.data.avatar} name={el.data.fullName} size={25} />
												{el.data.fullName}
											</div>
										);
									}
									if (el.type === 'question') {
										return (
											<div onClick={() => this.handleClickItem('history', el.value, el.type)} key={'history' + i} className={itemFocus === i ? classes.item + ' ' + classes.itemActive : classes.item}>
												<img className={classes.imageQuestion} src="/static/assets/images/question.png" alt="question" />
												<div className={classes.text}>
													{el.data.content}
												</div>
											</div>
										);
									}
									if (el.type === 'blog') {
										return (
											<div onClick={() => this.handleClickItem('history', el.value, el.type)} key={'history' + i} className={itemFocus === i ? classes.item + ' ' + classes.itemActive : classes.item}>
												<img className={classes.image} src={el.data.image && el.data.image.secure_url} alt="img" />
												<div className={classes.text}>
													{el.data.title}
												</div>
											</div>
										);
									}
									return (
										<div onClick={() => this.handleClickItem('history', el.value, el.type)} key={'history' + i} className={itemFocus === i ? classes.item + ' ' + classes.itemActive : classes.item}>
											<IconSearch /> Xem tất cả kết quả cho <strong style={{ marginLeft: 5 }}>{el.value}</strong>
										</div>
									);
								})
							}
						</div>
					</div>
				}
				{
					!loading && focus && text &&
					<div className={classes.list + ' list-drop'}>
						<div style={{ paddingLeft: 20 }} onClick={() => this.handleClickItem('searchMore', text)} className={itemFocus === 0 ? classes.item + ' ' + classes.itemActive : classes.item}>
							<IconSearch /> Xem tất cả kết quả cho <strong style={{ marginLeft: 5 }}>{text}</strong>
						</div>
						{
							this.dataQuestion.length > 0 &&
							<div className={classes.block}>
								<div className={classes.itemTitle}>Câu hỏi</div>
								{
									this.dataQuestion.map((el, i) => {
										return (
											<div key={'question' + i} onClick={() => this.handleClickItem('question', el.id, el)} className={itemFocus - this.dataBlogPost.length - 1 === i ? classes.item + ' ' + classes.itemActive : classes.item}>
												<img className={classes.imageQuestion} src="/static/assets/images/question.png" alt="question" />
												<div className={classes.text}>
													<Highlighter
														highlightClassName={classes.hightLight}
														searchWords={[text]}
														textToHighlight={el.content}
													/>
												</div>
											</div>
										);
									})
								}
							</div>
						}
						{
							this.dataBlogPost.length > 0 &&
							<div className={classes.block}>
								<div className={classes.itemTitle}>Blog</div>
								{
									this.dataBlogPost.map((el, i) => {
										return (
											<div key={'blog' + i} onClick={() => this.handleClickItem('blog', el.slug, el)} className={itemFocus - this.dataBlogPost.length - 1 === i ? classes.item + ' ' + classes.itemActive : classes.item}>
												<img className={classes.image} src={el.image && el.image.secure_url} alt="img" />
												<div className={classes.text}>
													<Highlighter
														highlightClassName={classes.hightLight}
														searchWords={[text]}
														textToHighlight={el.title}
													/>
												</div>
											</div>
										);
									})
								}
							</div>
						}
						{
							this.dataUser.length > 0 &&
							<div className={classes.block}>
								<div className={classes.itemTitle}>Người dùng</div>
								{
									this.dataUser.map((el, i) => {
										return (
											<div key={'user' + i} onClick={() => this.handleClickItem('user', el.id, el)} className={itemFocus - this.dataBlogPost.length - 1 === i ? classes.item + ' ' + classes.itemActive : classes.item}>
												<Avatar className={classes.avatar} src={el.avatar} name={el.fullName} size={25} />
												<Highlighter
													highlightClassName={classes.hightLight}
													searchWords={[text]}
													textToHighlight={el.fullName}
												/>
											</div>
										);
									})
								}
							</div>
						}
					</div>
				}
			</div>
		);
	}
}
