/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-19 16:30:44
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
	FacebookIcon, FacebookShareButton,
	TwitterIcon, TwitterShareButton,
	GooglePlusIcon, GooglePlusShareButton,
	LinkedinIcon, LinkedinShareButton,
} from 'react-share';

import withStyles from 'src/theme/jss/withStyles';

const styleSheet = (/* theme */) => ({
	content: {
		display: 'flex',
	},
	contentItem: {
		margin: '0 5px',
		cursor: 'pointer',
	},
});

@withStyles(styleSheet)
export default class BtnShare extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		shareUrl: PropTypes.string.isRequired,
		title: PropTypes.string,
	}

	static defaultProps = {
		title: '',
	}

	render() {
		const { classes, shareUrl, title } = this.props;

		return (
			<div className={classes.content}>
				<FacebookShareButton
					url={shareUrl}
					quote={title}
					className={classes.contentItem}
				>
					<FacebookIcon
						size={25}
						round
					/>
				</FacebookShareButton>
				<GooglePlusShareButton
					url={shareUrl}
					className={classes.contentItem}
				>
					<GooglePlusIcon
						size={25}
						round
					/>
				</GooglePlusShareButton>
				<TwitterShareButton
					url={shareUrl}
					quote={title}
					className={classes.contentItem}
				>
					<TwitterIcon
						size={25}
						round
					/>
				</TwitterShareButton>
				<LinkedinShareButton
					url={shareUrl}
					title={title}
					windowWidth={750}
					windowHeight={600}
					className={classes.contentItem}
				>
					<LinkedinIcon
						size={25}
						round
					/>
				</LinkedinShareButton>
			</div>
		);
	}
}
