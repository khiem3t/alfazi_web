/* --------------------------------------------------------
 * Author Khiem
 * Email khiem3t@gmail.com

 * Created: 2017-08-09 23:58:09
 *
 * LastModified: 2018-01-12 15:42:29
 *------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import GoogleLogin from 'react-google-login';
import { bindActionCreators } from 'redux';

import { notification } from 'antd';

import Router from 'next/router';

import withStyles from 'src/theme/jss/withStyles';

import GoogleIcon from 'react-icons/lib/fa/google-plus';

import AuthStorage from 'src/utils/AuthStorage';

import { loginGoogle } from 'src/redux/actions/auth';
import { toggleSignUpModal, toggleLoginModal } from 'src/redux/actions/modal';

const styleSheet = (/* theme */) => ({
	buttonGG: {
		backgroundColor: '#DD4B39',
		marginBottom: 20,
		width: '100%',
		boxSizing: 'border-box',
		position: 'relative',
		border: 'none',
		lineHeight: '36px',
		whiteSpace: 'nowrap',
		borderRadius: 2,
		fontSize: '16px',
		color: '#FFF',
		cursor: 'pointer',
		padding: '5px',
		textAlign: 'center',
		'& svg': {
			marginRight: 20,
			verticalAlign: 'middle',
			width: '25px',
			height: '25px',
		},
	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
			// auth: state.auth,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			loginGoogle,
			toggleLoginModal,
			toggleSignUpModal,
		}, dispatch),
	};
};

@connect(mapStateToProps, mapDispatchToProps)
@withStyles(styleSheet)
export default class GgBtnLogin extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		isLoginPage: PropTypes.bool,
		referralId: PropTypes.string,
		// action
		action: PropTypes.shape({
			loginGoogle: PropTypes.func.isRequired,
			toggleLoginModal: PropTypes.func.isRequired,
			toggleSignUpModal: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		isLoginPage: false,
		referralId: undefined,
	}

	handleResponseGoogle = (result) => {
		if (result.error === 'popup_closed_by_user') {
			return;
		}
		const { tokenId: accessToken } = result;
		const { referralId } = this.props;

		if (!result.error && accessToken) {
			this.props.action.loginGoogle({ accessToken, referralId }, () => {
				if (AuthStorage.loggedIn) {
					if (this.props.isLoginPage) {
						Router.push('/');
					} else {
						this.props.action.toggleLoginModal({ open: false });
						this.props.action.toggleSignUpModal({ open: false });
					}
				}
			});
		} else {
			if (result.error !== 'idpiframe_initialization_failed') {
				notification.error({
					message: 'Error message',
					description: result.error,
				});
			}
		}
	}

	render() {
		const { classes } = this.props;

		return (
			<GoogleLogin
				clientId="671782562952-rhmgci05iqn7bfg7l380c24ftb2kq5r5.apps.googleusercontent.com"
				onSuccess={this.handleResponseGoogle}
				onFailure={this.handleResponseGoogle}
				className={classes.buttonGG}
			>
				<GoogleIcon />
				<span> Login with Google</span>
			</GoogleLogin>
		);
	}
}
