/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 14:08:25
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Icon } from 'antd';

import { Link } from 'src/routes';

import withStyles from 'src/theme/jss/withStyles';

import AvatarBlock from 'src/components/User/AvatarBlock';
import Latex from 'react-latex';
import { getTopQuestionList } from 'src/redux/actions/question';

const styles = (/* theme */) => ({
	container: {
		background: '#fff',
		marginTop: 20,
	},
	list: {
		padding: '0 20px',
	},
	item: {
		padding: '10px 0',
		borderBottom: '1px solid #e8e8e8',
		'&:last-child': {
			border: 0,
		},
	},
	name: {
		marginTop: '10px',
		flex: '1',
		'& h4': {
			margin: '0',
		},
		'& span': {
			fontSize: '12px',
			fontStyle: 'italic',
		},
	},
	header: {
		padding: '10px',
		textAlign: 'center',
		borderBottom: '1px solid #e8e8e8',
		'& h3': {
			marginBottom: 0,
		},
	},
	footer: {
		textAlign: 'center',
		padding: '10px',
		borderTop: '1px solid #e8e8e8',
	},
	stats: {
		display: 'flex',
		justifyContent: 'flex-end',
		paddingTop: '10px',
	},
	social: {
		marginLeft: 20,
		cursor: 'pointer',
	},
	icon: {
		marginRight: 2,
	},
	content: {
		maxHeight: '60px',
		display: '-webkit-box',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		WebkitLineClamp: '3',
		WebkitBoxOrient: 'vertical',
		textAlign: 'justify',
		margin: '10px 0',
		color: 'rgba(0, 0, 0, 0.65)',
		'&:hover': {
			color: 'rgba(0, 0, 0, 0.65)',
		},
	},
});

const mapStateToProps = (state) => {
	return {
		topQuestionList: state.question.topQuestionList,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getTopQuestionList,
		}, dispatch),
	};
};

@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class TopQuestion extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		isTopPage: PropTypes.bool,
		action: PropTypes.shape({
			getTopQuestionList: PropTypes.func,
		}).isRequired,
		topQuestionList: PropTypes.shape({
			data: PropTypes.array,
		}).isRequired,
	}

	static defaultProps = {
		isTopPage: false,
	}

	componentDidMount() {
		this.props.action.getTopQuestionList({ filter: this.filter });
	}

	filter = {
		limit: this.props.isTopPage ? 10 : 5,
		skip: 0,
		order: ['viewsCount DESC', 'createdAt ASC'],
		include: {
			relation: 'creator',
			scope: {
				fields: ['id', 'username', 'avatar', 'fullName', 'role'],
			},
		},
		where: {
			status: 'active',
		},
		counts: ['answers', 'saves', 'flows'],
	}

	renderLoading() {
		const { classes } = this.props;

		return (
			<div className={classes.container}>
				<div className={classes.header}>
					<h3>Top Câu hỏi</h3>
				</div>
				<div className={classes.list}>
					{
						[0, 0, 0, 0].map((question, index) => {
							return (
								<div className={classes.item} key={index}>
									<AvatarBlock
										loading
										date="2018-03-07 10:58:36"
										size={40}
									/>
									<div className={classes.name}>
										<div className="loading-block" />
										<div className="loading-block" />
										<div className="loading-block" style={{ width: '50%' }} />

									</div>
								</div>
							);
						})
					}
				</div>
			</div>
		);
	}

	render() {
		const { classes, topQuestionList, isTopPage } = this.props;

		if (topQuestionList.loading) {
			return this.renderLoading();
		}

		return (
			<div className={classes.container}>
				<div className={classes.header}>
					<h3>Top Câu hỏi</h3>
				</div>
				<div className={classes.list}>
					{
						topQuestionList.data.map((question) => {
							return (
								<div className={classes.item} key={question.id}>
									<AvatarBlock
										userData={question.creator}
										date={question.createdAt}
										size={40}
									/>
									<div className={classes.name}>
										<Link route={`/question/${question.id}`}>
											<a className={classes.content}>

												<div>
													<Latex>
														{question.content}
													</Latex>
												</div>

											</a>
										</Link>
										<div className={classes.stats}>
											<Link route={`/question/${question.id}`}>
												<a className={classes.social}>
													<Icon className={classes.icon} type="book" />
													<span >{question.savesCount || 0}</span>
												</a>
											</Link>
											{/* <Link route={`/question/${question.id}`}>
												<a className={classes.social}>
													<Icon className={classes.icon} type="share-alt" />
													<span >{question.sharesCount || 0}</span>
												</a>
											</Link> */}
											<Link route={`/question/${question.id}`}>
												<a className={classes.social}>
													<Icon className={classes.icon} type="eye-o" />
													<span >{question.viewsCount || 0}</span>
												</a>
											</Link>
											<Link route={`/question/${question.id}`}>
												<a className={classes.social}>
													<Icon className={classes.icon} type="message" />
													<span >{question.answersCount || 0}</span>
												</a>
											</Link>
										</div>
									</div>
								</div>
							);
						})
					}
				</div>
				{
					!isTopPage &&
					<div className={classes.footer}>
						<Link href="/question-top">
							<a>Xem thêm</a>
						</Link>
					</div>
				}
			</div>
		);
	}
}
