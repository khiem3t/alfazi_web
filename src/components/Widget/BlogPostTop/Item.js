/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-19 17:06:57
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'src/utils/moment';
import { Link } from 'src/routes';

import withStyles from 'src/theme/jss/withStyles';

const styleSheet = (/* theme */) => ({
	root: {
		display: 'flex',
		marginBottom: 20,
	},
	left: {
		width: '80px',
		height: '60px',
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundColor: 'gary',
		backgroundPosition: 'center',
	},
	right: {
		flex: '1',
		marginLeft: '10px',
		'& h4': {
			lineHeight: '1.3',
			marginBottom: '5px',
			height: '37px',
			display: '-webkit-box',
			overflow: 'hidden',
			textOverflow: 'ellipsis',
			WebkitLineClamp: '2',
			WebkitBoxOrient: 'vertical',
		},
		'& span': {
			fontSize: '12px',
			color: '#9a9a9a',
		},
	},
});

const TopBlogPostItem = (props) => {
	const { classes, postData, loading } = props;

	if (loading) {
		return (
			<div className={classes.root}>
				<div className="loading-block" style={{ width: '80px', height: '60px' }} />
				<div className={classes.right}>
					<div className="loading-block" />
					<div className="loading-block" style={{ width: '60%' }} />
					<div className="loading-block" style={{ width: '40%' }} />
				</div>
			</div>
		);
	}

	return (
		<Link route={'/blog/post/' + postData.slug}>
			<a>
				<div className={classes.root}>
					<div className={classes.left} style={{ backgroundImage: 'url("' + (postData.image && postData.image.secure_url) + '")' }} />
					<div className={classes.right}>
						<h4>{postData.title}</h4>
						<span>{moment(postData.publishedDate).format('LL')}</span>
					</div>
				</div>
			</a>
		</Link>
	);
};

TopBlogPostItem.propTypes = {
	classes: PropTypes.object.isRequired,
	postData: PropTypes.object,
	loading: PropTypes.bool,
};

TopBlogPostItem.defaultProps = {
	postData: {},
	loading: false,
};

export default withStyles(styleSheet)(TopBlogPostItem);
