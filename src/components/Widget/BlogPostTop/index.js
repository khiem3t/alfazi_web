/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-20 17:12:47
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'src/utils/moment';

import { Link } from 'src/routes';

import withStyles from 'src/theme/jss/withStyles';

import { getPostListLatest } from 'src/redux/actions/blogPost';

import Item from './Item';

const styleSheet = (theme) => ({
	root: {},
	itemHighlight: {
		backgroundRepeat: 'no-repeat',
		backgroundColor: '#e9ebee',
		backgroundSize: 'cover',
		marginBottom: '30px',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'flex-end',
		position: 'relative',
		'& h3': {
			// color: '#fff',
		},
		'& span': {
			color: '#676767',
			fontSize: 12,
		},
		'&:before': {
			content: '""',
			position: 'absolute',
			top: '0',
			right: '0',
			left: '0',
			bottom: '0',
			background: 'rgba(0, 0, 0, 0.21)',
		},
	},
	bg: {
		height: '200px',
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
	},
	inner: {
		position: 'relative',
		zIndex: 1,
		padding: '15px',
		background: '#fff',
	},
	title: {
		marginBottom: '20px',
		[theme.breakpoints.down.sm]: {
			marginTop: '20px',
		},
		position: 'relative',
		fontSize: '18px',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			listLatest: state.blogPost.listLatest,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getPostListLatest,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BlogPostTop extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		loading: PropTypes.bool,
		postCurrentId: PropTypes.string,
		// store
		store: PropTypes.shape({
			listLatest: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getPostListLatest: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		loading: false,
		postCurrentId: undefined,
	}

	state = {
		loading: true,
	}

	componentDidMount() {
		if (!this.props.loading && this.props.postCurrentId) {
			this.props.action.getPostListLatest({ postCurrentId: this.props.postCurrentId }, () => {
				this.setState({
					loading: false,
				});
			}, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.postCurrentId && nextProps.postCurrentId !== this.props.postCurrentId) {
			this.setState({
				loading: true,
			});
			nextProps.action.getPostListLatest({ postCurrentId: nextProps.postCurrentId }, () => {
				this.setState({
					loading: false,
				});
			}, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}


	render() {
		const { classes, store: { listLatest = { data: [] } }, loading } = this.props;

		return (
			<div className={classes.root}>
				<div className={classes.title + ' text-center-sm-down'}>
					Bài viết mới nhất
				</div>
				{
					this.state.loading || loading ?
						[0, 0, 0, 0, 0].map((post, i) => {
							if (i === 0) {
								return (
									<div key={i} className={classes.itemHighlight}>
										<div className={classes.inner}>
											<div className="loading-block" />
											<div className="loading-block" style={{ width: '60%' }} />
											<div className="loading-block" style={{ width: '40%' }} />
										</div>
									</div>
								);
							}
							return <Item key={i} loading />;
						}) :
						listLatest.data.map((post, i) => {
							if (i === 0) {
								return (
									<Link key={post.id} route={'/blog/post/' + post.slug}>
										<a>
											<div className={classes.itemHighlight}>
												<div className={classes.bg} style={{ backgroundImage: 'url("' + (post.image && post.image.secure_url) + '")' }} />
												<div className={classes.inner}>
													<h3>{post.title}</h3>
													<span>{moment(post.publishedDate).format('LL')}</span>
												</div>
											</div>
										</a>
									</Link>
								);
							}
							return <Item key={post.id} postData={post} />;
						})
				}
			</div>
		);
	}
}

