/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 14:08:25
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Link from 'next/link';
import { bindActionCreators } from 'redux';

import { Select } from 'antd';


import withStyles from 'src/theme/jss/withStyles';

import AvatarBlock from 'src/components/User/AvatarBlock';

import { getTopUserList } from 'src/redux/actions/user';

import { getRankingList } from 'src/redux/actions/ranking';

import { weekOfYear } from 'src/utils';

const styles = (/* theme */) => ({
	container: {
		background: '#fff',
		marginTop: 20,
	},
	list: {
		padding: '0 20px',
	},
	item: {
		padding: '10px 0',
		borderBottom: '1px solid #e8e8e8',
		display: 'flex',
		alignItems: 'center',
		'&:last-child': {
			border: 0,
		},
	},
	tabbarRanking: {
		'& .ant-tabs-nav': {
			display: 'flex',
		},
		'& .ant-tabs-nav .ant-tabs-tab': {
			flex: '1',
			margin: '0',
			textAlign: 'center',
		},
	},
	footer: {
		textAlign: 'center',
		padding: '10px',
		borderTop: '1px solid #e8e8e8',
	},
	stats: {
		display: 'flex',
		fontSize: '12px',
		marginLeft: '50px',
		marginTop: '10px',

	},
	header: {
		padding: '10px',
		textAlign: 'center',
		borderBottom: '1px solid #e8e8e8',
		'& h3': {
			marginBottom: 0,
		},
	},
	action: {
		padding: '15px',
		textAlign: 'center',
	},
	user: {
		marginLeft: 10,
	},
	point: {
		fontSize: 12,
	},
	number: {
		fontSize: '20px',
		width: '50px',
		textAlign: 'center',
		fontWeight: 'bold',
	},
	noData: {
		textAlign: 'center',
		paddingBottom: '15px',
	},
});

const mapStateToProps = (state) => {
	return {
		store: {
			topUserList: state.user.topList,
			rankingList: state.ranking.rankList,
		},
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getTopUserList,
			getRankingList,
		}, dispatch),
	};
};

@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class Ranking extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		isRankingPage: PropTypes.bool,
		// store
		store: PropTypes.shape({
			topUserList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getTopUserList: PropTypes.func,
			getTransactionCoinsList: PropTypes.func,
			getRankingList: PropTypes.func,
		}).isRequired,
	}

	static defaultProps = {
		isRankingPage: false,
	}

	state = {
		isLoading: true,
	}

	componentDidMount() {
		const beforeMonth = new Date();
		let month = beforeMonth.getMonth();
		let year = beforeMonth.getFullYear();
		if (month === 0) {
			month = 12;
			year -= 1;
		}
		this.filter.where = { and: [
			{ type: 'rankMonth' },
			{ month },
			{ year },
		] };
		this.props.action.getRankingList({ filter: this.filter }, () => {
			this.setState({ isLoading: false });
		});
	}

	onChangeWard = (e) => {
		const now = new Date();

		if (e === 'daily') {
			const beforeday = new Date(new Date().setDate(new Date().getDate() - 1));
			const day = beforeday.getDate();
			const month = beforeday.getMonth() + 1;
			const year = beforeday.getFullYear();
			this.filter.where = { and: [
				{ type: 'rankDate' },
				{ day },
				{ month },
				{ year },
			] };
		}
		if (e === 'month') {
			const beforeMonth = new Date();
			let month = beforeMonth.getMonth();
			let year = beforeMonth.getFullYear();
			if (month === 0) {
				month = 12;
				year -= 1;
			}
			this.filter.where = { and: [
				{ type: 'rankMonth' },
				{ month },
				{ year },
			] };
		}
		if (e === 'week') {
			let year = now.getFullYear();
			let week = weekOfYear(now) - 1;
			if (weekOfYear(now) - 1 === 0) {
				const beforeYear = new Date(new Date().setFullYear(new Date().getFullYear() - 1));

				year = beforeYear.getFullYear();
				week = 53;
			}

			this.filter.where = { and: [
				{ type: 'rankWeek' },
				{ week },
				{ year },
			] };
		}
		this.setState({ isLoading: true });
		this.props.action.getRankingList({ filter: this.filter }, () => {
			this.setState({ isLoading: false });
		});
	}

	filter = {
		limit: this.props.isRankingPage ? 10 : 3,
		skip: 0,
		where: { type: 'rankMonth' },
		order: ['pointsCount DESC'],
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
		],
	}

	renderLoading() {
		const { classes } = this.props;

		const data = this.props.isRankingPage ? [0, 0, 0, 0, 0, 0, 0, 0, 0, 0] : [0, 0, 0];

		return (
			<div className={classes.list}>
				{
					data.map((user, i) => {
						return (
							<div className={classes.item} key={i}>
								<AvatarBlock loading size={45} className={classes.user} />
								<div className={classes.point}>
									<div className="loading-block" style={{ width: 40 }} />
								</div>
							</div>
						);
					})
				}
			</div>
		);
	}


	render() {
		const { classes, store: { rankingList }, isRankingPage } = this.props;

		return (
			<div className={classes.container}>
				<div className={classes.header}>
					<h3>Bảng xếp hạng</h3>
				</div>
				<div className={classes.action}>
					<Select defaultValue="month" style={{ width: '100%' }} onChange={this.onChangeWard}>
						<Select.Option value="month">Bảng xếp hạng tháng</Select.Option>
						<Select.Option value="week">Bảng xếp hạng tuần</Select.Option>
						<Select.Option value="daily">Bảng xếp hạng ngày</Select.Option>
					</Select>
				</div>
				{
					this.state.isLoading ? this.renderLoading() : (

						<div className={classes.list}>
							{
								rankingList.data.length === 0 ? (
									<div className={classes.noData}>Chưa có dữ liệu cập nhật</div>
								) :

									<div>
										{rankingList.data.map((user, i) => {
											return (
												<div className={classes.item} key={user.id}>
													{
														i > 2 ?
															<div className={classes.number}>
																{i + 1}
															</div> :
															<img src={`/static/assets/images/top${i + 1}.png`} alt={`top ${i + 1}`} width="40" />
													}
													<AvatarBlock userData={user.creator} size={30} className={classes.user} />
													<div className={classes.point}>
														{user.pointsCount || 0} điểm
													</div>
												</div>
											);
										})}
									</div>
							}
						</div>
					)
				}
				{
					!isRankingPage &&
					<div className={classes.footer}>
						<Link href="/ranking">
							<a>Xem thêm</a>
						</Link>
					</div>
				}
			</div>
		);
	}
}

