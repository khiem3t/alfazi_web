/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-20 17:44:10
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import { Row, Col } from 'antd';

import Item from 'src/components/Post/Card';

import { getPostListRelative } from 'src/redux/actions/blogPost';

const styleSheet = (/* theme */) => ({
	root: {
		marginBottom: '-15px',
	},
	title: {
		marginBottom: '20px',
		position: 'relative',
		fontSize: '18px',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			postListRelative: state.blogPost.listRelative,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getPostListRelative,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BlogPostRelative extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		categoryId: PropTypes.string,
		postCurrentId: PropTypes.string,
		// store
		store: PropTypes.shape({
			postListRelative: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getPostListRelative: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		categoryId: undefined,
		postCurrentId: undefined,
	}

	componentDidMount() {
		this.props.action.getPostListRelative({ categoryId: this.props.categoryId, postCurrentId: this.props.postCurrentId });
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.categoryId !== this.props.categoryId || nextProps.postCurrentId !== this.props.postCurrentId) {
			nextProps.action.getPostListRelative({ categoryId: nextProps.categoryId, postCurrentId: nextProps.postCurrentId });
		}
	}

	render() {
		const { classes, store: { postListRelative = { data: [] } }, categoryId, postCurrentId } = this.props;

		if (postListRelative.data.length === 0 || !categoryId || !postCurrentId) {
			return null;
		}

		return (
			<div className={classes.root}>
				<div className={classes.title + ' text-center-sm-down'}>
					Bài viết liên quan
				</div>
				<Row gutter={16}>
					{
						postListRelative.data.map((post) => {
							return (
								<Col md={12} key={post.id}>
									<Item postData={post} />
								</Col>
							);
						})
					}
				</Row>
			</div>
		);
	}
}
