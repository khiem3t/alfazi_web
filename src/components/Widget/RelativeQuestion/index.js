/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 14:08:25
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Icon } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import AvatarBlock from 'src/components/User/AvatarBlock';
import Latex from 'react-latex';
import { getRelativeQuestionList } from 'src/redux/actions/question';
import { Link } from 'src/routes';

const styles = (/* theme */) => ({
	container: {
		background: '#fff',
		marginTop: 20,
	},
	list: {
		padding: '0 20px',
	},
	item: {
		padding: '10px 0',
		borderBottom: '1px solid #e8e8e8',
		'&:last-child': {
			border: 0,
		},
	},
	name: {
		marginTop: '10px',
		flex: '1',
		'& h4': {
			margin: '0',
		},
		'& span': {
			fontSize: '12px',
			fontStyle: 'italic',
		},
	},
	header: {
		padding: '10px',
		textAlign: 'center',
		borderBottom: '1px solid #e8e8e8',
		'& h3': {
			marginBottom: 0,
		},
	},
	footer: {
		textAlign: 'center',
		padding: '10px',
		borderTop: '1px solid #e8e8e8',
	},
	stats: {
		display: 'flex',
		justifyContent: 'flex-end',
		paddingTop: '10px',
	},
	social: {
		marginLeft: 20,
		cursor: 'pointer',
	},
	icon: {
		marginRight: 2,
	},
	content: {
		maxHeight: '60px',
		display: '-webkit-box',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		WebkitLineClamp: '3',
		WebkitBoxOrient: 'vertical',
		textAlign: 'justify',
		margin: '10px 0',
		color: 'rgba(0, 0, 0, 0.65)',
		'&:hover': {
			color: 'rgba(0, 0, 0, 0.65)',
		},
	},
});

const mapStateToProps = (state) => {
	return {
		store: {
			relativeQuestionList: state.question.relativeQuestionList,
			questionView: state.question.questionView,
		},
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getRelativeQuestionList,
		}, dispatch),
	};
};

@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class RelativeQuestion extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		action: PropTypes.shape({
			getRelativeQuestionList: PropTypes.func,
		}).isRequired,
		store: PropTypes.shape({
			relativeQuestionList: PropTypes.object.isRequired,
			questionView: PropTypes.object.isRequired,
		}).isRequired,
	}

	state = {
		showMore: false,
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.store.questionView.id && this.props.store.questionView.id !== nextProps.store.questionView.id) {
			const { questionView } = nextProps.store;
			const conditions = [
				{ hashTag: { inq: (questionView.hashTag && questionView.hashTag.length) ? questionView.hashTag : undefined } },
				{ subject: questionView.subject },
				{ class: questionView.class },
			];

			this.filter.where.id = { neq: questionView.id };
			this.filter.where.and = conditions;


			nextProps.action.getRelativeQuestionList({ firstLoad: true, filter: this.filter }, (res1) => {
				if (res1.total > this.filter.limit) {
					this.setState({ showMore: true });
				}
				if (!res1.data.length) {
					this.filter.where.and = undefined;
					this.filter.where.or = conditions;

					// FIX ME:this is not a good approach
					nextProps.action.getRelativeQuestionList({ firstLoad: true, filter: this.filter }, (res2) => {
						if (res2.total > this.filter.limit) {
							this.setState({ showMore: true });
						}
						if (!res2.data.length) {
							this.filter.where.or = undefined;
							nextProps.action.getRelativeQuestionList({ firstLoad: true, filter: this.filter }, (res3) => {
								if (res3.total > this.filter.limit) {
									this.setState({ showMore: true });
								}
							});
						}
					});
				}
			});
		}
	}

	handleLoadMore = (e) => {
		e.preventDefault();
		this.filter.skip += this.filter.limit;
		this.props.action.getRelativeQuestionList({ filter: this.filter }, (res) => {
			if (res.data.length < this.filter.limit) {
				this.setState({ showMore: false });
			}
		});
	}

	filter = {
		limit: 4,
		skip: 0,
		include: {
			relation: 'creator',
			scope: {
				fields: ['id', 'username', 'avatar', 'fullName', 'role'],
			},
		},
		where: {
			status: 'active',
		},
		counts: ['answers', 'saves', 'flows'],
	}

	renderLoading() {
		const { classes } = this.props;

		return (
			<div className={classes.container}>
				<div className={classes.header}>
					<h3>Câu hỏi liên quan</h3>
				</div>
				<div className={classes.list}>
					{
						[0, 0, 0, 0].map((question, index) => {
							return (
								<div className={classes.item} key={index}>
									<AvatarBlock
										loading
										date="2018-03-07 10:58:36"
										size={40}
									/>
									<div className={classes.name}>
										<div className="loading-block" />
										<div className="loading-block" />
										<div className="loading-block" style={{ width: '50%' }} />

									</div>
								</div>
							);
						})
					}
				</div>
			</div>
		);
	}

	render() {
		const { classes, store: { relativeQuestionList } } = this.props;

		if (relativeQuestionList.loading) {
			return this.renderLoading();
		}

		return (
			<div className={classes.container}>
				<div className={classes.header}>
					<h3>Câu hỏi liên quan</h3>
				</div>
				<div className={classes.list}>
					{
						relativeQuestionList.data.map((question) => {
							return (
								<div className={classes.item} key={question.id}>
									<AvatarBlock
										userData={question.creator}
										date={question.createdAt}
										size={40}
									/>
									<div className={classes.name}>
										<Link route={'/question/' + question.id}>

											<a className={classes.content}>
												<Latex>
													{question.content}
												</Latex>
											</a>

										</Link>
										<div className={classes.stats}>
											<Link route={'/question/' + question.id}>
												<a className={classes.social}>
													<Icon className={classes.icon} type="book" />
													<span >{question.savesCount || 0}</span>
												</a>
											</Link>
											<Link route={'/question/' + question.id}>
												<a className={classes.social}>
													<Icon className={classes.icon} type="share-alt" />
													<span >{question.sharesCount || 0}</span>
												</a>
											</Link>
											<Link route={'/question/' + question.id}>
												<a className={classes.social}>
													<Icon className={classes.icon} type="eye-o" />
													<span >{question.viewsCount || 0}</span>
												</a>
											</Link>
											<Link route={'/question/' + question.id}>
												<a className={classes.social}>
													<Icon className={classes.icon} type="message" />
													<span >{question.answersCount || 0}</span>
												</a>
											</Link>
										</div>
									</div>
								</div>
							);
						})
					}
				</div>
				{
					this.state.showMore &&
						<div className={classes.footer}>
							<a href="#" onClick={this.handleLoadMore}>Xem thêm</a>
						</div>
				}
			</div>
		);
	}
}
