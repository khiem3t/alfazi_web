/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-02 00:26:56
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Form, Select, Input, Button, Upload, Icon, Card, Tabs } from 'antd';
import Latex from 'react-latex';
import { fileToObject } from 'antd/lib/upload/utils';

import AuthStorage from 'src/utils/AuthStorage';

import { classOptions, subjectOptions } from 'src/constants/selectOption';
import { createQuestion, updateQuestion } from 'src/redux/actions/question';
import { uploadFiles } from 'src/redux/actions/upload';
import MathEditor from 'src/components/Answer/MathEditor';

import Cropper from 'src/components/Stuff/ImageCropper';
import Lightbox from 'react-image-lightbox';
import _findIndex from 'lodash/findIndex';
import FaSubscript from 'react-icons/lib/fa/subscript';

import SwitchType from 'src/components/Question/SwitchType';

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			createQuestion,
			updateQuestion,
			uploadFiles,
		}, dispatch),
	};
};

@Form.create()
@connect(mapStateToProps, mapDispatchToProps)
export default class QuestionCreateBox extends PureComponent {
	static propTypes = {
		form: PropTypes.object.isRequired,
		questionData: PropTypes.object,
		onClose: PropTypes.func.isRequired,
		onRefresh: PropTypes.func,
		style: PropTypes.object,
		type: PropTypes.string,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			createQuestion: PropTypes.func.isRequired,
			updateQuestion: PropTypes.func.isRequired,
			uploadFiles: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		questionData: undefined,
		onRefresh: f => f,
		style: {},
		type: 'free',
	}

	state = {
		loading: false,
		fileList: [],
		mathEditorVisible: false,
		showCropper: false,
		showLightBox: false,
		img: {
			url: '',
			name: '',
			type: '',
			idx: 0,
			uid: -1,
		},
	}

	componentDidMount() {
		if (this.props.questionData && this.props.questionData.id) {
			this.props.form.setFieldsValue({
				content: this.props.questionData.content || '',
				class: this.props.questionData.class || '',
				subject: this.props.questionData.subject || '',
				hashtag: this.props.questionData.hashtag || [],
				type: this.props.questionData.type || '',
			});
			this.setState({ // eslint-disable-line
				fileList: this.props.questionData.images ? this.props.questionData.images.map((el, i) => {
					return {
						uid: (i + 1) * (-1),
						name: el,
						status: 'done',
						url: el,
					};
				}) : [],
			});
		}
	}

	onChangeTab = () => {
		const { form } = this.props;
		const contentText = form.getFieldValue('content') ? form.getFieldValue('content') : '';
		this.setState({ mathcontent: contentText });
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this.setState({
					loading: true,
				});

				const { fileList } = this.state;
				this.props.action.uploadFiles({ files: fileList }, (imageList) => {
					if (this.props.questionData && this.props.questionData.id) { // edit question
						const data = { ...values, updatedAt: new Date(), id: this.props.questionData.id, images: imageList };

						this.props.action.updateQuestion(data, () => {
							this.setState({
								loading: false,
								mathEditorVisible: false,
							}, () => {
								this.props.onClose();
								this.props.onRefresh();
							});
						}, () => {
							this.setState({
								loading: false,
							});
						});
					} else { // create question
						const data = { ...values, creatorId: AuthStorage.userId, images: imageList };

						this.props.action.createQuestion(data, () => {
							this.setState({
								loading: false,
							}, () => {
								window.scrollTo(0, 500); // eslint-disable-line
								this.props.onClose();
								this.props.onRefresh();
							});
						}, () => {
							this.setState({
								loading: false,
							});
						});
					}
				}, () => {
					this.setState({
						loading: false,
					});
				});
			}
		});
	}

	handleChangeText = (e) => {
		const { form } = this.props;
		form.setFieldsValue({
			content: e.target.value,
		});
	}

	showMathEditorModal = () => {
		this.setState({
			mathEditorVisible: true,
		});
	}
	handleMathEditorModalSubmit = () => {
		this.setState({
			mathEditorVisible: false,
		});
	}
	handleMathEditorModalCancel = () => {
		this.setState({
			mathEditorVisible: false,
		});
	}

	handelCancel = () => {
		this.setState({ mathEditorVisible: false });
	}

	handleChange = ({ fileList, file }) => {
		if (fileList.length < this.state.fileList.length) {
			this.setState({ fileList });
			return;
		}
		const index = _findIndex(fileList, file);
		fileList[index].status = 'done'; // eslint-disable-line
		const reader = new FileReader(); // eslint-disable-line
		reader.onload = () => {
			this.setState({
				showCropper: true,
				fileList,
				img: {
					...this.state.img,
					url: reader.result,
					name: file.name,
					type: file.type,
					idx: index,
				},
			});
		};
		reader.readAsDataURL(file.originFileObj);
	}

	handleCrop = (promise) => {
		promise.then(file => {
			const fileList = [...this.state.fileList];
			const { uid } = this.state.img;
			fileList.splice(this.state.img.idx, 1, {
				...fileToObject(file),
				uid,
				status: 'done',
			});
			this.setState({ fileList, showCropper: false, img: { ...this.state.img, uid: uid - 1 } });
			this.clearImgState();
		});
	}

	handlePreview = (file) => {
		this.setState({
			showLightBox: true,
			img: { ...this.state.img, url: file.thumbUrl },
		});
	}

	handleCropDefault = () => {
		this.setState({ showCropper: false });
		this.clearImgState();
	}

	clearImgState = () => {
		this.setState({ img: {
			...this.state.img,
			url: '',
			name: '',
			type: '',
			idx: 0,
		} });
	}

	insertContent = (content) => {
		const { form } = this.props;
		const contentText = form.getFieldValue('content') ? form.getFieldValue('content') : '';
		form.setFieldsValue({
			content: contentText + content,
		});
		this.setState({ mathEditorVisible: false });
	}


	render() {
		const { form: { getFieldDecorator }, questionData = {}, onClose, style, type } = this.props;

		const { mathEditorVisible } = this.state;

		return (
			<div>
				<div
					style={{
						position: 'fixed',
						top: '0',
						bottom: '0',
						left: '0',
						right: '0',
						background: 'rgba(0, 0, 0, 0.34)',
						zIndex: '1',
					}}
					onClick={onClose}
				/>
				<Card
					title={`${questionData.id ? 'Chỉnh sửa ' : 'Nhập '} câu hỏi`}
					style={{ ...style, position: 'absolute', zIndex: '2', width: '100%' }}
					extra={
						<Icon
							type="close-circle-o"
							style={{ fontSize: 20, color: 'red', cursor: 'pointer' }}
							onClick={onClose}
						/>
					}
				>
					<Form layout="horizontal" onSubmit={this.handleSubmit}>
						<Form.Item>
							{getFieldDecorator('type', {
								initialValue: questionData.type || type || 'free',
							})(
								<SwitchType />,
							)}
						</Form.Item>

						<Tabs onChange={this.onChangeTab}>
							<Tabs.TabPane tab="Chỉnh sửa" key="1">
								<Form.Item style={{ marginBottom: '10px' }}>
									{getFieldDecorator('content', {
										rules: [{ required: true, message: 'Hãy nhập nội dung câu hỏi.' }],
									})(
										<Input.TextArea
											rows={6}
											autoFocus
											placeholder="Nội dung câu hỏi"
										/>,
									)}
									<a onClick={() => this.showMathEditorModal()}>
										<FaSubscript style={{ fontSize: 20 }} />
									</a>
									{mathEditorVisible ? (
										<div>
											<MathEditor
												visible={this.state.mathEditorVisible}
												onChange={this.handleChangeText}
												onSubmit={this.insertContent}
												onCancel={this.handelCancel}
											/>
										</div>
									) : ''}
								</Form.Item>
							</Tabs.TabPane>
							<Tabs.TabPane tab="Xem trước" key="2">
								<div className=" pre-wrap">
									<Latex >
										{this.state.mathcontent}
									</Latex>
								</div>
							</Tabs.TabPane>
						</Tabs>


						<div style={{ display: 'flex', justifyContent: 'space-between' }}>
							<Form.Item style={{ width: '49%', marginBottom: '10px' }}>
								{getFieldDecorator('class', {
									rules: [{ required: true, message: 'Làm ơn chọn một lớp' }],
								})(
									<Select placeholder="Lớp liên quan">
										{
											classOptions.map((el) => {
												return (
													<Select.Option key={el.value} value={el.value}>{el.label}</Select.Option>
												);
											})
										}
									</Select>,
								)}
							</Form.Item>
							<Form.Item style={{ width: '49%', marginBottom: '10px' }}>
								{getFieldDecorator('subject', {
									rules: [{ required: true, message: 'Làm ơn chọn một môn học' }],
								})(
									<Select placeholder="Môn học liên quan">
										{
											subjectOptions.map((el) => {
												return (
													<Select.Option key={el.value} value={el.value}>{el.label}</Select.Option>
												);
											})
										}
									</Select>,
								)}
							</Form.Item>
						</div>
						<Form.Item style={{ marginBottom: '10px' }}>
							{getFieldDecorator('hashtag', {
							})(
								<Select
									mode="tags"
									tokenSeparators={[',', ' ', '.', '-']}
									placeholder="Hashtag"
								/>,
							)}
						</Form.Item>

						<Upload
							// action="//jsonplaceholder.typicode.com/posts/"
							accept="image/*"
							listType="picture-card"
							fileList={this.state.fileList}
							onPreview={this.handlePreview}
							onChange={this.handleChange}
							style={{ background: '#fff' }}
						>
							<div>
								<Icon type="picture" style={{ fontSize: 20, color: '#08c' }} />
								<div className="ant-upload-text">Tải ảnh lên</div>
							</div>
						</Upload>
						{this.state.showCropper && ReactDOM.createPortal(
							<Cropper
								src={this.state.img.url}
								onCrop={this.handleCrop}
								onDefault={this.handleCropDefault}
								name={this.state.img.name}
								type={this.state.img.type}
							/>,
							document.getElementById('index-cropper'),
						)
						}
						{this.state.showLightBox &&
							<Lightbox
								mainSrc={this.state.img.url}
								onCloseRequest={() => this.setState({ showLightBox: false })}
							/>
						}
						<div className="text-right" style={{ marginTop: 15, paddingTop: 20, borderTop: '1px solid #e8e8e8', display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
							<Button type="primary" htmlType="submit" loading={this.state.loading} style={{ marginLeft: 10 }}>
								{ questionData.id ? 'Chỉnh sửa ' : 'Đăng ' } câu hỏi
							</Button>
						</div>
					</Form>
				</Card>
			</div>
		);
	}
}

