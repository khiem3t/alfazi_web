/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-12 23:30:32
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import { Button, Popover } from 'antd';

import ShareList from 'src/components/Form/ShareList';

import LINK from 'src/constants/url';

const styleSheet = (/* theme */) => ({
	button: {
		border: 0,
		padding: '0 10px',
		background: '#f5f8ff',
	},
});

@withStyles(styleSheet)
export default class BtnShare extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionData: PropTypes.object.isRequired,
	}

	static defaultProps = {}

	render() {
		const { classes, questionData } = this.props;
		const shareUrl = LINK.WEB_URL + '/question/' + questionData.id;
		const title = questionData.content;

		return (
			<Popover
				content={<ShareList shareUrl={shareUrl} title={title} />}
				trigger="click"
			>
				<Button className={classes.button} icon="share-alt">Chia sẻ</Button>
			</Popover>
		);
	}
}
