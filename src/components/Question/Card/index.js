/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 11:04:39
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Latex from 'react-latex';

import { Link } from 'src/routes';
import Router from 'next/router';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import { Tag, Button, Icon, Tooltip } from 'antd';

import BtnSetting from 'src/components/Question/BtnSetting';
import BtnSave from 'src/components/Question/BtnSave';
import BtnShare from 'src/components/Question/BtnShare';
import AvatarBlock from 'src/components/User/AvatarBlock';
import ImageLightBox from 'src/components/LightBox/ImageLightBoxContainer';
import RibbonQuestion from 'src/components/Question/Ribbon';

const styles = (theme) => ({
	root: {
		marginBottom: 5,
		position: 'relative',
		[theme.breakpoints.up.lg]: {
			marginBottom: 10,
		},
		border: '1px solid #e8e8e8',
	},
	opacity5: {
		opacity: 0.5,
	},
	hidden: {
		// pointerEvents: 'none',
		position: 'relative',
		'&:after': {
			content: '""',
			position: 'absolute',
			top: '0',
			right: '0',
			bottom: '0',
			left: '0',
			width: '100%',
			height: '100%',
			background: 'rgba(255, 0, 0, 0.22) url("/static/assets/images/inactive.png")',
			backgroundRepeat: 'no-repeat',
			backgroundSize: 'contain',
			backgroundPosition: 'center',
			zIndex: -1,
		},
	},
	suspend: {
		pointerEvents: 'none',
		position: 'relative',
		'&:after': {
			content: '""',
			position: 'absolute',
			top: '0',
			right: '0',
			bottom: '0',
			left: '0',
			width: '100%',
			height: '100%',
			background: 'rgba(255, 0, 0, 0.22) url("/static/assets/images/inactive.png")',
			backgroundRepeat: 'no-repeat',
			backgroundSize: 'contain',
			backgroundPosition: 'center',
			opacity: 0.5,
		},
	},
	question: {
		position: 'relative',
		background: '#fff',
		padding: '15px 20px 7px',
		borderBottom: '1px solid #e8e8e8',
	},
	header: {
		display: 'flex',
		justifyContent: 'space-between',
		marginBottom: '15px',
		alignItems: 'flex-start',
	},
	body: {},
	text: {
		marginBottom: '15px',
		overflowY: 'auto',
	},
	tag: {
		marginBottom: '15px',
	},
	tagItem: {
		margin: '5px 5px 0 0',
	},
	imgWrapper: {
		overflowX: 'auto',
	},
	img: {
		marginBottom: '15px',
		display: 'flex',
		'& img': {
			height: 100,
			width: 'auto',
			marginRight: 3,
		},
	},
	stats: {
		display: 'flex',
		justifyContent: 'space-between',
		borderTop: '1px solid #e8e8e8',
		alignItem: 'center',
	},
	left: {
		display: 'flex',
		padding: '10px',
		alignItems: 'center',
	},
	right: {
		padding: '10px 0 10px 10px',
		'& a': {
			color: 'rgba(0, 0, 0, 0.65)',
		},
	},
	social: {
		marginLeft: 15,
	},
	footer: {
		borderTop: '1px solid #e8e8e8',
		paddingTop: '7px',
		display: 'flex',
		justifyContent: 'space-around',
		background: '#f5f8ff',
		margin: '0 -20px -8px',
		paddingBottom: '7px',
	},
	icon: {
		marginRight: 5,
	},
	button: {
		border: 0,
		padding: '0 10px',
		background: '#f5f8ff',
	},
	comment: {
		background: '#f5f8ff',
		padding: '0 15px',
		[theme.breakpoints.up.md]: {
			padding: '0 40px',
		},
	},
	itemWrapper: {
		padding: '15px 0',
		borderBottom: '1px solid #e8e8e8',
	},
	item: {
		display: 'flex',
		flex: 1,
	},
	itemContent: {
		marginLeft: '50px',
		flex: 1,
		'& h4': {
			margin: 0,
			lineHeight: 1,
		},
		'& span': {
			fontStyle: 'italic',
			color: '#90949c',
			fontSize: '12px',
		},
	},
	itemText: {
		marginTop: 5,
	},
	itemImg: {
		marginTop: '10px',
		'& img': {
			height: 100,
		},
	},
	itemAction: {
		display: 'flex',
		justifyContent: 'flex-end',
		marginTop: '10px',
	},
	moreComment: {
		textAlign: 'center',
		padding: 10,
	},
	pin: {
		marginRight: '30px',
		border: '1px solid',
		padding: '5px',
		borderRadius: '100%',
	},
});

const QuestionCard = ({ classes, loading, questionData }) => {
	const { creator = {} } = questionData;

	if (loading) {
		return (
			<div className={classes.root}>
				<div className={classes.question}>
					<div className={classes.header}>
						<div className={classes.avatarWrap}>
							<AvatarBlock
								loading
								date="2018-03-07 10:57:10"
							/>
						</div>
					</div>
					<div className={classes.body}>
						<div className={classes.text}>
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" style={{ width: '50%' }} />
						</div>
						<div
							style={{ height: 100 }}
						/>
					</div>
				</div>
			</div>
		);
	}

	return (
		<div
			className={
				classNames(classes.root, {
					[classes.suspend]: questionData.status !== 'active' && questionData.creatorId !== AuthStorage.userId,
					[classes.hidden]: questionData.status !== 'active' && questionData.creatorId === AuthStorage.userId,
				})
			}
		>
			<RibbonQuestion type={questionData.type} />
			<div
				className={
					classNames(classes.question, {
						[classes.opacity5]: questionData.status !== 'active' && questionData.creatorId === AuthStorage.userId,
					})
				}
			>
				<div className={classes.header}>
					<AvatarBlock
						userData={creator}
						date={questionData.createdAt}
					/>
					{
						questionData.pin &&
						<Tooltip title="Bài viết này đã được ghim">
							<Icon type="pushpin" className={classes.pin} />
						</Tooltip>
					}

				</div>
				<div className={classes.body}>
					<div className={classes.text + ' pre-wrap'}>
						<Latex>
							{questionData.content}
						</Latex>
					</div>
					<div className={classes.tag}>
						{
							questionData.subject &&
							<Tag className={classes.tagItem} color="blue" onClick={() => Router.push(`/?subject=${questionData.subject}`)}>{questionData.subject}</Tag>
						}
						{
							questionData.class &&
							<Tag className={classes.tagItem} color="volcano" onClick={() => Router.push(`/?class=${questionData.class}`)}>{questionData.class}</Tag>
						}
						{
							questionData.hashtag &&
							questionData.hashtag.map((el, i) => {
								return <Tag className={classes.tagItem} key={i} color="purple" onClick={() => Router.push(`/?hashtag=${el}`)}>{el}</Tag>;
							})
						}
					</div>
					<div className={classes.imgWrapper}>
						<ImageLightBox
							className={classes.img}
							images={questionData.images}
						/>
					</div>
					<div className={classes.stats}>
						<div className={classes.right}>
							<Link route={'/question/' + questionData.id}>
								<a>{questionData.answersCount > 0 ? questionData.answersCount + ' câu trả lời' : 'Chưa có câu trả lời.'}</a>
							</Link>
						</div>
						<div className={classes.left}>
							<Tooltip title={questionData.savesCount > 0 ? questionData.savesCount + ' người đã lưu câu hỏi này' : 'Chưa có ai lưu câu hỏi này.'}>
								<div className={classes.social}>
									<Icon className={classes.icon} type="book" />
									<span>{questionData.savesCount || 0}</span>
								</div>
							</Tooltip>
							<Tooltip title={questionData.viewsCount > 0 ? questionData.viewsCount + ' lần xem câu hỏi này' : 'Chưa có ai lưu câu hỏi này.'}>
								<div className={classes.social}>
									<Icon className={classes.icon} type="eye-o" />
									<span>{questionData.viewsCount || 0}</span>
								</div>
							</Tooltip>
							<Tooltip title={questionData.reportsCount > 0 ? questionData.reportsCount + ' người báo cáo vi phạm câu hỏi này' : 'Chưa có ai báo cáo vi phạm câu hỏi này.'}>
								<div className={classes.social}>
									<Icon className={classes.icon} type="dislike-o" />
									<span>{questionData.reportsCount || 0}</span>
								</div>
							</Tooltip>
							{/* <div className={classes.social}>
								<Icon className={classes.icon} type="message" />
								<span>{questionData.answersCount || 0}</span>
							</div> */}
						</div>
					</div>
				</div>
				<div className={classes.footer}>
					<Link route={'/question/' + questionData.id}>
						<Button className={classes.button} icon="message">Trả lời</Button>
					</Link>
					{/* <BtnFollow questionId={questionData.id} /> */}
					<BtnSave questionData={questionData} showCount={false} />
					<BtnShare questionData={questionData} />
					<BtnSetting
						questionData={questionData}
					/>
				</div>
			</div>
		</div>
	);
};

QuestionCard.propTypes = {
	classes: PropTypes.object.isRequired,
	questionData: PropTypes.object,
	loading: PropTypes.bool,
};

QuestionCard.defaultProps = {
	loading: false,
	questionData: {},
};

export default withStyles(styles)(QuestionCard);
