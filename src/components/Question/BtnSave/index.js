/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 07:23:32
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { saveQuestion, unsaveQuestion } from 'src/redux/actions/questionSave';
import AuthStorage from 'src/utils/AuthStorage';
import withStyles from 'src/theme/jss/withStyles';
import classNames from 'classnames';

import { Button, Tooltip } from 'antd';

import CheckLogin from 'src/components/Form/CheckLogin';

const styleSheet = (/* theme */) => ({
	button: {
		border: 0,
		padding: '0 10px',
		background: '#f5f8ff',
	},
	highlightedButton: {
		color: '#266e63',
	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			saveQuestion,
			unsaveQuestion,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnSave extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionData: PropTypes.object,
		showCount: PropTypes.bool,
		action: PropTypes.shape({
			saveQuestion: PropTypes.func.isRequired,
			unsaveQuestion: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		questionData: { saves: [] },
		showCount: true,
	};

	state = {
		isSaved: this.props.questionData.saves && this.props.questionData.saves[0] && this.props.questionData.saves[0].creatorId === AuthStorage.userId,
		savedId: this.props.questionData.saves && this.props.questionData.saves[0] && this.props.questionData.saves[0].id,
		savesCount: this.props.questionData.savesCount || 0,
	}

	handleBtnClick = () => {
		const { questionData: { id: questionId }, action } = this.props;
		const creatorId = AuthStorage.userId;
		if (!this.state.isSaved) {
			action.saveQuestion({ questionId, creatorId }, (response) => {
				this.setState(prevState => {
					return {
						isSaved: true,
						savedId: response.id,
						savesCount: prevState.savesCount + 1,
					};
				});
			});
		} else if (this.state.savedId) {
			action.unsaveQuestion({ savedQuestionId: this.state.savedId }, () => {
				this.setState(prevState => {
					return {
						isSaved: false,
						savedId: '',
						savesCount: prevState.savesCount - 1,
					};
				});
			});
		}
	}

	render() {
		const { classes, showCount } = this.props;

		return (
			<Tooltip title={!this.state.isSaved ? 'Lưu câu hỏi' : 'Bỏ lưu câu hỏi'}>
				<CheckLogin onClick={this.handleBtnClick}>
					<Button
						className={classNames(classes.button, {
							[classes.highlightedButton]: this.state.isSaved,
						})}
						icon="book"
					>
						{this.state.isSaved ? 'Đã Lưu' : 'Lưu'} {showCount && '(' + this.state.savesCount + ')'}
					</Button>
				</CheckLogin>
			</Tooltip>
		);
	}
}
