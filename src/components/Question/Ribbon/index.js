/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-30 15:02:33
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import { Tooltip } from 'antd';

const styleSheet = (/* theme */) => ({
	wrapper: {
		position: 'absolute',
		top: '-3px',
		right: '20px',
		cursor: 'pointer',
		'& img': {
			width: '20px',
			height: '20px',
			position: 'absolute',
			zIndex: '1',
			top: '10px',
			left: '2px',
		},
	},
	text: {
		top: '15px',
		position: 'absolute',
		color: '#fff',
		zIndex: '1',
		fontSize: '9px',
		textAlign: 'center',
		width: '100%',
	},
	root: {
		position: 'relative',
		zIndex: 1,
		width: '24px',
		height: '40px',
		background: '#ff403d',
		'&:before': {
			width: '0',
			right: '-3px',
			height: '0',
			content: '""',
			position: 'absolute',
			borderRight: '3px solid transparent',
			borderBottom: '3px solid #ca3011',
		},
		'&:after': {
			width: '0',
			height: '0',
			bottom: '-15px',
			content: '""',
			position: 'absolute',
			borderLeft: '12px solid #ff403d',
			borderRight: '12px solid #ff403d',
			borderBottom: '15px solid transparent',
		},
	},
	free: {
		background: '#178fff',
		'&:before': {
			borderBottom: '3px solid #306ca7',
		},
		'&:after': {
			borderLeft: '12px solid #178fff',
			borderRight: '12px solid #178fff',
		},
	},
});

const RibbonQuestion = (props) => {
	const { classes, type } = props;

	if (type === 'coin') {
		return (
			<Tooltip title="Câu hỏi tốn xu">
				<div className={classes.wrapper}>
					<div className={classes.root} />
					<img src="/static/assets/images/coins.png" alt="coins" />
				</div>
			</Tooltip>
		);
	}

	return (
		<Tooltip title="Câu hỏi miễn phí">
			<div className={classes.wrapper}>
				<div className={classes.root + ' ' + classes.free} />
				<div className={classes.text}>
					Free
				</div>
			</div>
		</Tooltip>
	);
};

RibbonQuestion.propTypes = {
	classes: PropTypes.object.isRequired,
	type: PropTypes.string,
};

RibbonQuestion.defaultProps = {
	type: 'free',
};

export default withStyles(styleSheet)(RibbonQuestion);
