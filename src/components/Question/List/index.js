/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 11:58:01
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';


import withStyles from 'src/theme/jss/withStyles';

import QuestionCard from 'src/components/Question/Card';

import QuestionListLoading from './Loading';

const styleSheet = (/* theme */) => ({
	list: {
		position: 'relative',
	},
	notFound: {
		padding: '50px 0',
		background: '#fff',
		textAlign: 'center',
		marginBottom: '20px',
	},
});

@withStyles(styleSheet)
export default class QuestionList extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionList: PropTypes.object,
		loadingMore: PropTypes.bool,
		loading: PropTypes.bool,
		pin: PropTypes.bool,
	}

	static defaultProps = {
		questionList: { data: [] },
		loadingMore: false,
		loading: false,
		pin: false,
	}

	render() {
		const { classes, questionList, loadingMore, loading, pin } = this.props;

		return (
			<div className={classes.list}>
				{
					questionList.total === 0 && !loading &&
					<div className={classes.notFound}>
						Không tìm thấy câu hỏi nào.
					</div>
				}

				{
					loading ?
						<QuestionListLoading /> :
						questionList.data.map((question) => {
							if (question.pin && pin) {
								return null;
							}
							return (
								<QuestionCard key={question.id} questionData={question} />
							);
						})
				}
				{
					loadingMore && <QuestionListLoading />
				}
			</div>
		);
	}
}
