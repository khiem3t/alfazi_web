/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 23:58:08
*------------------------------------------------------- */

import React from 'react';
// import PropTypes from 'prop-types';

import QuestionCard from 'src/components/Question/Card';

const QuestionListLoading = (/* props */) => {
	return (
		<div>
			{
				[0, 0, 0, 0].map((question, i) => {
					return (
						<QuestionCard key={i} loading />
					);
				})
			}
		</div>
	);
};

QuestionListLoading.propTypes = {
	// classes: PropTypes.object.isRequired,
};

QuestionListLoading.defaultProps = {
	// classes: {},
};

export default QuestionListLoading;
