/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-02 09:32:05
*------------------------------------------------------- */

import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import Router from 'next/router';

import { Button, Menu, Dropdown, Spin, Icon, notification, Modal } from 'antd';

import MdKeyboardControl from 'react-icons/lib/md/keyboard-control';
import FaFlag from 'react-icons/lib/fa/flag';
import FaEyeSlash from 'react-icons/lib/fa/eye-slash';
import FaEye from 'react-icons/lib/fa/eye';

import QuestionCreateBox from 'src/components/Question/QuestionCreateBox';
import ModalReport from 'src/components/Modals/Report';

import { updateQuestion, deleteQuestion, pinQuestion } from 'src/redux/actions/question';

const styleSheet = (/* theme */) => ({
	root: {
		height: 32,
	},
	btn: {
		background: 'transparent',
		border: 0,
		fontSize: 20,
		'& svg': {
			marginTop: '-5px',
		},
		'&:hover': {
			background: 'transparent',
		},
	},
	icon: {
		marginRight: 15,
	},
});

function mapStateToProps(state) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			updateQuestion,
			deleteQuestion,
			pinQuestion,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnSetting extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionData: PropTypes.object.isRequired,
		node: PropTypes.node,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			updateQuestion: PropTypes.func.isRequired,
			deleteQuestion: PropTypes.func.isRequired,
			pinQuestion: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		node: undefined,
	}

	state = {
		loading: false,
		openQuestionCreateBox: false,
		openModalReport: false,
	}

	handleHideQuestion = () => {
		Modal.confirm({
			title: 'Nếu ẩn câu hỏi này thì không ai có thể nhìn thấy nó. Bạn có chắc chắn muốn ẩn không?',
			onOk: () => {
				this.setState({
					loading: true,
				});
				this.props.action.updateQuestion({
					status: 'hidden',
					updatedAt: new Date(),
					id: this.props.questionData.id,
				}, () => {
					this.setState({
						loading: false,
					}, () => {
						notification.success({
							message: 'Chúc mừng!',
							description: 'Bạn đã ẩn câu hỏi thành công.',
						});
					});
				}, () => {
					this.setState({
						loading: false,
					});
				});
			},
		});
	}
	handleDeleteQuestion = () => {
		Modal.confirm({
			title: 'Bạn có chắc chắn muốn xóa câu hỏi này không?',
			onOk: () => {
				this.setState({
					loading: true,
				});
				this.props.action.deleteQuestion({
					id: this.props.questionData.id,
				}, () => {
					this.setState({
						loading: false,
					}, () => {
						notification.success({
							message: 'Chúc mừng!',
							description: 'Bạn đã xóa câu hỏi thành công.',
						});
						if (Router.router && Router.router.pathname === '/question') {
							Router.push('/');
						}
					});
				}, () => {
					this.setState({
						loading: false,
					});
				});
			},
		});
	}

	handleShowQuestion = () => {
		Modal.confirm({
			title: 'Bạn có chắc chắn muốn hiển thị câu hỏi này cho tất cả mọi người không?',
			onOk: () => {
				this.setState({
					loading: true,
				});
				this.props.action.updateQuestion({
					status: 'active',
					updatedAt: new Date(),
					id: this.props.questionData.id,
				}, () => {
					this.setState({
						loading: false,
					}, () => {
						notification.success({
							message: 'Chúc mừng!',
							description: 'Bạn đã hiện câu hỏi thành công.',
						});
					});
				}, () => {
					this.setState({
						loading: false,
					});
				});
			},
		});
	}

	handleSelectMenu = ({ key }) => {
		if (key === 'edit') {
			this.setState({
				openQuestionCreateBox: true,
			});
		}
		if (key === 'report') {
			this.setState({
				openModalReport: true,
			});
		}
		if (key === 'hide') {
			this.handleHideQuestion();
		}
		if (key === 'delete') {
			this.handleDeleteQuestion();
		}
		if (key === 'show') {
			this.handleShowQuestion();
		} if (key === 'pin') {
			this.handlePinQuestion();
		} if (key === 'pin-remove') {
			this.handlePinRemove();
		}
	}

	handlePinRemove = () => {
		this.setState({
			loading: true,
		});

		this.props.action.pinQuestion({}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	handlePinQuestion = () => {
		this.setState({
			loading: true,
		});

		this.props.action.pinQuestion({
			id: this.props.questionData.id,
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	handleCloseModal = () => {
		this.setState({
			openModalReport: false,
		});
	}


	render() {
		const { classes, node, questionData = {} } = this.props;

		const menu = (
			<Menu onClick={this.handleSelectMenu}>
				{
					AuthStorage.userId !== questionData.creatorId &&
					<Menu.Item key="report">
						<FaFlag className={classes.icon} /> Báo cáo vi phạm
					</Menu.Item>
				}
				{
					['mod', 'admin'].includes(AuthStorage.role) &&
					!questionData.pin &&
					<Menu.Item key="pin">
						<Icon type="pushpin" className={classes.icon} /> Ghim bài viết
					</Menu.Item>
				}
				{
					['mod', 'admin'].includes(AuthStorage.role) &&
					questionData.pin &&
					<Menu.Item key="pin-remove">
						<Icon type="pushpin" className={classes.icon} /> Bỏ ghim
					</Menu.Item>
				}
				{
					AuthStorage.userId === questionData.creatorId && questionData.status === 'active' &&
					<Menu.Item key="edit">
						<Icon type="edit" className={classes.icon} /> Chỉnh sửa câu hỏi
					</Menu.Item>
				}
				{
					AuthStorage.userId === questionData.creatorId && questionData.status === 'active' &&
					<Menu.Item key="hide">
						<FaEyeSlash className={classes.icon} /> Ẩn câu hỏi
					</Menu.Item>
				}
				{
					AuthStorage.userId === questionData.creatorId && questionData.status === 'active' &&
					<Menu.Item key="delete">
						<Icon type="delete" className={classes.icon} /> Xóa câu hỏi
					</Menu.Item>
				}
				{
					AuthStorage.userId === questionData.creatorId && questionData.status === 'hidden' &&
					<Menu.Item key="show">
						<FaEye className={classes.icon} /> Hiện câu hỏi
					</Menu.Item>
				}
			</Menu>
		);

		return (
			<Fragment>
				<Dropdown overlay={menu} trigger={['click']} className={classes.root}>
					{
						this.state.loading ?
							<Spin /> :
							<a className="ant-dropdown-link">
								{
									node ||
									<Button shape="circle" className={classes.btn}>
										<MdKeyboardControl />
									</Button>
								}
							</a>
					}
				</Dropdown>
				{
					this.state.openQuestionCreateBox &&
					<QuestionCreateBox
						questionData={questionData}
						onClose={() => this.setState({
							openQuestionCreateBox: false,
						})}
						style={{ position: 'absolute', width: '100%', zIndex: 5, top: 0, left: 0 }}
					/>
				}
				{
					this.state.openModalReport &&
					<ModalReport
						visible={this.state.openModalReport}
						onCancel={this.handleCloseModal}
						destroyOnClose
						type="question"
						questionData={questionData}
					/>
				}
			</Fragment>
		);
	}
}
