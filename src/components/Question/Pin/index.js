/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 11:58:01
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';


import withStyles from 'src/theme/jss/withStyles';

import QuestionCard from 'src/components/Question/Card';

import QuestionListLoading from 'src/components/Question/List/Loading';

const styleSheet = (/* theme */) => ({
	list: {
		position: 'relative',
	},
	notFound: {
		padding: '50px 0',
		background: '#fff',
		textAlign: 'center',
		marginBottom: '20px',
	},
});

@withStyles(styleSheet)
export default class PinList extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionList: PropTypes.object,
		loadingMore: PropTypes.bool,
		loading: PropTypes.bool,
	}

	static defaultProps = {
		questionList: { data: [] },
		loadingMore: false,
		loading: false,
	}

	render() {
		const { classes, questionList, loadingMore, loading } = this.props;

		return (
			<div className={classes.list}>
				{
					questionList.total === 0 && !loading &&
					<div />
				}

				{
					loading ?
						<QuestionListLoading /> :
						questionList.data.map((question) => {
							return (
								<QuestionCard key={question.id} questionData={question} />
							);
						})
				}
				{
					loadingMore && <QuestionListLoading />
				}
			</div>
		);
	}
}
