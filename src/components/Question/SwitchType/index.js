/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-31 09:37:32
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import { Switch } from 'antd';

import { MINIMUM_TIME_TO_UNLOCK, QUESTION_FEE, QUESTION_FEE_REFUND } from 'src/constants/parameters';

const styleSheet = (/* theme */) => ({
	root: {
		display: 'flex',
		alignItems: 'center',
	},
	text: {
		marginLeft: 15,
		lineHeight: '20px',
	},
	switch: {
		height: '44px',
		minWidth: '110px',
		lineHeight: '20px',
		// marginBottom: '20px',
		'& .ant-switch-inner': {
			fontSize: '20px',
			lineHeight: '40px',
			marginLeft: '52px',
			marginRight: '6px',
			display: 'block',
			height: '100%',
			'& img': {
				height: '35px',
			},
		},
		'&:after': {
			width: '40px',
			height: '40px',
			borderRadius: '40px',
			right: 'auto',
		},
		'&.ant-switch-checked .ant-switch-inner': {
			marginLeft: '20px',
		},
		'&.ant-switch-checked:before, &.ant-switch-checked:after': {
			left: '100%',
			marginLeft: '-40px',
		},
	},
});

@withStyles(styleSheet)
export default class SwitchType extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		onChange: PropTypes.func,
		value: PropTypes.string,
	}

	static defaultProps = {
		onChange: f => f,
		value: 'free',
	}

	state = {
		checked: this.props.value !== 'free',
	}

	handleChange = (checked) => {
		this.setState({
			checked,
		});
		this.props.onChange(checked ? 'coin' : 'free');
	}

	render() {
		const { classes, onChange, value, ...rest } = this.props;

		return (
			<div className={classes.root}>
				<Switch
					{...rest}
					checkedChildren={<img src="/static/assets/images/coins.png" alt="" />}
					unCheckedChildren={<div>Free</div>}
					checked={this.state.checked}
					className={classes.switch}
					onChange={this.handleChange}
				/>
				{
					this.state.checked ?
						<div className={classes.text}>
							Bạn sẽ mất {QUESTION_FEE} xu để đăng câu hỏi này, sau khi chọn được câu trả lời chính xác nhất cho câu hỏi này, bạn sẽ được trả lại {QUESTION_FEE_REFUND} xu.
						</div> :
						<div className={classes.text}>
							Câu hỏi miễn phí, bạn sẽ phải đợi {MINIMUM_TIME_TO_UNLOCK} phút để có thể xem câu trả lời cho câu hỏi loại này.
						</div>
				}
			</div>
		);
	}
}
