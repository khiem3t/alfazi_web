/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 07:23:32
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import { Button } from 'antd';
import AuthStorage from 'src/utils/AuthStorage';
import { createFollowQuestion, deleteFollowQuestion } from 'src/redux/actions/questionFollow';

const styleSheet = (/* theme */) => ({
	button: {
		border: 0,
	},
	test: {
		color: 'blue',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			createFollowQuestion,
			deleteFollowQuestion,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnFollow extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionId: PropTypes.string.isRequired,
		action: PropTypes.shape({
			createFollowQuestion: PropTypes.func.isRequired,
			deleteFollowQuestion: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	render() {
		const { classes } = this.props;
		return (
			<Button
				className={classes.button}
				icon="star-o"
				onClick={this.handleFollow}
			>
				Theo dõi
			</Button>
		);
	}
}
