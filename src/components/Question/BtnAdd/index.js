/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-02 00:26:56
*------------------------------------------------------- */

import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import CheckLogin from 'src/components/Form/CheckLogin';
import QuestionCreateBox from 'src/components/Question/QuestionCreateBox';

const styleSheet = (theme) => ({
	btn: {
		[theme.breakpoints.up.lg]: {
			marginBottom: 10,
		},
		width: '100%',
		display: 'flex',
		marginBottom: '5px',
		alignContent: 'center',
		background: '#fff',
		border: '1px solid #e8e8e8',
		padding: '20px',
		cursor: 'text',
	},
	text: {
		flex: '1',
		display: 'flex',
		alignItems: 'center',
		marginLeft: '15px',
		color: '#90949c',
		// fontStyle: 'italic',
	},
});

@withStyles(styleSheet)
export default class BtnAddQuestion extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		onRefresh: PropTypes.func,
		node: PropTypes.node,
		type: PropTypes.string,
	}

	static defaultProps = {
		onRefresh: f => f,
		node: undefined,
		type: 'free',
	}

	state = {
		btnAddVisible: true,
	}

	toggleBtnVisibility = () => {
		this.setState({ btnAddVisible: !this.state.btnAddVisible });
	}

	render() {
		const { node, classes, onRefresh, type } = this.props;

		return (
			<Fragment>
				{
					this.state.btnAddVisible &&
					<CheckLogin onClick={this.toggleBtnVisibility}>
						{
							node ||
							<div className={classes.btn} >
								<div className={classes.img}>
									<img src="/static/assets/images/question.png" alt="Question" />
								</div>
								<div className={classes.text}>
									Hãy để mọi người giải đáp thắc mắc của bạn. Đặt câu hỏi ngay.
								</div>
							</div>
						}
					</CheckLogin>
				}
				{ !this.state.btnAddVisible &&
					<QuestionCreateBox
						type={type}
						onClose={this.toggleBtnVisibility}
						onRefresh={onRefresh}
						style={{ marginBottom: 10 }}
					/>
				}
			</Fragment>
		);
	}
}

