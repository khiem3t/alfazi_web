/* --------------------------------------------------------
* Author Nguyễn Đăng Anh Thi
* Email nguyendanganhthi247@gmail.com
*------------------------------------------------------- */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Cropper from 'cropperjs';
import withStyles from 'src/theme/jss/withStyles';
import { Button, message, Tooltip } from 'antd';
import RotateLeft from 'react-icons/lib/fa/rotate-left';

const styleSheet = (/* theme */) => ({
	root: {
		position: 'fixed',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		background: 'rgba(0, 0, 0, 0.8)',
		zIndex: 1000,
		height: '100%',
	},
	action: {
		boxSizing: 'border-box',
		position: 'absolute',
		width: '100%',
		display: 'flex',
		justifyContent: 'center',
		bottom: 50,
		'& button': {
			marginLeft: '15px',
		},
	},
	rotateIcon: {
		width: 50,
		height: 50,
		borderRadius: '50%',
		position: 'fixed',
		zIndex: 200,
		right: '5vw',
		top: '5vh',
		background: '#00D88D',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		cursor: 'pointer',
	},
});

@withStyles(styleSheet)
export default class ImageCropper extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		src: PropTypes.string,
		name: PropTypes.string,
		onCrop: PropTypes.func.isRequired,
		onDefault: PropTypes.func.isRequired,
		type: PropTypes.string,
	}

	static defaultProps = {
		src: '',
		name: '',
		type: 'image/jpeg',
	}

	componentDidMount() {
		message.info('Kéo thả chuột để tùy chỉnh ảnh', 2);
		this.cropper = new Cropper(this.img, {
			checkOrientation: false,
			dragMode: 'move',
			viewMode: 1,
			aspectRatio: 16 / 9,
			background: false,
			autoCropArea: 0.9,
			minCropBoxWidth: 400,
			minCropBoxHeight: 300,
			minCanvasWidth: 300,
			minCanvasHeight: 200,
		});
	}

	componentWillUnmount() {
		if (this.img) {
			this.cropper.destroy();
			delete this.img;
			delete this.cropper;
		}
	}

	crop() {
		const canvas = this.cropper.getCroppedCanvas();
		return new Promise(resolve => {
			canvas.toBlob(blob => {
				resolve(new File([blob], this.props.name, { 'type': this.props.type })); // eslint-disable-line
			});
		});
	}

	rotate = () => {
		this.cropper.rotate(-90);
	}

	render() {
		const { classes, src, onCrop, onDefault } = this.props;
		return (
			<div className={classes.root}>
				<Tooltip title="xoay 90 độ" placement="bottom">
					<span className={classes.rotateIcon} onClick={this.rotate}>
						<RotateLeft color="#fff" size={25} />
					</span>
				</Tooltip>
				<img src={src} ref={node => { this.img = node; }} alt="broken" />
				<div className={classes.action}>
					<Button type="primary" onClick={() => onCrop(this.crop())}>
						Cắt ảnh theo khung
					</Button>
					<Button type="primary" onClick={() => onDefault()}>
						Để mặc định
					</Button>
				</div>
			</div>
		);
	}
}
