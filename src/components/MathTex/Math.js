/* eslint-disable */
/* --------------------------------------------------------
* Author Nguyễn Đăng Anh Thi
* Email nguyendanganhthi247@gmail.com
*------------------------------------------------------- */

import { Component } from 'react';
import PropTypes from 'prop-types';
import katex from 'katex';

export default class Math extends Component {
	constructor(props) {
		super(props);
		this.usedProp = props.math ? 'math' : 'children';
		this.state = this.createNewState(null, props);
	}

	createNewState(prevState, props) {
		try {
			const html = this.generateHtml(props);
			return { html, error: undefined };
		} catch (error) {
			return { error, html: undefined };
		}
	}

	generateHtml(props) {
		const { displayMode, errorColor, renderError } = props;
		return katex.renderToString(
			props[this.usedProp],
			{ displayMode, errorColor, throwOnError: Boolean(renderError) },
		);
	}

	render() {
		if (this.state.html) {
			return <span dangerouslySetInnerHTML={{__html: this.state.html}} />;
		}

		if	(this.props.renderError) {
			return this.props.renderError(this.state.error);
		}

		throw this.state.error;
	}
}

Math.propTypes = {
	children: PropTypes.string,
	math: PropTypes.string,
	renderError: PropTypes.func,
	displayMode: PropTypes.bool,
};
