/* --------------------------------------------------------
* Author Nguyễn Đăng Anh Thi
* Email nguyendanganhthi247@gmail.com
*------------------------------------------------------- */
/* eslint-disable */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import renderMathInElement from './lib/auto-render';

export default class RenderMath extends Component {
	static propTypes = {
		children: PropTypes.node.isRequired,
	}

	isDOMNode(node) {
		 return node && (node.nodeType === 1 || node.nodeType == 11);
	}


	componentDidMount() {
		let mathContainer;
		if (this.isDOMNode(this.mathContainer)) {
			mathContainer = this.mathContainer;
		} else {
			mathContainer = ReactDOM.findDOMNode(this.mathContainer);
		}

		console.log('mathContainer', mathContainer);
		const textContainMathRegex = /\$(.*?)\$/gm;
		// const textContainMathRegex = /\(.*?\)/gm;
		if (!mathContainer.textContent.match(textContainMathRegex)) {
			
			return;
		}
		

	 	if (mathContainer) {
			renderMathInElement(
				mathContainer,
				{
					delimiters: [
						{ left: '$', right: '$', display: false },
						// {left: "\\(", right: "\\)", display: false},
					],
				},
			);
		}

	}

	render() {
		return (
			<React.Fragment>
				{React.Children.map(this.props.children, (child, i) => {
					if (i > 0) {
						throw new TypeError('RenderMath accept only one single child');
					}
					return React.cloneElement(child, { ref: (node) => this.mathContainer = node, });
				})}
			</React.Fragment>
		);
	}
}

