/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-06 01:56:03
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AuthStorage from 'src/utils/AuthStorage';

import { Tooltip, Icon } from 'antd';

import { createAnswerPick, deleteAnswerPick } from 'src/redux/actions/answerPick';

import CheckLogin from 'src/components/Form/CheckLogin';

import withStyles from 'src/theme/jss/withStyles';

const styleSheet = (/* theme */) => ({
	root: {
		color: '#f32929',
		'&:hover': {
			color: '#f32929',
		},
	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			createAnswerPick,
			deleteAnswerPick,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnPick extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		answerData: PropTypes.object,
		myQuestion: PropTypes.bool,
		questionHasPick: PropTypes.bool,
		questionType: PropTypes.string,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			createAnswerPick: PropTypes.func.isRequired,
			deleteAnswerPick: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		answerData: { likes: [] },
		myQuestion: false,
		questionHasPick: false,
		questionType: 'free',
	}

	state = {
		hasPick: !!this.props.answerData.pick && !!this.props.answerData.pick.id,
	}

	handlePick = () => {
		if (this.props.answerData.id) {
			if (!this.state.hasPick) {
				this.props.action.createAnswerPick({
					creatorId: AuthStorage.userId,
					receiverId: this.props.answerData.creatorId,
					questionId: this.props.answerData.questionId,
					questionType: this.props.questionType,
					answerId: this.props.answerData.id,
				}, () => {
					this.setState({
						hasPick: !this.state.hasPick,
					});
				});
			} else {
				// this.props.action.deleteAnswerPick({
				// 	id: this.props.answerData.pick.id,
				// }, () => {
				// 	this.setState({
				// 		hasPick: !this.state.hasPick,
				// 	});
				// });
			}
		}
	}

	render() {
		const { classes, myQuestion, questionHasPick, answerData } = this.props;

		if (answerData.creatorId === AuthStorage.userId) {
			return null;
		}

		if (!myQuestion || questionHasPick) {
			return this.state.hasPick ? (
				<Tooltip title="Câu trả lời này đã được người đặt câu hỏi đánh dấu là chính xác">
					<a className={classes.root}>
						<Icon type="heart" />
					</a>
				</Tooltip>
			) : null;
		}

		return (
			<CheckLogin onClick={this.handlePick}>
				<Tooltip title={!this.state.hasPick ? 'Đánh dấu câu trả lời đúng, Bạn chỉ được phép chọn duy nhất một câu trả lời đúng.' : 'Bỏ đánh dấu'}>
					<a className={classes.root}>
						{
							this.state.hasPick ?
								<Icon type="heart" /> :
								<Icon type="heart-o" />
						}
					</a>
				</Tooltip>
			</CheckLogin>
		);
	}
}
