/* eslint jsx-a11y/no-noninteractive-element-interactions: 0 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Select, Input, Button } from 'antd';

import withStyles from 'src/theme/jss/withStyles';
import Math from 'src/components/MathTex/Math';
import renderMathInElement from 'src/components/MathTex/lib/auto-render';
import mathExpressions from 'src/components/MathTex/mathExpression';
import mathLatex from 'src/components/Answer/MathEditor/mathLatex';


const styleSheet = (/* theme */) => ({
	root: {
		backgroundColor: '#68e4d194',
		padding: '10px',
		// borderRadius: '5px',
	},
	keyboardWrap: {
		backgroundColor: '#fff',
		padding: '10px',
		// borderRadius: '5px',
	},
	keyboard: {
		display: 'flex',
		alignSelf: 'center',
		flexFlow: 'row wrap',
		marginTop: '1.125rem',
		marginBottom: '1.125rem',

	},
	keyboardItem: {
		'& .katex': {
			fontSize: 'initial',
			fontWeight: '600',
		},
		minHeight: '2.25rem',
		marginRight: '3px',
		marginBottom: '0.375rem',
		boxShadow: '0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12)',
		border: '1px solid #f9f9f9',
		display: 'inline-block',
		WebkitBorderRadius: '5px',
		cursor: 'pointer',
		padding: '10px',
		'&:hover': {
			position: 'relative',
			top: '1px',
			left: '1px',
			borderColor: '#e5e5e5',
			cursor: 'pointer',
		},
	},
	answerHeader: {
		padding: '10px 0',

	},
	renderingBox: {
		marginTop: '10px',
		padding: '10px',
		fontSize: '17px',
		whiteSpace: 'pre-line',
		maxHeight: '100px',
		overflowY: 'scroll',
	},
	mathShow: {
		width: '100%',
		border: 'none',
		fontSize: 'x-large',
	},
	preview: {
		minHeight: '100px',
		backgroundColor: '#eee',
		marginBottom: '10px',
		padding: '10px',
		// borderRadius: '5px',
	},
	actionMathEditor: {
		marginTop: '10px',
		textAlign: 'right',
	},
});

@withStyles(styleSheet)
export default class MathEditor extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		style: PropTypes.object,
		onSubmit: PropTypes.func.isRequired,
		onCancel: PropTypes.func.isRequired,
	}

	static defaultProps = {
		style: {},
	}

	constructor(props) {
		super(props);
		this.state = {
			selectedExpressions: Object.values(mathExpressions)[0],
			content: '',
		};
		this.handleSelectChange = this.handleSelectChange.bind(this);
	}

	componentDidMount() {
		const mathFieldSpan = document.getElementById('math-field');
		const latexSpan = document.getElementById('latex');
		const MQ = MathQuill.getInterface(2);
		const mathField = MQ.MathField(mathFieldSpan, {
			spaceBehavesLikeTab: true,
			handlers: {
				edit() {
					let value = mathField.latex();
					value = value.replace(/<>/g, '\\ne');

					mathField.focus();
					latexSpan.textContent = value; // simple API
				},
			},
		});
		this.mathField = mathField;
	}

	componentDidUpdate() {
		if (this.renderingBox) {
			renderMathInElement(
				this.renderingBox,
				{
					delimiters: [
						{ left: '$', right: '$', display: false },
					],
				},
			);
		}
	}

    resset = () => {
    	this.mathField.latex('');
    	this.props.onCancel();
    }

	handleSelectChange = (option) => {
		this.setState({
			selectedExpressions: mathExpressions[option],
		});
	}

	handleMathKeyboardClick(key) {
		const expression = mathLatex[key];
		this.setState(prevState => {
			return {
				content: prevState.content + '$' + expression + '$',
			};
		});
		this.mathField.cmd(expression);
		this.mathField.focus();
	}


	handlerInserContent = () => {
		let content = this.mathField.latex() || '';
		content = '$' + content + '$'; // eslint-disable-line no-useless-escape
		this.props.onSubmit(content);
	}


	render() {
		const { classes, style } = this.props;

		const mathExpressionOptions = Object.keys(mathExpressions);

		return (
			<div className={classes.root} style={style}>
				<div className={classes.preview}>
					<div className={classes.answerHeader}>Nhập vào đây</div>
					<span id="math-field" className={classes.mathShow} />
					<p>LaTeX: <span id="latex" /></p>
				</div>
				<div className={classes.keyboardWrap}>
					<Select
						style={{ width: 200 }}
						defaultValue={mathExpressionOptions[0]}
						onChange={this.handleSelectChange}
					>
						{
							mathExpressionOptions.map(title => (
								<Select.Option value={title} key={title}>{title}</Select.Option>
							))
						}
					</Select>
					<div className={classes.keyboard}>
						{
							Object.keys(this.state.selectedExpressions).map(key => (
								<div
									className={classes.keyboardItem}
									key={key}
									onClick={() => this.handleMathKeyboardClick(key)}
								>
									<Math math={this.state.selectedExpressions[key]} />
								</div>
							))
						}
					</div>
				</div>
				<div className={classes.actionMathEditor}>
					<Button loading={this.state.loading} onClick={() => this.handlerInserContent()} type="primary">Thêm vào</Button> {' '}
					<Button loading={this.state.loading} onClick={() => this.resset()}>Hủy</Button>
				</div>
			</div>
		);
	}
}
