/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-02 00:26:56
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Form, Input, Button, Modal, Upload, Icon, Tabs } from 'antd';
import Latex from 'react-latex';

import MathEditor from 'src/components/Answer/MathEditor';
import FaSubscript from 'react-icons/lib/fa/subscript';


import { updateAnswer } from 'src/redux/actions/answer';
import { uploadFiles } from 'src/redux/actions/upload';

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			updateAnswer,
			uploadFiles,
		}, dispatch),
	};
};

@Form.create()
@connect(mapStateToProps, mapDispatchToProps)
export default class ModalEdit extends PureComponent {
	static propTypes = {
		form: PropTypes.object.isRequired,
		answerData: PropTypes.object,
		onRefresh: PropTypes.func,
		onCancel: PropTypes.func,
		visible: PropTypes.bool.isRequired,

		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			updateAnswer: PropTypes.func.isRequired,
			uploadFiles: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		onRefresh: f => f,
		answerData: undefined,
		onCancel: f => f,
	}

	state = {
		loading: false,
		fileList: [],
		mathEditorVisible: false,
	}

	componentDidMount() {
		if (this.props.answerData && this.props.answerData.id) {
			this.props.form.setFieldsValue({
				content: this.props.answerData.content || '',
			});
			this.setState({ // eslint-disable-line
				fileList: this.props.answerData.image ? [{
					uid: -1,
					name: this.props.answerData.image,
					status: 'done',
					url: this.props.answerData.image,
				}] : [],
			});
		}
	}

	onChangeTab = () => {
		const { form } = this.props;
		const contentText = form.getFieldValue('content') ? form.getFieldValue('content') : '';
		this.setState({ mathcontent: contentText });
	}

	handleChange = (val) => {
		this.setState({ fileList: [{ ...val.file, status: 'notDone' }] });
	}

	showMathEditorModal = () => {
		this.setState({
			mathEditorVisible: true,
		});
	}

	insertContent = (content) => {
		const { form } = this.props;
		const contentText = form.getFieldValue('content') ? form.getFieldValue('content') : '';
		form.setFieldsValue({
			content: contentText + content,
		});
		this.setState({ mathEditorVisible: false });
	}

	handelCancel = () => {
		this.setState({ mathEditorVisible: false });
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this.setState({
					loading: true,
				});

				const { fileList } = this.state;

				if (this.props.answerData && this.props.answerData.id) { // edit question
					this.props.action.uploadFiles({ files: fileList }, (imageList = []) => {
						const data = { ...values, updatedAt: new Date(), id: this.props.answerData.id, image: imageList[0] };

						this.props.action.updateAnswer(data, () => {
							this.setState({
								loading: false,
								mathEditorVisible: false,
							}, () => {
								this.props.onCancel();
								this.props.onRefresh();
								this.props.form.resetFields();
							});
						}, () => {
							this.setState({
								loading: false,
							});
						});
					}, () => {
						this.setState({
							loading: false,
						});
					});
				}
			}
		});
	}

	render() {
		const { form: { getFieldDecorator }, visible, answerData, ...rest } = this.props;
		const { mathEditorVisible } = this.state;
		return (
			<Modal
				title={(answerData ? 'Chỉnh sửa ' : 'Tạo ') + 'câu hỏi'}
				visible={visible}
				footer={null}
				maskClosable={!this.state.loading}
				closable={!this.state.loading}
				{...rest}
			>
				<Form layout="horizontal" onSubmit={this.handleSubmit}>
					<Tabs onChange={this.onChangeTab}>
						<Tabs.TabPane tab="Chỉnh sửa" key="1">
							<Form.Item>
								{getFieldDecorator('content', {
									rules: [{ required: true, message: 'Hãy nhập nội dung câu hỏi.' }],
								})(
									<Input.TextArea
										id="editor1"
										rows={6}
										placeholder="Nội dung câu hỏi"
									/>,
								)}
								<a onClick={() => this.showMathEditorModal()}>
									<FaSubscript />
								</a>
								{mathEditorVisible ? (
									<div>
										<MathEditor
											visible={this.state.mathEditorVisible}
											onChange={this.handleChangeText}
											onSubmit={this.insertContent}
											onCancel={this.handelCancel}
										/>
									</div>
								) : ''}
							</Form.Item>
						</Tabs.TabPane>
						<Tabs.TabPane tab="Xem trước" key="2">
							<div className=" pre-wrap">
								<Latex >
									{this.state.mathcontent}
								</Latex>
							</div>
						</Tabs.TabPane>
					</Tabs>


					<Upload
						// action="//jsonplaceholder.typicode.com/posts/"
						accept="image/*"
						listType="picture-card"
						fileList={this.state.fileList}
						onPreview={this.handlePreview}
						onChange={this.handleChange}
					>
						<div>
							<Icon type="plus" />
							<div className="ant-upload-text">Upload</div>
						</div>
					</Upload>

					<div className="text-right" style={{ marginTop: 30 }}>
						<Button size="large" type="primary" htmlType="submit" loading={this.state.loading}>
							Chỉnh sửa câu trả lời
						</Button>
					</div>
				</Form>
			</Modal>
		);
	}
}

