/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 11:17:28
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Countdown from 'react-countdown-now';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Latex from 'react-latex';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import { Button, Modal, Icon } from 'antd';

import AvatarBlock from 'src/components/User/AvatarBlock';
import BtnLike from 'src/components/Answer/BtnLike';
import BtnPick from 'src/components/Answer/BtnPick';
import BtnSetting from 'src/components/Answer/BtnSetting';
import RenderMath from 'src/components/MathTex/RenderMath';
import ImageLightBox from 'src/components/LightBox/ImageLightBoxContainer';
import BtnAddReply from 'src/components/Reply/BtnAdd';
import ReplyList from 'src/components/Reply/List';
import CheckLogin from 'src/components/Form/CheckLogin';

import { MINIMUM_TIME_TO_UNLOCK } from 'src/constants/parameters';

import { changeQuestionType } from 'src/redux/actions/question';

const styleSheet = (/* theme */) => ({
	item: {
		// display: 'flex',
		flex: 1,
		padding: '15px 20px 10px 20px',
		borderBottom: '1px solid #e8e8e8',
		'&:hover $btnSetting': {
			opacity: 1,
		},
	},
	itemContent: {
		marginLeft: '50px',
		flex: 1,
	},
	itemText: {
		marginTop: 5,
	},
	itemImg: {
		marginTop: '20px',
		'& img': {
			height: 100,
			maxWidth: '100%',
		},
	},
	itemAction: {
		display: 'flex',
		justifyContent: 'flex-end',
		flex: 1,
	},
	social: {
		marginLeft: 15,
	},
	top: {
		display: 'flex',
		alignItems: 'flex-start',
	},
	btnSetting: {
		opacity: 0,
		transition: '.3s opacity',
	},
	lock: {
		padding: '15px 20px 10px 20px',
		borderBottom: '1px solid #e8e8e8',
		position: 'relative',
		background: '#f5f8ff',
		textAlign: 'center',
		display: 'flex',
	},
	textFade: {
		flex: 1,
		marginLeft: 10,
		userSelect: 'none',
		color: 'transparent',
		cursor: 'pointer',
		position: 'relative',
		'&:after': {
			content: '"Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam. Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam."',
			textShadow: '0 0 12px rgba(0, 0, 0, 0.6)',
			display: 'block',
			textAlign: 'justify',
		},
	},
	nameFade: {
		flex: 1,
		marginLeft: 10,
		userSelect: 'none',
		color: 'transparent',
		cursor: 'pointer',
		position: 'relative',
		fontSize: 18,
		'&:after': {
			content: '"Khiem"',
			textShadow: '0 0 12px rgba(0, 0, 0, 0.6)',
			display: 'block',
			textAlign: 'justify',
		},
	},
	action: {
		position: 'absolute',
		top: '0',
		left: '0',
		bottom: '0',
		right: '0',
		textAlign: 'center',
		width: '100%',
		display: 'flex',
		justifyContent: 'center',
		flexDirection: 'column',
		alignItems: 'center',
	},
	lockText: {
		flex: 1,
	},
	lockAvatar: {
		width: '40px',
		height: '40px',
		borderRadius: '50%',
		background: 'rgba(0, 0, 0, 0.24)',
		boxShadow: '0px 1px 16px 0px grey',
	},
	footer: {
		marginTop: '20px',
		display: 'flex',
		alignContent: 'center',
	},
	itemBtnShowReply: {

	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			changeQuestionType,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class AnswerCard extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		answerData: PropTypes.object,
		loading: PropTypes.bool,
		myQuestion: PropTypes.bool,
		questionHasPick: PropTypes.bool,
		questionType: PropTypes.string,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			changeQuestionType: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		loading: false,
		myQuestion: false,
		questionHasPick: false,
		answerData: {},
		questionType: 'free',
	}

	state = {
		hidden: true,
		openReply: false,
		openReplyList: false,
	}

	handleComplete = () => {
		this.setState({
			hidden: false,
		});
	}

	handleChangeQuestionType = () => {
		if (this.props.myQuestion && this.props.questionType === 'free') {
			Modal.confirm({
				title: 'Bạn có chắc muốn chuyển câu hỏi này thành câu hỏi xu?',
				content: 'Việc này sẽ mất của bạn 6 xu.',
				okText: 'Yes',
				okType: 'danger',
				cancelText: 'No',
				onOk: () => {
					this.props.action.changeQuestionType({ id: this.props.answerData.questionId, type: 'coin' });
				},
			});
		}
	}

	handleReply = () => {
		this.setState({
			openReply: !this.state.openReply,
			openReplyList: !this.state.openReply,
		});
	}

	handleShowReply = () => {
		this.setState({
			openReplyList: !this.state.openReplyList,
		});
	}

	render() {
		const { classes, loading, answerData = {}, myQuestion, questionHasPick, questionType } = this.props;

		const { creator = {} } = answerData;

		if (loading) {
			return (
				<div className={classes.item} style={{ display: 'flex' }}>
					<div className="loading-block" style={{ width: 40, height: 40, borderRadius: 50 }} />
					<div className={classes.itemContent} style={{ marginLeft: '5px' }}>
						<div>
							<div className="loading-block" style={{ width: 200 }} />
						</div>
						<div className="loading-block" style={{ width: 100 }} />
						<div className={classes.itemText}>
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" style={{ width: '50%' }} />
						</div>
					</div>
				</div>
			);
		}

		if (answerData.creatorId !== AuthStorage.userId && this.state.hidden && questionType === 'free' && moment().diff(moment(answerData.createdAt), 'seconds') <= MINIMUM_TIME_TO_UNLOCK * 60) {
			return (
				<div className={classes.lock}>
					<div className={classes.lockAvatar} />
					<div className={classes.lockText}>
						<div className={classes.nameFade} />
						<div className={classes.textFade} />
					</div>
					<div className={classes.action}>
						<Countdown
							date={Date.now() + (((MINIMUM_TIME_TO_UNLOCK * 60) - moment().diff(moment(answerData.createdAt), 'seconds')) * 1000)}
							renderer={({ minutes, seconds, completed }) => {
								if (completed) {
									// Render a completed state
									return null;
								}
								return (
									<div>
										<div> Vì là câu hỏi miễn phí nên bạn sẽ được xem câu trả lời này trong {minutes} phút {seconds} giây.</div>
										<div>Hãy chuyển câu hỏi này thành câu hỏi xu để có thể xem câu trả lời ngay lập tức.</div>
										{
											myQuestion &&
											<Button type="primary" style={{ marginTop: 10 }} onClick={this.handleChangeQuestionType}>Chuyển thành câu hỏi xu</Button>
										}
									</div>
								);
							}}
							onComplete={this.handleComplete}
						/>

					</div>
				</div>
			);
		}

		return (
			<div className={classes.item}>
				<div className={classes.top}>
					<AvatarBlock
						userData={creator}
						date={answerData.createdAt}
						size={40}
					/>
					<BtnSetting
						className={classes.btnSetting}
						answerData={answerData}
					/>
				</div>

				<div className={classes.itemContent}>

					<div className={classes.itemText + ' pre-wrap'}>
						<Latex>
							{answerData.content}
						</Latex>
					</div>

					{
						answerData.image &&
						<div className={classes.itemImg}>
							<ImageLightBox img={answerData.image} alt={answerData.content} />
						</div>
					}

					<div className={classes.footer}>
						<div className={classes.itemBtnShowReply}>
							{
								answerData.repliesCount > 0 &&
								<a onClick={this.handleShowReply}>Xem {answerData.repliesCount} phản hồi {this.state.openReplyList ? <Icon type="up" /> : <Icon type="down" />}</a>
							}
						</div>

						<div className={classes.itemAction}>
							<div className={classes.social}>
								<BtnPick
									answerData={answerData}
									myQuestion={myQuestion}
									questionHasPick={questionHasPick}
									questionType={questionType}
								/>
							</div>
							<div className={classes.social}>
								<BtnLike
									answerData={answerData}
								/>
							</div>
							<div className={classes.social}>
								<CheckLogin onClick={this.handleReply}>
									<a><Icon type="enter" /> Trả lời</a>
								</CheckLogin>
							</div>
						</div>
					</div>
					{
						this.state.openReply &&
						<BtnAddReply answerData={answerData} />
					}
					{
						this.state.openReplyList &&
						<ReplyList answerData={answerData} />
					}
				</div>
			</div>
		);
	}
}

