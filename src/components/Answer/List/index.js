/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 11:58:01
*------------------------------------------------------- */

import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import isEqual from 'lodash/isEqual';

import { Button, Tooltip, Icon } from 'antd';
import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import AnswerCard from 'src/components/Answer/Card';

import { getAnswerList } from 'src/redux/actions/answer';

import BtnAddAnswer from 'src/components/Answer/BtnAdd';
import BtnSave from 'src/components/Question/BtnSave';
import BtnShare from 'src/components/Question/BtnShare';
import BtnOpenLogin from 'src/components/Form/BtnOpenLogin';
import BtnSetting from 'src/components/Question/BtnSetting';

import AnswerListLoading from './Loading';

const styleSheet = (/* theme */) => ({
	notFound: {
		padding: '50px 0',
		background: '#f5f8ff',
		textAlign: 'center',
		marginBottom: '20px',
	},
	answerList: {
		// padding: '0 20px',
		background: '#f5f8ff',
	},
	moreComment: {
		textAlign: 'center',
		padding: '20px 0',
	},
	header: {
		padding: '7px',
		background: '#f5f8ff',
		borderBottom: '1px solid #e8e8e8',
	},
	answerAction: {
		display: 'flex',
		justifyContent: 'space-around',
	},
	button: {
		background: 'transparent',
		border: '0',
		'&:hover, &:active, &:focus': {
			background: 'transparent',
		},
	},
	lock: {
		padding: '15px 20px 10px 20px',
		borderBottom: '1px solid #e8e8e8',
		position: 'relative',
		background: '#f5f8ff',
		textAlign: 'center',
		display: 'flex',
	},
	textFade: {
		flex: 1,
		marginLeft: 10,
		userSelect: 'none',
		color: 'transparent',
		cursor: 'pointer',
		position: 'relative',
		'&:after': {
			content: '"Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam. Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam."',
			textShadow: '0 0 12px rgba(0, 0, 0, 0.6)',
			display: 'block',
			textAlign: 'justify',
		},
	},
	nameFade: {
		flex: 1,
		marginLeft: 10,
		userSelect: 'none',
		color: 'transparent',
		cursor: 'pointer',
		position: 'relative',
		fontSize: 18,
		'&:after': {
			content: '"Khiem"',
			textShadow: '0 0 12px rgba(0, 0, 0, 0.6)',
			display: 'block',
			textAlign: 'justify',
		},
	},
	action: {
		position: 'absolute',
		top: '0',
		left: '0',
		bottom: '0',
		right: '0',
		textAlign: 'center',
		width: '100%',
		display: 'flex',
		justifyContent: 'center',
		flexDirection: 'column',
		alignItems: 'center',
	},
	lockText: {
		flex: 1,
	},
	lockAvatar: {
		width: '40px',
		height: '40px',
		borderRadius: '50%',
		background: 'rgba(0, 0, 0, 0.24)',
		boxShadow: '0px 1px 16px 0px grey',
	},
	stats: {
		display: 'flex',
		justifyContent: 'space-between',
		borderBottom: '1px solid #e8e8e8',
		alignItem: 'center',
		background: '#fff',
		padding: '0 20px',
	},
	left: {
		borderTop: '1px solid #e8e8e8',
		display: 'flex',
		padding: '10px',
		alignItems: 'center',
	},
	right: {
		flex: 1,
		borderTop: '1px solid #e8e8e8',
		padding: '10px',
		'& a': {
			color: 'rgba(0, 0, 0, 0.65)',
		},
	},
	social: {
		marginLeft: 15,
	},
	icon: {
		marginRight: 5,
	},
});

const mapStateToProps = (state) => {
	return {
		store: {
			auth: state.auth,
			answerList: state.answer.list,
		},
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getAnswerList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class AnswerList extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionId: PropTypes.string.isRequired,
		where: PropTypes.object,
		questionData: PropTypes.object,
		onRefresh: PropTypes.func,
		questionHasPick: PropTypes.bool,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
			answerList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getAnswerList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		where: {},
		questionData: {},
		onRefresh: f => f,
		questionHasPick: false,
	}

	state = {
		loading: true,
		loadingMore: false,
	};

	componentDidMount() {
		this.filter.where = { ...this.filter.where, ...this.props.where };
		this.props.action.getAnswerList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	componentWillReceiveProps(nextProps) {
		if (!isEqual(nextProps.where, this.props.where)) {
			this.setState({
				loading: true,
			});
			this.filter.skip = 0;
			this.filter.page = 1;
			this.filter.where = { ...nextProps.where };
			nextProps.action.getAnswerList({ filter: this.filter, firstLoad: true }, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	filter = {
		limit: 12,
		skip: 0,
		page: 1,
		order: 'createdAt DESC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
			{
				relation: 'pick',
			},
			{
				relation: 'likes',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						creatorId: AuthStorage.userId,
					},
				},
			},
		],
		counts: ['likes', 'replies'],
		where: {
			status: 'active',
			questionId: this.props.questionId,
		},
	}

	handleRefresh = () => {
		this.props.onRefresh();
		this.setState({
			loading: true,
		});

		this.filter.page = 1;
		this.filter.skip = 0;

		this.props.action.getAnswerList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getAnswerList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	renderAnswers() {
		const { classes, questionData, questionHasPick, store: { answerList = { data: [] } } } = this.props;
		if (!answerList.data.length) {
			return (
				<div className={classes.notFound}>
					Chưa có câu trả lời nào
				</div>
			);
		}

		return answerList.data.map((answer) => {
			return (
				<AnswerCard
					key={answer.id}
					answerData={answer}
					myQuestion={questionData.creatorId === AuthStorage.userId}
					questionHasPick={questionHasPick}
					questionType={questionData.type}
				/>
			);
		});
	}

	render() {
		const { classes, questionId, questionData, store: { answerList = { data: [] } } } = this.props;

		return (
			<div>
				{
					!this.state.loading &&
					<Fragment>
						<div className={classes.stats}>
							<div className={classes.right}>
								{answerList.total > 0 ? answerList.total + ' câu trả lời' : 'Chưa có câu trả lời nào.'}
							</div>
							<div className={classes.left}>
								<Tooltip title={questionData.savesCount > 0 ? questionData.savesCount + ' người đã lưu câu hỏi này' : 'Chưa có ai lưu câu hỏi này.'}>
									<div className={classes.social}>
										<Icon className={classes.icon} type="book" />
										<span>{questionData.savesCount || 0}</span>
									</div>
								</Tooltip>
								<Tooltip title={questionData.viewsCount > 0 ? questionData.viewsCount + ' lần xem câu hỏi này' : 'Chưa có ai lưu câu hỏi này.'}>
									<div className={classes.social}>
										<Icon className={classes.icon} type="eye-o" />
										<span>{questionData.viewsCount || 0}</span>
									</div>
								</Tooltip>
								<Tooltip title={questionData.reportsCount > 0 ? questionData.reportsCount + ' người báo cáo vi phạm câu hỏi này' : 'Chưa có ai báo cáo vi phạm câu hỏi này.'}>
									<div className={classes.social}>
										<Icon className={classes.icon} type="dislike-o" />
										<span>{questionData.reportsCount || 0}</span>
									</div>
								</Tooltip>
								{/* <div className={classes.social}>
									<Icon className={classes.icon} type="message" />
									<span>{questionData.answersCount || 0}</span>
								</div> */}
							</div>
						</div>
						<div style={{ position: 'relative', zIndex: 1 }}>
							<div className={classes.header}>
								<div className={classes.answerAction}>
									<Button className={classes.button} icon="message">Trả lời</Button>
									{
										questionData.saves &&
										<BtnSave questionData={questionData} showCount={false} />
									}
									<BtnShare questionData={questionData} />
									<BtnSetting
										questionData={questionData}
									/>
								</div>
							</div>
							<BtnAddAnswer
								questionId={questionId}
								onRefresh={this.handleRefresh}
							/>
						</div>
					</Fragment>
				}

				<div className={classes.answerList}>
					{
						!AuthStorage.loggedIn ? // eslint-disable-line
							<div className={classes.lock}>
								<div className={classes.lockAvatar} />
								<div className={classes.lockText}>
									<div className={classes.nameFade} />
									<div className={classes.textFade} />
								</div>
								<div className={classes.action}>
									<p>Bạn phải đăng nhập để thấy câu trả lời.</p>
									<BtnOpenLogin />
								</div>
							</div> :
							this.state.loading ?
								<AnswerListLoading /> :
								this.renderAnswers()
					}
					{
						this.state.loadingMore && <AnswerListLoading />
					}
					{
						!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < answerList.total &&
						<div className={classes.moreComment}>
							<a onClick={this.handleViewMore}>Xem thêm câu trả lời</a>
						</div>
					}
				</div>
			</div>
		);
	}
}
