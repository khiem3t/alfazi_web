/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 17:09:44
*------------------------------------------------------- */

import React from 'react';
// import PropTypes from 'prop-types';

import AnswerCard from 'src/components/Answer/Card';

const QuestionListLoading = (/* props */) => {
	return (
		<div>
			{
				[0, 0].map((el, i) => {
					return (
						<AnswerCard key={i} loading />
					);
				})
			}
		</div>
	);
};

QuestionListLoading.propTypes = {
	// classes: PropTypes.object.isRequired,
};

QuestionListLoading.defaultProps = {
	// classes: {},
};

export default QuestionListLoading;
