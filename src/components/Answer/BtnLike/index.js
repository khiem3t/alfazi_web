/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-06 01:56:03
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AuthStorage from 'src/utils/AuthStorage';

import { Tooltip, Icon } from 'antd';

import { createAnswerLike, deleteAnswerLike } from 'src/redux/actions/answerLike';

import withStyles from 'src/theme/jss/withStyles';

import CheckLogin from 'src/components/Form/CheckLogin';

const styleSheet = (/* theme */) => ({
	root: {
		color: 'rgba(0, 0, 0, 0.44)',
	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			createAnswerLike,
			deleteAnswerLike,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnLike extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		answerData: PropTypes.object,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			createAnswerLike: PropTypes.func.isRequired,
			deleteAnswerLike: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		answerData: { likes: [] },
	}

	state = {
		hasLiked: this.props.answerData.likes.length > 0 && this.props.answerData.likes[0] && this.props.answerData.likes[0].creatorId === AuthStorage.userId,
		count: this.props.answerData.likesCount || 0,
		// likeId: this.props.answerData.likes[0] && this.props.answerData.likes[0].id,
	}

	handleLike = () => {
		if (this.props.answerData.id) {
			if (!this.state.hasLiked) {
				this.props.action.createAnswerLike({
					creatorId: AuthStorage.userId,
					receiverId: this.props.answerData.creatorId,
					questionId: this.props.answerData.questionId,
					answerId: this.props.answerData.id,
				}, (/* like */) => {
					this.setState({
						hasLiked: !this.state.hasLiked,
						count: this.state.count + 1,
						// likeId: like.id,
					});
				});
			} else {
				// if (this.state.likeId) {
				// 	this.props.action.deleteAnswerLike({
				// 		id: this.state.likeId,
				// 	}, () => {
				// 		this.setState({
				// 			hasLiked: !this.state.hasLiked,
				// 			count: this.state.count - 1,
				// 		});
				// 	});
				// }
			}
		}
	}

	render() {
		const { classes, answerData } = this.props;

		if (answerData.creatorId === AuthStorage.userId) {
			return (
				<Tooltip title={'Có ' + this.state.count + ' người thích câu trả lời của bạn'}>
					<a className={!this.state.hasLiked ? classes.root : ''}>
						{/* {
						this.state.hasLiked ?
							<Icon type="dislike" /> :
							<Icon type="like" />
					} */}
						<Icon type="like" />
						<span >{' ' + this.state.count}</span>
					</a>
				</Tooltip>
			);
		}

		return (
			<CheckLogin onClick={this.handleLike}>
				<Tooltip title={!this.state.hasLiked ? 'Thích bình luận này' : 'Bạn đã thích bình luận này'}>
					<a className={!this.state.hasLiked ? classes.root : ''}>
						{/* {
							this.state.hasLiked ?
								<Icon type="dislike" /> :
								<Icon type="like" />
						} */}
						<Icon type="like" />
						<span >{' ' + this.state.count}</span>
					</a>
				</Tooltip>
			</CheckLogin>
		);
	}
}
