/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 16:51:49
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Input, Icon, Tabs, Button } from 'antd';

import Latex from 'react-latex';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import Avatar from 'src/components/Stuff/Avatar';
import CheckLogin from 'src/components/Form/CheckLogin';
import Cropper from 'src/components/Stuff/ImageCropper';
import LightBox from 'src/components/LightBox/ImageLightBoxContainer';
import MathEditor from 'src/components/Answer/MathEditor';

import FaSubscript from 'react-icons/lib/fa/subscript';
// import FaPaperPlane from 'react-icons/lib/fa/paper-plane';

import { createAnswer } from 'src/redux/actions/answer';
import { uploadFiles } from 'src/redux/actions/upload';

const styleSheet = (theme) => ({
	root: {
		padding: '15px 20px',
		background: '#fff',
		borderBottom: '1px solid #e8e8e8',
		marginTop: '15px',
		// borderRadius: '5px',
		minHeight: '176px',
	},

	textBox: {
		padding: '8px 15px',
		border: '1px solid ' + theme.palette.primary[900],
		flex: '1',
		// fontStyle: 'italic',
		color: 'rgba(0, 0, 0, 0.65)',
		cursor: 'text',
	},
	avatar: {
		padding: '20px',
	},
	text: {},
	form: {
		flex: 1,
		display: 'flex',
		alignItems: 'top',
		'& $textArea': {
			padding: '8px 10px',
			minHeight: 40,
			flex: 1,
		},
	},
	tab: {
		width: '100%',
	},
	action: {
		position: 'relative',
		marginTop: '55px',
		justifyContent: 'space-between',
		marginLeft: '15px',
		'& .ant-upload-list': {
			top: '0',
			// width: '100%',
			// position: 'relative',
		},
	},
	itemImg: {
		marginTop: '15px',
		position: 'relative',
		display: 'inline-block',
		paddingLeft: 50,
		'& img': {
			height: 100,
		},
	},
	removeImg: {
		position: 'absolute',
		top: 0,
		color: '#e05e5e',
		fontSize: '20px',
		right: '5px',
	},
	btnList: {
		marginTop: '10px',
		'& a': {
			display: 'inline-block',
			marginRight: '15px',
			fontSize: '22px',
			'& svg': {
				verticalAlign: '0 !important',
			},
		},

	},
	uploadImage: {
		display: 'inline-block',
	},
	submit: {
		display: 'inline-block',
		float: 'right',
		'& a': {
			marginRight: '0',
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			auth: state.auth,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			createAnswer,
			uploadFiles,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnAddAnswer extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionId: PropTypes.string.isRequired,
		onRefresh: PropTypes.func,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			createAnswer: PropTypes.func.isRequired,
			uploadFiles: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		onRefresh: f => f,
	}

	state = {
		file: null,
		urlPreview: '',
		content: '',
		loading: false,
		mathEditorVisible: false,
		editor1: 'editor1',
		showCroper: false,
		showLightBox: false,
	}

	onChangeTab = () => {
		const { content } = this.state;
		this.setState({ mathcontent: content });
	}

	handleFocus = (e) => {
		const { editor1 } = this.state;
		e.target.setAttribute('id', editor1);
	}

	handleRemoveFile = () => {
		this.setState({
			urlPreview: '',
			file: null,
			showLightBox: false,
		});
	}

	handleChangeFile = (event) => {
		event.preventDefault();
		const reader = new FileReader(); // eslint-disable-line
		const file = event.target.files[0];
		reader.onloadend = () => {
			this.setState({
				showCroper: true,
				urlPreview: reader.result,
				file,
			});
		};
		reader.readAsDataURL(file);
	}

	handleChangeText = (e) => {
		this.setState({
			content: e.target.value,
		});
	}

	showMathEditorModal = () => {
		const { mathEditorVisible } = this.state;
		this.setState({
			mathEditorVisible: !mathEditorVisible,
		});
	}
	handleMathEditorModalSubmit = () => {
		this.setState({
			mathEditorVisible: false,
		});
	}
	handleMathEditorModalCancel = () => {
		this.setState({
			mathEditorVisible: false,
		});
	}

	updateContentWithMathExpressions = (expression) => {
		this.setState(prevState => {
			return {
				content: prevState.content + expression,
			};
		});
	}

	insertContent = (content) => {
		this.setState(prevState => {
			return {
				content: prevState.content + (prevState.content ? '\n' : '') + content,
			};
		});
	}

	handleSubmit = () => {
		if (this.props.questionId) {
			const { file, content } = this.state;

			if (file || content) {
				this.setState({
					loading: true,
				});

				this.props.action.uploadFiles({ files: [{ status: 'error', originFileObj: file }] }, (imageList = []) => {
					this.props.action.createAnswer({
						content,
						image: imageList[0],
						creatorId: AuthStorage.userId,
						questionId: this.props.questionId,
					}, () => {
						this.setState({
							loading: false,
							file: null,
							content: '',
							urlPreview: '',
							editor1: '',
						});
						this.props.onRefresh();
					});
				});
			}
		}
	}

	handleCrop = (promise) => {
		promise.then(file => {
			const reader = new FileReader(); // eslint-disable-line
			reader.onload = () => {
				this.setState({ file, urlPreview: reader.result, showCroper: false, showLightBox: true });
			};

			reader.readAsDataURL(file);
		});
	}

	handleCropDefault = () => {
		this.setState({ showCroper: false, showLightBox: true });
	}

	handelCancel = () => {
		this.setState({ mathEditorVisible: false });
	}


	render() {
		const { classes, store: { auth } } = this.props;

		const { mathEditorVisible } = this.state;

		return (
			<div className={classes.root}>
				<div className={classes.form}>
					{
						auth.id &&
						<Avatar className={classes.avatar} size={40} src={auth.avatar} name={auth.fullName} style={{ marginRight: 10 }} />
					}

					<Tabs onChange={this.onChangeTab} className={classes.tab}>
						<Tabs.TabPane tab="Chỉnh sửa" key="1">
							<Input.TextArea
								id="text-result"
								autosize
								placeholder="Thêm câu trả lời"
								onFocus={this.handleFocus}
								onChange={this.handleChangeText}
								value={this.state.content}
							/>
							<div className={classes.btnList}>
								<label htmlFor="imgInp" className={classes.uploadImage}>
									<a>
										<Icon type="picture" />
									</a>
									<input
										type="file"
										id="imgInp"
										style={{ display: 'none' }}
										onChange={(event) => {
											this.handleChangeFile(event);
											// add this to trigger onchange when selecting the same files
										event.target.value = null; // eslint-disable-line
										}}
									/>
								</label>
								<a onClick={() => this.showMathEditorModal()}>
									<FaSubscript />
								</a>
								<div className={classes.submit}>
									<CheckLogin onClick={this.handleSubmit}>
										<a>
											{
												this.state.loading ?
													<Icon type="loading" style={{ fontSize: '18px' }} /> :
													<Button value="small">Trả lời</Button>
											}
										</a>
									</CheckLogin>
								</div>

							</div>
							{mathEditorVisible ? (
								<div>
									<MathEditor
										style={{ marginTop: 10 }}
										visible={this.state.mathEditorVisible}
										onOk={this.handleMathEditorModalSubmit}
										onCancel={this.handelCancel}
										onChange={this.handleChangeText}
										onSubmit={this.insertContent}
										updateContentWithMathExpressions={this.updateContentWithMathExpressions}
									/>
								</div>
							) : ''}
						</Tabs.TabPane>
						<Tabs.TabPane tab="Xem trước" key="2">
							<div className={classes.text + ' pre-wrap'}>
								<Latex >
									{this.state.mathcontent}
								</Latex>
							</div>
						</Tabs.TabPane>
					</Tabs>


					{	this.state.showCroper &&
						ReactDOM.createPortal(
							<Cropper
								src={this.state.urlPreview}
								onCrop={this.handleCrop}
								onDefault={this.handleCropDefault}
								name={this.state.file.name}
								type={this.state.file.type}
							/>,
							document.getElementById('question-cropper'),
						)
					}


				</div>

				{
					this.state.showLightBox && this.state.urlPreview &&
					<div className={classes.itemImg}>
						<a className={classes.removeImg} onClick={this.handleRemoveFile}>
							<Icon type="close-circle" />
						</a>
						<LightBox img={this.state.urlPreview} alt="preview" />
					</div>
				}
			</div>
		);
	}
}
