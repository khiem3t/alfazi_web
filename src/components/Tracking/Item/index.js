/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-13 10:44:43
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'src/utils/moment';

import withStyles from 'src/theme/jss/withStyles';

import { Timeline, Tooltip } from 'antd';

import { Link } from 'src/routes';

import AvatarBlock from 'src/components/User/AvatarBlock';

const styleSheet = (/* theme */) => ({
	content: {
		marginBottom: 10,
		display: 'flex',
		alignItems: 'flex-start',
		flexWrap: 'wrap',
	},
	date: {
		fontSize: '12px',
		fontStyle: 'italic',
		cursor: 'pointer',
	},
	user: {
		display: 'inline-flex',
		flex: 'unset',
		marginRight: 5,
		marginLeft: 5,
		'& h5': {
			marginLeft: '-5px',
		},
	},
	explain: {
		marginBottom: '10px',
		padding: '15px',
		background: '#eaeaea',
		cursor: 'pointer',
		'& p': {
			maxHeight: '40px',
			margin: '0',
			display: '-webkit-box',
			overflow: 'hidden',
			textOverflow: 'ellipsis',
			WebkitLineClamp: '2',
			WebkitBoxOrient: 'vertical',
		},
	},
	imgs: {
		display: 'flex',
		overflowX: 'auto',
		'& img': {
			height: 80,
		},
	},
	link: {
		margin: '0 5px',
	},
});

const TrackingItem = (props) => {
	const { classes, trackingData = {} } = props;
	const {
		creator = {},
		question = {},
		achievement = {},
		badge = {},
		answer = { question: { creator: {} } },
		save = { question: { creator: {} } },
		pick = { answer: { creator: {} } },
		like = { answer: { creator: {} } },
		report = {
			answer: { },
			question: { },
			receiver: { },
		},
	} = trackingData;

	const content = () => {
		if (trackingData.type === 'receivedBadge') {
			return (
				<Timeline.Item>
					<div className={classes.inner}>
						<div className={classes.content}>
							<AvatarBlock userData={creator} size={20} className={classes.user} /> đã đạt được huy hiệu <strong>"{badge.name}"</strong>.
						</div>
						<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
							<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
						</Tooltip>
					</div>
				</Timeline.Item>
			);
		}
		if (trackingData.type === 'receivedAchievement') {
			return (
				<Timeline.Item>
					<div className={classes.inner}>
						<div className={classes.content}>
							<AvatarBlock userData={creator} size={20} className={classes.user} /> đã đạt được thành tựu <strong>"{achievement.name}"</strong>.
						</div>
						<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
							<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
						</Tooltip>
					</div>
				</Timeline.Item>
			);
		}
		if (trackingData.type === 'questionCreated') {
			return (
				<Timeline.Item>
					<div className={classes.inner}>
						<div className={classes.content}>
							<AvatarBlock userData={creator} size={20} className={classes.user} /> đã đăng một
							<Link route={'/question/' + question.id}><a className={classes.link}>câu hỏi</a></Link>.
						</div>
						<Link route={'/question/' + question.id}>
							<div className={classes.explain}>
								<p>{question.content}</p>
								{
									question.images &&
									<div className={classes.imgs}>
										{
											question.images.map((el) => {
												return <img src={el} alt="question.content" />;
											})
										}
									</div>
								}
							</div>
						</Link>
						<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
							<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
						</Tooltip>
					</div>
				</Timeline.Item>
			);
		}
		if (trackingData.type === 'answerCreated') {
			return (
				<Timeline.Item>
					<div className={classes.inner}>
						<div className={classes.content}>
							<AvatarBlock userData={creator} size={20} className={classes.user} /> đã trả lời
							<Link route={'/question/' + answer.question.id + '#' + answer.id}><a className={classes.link}>câu hỏi</a></Link> của
							<AvatarBlock userData={answer.question.creator} size={20} className={classes.user} />
						</div>
						<Link route={'/question/' + answer.question.id + '#' + answer.id}>
							<div className={classes.explain}>
								<p>{answer.content}</p>
								{
									answer.image &&
									<div className={classes.imgs}>
										<img src={answer.image} alt="question.content" />
									</div>
								}
							</div>
						</Link>
						<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
							<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
						</Tooltip>
					</div>
				</Timeline.Item>
			);
		}
		if (trackingData.type === 'questionSaveCreated') {
			return (
				<Timeline.Item>
					<div className={classes.inner}>
						<div className={classes.content}>
							<AvatarBlock userData={creator} size={20} className={classes.user} /> đã lưu một
							<Link route={'/question/' + save.question.id}><a className={classes.link}>câu hỏi</a></Link> của
							<AvatarBlock userData={save.question.creator} size={20} className={classes.user} />
						</div>
						<Link route={'/question/' + question.id}>
							<div className={classes.explain}>
								<p>{save.question.content}</p>
								{
									save.question.images &&
									<div className={classes.imgs}>
										{
											save.question.images.map((el) => {
												return <img src={el} alt="question.content" />;
											})
										}
									</div>
								}
							</div>
						</Link>
						<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
							<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
						</Tooltip>
					</div>
				</Timeline.Item>
			);
		}
		if (trackingData.type === 'answerLikeCreated') {
			return (
				<Timeline.Item>
					<div className={classes.inner}>
						<div className={classes.content}>
							<AvatarBlock userData={creator} size={20} className={classes.user} /> đã thích
							<Link route={'/question/' + like.questionId + '#' + like.answerId}><a className={classes.link}>câu trả lời</a></Link> của
							<AvatarBlock userData={like.answer.creator} size={20} className={classes.user} />
						</div>
						<Link route={'/question/' + like.questionId + '#' + like.answerId}>
							<div className={classes.explain}>
								<p>{like.answer.content}</p>
								{
									like.answer.image &&
									<div className={classes.imgs}>
										<img src={like.answer.image} alt="question.content" />
									</div>
								}
							</div>
						</Link>
						<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
							<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
						</Tooltip>
					</div>
				</Timeline.Item>
			);
		}
		if (trackingData.type === 'answerPickCreated') {
			return (
				<Timeline.Item>
					<div className={classes.inner}>
						<div className={classes.content}>
							<AvatarBlock userData={creator} size={20} className={classes.user} /> đã chọn
							<Link route={'/question/' + pick.questionId + '#' + pick.answerId}><a className={classes.link}>câu trả lời</a></Link> của
							<AvatarBlock userData={pick.answer.creator} size={20} className={classes.user} /> là đúng.
						</div>
						<Link route={'/question/' + pick.questionId + '#' + pick.answerId}>
							<div className={classes.explain}>
								<p>{pick.answer.content}</p>
								{
									pick.answer.image &&
									<div className={classes.imgs}>
										<img src={pick.answer.image} alt="question.content" />
									</div>
								}
							</div>
						</Link>
						<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
							<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
						</Tooltip>
					</div>
				</Timeline.Item>
			);
		}
		if (trackingData.type === 'reportCreated') {
			if (report.type === 'answer') {
				return (
					<Timeline.Item>
						<div className={classes.inner}>
							<div className={classes.content}>
								<AvatarBlock userData={creator} size={20} className={classes.user} /> đã báo cáo
								<Link route={'/question/' + report.questionId + '#' + report.answerId}><a className={classes.link}>câu trả lời</a></Link> của
								<AvatarBlock userData={report.receiver} size={20} className={classes.user} /> sai phạm.
							</div>
							<Link route={'/question/' + report.questionId + '#' + report.answerId}>
								<div className={classes.explain}>
									<p>{report.answer.content}</p>
									{
										report.answer.image &&
										<div className={classes.imgs}>
											<img src={report.answer.image} alt="question.content" />
										</div>
									}
								</div>
							</Link>
							<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
								<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
							</Tooltip>
						</div>
					</Timeline.Item>
				);
			}
			if (report.type === 'question') {
				return (
					<Timeline.Item>
						<div className={classes.inner}>
							<div className={classes.content}>
								<AvatarBlock userData={creator} size={20} className={classes.user} /> đã báo cáo
								<Link route={'/question/' + report.questionId}><a className={classes.link}>câu hỏi</a></Link> của
								<AvatarBlock userData={report.receiver} size={20} className={classes.user} /> sai phạm.
							</div>
							<Link route={'/question/' + report.questionId}>
								<div className={classes.explain}>
									<p>{report.question.content}</p>
									{
										report.question.images &&
										<div className={classes.imgs}>
											{
												report.question.images.map((el) => {
													return <img src={el} alt="question.content" />;
												})
											}
										</div>
									}
								</div>
							</Link>
							<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
								<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
							</Tooltip>
						</div>
					</Timeline.Item>
				);
			}
			return (
				<Timeline.Item>
					<div className={classes.inner}>
						<div className={classes.content}>
							<AvatarBlock userData={creator} size={20} className={classes.user} /> đã báo cáo
							<AvatarBlock userData={report.receiver} size={20} className={classes.user} /> vi phạm.
						</div>
						<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
							<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
						</Tooltip>
					</div>
				</Timeline.Item>
			);
		}
		// return (
		// 	<Timeline.Item>
		// 		<div className={classes.inner}>
		// 			<div className={classes.content}>
		// 				{trackingData.type}
		// 			</div>
		// 			<Tooltip title={moment(trackingData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
		// 				<span className={classes.date}>{moment(trackingData.createdAt).formatCustom()}</span>
		// 			</Tooltip>
		// 		</div>
		// 	</Timeline.Item>
		// );
	};

	return content() || null;
};

TrackingItem.propTypes = {
	classes: PropTypes.object.isRequired,
	trackingData: PropTypes.object.isRequired,
};

TrackingItem.defaultProps = {
	// classes: {},
};

export default withStyles(styleSheet)(TrackingItem);
