/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 14:12:00
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import isEqual from 'lodash/isEqual';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import { Timeline } from 'antd';

import { getTrackingList } from 'src/redux/actions/tracking';

import TrackingItem from 'src/components/Tracking/Item';

const styleSheet = (/* theme */) => ({
	root: {
		padding: 20,
		background: '#fff',
		minHeight: 'calc(100vh - 500px)',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			trackingList: state.tracking.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getTrackingList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class TrackingList extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		where: PropTypes.object,
		// store
		store: PropTypes.shape({
			trackingList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getTrackingList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		where: {},
	}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		this.filter.where = { ...this.filter.where, ...this.props.where };
		this.props.action.getTrackingList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	componentWillReceiveProps(nextProps) {
		if (!isEqual(nextProps.where, this.props.where)) {
			this.setState({
				loading: true,
			});
			this.filter.skip = 0;
			this.filter.page = 1;
			this.filter.where = { ...nextProps.where };
			nextProps.action.getTrackingList({ filter: this.filter, firstLoad: true }, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	filter = {
		limit: 20,
		skip: 0,
		page: 1,
		order: 'createdAt DESC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
			{
				relation: 'question',
				scope: {
					fields: ['id', 'content', 'images'],
				},
			},
			{
				relation: 'badge',
				scope: {
					fields: ['id', 'name'],
				},
			},
			{
				relation: 'achievement',
				scope: {
					fields: ['id', 'name'],
				},
			},
			{
				relation: 'answer',
				scope: {
					fields: ['id', 'content', 'image', 'questionId'],
					include: [
						{
							relation: 'question',
							scope: {
								fields: ['id', 'content', 'images', 'creatorId'],
								include: [
									{
										relation: 'creator',
										scope: {
											fields: ['id', 'username', 'avatar', 'fullName', 'role'],
										},
									},
								],
							},
						},
					],
				},
			},
			{
				relation: 'save',
				scope: {
					// fields: ['id'],
					include: [
						{
							relation: 'question',
							scope: {
								fields: ['id', 'content', 'images', 'creatorId'],
								include: [
									{
										relation: 'creator',
										scope: {
											fields: ['id', 'username', 'avatar', 'fullName', 'role'],
										},
									},
								],
							},
						},
					],
				},
			},
			{
				relation: 'pick',
				scope: {
					// fields: ['id'],
					include: [
						{
							relation: 'answer',
							scope: {
								fields: ['id', 'content', 'image', 'creatorId'],
								include: [
									{
										relation: 'creator',
										scope: {
											fields: ['id', 'username', 'avatar', 'fullName', 'role'],
										},
									},
								],
							},
						},
					],
				},
			},
			{
				relation: 'like',
				scope: {
					// fields: ['id'],
					include: [
						{
							relation: 'answer',
							scope: {
								fields: ['id', 'content', 'image', 'creatorId'],
								include: [
									{
										relation: 'creator',
										scope: {
											fields: ['id', 'username', 'avatar', 'fullName', 'role'],
										},
									},
								],
							},
						},
					],
				},
			},
			{
				relation: 'report',
				scope: {
					include: [
						{
							relation: 'receiver',
							scope: {
								fields: ['id', 'username', 'avatar', 'fullName', 'role'],
							},
						},
						{
							relation: 'question',
							scope: {
								fields: ['id', 'content', 'images'],
								include: [
									{
										relation: 'creator',
										scope: {
											fields: ['id', 'username', 'avatar', 'fullName', 'role'],
										},
									},
								],
							},
						},
						{
							relation: 'answer',
							scope: {
								fields: ['id', 'content', 'image'],
								include: [
									{
										relation: 'creator',
										scope: {
											fields: ['id', 'username', 'avatar', 'fullName', 'role'],
										},
									},
								],
							},
						},
					],
				},
			},
		],
		where: {
			creatorId: AuthStorage.userId,
		},
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getTrackingList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	render() {
		const { classes, store: { trackingList } } = this.props;

		return (
			<div className={classes.root}>
				<h3>Lịch sử hoạt động</h3>
				<Timeline style={{ padding: 20 }} pending={this.state.loadingMore ? 'Đang tải thêm...' : ''}>
					{
						trackingList.data.map((item) => {
							return <TrackingItem trackingData={item} key={item.id} />;
						})
					}
				</Timeline>
				{
					!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < trackingList.total &&
					<div className="text-center">
						<a onClick={this.handleViewMore}>Xem thêm lịch sử</a>
					</div>
				}
			</div>
		);
	}
}
