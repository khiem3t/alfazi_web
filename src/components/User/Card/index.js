/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 16:09:56
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

// import AuthStorage from 'src/utils/AuthStorage';

import AvatarBlock from 'src/components/User/AvatarBlock';
import StatsBlock from 'src/components/User/StatsBlock';

const styleSheet = (theme) => ({
	root: {
		borderBottom: '1px solid #e8e8e8',
		background: '#fff',
		position: 'relative',
		marginTop: '20px',
	},
	cover: {
		backgroundColor: theme.palette.primary[900] + ' !important',
		height: '95px',
		borderTopLeftRadius: 'inherit',
		borderTopRightRadius: 'inherit',
		backgroundPosition: '0 50%',
		width: '100%',
		padding: '0',
		marginTop: '-1px',
		backgroundImage: 'url("/static/assets/images/cover/cover-default.jpg")',
		backgroundSize: 'cover !important',
		position: 'relative',
		'&:before': {
			content: '""',
			position: 'absolute',
			background: 'rgba(0, 0, 0, 0.4)',
			top: '0',
			right: '0',
			left: '0',
			bottom: '0',
			width: '100%',
			height: '100%',
		},
	},
	body: {},
	avatar: {
		padding: '1px',
		verticalAlign: 'bottom',
		display: 'flex',
		alignItems: 'flex-end',
		zIndex: '1',
		position: 'relative',
		margin: '-55px 0 0 15px',
		color: '#fff',
		'& span': {
			color: '#fff',
		},
		'& h5': {
			fontSize: '16px',
			'& a': {
				color: '#fff',
			},
		},
	},
	name: {
		flex: 1,
		marginLeft: '5px',
		fontSize: '18px',
		marginTop: '0',
		color: 'rgba(0, 0, 0, 0.85)',
		fontWeight: '500',
		marginBottom: 5,
	},
	stats: {
		display: 'flex',
		padding: '15px 10px 0',
	},
	item: {
		flex: '1',
		textAlign: 'center',
		padding: '0 5px',
	},
	text: {},
	number: {
		fontSize: '26px',
		fontWeight: '500',
		color: theme.palette.primary[900],
	},
	btn: {
		position: 'absolute',
		top: '10px',
		right: '10px',
		fontSize: '12px',
		padding: '5px',
		height: 'auto',
		lineHeight: '1',
	},
});

const UserCard = (props) => {
	const { classes, userData = {}, loading } = props;

	if (loading) {
		return (
			<div className={classes.root}>
				<div className="loading-block" style={{ height: 95 }} />
				<div className={classes.body}>
					<div className={classes.avatar}>
						<div className="loading-block" style={{ width: 45, height: 45, borderRadius: 50 }} />
						<div className={classes.name}>
							<div className="loading-block" style={{ width: '80%' }} />
						</div>
					</div>
					<div className={classes.stats}>
						<div className={classes.item}>
							<div className={classes.text}>
								<div className="loading-block" />
							</div>
							<div className={classes.number}>
								<div className="loading-block" style={{ height: 26 }} />
							</div>
						</div>
						<div className={classes.item}>
							<div className={classes.text}>
								<div className="loading-block" />
							</div>
							<div className={classes.number}>
								<div className="loading-block" style={{ height: 26 }} />
							</div>
						</div>
						<div className={classes.item}>
							<div className={classes.text}>
								<div className="loading-block" />
							</div>
							<div className={classes.number}>
								<div className="loading-block" style={{ height: 26 }} />
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	return (
		<div className={classes.root}>
			<div className={classes.cover} style={{ backgroundImage: `url("${userData.cover ? userData.cover : '/static/assets/images/cover/cover-default.jpg'}")` }} />
			<div className={classes.body}>
				<div className={classes.avatar}>
					<AvatarBlock
						userData={userData}
						size={45}
						showLevel
					/>
				</div>
				<div style={{ padding: '25px 15px 15px' }}>
					<StatsBlock userData={userData} />
				</div>
			</div>
		</div>
	);
};

UserCard.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
	loading: PropTypes.bool,
};

UserCard.defaultProps = {
	loading: false,
	userData: {},
};

export default withStyles(styleSheet)(UserCard);
