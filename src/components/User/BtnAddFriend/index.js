/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 07:23:32
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Button, Icon, Dropdown, Menu } from 'antd';

import AuthStorage from 'src/utils/AuthStorage';

import CheckLogin from 'src/components/Form/CheckLogin';

import { createRelationship, updateRelationship, deleteRelationship } from 'src/redux/actions/relationship';

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			createRelationship,
			updateRelationship,
			deleteRelationship,
		}, dispatch),
	};
};

@connect(mapStateToProps, mapDispatchToProps)
export default class BtnAddFriend extends PureComponent {
	static propTypes = {
		style: PropTypes.object,
		relationshipData: PropTypes.object,
		className: PropTypes.string,
		friendId: PropTypes.string.isRequired,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// // action
		action: PropTypes.shape({
			createRelationship: PropTypes.func.isRequired,
			updateRelationship: PropTypes.func.isRequired,
			deleteRelationship: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		className: '',
		style: {},
		relationshipData: {},
	}

	state = {
		relationshipData: this.props.relationshipData || {},
		loading: false,
	}

	handleCreateRelationship = () => {
		if (this.props.friendId) {
			this.setState({
				loading: true,
			});

			this.props.action.createRelationship({
				creatorId: AuthStorage.userId,
				friendId: this.props.friendId,
			}, (relationshipData) => {
				this.setState({
					relationshipData,
					loading: false,
				});
			}, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	handleSelectMenu = ({ key }) => {
		if (!!this.state.relationshipData.id && AuthStorage.loggedIn) {
			if (key === 'delete') {
				this.setState({
					loading: true,
				});
				this.props.action.deleteRelationship({
					id: this.state.relationshipData.id,
				}, () => {
					this.setState({
						relationshipData: {},
						loading: false,
					});
				}, () => {
					this.setState({
						loading: false,
					});
				});
			}
			if (key === 'accept') {
				this.setState({
					loading: true,
				});

				this.props.action.updateRelationship({
					id: this.state.relationshipData.id,
					updatedAt: new Date(),
					status: 'accepted',
				}, (relationshipData) => {
					this.setState({
						relationshipData,
						loading: false,
					});
				}, () => {
					this.setState({
						loading: false,
					});
				});
			}
		}
	}

	render() {
		const { className, style } = this.props;
		const { relationshipData } = this.state;

		if (AuthStorage.loggedIn && relationshipData.status === 'accepted') {
			const menu = (
				<Menu onClick={this.handleSelectMenu}>
					<Menu.Item key="delete"><Icon type="close" style={{ marginRight: 15 }} />Hủy kết bạn</Menu.Item>
				</Menu>
			);
			return (
				<Dropdown overlay={menu}>
					<Button className={className} loading={this.state.loading} style={style}>
						<Icon type="check" />Đã là bạn <Icon type="down" />
					</Button>
				</Dropdown>
			);
		}

		if (AuthStorage.loggedIn && relationshipData.status === 'pending' && relationshipData.creatorId === AuthStorage.userId) {
			const menu = (
				<Menu onClick={this.handleSelectMenu}>
					<Menu.Item key="delete"><Icon type="close" style={{ marginRight: 15 }} />Hủy yêu cầu</Menu.Item>
				</Menu>
			);

			return (
				<Dropdown overlay={menu}>
					<Button className={className} loading={this.state.loading} style={style}>
						<Icon type="clock-circle-o" />Chờ xác nhận <Icon type="down" />
					</Button>
				</Dropdown>
			);
		}

		if (AuthStorage.loggedIn && relationshipData.status === 'pending' && relationshipData.friendId === AuthStorage.userId) {
			const menu = (
				<Menu onClick={this.handleSelectMenu}>
					<Menu.Item key="accept"><Icon type="check" style={{ marginRight: 15 }} />Đồng ý</Menu.Item>
					<Menu.Item key="delete"><Icon type="close" style={{ marginRight: 15 }} />Từ chối</Menu.Item>
				</Menu>
			);
			return (
				<Dropdown overlay={menu}>
					<Button className={className} loading={this.state.loading} style={style} type="primary">
						<Icon type="user-add" />Trả lời yêu cầu <Icon type="down" />
					</Button>
				</Dropdown>
			);
		}

		return (
			<CheckLogin onClick={this.handleCreateRelationship}>
				<Button className={className} loading={this.state.loading} style={style}>
					<Icon type="user-add" />Thêm bạn
				</Button>
			</CheckLogin>
		);
	}
}
