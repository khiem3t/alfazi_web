/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 14:41:36
*------------------------------------------------------- */

import React from 'react';
// import PropTypes from 'prop-types';

import { Row, Col } from 'antd';

import Item from 'src/components/User/Item';

const UserListLoading = (/* props */) => {
	return (
		<Row gutter={10} style={{ marginTop: 15 }}>
			<Col xs={24} sm={12}>
				<Item loading />
			</Col>
			<Col xs={24} sm={12}>
				<Item loading />
			</Col>
		</Row>
	);
};

UserListLoading.propTypes = {
	// classes: PropTypes.object.isRequired,
};

UserListLoading.defaultProps = {
	// classes: {},
};

export default UserListLoading;
