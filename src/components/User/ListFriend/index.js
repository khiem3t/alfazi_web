/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 14:12:00
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import isEqual from 'lodash/isEqual';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import { Row, Col, Button } from 'antd';


import { getRelationshipList } from 'src/redux/actions/relationship';

import Item from 'src/components/User/Item';
import ListLoading from './Loading';

const styleSheet = (/* theme */) => ({
	notFound: {
		marginTop: 20,
		padding: '50px 0',
		background: '#fff',
		textAlign: 'center',
		marginBottom: '20px',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			relationshipList: state.relationship.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getRelationshipList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class ListFriend extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		userId: PropTypes.string.isRequired,
		where: PropTypes.object,
		// store
		store: PropTypes.shape({
			relationshipList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getRelationshipList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		where: {},
	}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		this.filter.where = { ...this.filter.where, ...this.props.where };
		this.props.action.getRelationshipList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	componentWillReceiveProps(nextProps) {
		if (!isEqual(nextProps.where, this.props.where)) {
			this.setState({
				loading: true,
			});
			this.filter.skip = 0;
			this.filter.page = 1;
			this.filter.where = { ...nextProps.where };
			nextProps.action.getRelationshipList({ filter: this.filter, firstLoad: true }, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	filter = {
		limit: 10,
		skip: 0,
		page: 1,
		order: 'createdAt DESC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'cover', 'fullName', 'level', 'coinsCount', 'pointsCount'],
					include: [
						{
							relation: 'friendsSent',
							scope: {
								limit: 1,
								skip: 0,
								where: {
									friendId: AuthStorage.userId,
								},
							},
						},
						{
							relation: 'friendsReceived',
							scope: {
								limit: 1,
								skip: 0,
								where: {
									creatorId: AuthStorage.userId,
								},
							},
						},
					],
				},
			},
			{
				relation: 'friend',
				scope: {
					fields: ['id', 'username', 'avatar', 'cover', 'fullName', 'level', 'coinsCount', 'pointsCount'],
					include: [
						{
							relation: 'friendsSent',
							scope: {
								limit: 1,
								skip: 0,
								where: {
									friendId: AuthStorage.userId,
								},
							},
						},
						{
							relation: 'friendsReceived',
							scope: {
								limit: 1,
								skip: 0,
								where: {
									creatorId: AuthStorage.userId,
								},
							},
						},
					],
				},
			},
		],
		where: {
			status: 'accepted',
			or: [
				{ creatorId: this.props.userId },
				{ friendId: this.props.userId },
			],
		},
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getRelationshipList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	render() {
		const { classes, store: { relationshipList }, userId } = this.props;

		return (
			<div className={classes.root}>
				<h3 style={{ margin: 0 }} className="text-center-sm-down">
					Danh sách bạn bè
					{
						relationshipList.total > 0 && <span style={{ marginLeft: 10 }}>({relationshipList.total})</span>
					}
				</h3>
				{
					relationshipList.data.length === 0 && !this.state.loading ?
						<div className={classes.notFound}>
							Hiện tại chưa có bạn bè.
						</div> :
						<div>
							<Row gutter={10} style={{ marginTop: 15 }}>
								{
									this.state.loading ?
										<ListLoading /> :
										relationshipList.data.map((relation) => {
											const userData = userId === relation.friendId ? relation.creator : relation.friend;

											return (
												<Col xs={24} sm={12} md={12} key={relation.id}>
													<Item userData={userData} />
												</Col>
											);
										})
								}
								{
									this.state.loadingMore && <ListLoading />
								}
							</Row>
							{
								!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < relationshipList.total &&
								<div className="text-center" style={{ clear: 'both' }}>
									<Button type="primary" style={{ width: 300, margin: '0 auto' }} onClick={this.handleViewMore}>Xem thêm</Button>
								</div>
							}
						</div>
				}
			</div>
		);
	}
}
