/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-27 03:30:23
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import { Button, Input, Tooltip } from 'antd';

import link from 'src/constants/url';

const styleSheet = (/* theme */) => ({
	root: {
		marginTop: 20,
	},
	wrapper: {
		display: 'flex',
		marginTop: '10px',
	},
	linkReferral: {
		background: '#e5e5eb',
	},
});

const StatsBlock = (props) => {
	const { classes, userData } = props;

	const handleClickCopy = () => {
		const copyText = document.getElementById('referralLink');
		copyText.select();
		document.execCommand('Copy');
	};

	return (
		<div className={classes.root} onClick={handleClickCopy}>
			<h4>Link giới thiệu</h4>
			<Tooltip title="Sao chép">
				<div className={classes.wrapper}>
					<Input id="referralLink" readOnly className={classes.linkReferral} value={`${link.WEB_URL}/sign-up?referralId=${userData.id}`} />
					<Button type="primary" icon="copy" />
				</div>
			</Tooltip>
		</div>
	);
};

StatsBlock.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
};

StatsBlock.defaultProps = {
	userData: {},
};

export default withStyles(styleSheet)(StatsBlock);
