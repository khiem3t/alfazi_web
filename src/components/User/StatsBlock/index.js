/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-27 03:30:23
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import StatsCoins from 'src/components/User/StatsBlock/Coins';
import StatsPoints from 'src/components/User/StatsBlock/Points';
import StatsQuestions from 'src/components/User/StatsBlock/Questions';
import BadgeList from 'src/components/Badge/List';
import Referral from 'src/components/User/StatsBlock/Referral';

const styleSheet = (/* theme */) => ({
	root: {},
});

const StatsBlock = (props) => {
	const { classes, userData } = props;

	return (
		<div className={classes.root}>
			<StatsCoins userData={userData} />
			<StatsPoints userData={userData} />
			<StatsQuestions userData={userData} />
			<BadgeList userData={userData} />
			<Referral userData={userData} />
		</div>
	);
};

StatsBlock.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
};

StatsBlock.defaultProps = {
	userData: {},
};

export default withStyles(styleSheet)(StatsBlock);
