/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-26 17:09:22
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import LevelProgress from 'src/components/User/LevelProgress';

const styleSheet = (/* theme */) => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		marginTop: 20,
	},
	img: {
		width: 45,
		padding: 5,
		'& img': {
			width: '100%',
		},
	},
	content: {
		flex: '1',
		marginLeft: '15px',
		// color: theme.palette.primary[900],
	},
	coin: {
		fontSize: '18px',
		fontWeight: 'bold',
	},
	level: {
		marginTop: '4px',
		padding: '0',
	},
});

const StatsPoints = (props) => {
	const { classes, userData } = props;

	return (
		<div className={classes.root}>
			<div className={classes.img}>
				<img src="/static/assets/images/point.png" alt="point" />
			</div>
			<div className={classes.content}>
				<div className={classes.coin}>
					{userData.pointsCount || 0} điểm
				</div>
				<LevelProgress userData={userData} className={classes.level} />
			</div>
		</div>
	);
};

StatsPoints.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
};

StatsPoints.defaultProps = {
	userData: {},
};

export default withStyles(styleSheet)(StatsPoints);
