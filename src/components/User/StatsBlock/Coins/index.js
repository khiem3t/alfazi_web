/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-26 17:09:22
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import Link from 'next/link';

const styleSheet = (/* theme */) => ({
	root: {
		display: 'flex',
		alignItems: 'center',
	},
	content: {
		flex: '1',
		marginLeft: '15px',
		// color: theme.palette.primary[900],
	},
	coin: {
		fontSize: '18px',
		fontWeight: 'bold',
	},
	link: {
		display: 'flex',
		justifyContent: 'space-between',
	},
});

const StatsCoins = (props) => {
	const { classes, userData } = props;

	return (
		<div className={classes.root}>
			<img src="/static/assets/images/coins.png" alt="coin" width="45px" />
			<div className={classes.content}>
				<div className={classes.coin}>
					{userData.coinsCount || 0} xu
				</div>
				<div className={classes.link}>
					{
						userData.id === AuthStorage.userId &&
						<Link href="/history-coins">
							<a>Lịch sử dùng xu</a>
						</Link>
					}

					<a href="/blog/post/lam-the-nao-de-tang-tai-khoan-xu" target="_blank" rel="noopener noreferrer">Cách nhận xu</a>
				</div>
			</div>
		</div>
	);
};

StatsCoins.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
};

StatsCoins.defaultProps = {
	userData: {},
};

export default withStyles(styleSheet)(StatsCoins);
