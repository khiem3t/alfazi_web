/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-10 11:14:41
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import moment from 'src/utils/moment';

import { checkBase64, dataURItoBlob } from 'src/utils/index';

import AuthStorage from 'src/utils/AuthStorage';

import { Modal, Button, Icon, Form, Input, Select, DatePicker } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import ImgCropper from 'src/components/Form/ImgCropper';

import { provinceOptions } from 'src/constants/selectOption';

import { uploadFiles } from 'src/redux/actions/upload';
import { editProfile } from 'src/redux/actions/auth';

const styleSheet = (theme) => ({
	'@global .edit-profile .ant-modal-body': {
		padding: 0,
	},
	root: {
		position: 'relative',
	},
	cover: {
		backgroundColor: theme.palette.primary[900] + ' !important',
		background: 'url("/static/assets/images/cover/cover-default-big.jpg") no-repeat',
		height: 300,
		width: '100%',
		backgroundSize: 'cover !important',
		backgroundPosition: 'center center !important',
		position: 'relative',
		'&:before': {
			content: '""',
			position: 'absolute',
			background: 'rgba(0, 0, 0, 0.4)',
			top: '0',
			right: '0',
			left: '0',
			bottom: '0',
			width: '100%',
			height: '100%',
		},
	},
	tabWrapper: {
		background: '#fff',
		borderBottom: '1px solid #e8e8e8',
	},
	user: {
		zIndex: '1',
		textAlign: 'center',
		display: 'flex',
		alignItems: 'flex-end',
		'& h1': {
			color: '#fff',
			marginLeft: '15px',
			marginBottom: 5,
		},
	},
	avatar: {
		padding: '5px',
		background: '#fff',
		border: '1px solid #e8e8e8',
		display: 'flex',
		margin: '0 0 10px 10px',
	},
	info: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'flex-end',
		height: '100%',
	},
	inputFile: {
		right: '15px',
		top: '15px',
		color: '#fff',
		position: 'absolute',
		fontSize: '12px',
		display: 'flex',
		alignItems: 'center',
		padding: '3px 5px',
		background: '#00000059',
		cursor: 'pointer',
		'& i': {
			fontSize: '20px',
			marginRight: 5,
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			uploadFiles,
			editProfile,
		}, dispatch),
	};
};

@Form.create()
@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BtnEditProfile extends Component {
	static propTypes = {
		form: PropTypes.object.isRequired,
		classes: PropTypes.object.isRequired,
		userData: PropTypes.object.isRequired,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			uploadFiles: PropTypes.func.isRequired,
			editProfile: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		visible: false,
		confirmLoading: false,
		file: null,
		urlPreview: '',
	}

	showModal = () => {
		this.setState({
			visible: true,
		}, () => {
			if (this.props.userData && this.props.userData.id) {
				this.props.form.setFieldsValue({
					email: this.props.userData.email || '',
					avatar: this.props.userData.avatar || null,
					phone: this.props.userData.phone || '',
					gender: this.props.userData.gender === 'male' ? 'Nam' : 'Nữ',
					address: this.props.userData.address || '',
					desc: this.props.userData.desc || '',
					school: this.props.userData.school || '',
					class: this.props.userData.class || '',
					province: this.props.userData.province || '',
					fullName: this.props.userData.fullName || '',
					birthDate: moment(this.props.userData.birthDate, 'YYYY-MM-DD'),
				});
			}
		});
	}

	handleOk = () => {
		if (this.props.userData && this.props.userData.id) {
			this.props.form.validateFields((err, values) => {
				if (err) {
					return;
				}

				const { file } = this.state;
				const { avatar, ...user } = values;
				this.setState({
					confirmLoading: true,
				});

				user.id = AuthStorage.userId;

				if (file) {
					file.renameFilename = AuthStorage.userId + '-cover.png';
				}

				this.props.action.uploadFiles({ files: [{ status: 'error', originFileObj: file }] }, (res = []) => {
					user.cover = res[0] ? res[0] + '?' + (+new Date()) : this.props.userData.cover;

					this.props.action.uploadFiles({ files: [{ status: 'error', originFileObj: checkBase64(avatar) ? dataURItoBlob(avatar, AuthStorage.userId + '-avatar.png') : null }] }, (avatars = []) => {
						user.avatar = avatars[0] ? avatars[0] + '?' + (+new Date()) : avatar;
						user.updatedAt = new Date();

						this.props.action.editProfile(user, () => {
							this.setState({
								visible: false,
								confirmLoading: false,
								file: null,
								urlPreview: '',
							});
						}, () => {
							this.setState({
								confirmLoading: false,
							});
						});
					}, () => {
						this.setState({
							confirmLoading: false,
						});
					});
				}, () => {
					this.setState({
						confirmLoading: false,
					});
				});

				// form.resetFields();
				// this.setState({ visible: false });
			});
		}
	}
	handleCancel = () => {
		this.props.form.resetFields();
		this.setState({
			visible: false,
			file: null,
			urlPreview: '',
		});
	}

	handleChangeFile = (event) => {
		event.preventDefault();
		const reader = new FileReader(); // eslint-disable-line
		const file = event.target.files[0];

		reader.onloadend = () => {
			this.setState({
				urlPreview: reader.result,
				file,
			});
		};
		reader.readAsDataURL(file);
	}

	render() {
		const { form: { getFieldDecorator }, classes } = this.props;

		return (
			<div className={classes.root}>
				<Button onClick={this.showModal}>
					<Icon type="edit" /> Chỉnh sửa hồ sơ
				</Button>

				<Modal
					title="Chỉnh sửa hồ sơ"
					style={{ top: 40 }}
					visible={this.state.visible}
					onOk={this.handleOk}
					onCancel={this.handleCancel}
					maskClosable={false}
					destroyOnClose
					width="70%"
					wrapClassName="edit-profile"
					confirmLoading={this.state.confirmLoading}
				>
					<Form onSubmit={this.handleSubmit}>
						<div className={classes.cover} style={{ backgroundImage: `url("${this.state.urlPreview ? this.state.urlPreview : this.props.userData.cover}")` }}>
							<label htmlFor="imgInp" className={classes.inputFile}>
								<Icon type="camera" /> Thay đổi hình nền
								<input type="file" id="imgInp" style={{ display: 'none' }} onChange={this.handleChangeFile} />
							</label>
							<div className={classes.info}>
								<div className={classes.user}>
									<div className={classes.avatar}>
										{getFieldDecorator('avatar', {
										})(
											<ImgCropper size={150} borderRadius={0} />,
										)}
									</div>
									{
										getFieldDecorator('fullName', {
											rules: [{ required: true, message: 'Nhập tên của bạn' }],
										})(
											<Input
												placeholder="Họ và Tên"
												style={{
													background: 'transparent',
													border: '0',
													marginBottom: '15px',
													fontSize: '25px',
													marginLeft: '10px',
													color: '#fff',
												}}
											/>,
										)
									}
								</div>
							</div>
						</div>
						<div style={{ margin: '30px 0' }}>
							<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
								{getFieldDecorator('desc', {
									rules: [{ required: true, message: 'Giới thiệu bản thân' }],
								})(
									<Input.TextArea
										row={7}
										placeholder="Giới thiệu bản thân"
									/>,
								)}
							</Form.Item>
							<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
								{getFieldDecorator('gender', {
									initialValue: 'male',
									// rules: [{ required: true, message: 'Please input your email!' }, { pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, message: 'Email is invalid!' }],
								})(
									<Select>
										<Select.Option value="male">Nam</Select.Option>
										<Select.Option value="female">Nữ</Select.Option>
									</Select>,
								)}
							</Form.Item>
							<Form.Item
								wrapperCol={{ span: 12, offset: 6 }}
							>
								{getFieldDecorator('birthDate', {
									initialValue: moment('1990-01-01', 'YYYY-MM-DD'),
									// rules: [{ required: true, message: 'Please input your email!' }, { pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, message: 'Email is invalid!' }],
								})(
									<DatePicker format="DD-MM-YYYY" />,
								)}
							</Form.Item>
							<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
								{getFieldDecorator('phone', {
								})(
									<Input
										placeholder="Điện thoại"
									/>,
								)}
							</Form.Item>
							<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
								{getFieldDecorator('province', {
								})(
									<Select placeholder="Tỉnh thành">
										{
											provinceOptions.map((el) => {
												return (
													<Select.Option key={el.value} value={el.value}>{el.label}</Select.Option>
												);
											})
										}
									</Select>,
								)}
							</Form.Item>
							<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
								{getFieldDecorator('address', {
								})(
									<Input
										placeholder="Địa chỉ"
									/>,
								)}
							</Form.Item>
							<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
								{getFieldDecorator('school', {
								})(
									<Input
										placeholder="Trường học"
									/>,
								)}
							</Form.Item>
							<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
								{getFieldDecorator('class', {
								})(
									<Input
										placeholder="Lớp học"
									/>,
								)}
							</Form.Item>
						</div>
					</Form>
				</Modal>
			</div>
		);
	}
}
