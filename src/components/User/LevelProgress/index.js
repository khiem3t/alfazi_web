/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-14 11:49:12
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';
import { Tooltip } from 'antd';

const styleSheet = (/* theme */) => ({
	level: {
		display: 'flex',
		width: '100%',
		marginTop: '20px',
		padding: '0 20px',
		alignItems: 'center',
	},
	oldLevel: {
		width: '20px',
		height: '20px',
		borderRadius: '50%',
		background: '#2ecc71',
		textAlign: 'center',
		fontSize: '12px',
		color: '#fff',

	},
	newLevel: {
		width: '20px',
		height: '20px',
		borderRadius: '50%',
		background: '#146159',
		textAlign: 'center',
		fontSize: '12px',
		color: '#fff',
	},
	proses: {
		flex: '1',
		padding: '0 5px',
		position: 'relative',
		cursor: 'pointer',
	},
	bar: {
		width: '100%',
		height: '4px',
		background: '#e6e6e6',
	},
	barInner: {
		position: 'absolute',
		width: '0%',
		height: '4px',
		background: '#146059',
		top: '0',
		transition: 'width .5s',
	},
});

const LevelProgress = (props) => {
	const { classes, userData, className } = props;

	const pointsNeeded = (5 * (userData.level + 1) * (userData.level + 2)) - (5 * userData.level * (userData.level + 1));
	const pointsHas = userData.pointsCount - (5 * userData.level * (userData.level + 1));

	return (
		<Tooltip title={'Điểm cho cấp kế tiếp ' + pointsHas + '/' + pointsNeeded}>
			<div className={classes.level + ' ' + className}>
				<div className={classes.oldLevel}>
					{userData.level}
				</div>
				<div className={classes.proses}>
					<div className={classes.bar} />
					<div className={classes.barInner} style={{ width: ((pointsHas * 100) / pointsNeeded) + '%' }} />
				</div>
				<div className={classes.newLevel}>
					{userData.level + 1}
				</div>
			</div>
		</Tooltip>
	);
};

LevelProgress.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
	className: PropTypes.string,
};

LevelProgress.defaultProps = {
	className: '',
	userData: {
		pointsCount: 0,
		level: 0,
	},
};

export default withStyles(styleSheet)(LevelProgress);
