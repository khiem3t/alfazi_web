/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 16:09:08
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'src/utils/moment';
import classNames from 'classnames';

import withStyles from 'src/theme/jss/withStyles';

import { Tooltip } from 'antd';

import { Link } from 'src/routes';

import Avatar from 'src/components/Stuff/Avatar';

import FaUserSecret from 'react-icons/lib/fa/user-secret';

const styleSheet = (theme) => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		flex: 1,
	},
	info: {
		marginLeft: '10px',
		'& h5': {
			margin: 0,
			color: theme.palette.primary[900],
			fontSize: '14px',
			lineHeight: '1.3',
			display: 'flex',
			alignItems: 'flex-end',
		},
		'& span': {
			fontStyle: 'italic',
			color: '#90949c',
			fontSize: '12px',
		},
	},
	admin: {
		display: 'flex',
		textShadow: '2px 2px 5px #f9b840',
		color: 'rgb(229, 161, 24) !important',
		'&:hover': {
			color: 'rgb(229, 161, 24) !important',
		},
	},
	adminAvatar: {
		boxShadow: '2px 2px 5px #f9b840',
		color: 'red',
		border: '1px solid rgb(229, 161, 24)',
		borderRadius: '50%',
	},
	mod: {
		display: 'flex',
		textShadow: '2px 2px 5px #eca9a9',
		color: 'red !important',
		'&:hover': {
			color: 'red !important',
		},
	},
	modAvatar: {
		boxShadow: '2px 2px 5px #eca9a9',
		color: 'red',
		border: '1px solid red',
		borderRadius: '50%',
	},
	iconAdmin: {
		marginRight: 5,
		cursor: 'pointer',
		textShadow: '2px 2px 5px #eca9a9',
		fontSize: 17,
		// color: theme.palette.primary[900],
	},
});

const AvatarBlock = (props) => {
	const { classes, userData, date, size, className, loading, showLevel } = props;

	if (loading) {
		return (
			<div className={classes.root + ' ' + className}>
				<div className="loading-block" style={{ width: size, height: size, borderRadius: 50, margin: 0 }} />
				<div className={classes.info}>
					<div>
						<div className="loading-block" style={{ width: 150 }} />
					</div>
					{
						date &&
						<div className="loading-block" style={{ width: 80 }} />
					}
				</div>
			</div>
		);
	}

	return (
		<div className={classes.root + ' ' + className}>
			<Link route={'/profile/' + userData.id}>
				<Tooltip title={userData.role === 'admin' ? 'Quản trị viên' : 'Điều hành viên'}>
					<a
						style={{ display: 'inherit' }}
						className={
							classNames({
								[classes.adminAvatar]: !showLevel && userData.role === 'admin',
								[classes.modAvatar]: !showLevel && userData.role === 'mod',
							})
						}
					>
						<Avatar size={size} src={userData.avatar} name={userData.fullName} />
					</a>
				</Tooltip>
			</Link>
			<div className={classes.info + ' info'}>
				<Tooltip title={userData.role === 'admin' ? 'Quản trị viên' : 'Điều hành viên'}>
					<h5>
						<Link route={'/profile/' + userData.id}>
							<a
								className={
									classNames({
										[classes.admin]: !showLevel && userData.role === 'admin',
										[classes.mod]: !showLevel && userData.role === 'mod',
									})
								}
							>
								{
									!showLevel && (userData.role === 'admin' || userData.role === 'mod') &&
									<FaUserSecret className={classes.iconAdmin} />

								}
								{userData.fullName}
							</a>
						</Link>
					</h5>
				</Tooltip>
				{
					date &&
					<Tooltip title={moment(date).format('[Đăng Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
						<a>
							<span>{moment(date).formatCustom()}</span>
						</a>
					</Tooltip>
				}
				{
					showLevel && (
						userData.role === 'admin' ? // eslint-disable-line
							<span>Quản trị viên</span> :
								userData.role === 'mod' ? // eslint-disable-line
								<span>Điều hành viên</span> : // eslint-disable-line
								<span>{userData.level === 0 ? 'Thành viên mới' : 'Thành viên cấp ' + userData.level}</span>
					)
				}
			</div>
		</div>
	);
};

AvatarBlock.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
	date: PropTypes.string,
	className: PropTypes.string,
	size: PropTypes.number,
	loading: PropTypes.bool,
	showLevel: PropTypes.bool,
};

AvatarBlock.defaultProps = {
	date: undefined,
	size: 50,
	className: '',
	loading: false,
	showLevel: false,
	userData: {},
};

export default withStyles(styleSheet)(AvatarBlock);
