/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 16:09:56
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import BtnAddFriend from 'src/components/User/BtnAddFriend';
import AvatarBlock from 'src/components/User/AvatarBlock';

const styleSheet = (/* theme */) => ({
	root: {
		borderBottom: '1px solid #e8e8e8',
		background: '#fff',
		position: 'relative',
		marginBottom: 10,
		padding: 15,
		// height: 140,
	},
	btn: {
		position: 'absolute',
		top: '15px',
		right: '15px',
		fontSize: '12px',
		padding: '5px',
		height: 'auto',
		lineHeight: '1',
	},
	info: {
		paddingLeft: 55,
		fontSize: 14,
		color: '#90949c',
	},
});

const UserCard = (props) => {
	const { classes, userData = {}, loading } = props;
	const { friendsReceived = [], friendsSent = [] } = userData;

	const relationship = [...friendsReceived, ...friendsSent];

	if (loading) {
		return (
			<div className={classes.root}>
				<div style={{ display: 'flex' }}>
					<div className="loading-block" style={{ width: 45, height: 45, borderRadius: 50 }} />
					<div style={{ flex: 1 }}>
						<div className="loading-block" style={{ width: '80%' }} />
						<div className="loading-block" style={{ width: '40%' }} />
						<div className="loading-block" style={{ marginTop: 10 }} />
						<div className="loading-block" />
						<div className="loading-block" />
					</div>
				</div>
			</div>
		);
	}

	return (
		<div className={classes.root}>
			{
				AuthStorage.userId !== userData.id &&
				<BtnAddFriend
					className={classes.btn}
					friendId={userData.id}
					relationshipData={relationship[0]}
				/>
			}
			<AvatarBlock
				userData={userData}
				size={45}
				showLevel
			/>
			<div className={classes.info}>
				<div className={classes.infoItem}>
					{userData.address}
				</div>
				<div className={classes.infoItem}>
					{userData.province}
				</div>
				<div className={classes.infoItem}>
					{(userData.school ? userData.school : '') + (userData.class ? '-' + userData.class : '')}
				</div>
			</div>
		</div>
	);
};

UserCard.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object,
	loading: PropTypes.bool,
};

UserCard.defaultProps = {
	loading: false,
	userData: {},
};

export default withStyles(styleSheet)(UserCard);
