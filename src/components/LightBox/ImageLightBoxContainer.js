/* --------------------------------------------------------
* Author Nguyễn Đăng Anh Thi
* Email nguyendanganhthi247@gmail.com
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import Lightbox from 'react-image-lightbox';
import PropTypes from 'prop-types';
import _isEqual from 'lodash/isEqual';

export default class ImageLightBoxContainer extends PureComponent {
	static propTypes = {
		className: PropTypes.string,
		images: PropTypes.arrayOf(PropTypes.string),
		img: PropTypes.string,
		alt: PropTypes.string,
	}

	static defaultProps = {
		className: '',
		images: [],
		img: '',
		alt: '',
	}

	state = {
		img: this.props.img,
		images: this.props.images,
		photoIndex: 0,
		isOpen: false,
	};


	componentWillReceiveProps(nextProps) {
		if (typeof nextProps.img !== 'undefined' && nextProps.img !== this.state.img) {
			this.setState({ img: nextProps.img });
		} else if (!_isEqual(nextProps.images, this.state.images)) {
			this.setState({ images: nextProps.images });
		}
	}

	isImages() {
		const { images } = this.state;
		return images.length && images.length > 1;
	}

	isImg() {
		const { images, img } = this.state;
		return img || images.length === 1;
	}

	toggleLightBox = () => {
		this.setState(prevState => ({
			isOpen: !prevState.isOpen,
		}));
	}

	pickNextImage = () => {
		const { photoIndex, images } = this.state;
		this.setState({
			photoIndex: (photoIndex + 1) % images.length,
		});
	}

	pickPrevImage = () => {
		const { photoIndex, images } = this.state;
		this.setState({
			photoIndex: (photoIndex + images.length - 1) % images.length,
		});
	}

	imageOnClick = (index = 0) => {
		this.setState({ photoIndex: index });
		this.toggleLightBox();
	}

	renderLightBox = () => {
		const { images, img, photoIndex } = this.state;
		if (this.isImages()) {
			return (
				<Lightbox
					mainSrc={images[photoIndex]}
					nextSrc={images[(photoIndex + 1) % images.length]}
					prevSrc={images[(photoIndex + images.length - 1) % images.length]}
					onCloseRequest={this.toggleLightBox}
					onMovePrevRequest={this.pickPrevImage}
					onMoveNextRequest={this.pickNextImage}
				/>
			);
		} else if (this.isImg()) {
			return (
				<Lightbox
					mainSrc={img || images[0]}
					onCloseRequest={this.toggleLightBox}
				/>
			);
		}
	}

	renderImage() {
		const { images, img } = this.state;
		const { alt } = this.props;
		if (this.isImages()) {
			return (
				images.map((src, i) => {
					return <img key={i} src={src} alt={alt} style={{ cursor: 'zoom-in'}} onClick={() => this.imageOnClick(i)} />; // eslint-disable-line
				})
			);
		} else if (this.isImg()) {
			const src = img || images[0];
			return <img src={src} alt={alt} style={{ cursor: 'zoom-in'}} onClick={this.imageOnClick} />; // eslint-disable-line
		}
	}

	render() {
		const { className } = this.props;
		return (
			<div className={className}>
				{this.renderImage()}
				{ this.state.isOpen && this.renderLightBox()}
			</div>
		);
	}
}
