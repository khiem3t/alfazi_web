/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-19 15:32:18
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import moment from 'src/utils/moment';

import { Link } from 'src/routes';

import withStyles from 'src/theme/jss/withStyles';

import { Icon } from 'antd';

const styleSheet = (/* theme */) => ({
	item: {
		background: '#fff',
		boxShadow: '0px 1px 4px 0px rgba(0, 0, 0, 0.14)',
		marginBottom: 16,
	},
	imgWrapper: {
		position: 'relative',
		overflow: 'hidden',
		'&:hover $img': {
			overflow: 'hidden',
			transform: 'scale(1.1) rotate(3deg)',
		},
	},
	img: {
		overflow: 'hidden',
		width: '100%',
		transition: 'all 0.5s ease 0s',
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
		backgroundColor: 'gray',
		height: 200,
	},
	content: {
		padding: '15px',
		'& h3': {
			display: '-webkit-box',
			overflow: 'hidden',
			height: '48px',
			textOverflow: 'ellipsis',
			WebkitLineClamp: '2',
			WebkitBoxOrient: 'vertical',
		},
	},
	summary: {
		height: '63px',
		display: '-webkit-box',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		WebkitLineClamp: '3',
		WebkitBoxOrient: 'vertical',
	},
	stats: {
		padding: '5px 15px 15px',
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	category: {
		display: 'inline-block',
		position: 'absolute',
		color: '#fff',
		background: 'rgba(0, 0, 0, 0.7)',
		bottom: '10px',
		right: '10px',
		padding: '2px 5px',
		fontSize: 12,
		zIndex: 1,
	},
	statsItem: {
		display: 'flex',
		alignItems: 'center',
		fontSize: '12px',
		color: '#676767',
		'& i': {
			fontSize: '14px',
			marginRight: '3px',
		},
	},
});

const BlogPostCard = (props) => {
	const { classes, postData = {}, loading } = props;
	const { category = {} } = postData;

	if (loading) {
		return (
			<div className={classes.item}>
				<div className={classes.imgWrapper}>
					<div className="loading-block" style={{ height: 200 }} />
				</div>
				<div className={classes.content}>
					<div className="loading-block" />
					<div className="loading-block" style={{ width: '60%', marginBottom: 20 }} />
					<div className={classes.summary}>
						<div className="loading-block" />
						<div className="loading-block" />
						<div className="loading-block" style={{ width: '60%' }} />
					</div>
				</div>
			</div>
		);
	}

	if (!postData.slug) {
		return null;
	}

	return (
		<div className={classes.item}>
			<Link route={'/blog/post/' + postData.slug}>
				<a>
					<div className={classes.imgWrapper}>
						<div className={classes.img} style={{ backgroundImage: 'url("' + (postData.image && postData.image.secure_url) + '")' }} />
						<div className={classes.category}>{category.name}</div>
					</div>
				</a>
			</Link>
			<div className={classes.content}>
				<Link route={'/blog/post/' + postData.slug}>
					<a>
						<h3>{postData.title}</h3>
					</a>
				</Link>
				<div className={classes.summary}>
					{postData.brief}
				</div>
			</div>
			<div className={classes.stats}>
				<div className={classes.statsItem}>
					<Icon type="clock-circle-o" />
					{moment(postData.publishedDate).format('LL')}

				</div>
				<Link route={'/blog/post/' + postData.slug}>
					<a>
						<div className={classes.statsItem}>
							<Icon type="eye-o" />
							{postData.viewsCount || 0}
						</div>
					</a>
				</Link>
			</div>
		</div>
	);
};

BlogPostCard.propTypes = {
	classes: PropTypes.object.isRequired,
	postData: PropTypes.object,
	loading: PropTypes.bool,
};

BlogPostCard.defaultProps = {
	postData: {},
	loading: false,
};

export default withStyles(styleSheet)(BlogPostCard);
