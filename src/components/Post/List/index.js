/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-19 15:07:14
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Row, Col } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import Container from 'src/components/Layout/Container';

import Item from 'src/components/Post/Card/';

const styleSheet = (/* theme */) => ({
	root: {},
	tab: {
		textAlign: 'center',
		marginTop: '20px',
	},
	notFound: {
		textAlign: 'center',
		padding: '50px 0',
		margin: '30px 0',
		background: '#fff',
	},
});

@withStyles(styleSheet)
export default class PostList extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		dataList: PropTypes.array,
		loading: PropTypes.bool,
	}

	static defaultProps = {
		dataList: [],
		loading: false,
	}

	render() {
		const { classes, dataList, loading } = this.props;

		if (loading) {
			return (
				<div className={classes.root}>
					<Container>
						<Row gutter={16}>
							{
								[0, 0, 0, 0, 0, 0].map((post, i) => {
									return (
										<Col key={i} sm={12} lg={8}>
											<Item loading />
										</Col>
									);
								})
							}
						</Row>
					</Container>
				</div>
			);
		}

		if (dataList.length === 0 && !loading) {
			return (
				<Container>
					<div className={classes.notFound}>
						Chưa có bài viết nào.
					</div>
				</Container>
			);
		}

		return (
			<div className={classes.root}>
				<Container>
					<Row gutter={16}>
						{
							dataList.map((post) => {
								return (
									<Col key={post.id} sm={12} lg={8}>
										<Item postData={post} />
									</Col>
								);
							})
						}
					</Row>
				</Container>
			</div>
		);
	}
}
