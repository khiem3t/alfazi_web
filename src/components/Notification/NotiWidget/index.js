/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-08 15:51:23
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import onClickOutside from 'react-onclickoutside';

import { Popover, Badge } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import NotiList from 'src/components/Notification/NotiWidget/List';
import FaBellO from 'react-icons/lib/fa/bell-o';

import { updateAllNoti } from 'src/redux/actions/notification';

const styleSheet = (theme) => ({
	title: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
		width: '420px',
		fontSize: '12px',
		padding: '2px 0',
		[theme.breakpoints.down.xs]: {
			width: 'calc(100vw - 32px)',
		},
	},
	root: {
		marginRight: '30px',
	},
	inner: {
		padding: '10px',
	},
	btnIcon: {
		border: '0',
		fontSize: '20px',
		color: theme.palette.primary[900],
		cursor: 'pointer',
	},
	badge: {
		'& sup': {
			height: '14px',
			fontSize: '9px',
			minWidth: '0',
			lineHeight: '12px',
			borderRadius: '2px',
			padding: '1px 3px',
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			countUnread: state.notification.countUnread,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			updateAllNoti,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
// @onClickOutside
export default class NotiWidget extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		style: PropTypes.object,
		// store
		store: PropTypes.shape({
			countUnread: PropTypes.number.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			updateAllNoti: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		style: {},
	}

	state = {
		visible: false,
	}

	handleClickOutside = () => {
		// if (this.state.visible) {
		// 	this.setState({
		// 		visible: false,
		// 	});
		// }
	};

	handleOpen = () => {
		this.setState({ visible: true });
	}

	handleVisibleChange = (visible) => {
		this.setState({ visible });
	}

	handleReadAll = () => {
		this.props.action.updateAllNoti({ data: { updatedAt: new Date(), read: true } });
	}

	render() {
		const { classes, store: { countUnread }, style } = this.props;

		const title = (
			<div className={classes.title}>
				<div>Thông báo</div>
				<a onClick={this.handleReadAll}>Đánh dấu tất cả là đã đọc</a>
			</div>
		);

		return (
			<div className={classes.root} style={style}>
				<Popover content={<NotiList visible={this.state.visible} />} title={title} trigger="click" placement="bottomRight" visible={this.state.visible} onVisibleChange={this.handleVisibleChange}>
					<div className={classes.inner}>
						<Badge className={classes.badge} count={countUnread}>
							<FaBellO className={classes.btnIcon} />
						</Badge>
					</div>
				</Popover>
			</div>
		);
	}
}
