/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-06 10:59:53
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'src/utils/moment';

import { Icon } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import Avatar from 'src/components/Stuff/Avatar';

import FaBellO from 'react-icons/lib/fa/bell-o';

const styleSheet = (theme) => ({
	text: {
		flex: '1',
		marginLeft: '10px',
		'& p': {
			marginBottom: 2,
			lineHeight: '16px',
			display: '-webkit-box',
			overflow: 'hidden',
			maxHeight: '59px',
			textOverflow: 'ellipsis',
			WebkitLineClamp: '2',
			WebkitBoxOrient: 'vertical',
		},
		'& span': {
			fontSize: '11px',
			// fontStyle: 'italic',
			color: '#90949c',
		},
		'& strong': {
			color: theme.palette.primary[900],
		},
		'& i': {
			fontSize: '14px',
			marginRight: '5px',
			color: theme.palette.primary[900],
		},
	},
	control: {
		marginLeft: '20px',
		opacity: '0',
		'& svg': {
			fontSize: '18px',
			color: 'rgba(0, 0, 0, 0.83)',
		},
		'&.unread': {
			background: '#f0fdff',
		},
	},
	content: {
		flex: 1,
		display: 'flex',
		alignItems: 'center',
		color: '#000 !important',
	},
	avatarIcon: {
		height: '50px',
		width: '50px',
		borderRadius: '50%',
		textAlign: 'center',
		background: '#f5222d',
		fontSize: '24px',
		color: '#fff',
		lineHeight: '44px',
		alignItems: 'center',
		display: 'flex',
		justifyContent: 'center',
	},
});

const NotiItemContent = ({ classes, loading, notiData = {}, onRead }) => {
	const { creator = {}, reply = {}, answer = {}, data = {} } = notiData;
	const { report = {} } = data;

	if (loading) {
		return (
			<div className={classes.item}>
				<div className="loading-block" style={{ width: 50, height: 50, borderRadius: 50, margin: 0 }} />
				<div className={classes.text}>
					<div className="loading-block" />
					<div className="loading-block" />
					<div className="loading-block" style={{ width: '50%' }} />
				</div>
			</div>
		);
	}

	const handleItemClick = (url) => {
		onRead(url);
	};

	const getContent = () => {
		if (notiData.type === 'receivedAchievement') {
			const { data: dataNoti = {} } = notiData;
			const { achievement = {} } = dataNoti;

			return (
				<a className={classes.content} onClick={() => handleItemClick('/achievement')}>
					<div className={classes.avatarIcon}>
						<Icon type="trophy" />
					</div>
					<div className={classes.text}>
						<p>Bạn nhận được {achievement.amount || 0} xu khi <strong>{achievement.name || 'đạt được một phần thưởng'}</strong>.</p>
						<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'reachedAchievement') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/achievement')}>
					<div className={classes.avatarIcon}>
						<Icon type="trophy" />
					</div>
					<div className={classes.text}>
						<p>Bạn nhận được một phần thưởng, hãy bấm vào để xem chi tiết.</p>
						<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'receivedBadge') {
			const { data: dataNoti = {} } = notiData;
			const { badge = {} } = dataNoti;

			return (
				<a className={classes.content} onClick={() => handleItemClick('/profile/' + notiData.receiverId)}>
					<div className={classes.avatarIcon}>
						<FaBellO />
					</div>
					<div className={classes.text}>
						<p>Bạn nhận được một huy hiệu mới <img src={badge.image} alt={badge.name} style={{ width: 22, margin: '0 5px' }} /> {badge.name}</p>
						<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'attendance') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/profile/' + notiData.receiverId)}>
					<div className={classes.avatarIcon}>
						<FaBellO />
					</div>
					<div className={classes.text}>
						<p>Bạn nhận được 20 xu khi đăng nhập lần đầu và tham gia hỏi đáp đạt điểm 10 trong ngày hôm nay.</p>
						<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'userCreated') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/profile/' + notiData.receiverId)}>
					<div className={classes.avatarIcon}>
						<FaBellO />
					</div>
					<div className={classes.text}>
						<p>Bạn đã được tặng 20 xu khi đã đăng kí tài khoản thành công</p>
						<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'upLevel') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/profile/' + notiData.receiverId)}>
					<div className={classes.avatarIcon}>
						<FaBellO />
					</div>
					<div className={classes.text}>
						<p>Chúc mừng bạn đã đạt cấp {notiData.data.newLevel}</p>
						<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'relationshipCreated') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/profile/' + notiData.creatorId)}>
					<Avatar name={creator.fullName} src={creator.avatar} size={40} />
					<div className={classes.text}>
						<p><strong>{creator.fullName}</strong> đã Đã gửi lời mời kết bạn với bạn.</p>
						<span><Icon type="user-add" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'answerLikeCreated') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/question/' + notiData.questionId + '?answerId=' + notiData.answerId)}>
					<Avatar name={creator.fullName} src={creator.avatar} size={40} />
					<div className={classes.text}>
						<p><strong>{creator.fullName}</strong> đã thích câu trả lời của bạn: {answer.content}</p>
						<span><Icon type="like" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'answerPickCreated') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/question/' + notiData.questionId + '?answerId=' + notiData.answerId)}>
					<Avatar name={creator.fullName} src={creator.avatar} size={40} />
					<div className={classes.text}>
						<p><strong>{creator.fullName}</strong> đã chọn câu trả lời của bạn là đúng: {answer.content}</p>
						<span><Icon type="heart" style={{ color: 'red' }} />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'answerCreated') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/question/' + notiData.questionId + '?answerId=' + notiData.answerId)}>
					<Avatar name={creator.fullName} src={creator.avatar} size={40} />
					<div className={classes.text}>
						<p><strong>{creator.fullName}</strong> đã trả lời câu hỏi của bạn: {answer.content}</p>
						<span><Icon type="message" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'replyCreated') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/question/' + notiData.questionId + '?replyId=' + notiData.replyId)}>
					<Avatar name={creator.fullName} src={creator.avatar} size={40} />
					<div className={classes.text}>
						<p><strong>{creator.fullName}</strong> đã phản hồi trong câu trả lời của bạn: {reply.content}</p>
						<span><Icon type="message" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'reportCreated') {
			if (report.type === 'question') {
				return (
					<a className={classes.content} onClick={() => handleItemClick('/question/' + notiData.questionId)}>
						<Avatar name={creator.fullName} src={creator.avatar} />
						<div className={classes.text}>
							<p><strong>{creator.fullName}</strong> đã báo cáo câu hỏi của bạn là vi phạm: {report.question && report.question.content}</p>
							<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
						</div>
					</a>
				);
			}
			if (report.type === 'answer') {
				return (
					<a className={classes.content} onClick={() => handleItemClick('/question/' + notiData.questionId + '?answerId=' + notiData.answerId)}>
						<Avatar name={creator.fullName} src={creator.avatar} size={40} />
						<div className={classes.text}>
							<p><strong>{creator.fullName}</strong> đã báo cáo câu trả lời của bạn là vi phạm: {report.answer && report.answer.content}</p>
							<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
						</div>
					</a>
				);
			}

			return (
				<a className={classes.content} onClick={() => handleItemClick('/profile/' + notiData.receiverId)}>
					<Avatar name={creator.fullName} src={creator.avatar} size={40} />
					<div className={classes.text}>
						<p><strong>{creator.fullName}</strong> đã báo cáo tài khoản của bạn vi phạm.</p>
						<span><Icon type="notification" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		if (notiData.type === 'relationshipAccepted') {
			return (
				<a className={classes.content} onClick={() => handleItemClick('/profile/' + notiData.creatorId)}>
					<Avatar name={creator.fullName} src={creator.avatar} size={40} />
					<div className={classes.text}>
						<p><strong>{creator.fullName}</strong> đã chập nhận lời mới kết bạn của bạn.</p>
						<span><Icon type="user-add" />{moment(notiData.createdAt).formatCustom()}</span>
					</div>
				</a>
			);
		}
		return (
			<a className={classes.content} onClick={() => handleItemClick('/profile/' + notiData.creatorId)}>
				<div className={classes.avatarIcon}>
					<FaBellO />
				</div>
				<div className={classes.text}>
					<p>{notiData.type}</p>
					<span><Icon type="link" />{moment(notiData.createdAt).formatCustom()}</span>
				</div>
			</a>
		);
	};

	return (
		getContent()
	);
};

NotiItemContent.propTypes = {
	classes: PropTypes.object.isRequired,
	notiData: PropTypes.object,
	loading: PropTypes.bool,
	onRead: PropTypes.func,
};

NotiItemContent.defaultProps = {
	notiData: {},
	loading: false,
	onRead: f => f,
};

export default withStyles(styleSheet)(NotiItemContent);
