/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-06 10:59:53
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Menu, Dropdown } from 'antd';

import { Router } from 'src/routes';

import withStyles from 'src/theme/jss/withStyles';

import MdMoreVert from 'react-icons/lib/md/more-vert';

import { updateNoti, updateAllNoti, deleteNoti } from 'src/redux/actions/notification';

import NotiItemContent from './Content';

const styleSheet = (theme) => ({
	item: {
		padding: '10px 16px',
		cursor: 'pointer',
		display: 'flex',
		justifyContent: 'space-between',
		fontSize: '12px',
		borderBottom: '1px solid #e8e8e8',
		fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
		color: '#000',
		'&:hover $control': {
			opacity: 1,
		},
	},
	unread: {
		background: theme.palette.primary[50],
		borderBottom: '1px solid #dcdbdb',
	},
	control: {
		marginLeft: '20px',
		opacity: '0',
		'& svg': {
			fontSize: '18px',
			color: 'rgba(0, 0, 0, 0.83)',
		},
		'&.unread': {
			background: '#f0fdff',
		},
	},
	text: {
		flex: '1',
		marginLeft: '10px',
		'& p': {
			marginBottom: 2,
			lineHeight: '16px',
			display: '-webkit-box',
			overflow: 'hidden',
			maxHeight: '59px',
			textOverflow: 'ellipsis',
			WebkitLineClamp: '2',
			WebkitBoxOrient: 'vertical',
		},
		'& span': {
			fontSize: '11px',
			// fontStyle: 'italic',
			color: '#90949c',
		},
		'& strong': {
			color: theme.palette.primary[900],
		},
		'& i': {
			fontSize: '14px',
			marginRight: '5px',
			color: theme.palette.primary[900],
		},
	},
	content: {
		flex: 1,
		display: 'flex',
		alignItems: 'center',
		color: '#000 !important',
	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			updateNoti,
			updateAllNoti,
			deleteNoti,
		}, dispatch),
	};
};

const NotiItem = ({ classes, loading, notiData = {}, action }) => {
	if (loading) {
		return (
			<div className={classes.item}>
				<div className="loading-block" style={{ width: 50, height: 50, borderRadius: 50, margin: 0 }} />
				<div className={classes.text}>
					<div className="loading-block" />
					<div className="loading-block" />
					<div className="loading-block" style={{ width: '50%' }} />
				</div>
			</div>
		);
	}

	const handleSelectMenu = ({ key }) => {
		if (key === 'read') {
			action.updateNoti({ id: notiData.id, updatedAt: new Date(), read: true });
		}
		if (key === 'delete') {
			action.deleteNoti({ id: notiData.id });
		}
	};

	const menu = (
		<Menu onClick={handleSelectMenu}>
			{
				!notiData.read &&
				<Menu.Item key="read">
					Mark as Read
				</Menu.Item>
			}
			<Menu.Item key="delete">
				Hide this notification
			</Menu.Item>
		</Menu>
	);

	const handleItemClick = (url) => {
		if (!notiData.read) {
			action.updateNoti({ id: notiData.id, updatedAt: new Date(), read: true });
		}
		Router.pushRoute(url);
	};

	const { data = {} } = notiData;

	const notiSend = {
		...notiData,
		creator: { ...data.creator, ...notiData.creator },
		question: { ...data.question, ...notiData.question },
		answer: { ...data.answer, ...notiData.answer },
	};

	return (
		<div
			className={
				classNames(classes.item, {
					[classes.unread]: !notiData.read,
				})
			}
		>
			<NotiItemContent
				notiData={notiSend}
				onRead={handleItemClick}
			/>
			<div className={classes.control}>
				<Dropdown overlay={menu} trigger={['click']}>
					<a className="ant-dropdown-link" href="#">
						<MdMoreVert />
					</a>
				</Dropdown>
			</div>
		</div>
	);
};

NotiItem.propTypes = {
	classes: PropTypes.object.isRequired,
	notiData: PropTypes.object,
	loading: PropTypes.bool,
	// store
	// store: PropTypes.shape({
	// 	modal: PropTypes.object.isRequired,
	// }).isRequired,
	// action
	action: PropTypes.shape({
		updateNoti: PropTypes.func.isRequired,
		updateAllNoti: PropTypes.func.isRequired,
		deleteNoti: PropTypes.func.isRequired,
	}).isRequired,
};

NotiItem.defaultProps = {
	notiData: {},
	loading: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styleSheet)(NotiItem));
