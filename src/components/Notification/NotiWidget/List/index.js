/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-07 23:55:23
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Waypoint from 'react-waypoint';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import NotiItem from 'src/components/Notification/NotiWidget/Item';

import { getNotiList } from 'src/redux/actions/notification';

import FaBellSlashO from 'react-icons/lib/fa/bell-slash-o';

const styleSheet = (theme) => ({
	content: {
		margin: '-12px -16px',
		width: '452px',
		height: '300px',
		overflowY: 'auto',
		[theme.breakpoints.down.xs]: {
			position: 'fixed',
			top: '82px',
			bottom: '0',
			left: '0',
			right: '0',
			width: '100%',
			height: 'auto',
			zIndex: '7',
			background: '#fff',
			borderTop: '1px solid #e8e8e8',
			margin: '0',
		},
	},
	notFound: {
		height: '300px',
		justifyContent: 'center',
		display: 'flex',
		alignItems: 'center',
		flexDirection: 'column',
		'& svg': {
			fontSize: 30,
			marginBottom: 20,
		},
		[theme.breakpoints.down.xs]: {
			position: 'fixed',
			top: '82px',
			bottom: '0',
			left: '0',
			right: '0',
			width: '100%',
			height: 'auto',
			zIndex: '7',
			background: '#fff',
			borderTop: '1px solid #e8e8e8',
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			notiList: state.notification.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getNotiList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class ClassName extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		// store
		store: PropTypes.shape({
			notiList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getNotiList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		this.props.action.getNotiList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	filter = {
		limit: 8,
		skip: 0,
		page: 1,
		order: 'createdAt DESC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
			{
				relation: 'question',
				scope: {
					fields: ['id', 'content'],
				},
			},
			{
				relation: 'answer',
				scope: {
					fields: ['id', 'content'],
				},
			},
			{
				relation: 'reply',
				scope: {
					fields: ['id', 'content'],
				},
			},
		],
		where: {
			receiverId: AuthStorage.userId,
			creatorId: { neq: AuthStorage.userId },
			type: { nin: ['relationshipCreated', 'reachedAchievement'] },
		},
	}

	handleLoadMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getNotiList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	render() {
		const { classes, store: { notiList } } = this.props;

		if (!this.state.loading && notiList.total === 0) {
			return (
				<div className={classes.notFound}>
					<FaBellSlashO />
					Không có thông báo nào
				</div>
			);
		}

		return (
			<div className={classes.content}>
				{
					!this.state.loading > 0 ?
						notiList.data.map((noti) => {
							return <NotiItem key={noti.id} notiData={noti} />;
						}) :
						[0, 0, 0, 0].map((noti, i) => {
							return <NotiItem key={i} loading />;
						})
				}
				{
					this.state.loadingMore && [
						<NotiItem key={1} loading />,
						<NotiItem key={2} loading />,
					]
				}
				{
					!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < notiList.total &&
					<Waypoint
						onEnter={this.handleLoadMore}
					/>
				}

			</div>
		);
	}
}
