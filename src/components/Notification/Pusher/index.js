/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-06 10:59:53
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import { Router } from 'src/routes';

import withStyles from 'src/theme/jss/withStyles';

import NotiItemContent from 'src/components/Notification/NotiWidget/Item/Content';
// import FriendReqItem from 'src/components/Notification/FriendReqWidget/Item';

const styleSheet = (/* theme */) => ({
	item: {
		paddingRight: 30,
		cursor: 'pointer',
		display: 'flex',
		justifyContent: 'space-between',
		fontSize: '12px',
		fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
		color: '#000',
		'&:hover $control': {
			opacity: 1,
		},
	},
});

const NotiItem = ({ classes, loading, notiData = {} }) => {
	const { data = {} } = notiData;

	if (loading) {
		return (
			<div className={classes.item}>
				<div className="loading-block" style={{ width: 50, height: 50, borderRadius: 50, margin: 0 }} />
				<div className={classes.text}>
					<div className="loading-block" />
					<div className="loading-block" />
					<div className="loading-block" style={{ width: '50%' }} />
				</div>
			</div>
		);
	}

	const handleItemClick = (url) => {
		// if (!notiData.read) {
		// 	action.updateNoti({ id: notiData.id, updatedAt: new Date(), read: true });
		// }
		Router.pushRoute(url);
	};

	return (
		<div
			className={classes.item}
		>
			<NotiItemContent
				notiData={{ ...notiData, ...data }}
				onRead={handleItemClick}
			/>
		</div>
	);
};

NotiItem.propTypes = {
	classes: PropTypes.object.isRequired,
	notiData: PropTypes.object,
	loading: PropTypes.bool,
};

NotiItem.defaultProps = {
	notiData: {},
	loading: false,
};

export default withStyles(styleSheet)(NotiItem);
