/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-06 10:59:53
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';

import { Popover, Badge, Icon } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import NotiList from 'src/components/Notification/FriendReqWidget/List';

const styleSheet = (theme) => ({
	'@global .ant-popover': {
		position: 'fixed',
		zIndex: 9,
	},
	title: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
		width: '420px',
		fontSize: '12px',
		padding: '2px 0',
		[theme.breakpoints.down.xs]: {
			width: 'calc(100vw - 32px)',
		},
	},
	root: {
		marginRight: '5px',
	},
	inner: {
		padding: '10px',
	},
	btnIcon: {
		border: '0',
		fontSize: '20px',
		color: theme.palette.primary[900],
		cursor: 'pointer',
	},
	badge: {
		'& sup': {
			height: '14px',
			fontSize: '9px',
			minWidth: '0',
			lineHeight: '12px',
			borderRadius: '2px',
			padding: '1px 3px',
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			countFriendReqUnread: state.relationship.countUnread,
		},
	};
}

const mapDispatchToProps = (/* dispatch */) => {
	return {
		// action: bindActionCreators({
		// toggleLoginModal,
		// }, dispatch),
	};
};

const FriendReqWidget = ({ style, classes, store: { countFriendReqUnread = 0 } }) => {
	const title = (
		<div className={classes.title}>
			<div>Lời mới kết bạn</div>
		</div>
	);

	return (
		<div className={classes.root} style={style}>
			<Popover content={<NotiList />} title={title} trigger="click" placement="bottomRight">
				<div className={classes.inner}>
					<Badge className={classes.badge} count={countFriendReqUnread}>
						<Icon className={classes.btnIcon} type="usergroup-add" />
					</Badge>
				</div>
			</Popover>
		</div>
	);
};

FriendReqWidget.propTypes = {
	classes: PropTypes.object.isRequired,
	style: PropTypes.object,
	// store
	store: PropTypes.shape({
		countFriendReqUnread: PropTypes.number.isRequired,
	}).isRequired,
	// action
	// action: PropTypes.shape({
	// 	toggleLoginModal: PropTypes.func.isRequired,
	// }).isRequired,
};

FriendReqWidget.defaultProps = {
	style: {},
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styleSheet)(FriendReqWidget));

