/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-07 23:55:23
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import NotiItem from 'src/components/Notification/FriendReqWidget/Item';

import { getFriendReqList } from 'src/redux/actions/relationship';

import FaBellSlashO from 'react-icons/lib/fa/bell-slash-o';

const styleSheet = (theme) => ({
	content: {
		margin: '-12px -16px',
		width: '452px',
		height: '300px',
		overflowY: 'auto',
		[theme.breakpoints.down.xs]: {
			position: 'fixed',
			top: '82px',
			bottom: '0',
			left: '0',
			right: '0',
			width: '100%',
			height: 'auto',
			zIndex: '7',
			background: '#fff',
			borderTop: '1px solid #e8e8e8',
			margin: '0',
		},
	},
	notFound: {
		height: '300px',
		justifyContent: 'center',
		display: 'flex',
		alignItems: 'center',
		flexDirection: 'column',
		'& svg': {
			fontSize: 30,
			marginBottom: 20,
		},
		[theme.breakpoints.down.xs]: {
			position: 'fixed',
			top: '82px',
			bottom: '0',
			left: '0',
			right: '0',
			width: '100%',
			height: 'auto',
			zIndex: '7',
			background: '#fff',
			borderTop: '1px solid #e8e8e8',
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			friendReqList: state.relationship.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getFriendReqList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class ClassName extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		// store
		store: PropTypes.shape({
			friendReqList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getFriendReqList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		loading: true,
	}

	componentDidMount() {
		this.props.action.getFriendReqList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	filter = {
		limit: 8,
		skip: 0,
		order: 'createdAt DESC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
		],
		where: {
			friendId: AuthStorage.userId,
			status: 'pending',
		},
	}

	render() {
		const { classes, store: { friendReqList } } = this.props;

		if (!this.state.loading && friendReqList.total === 0) {
			return (
				<div className={classes.notFound}>
					<FaBellSlashO />
					Không có yêu cầu kết bạn nào
				</div>
			);
		}

		return (
			<div className={classes.content}>
				{
					!this.state.loading > 0 ?
						friendReqList.data.map((noti) => {
							return <NotiItem key={noti.id} relationshipData={noti} />;
						}) :
						[0, 0, 0, 0].map((noti, i) => {
							return <NotiItem key={i} loading />;
						})
				}
			</div>
		);
	}
}
