/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-06 10:59:53
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Button } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import AvatarBlock from 'src/components/User/AvatarBlock';

import { deleteRelationship, updateRelationship } from 'src/redux/actions/relationship';

const styleSheet = (theme) => ({
	item: {
		padding: '10px 16px',
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		fontSize: '12px',
		borderBottom: '1px solid #e8e8e8',
		fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
		color: '#000',
		'&:hover $control': {
			opacity: 1,
		},
		'@media (max-width: 370px)': {
			padding: '5px',
		},
	},
	unread: {
		background: theme.palette.primary[50],
		borderBottom: '1px solid #dcdbdb',
	},
	text: {
		flex: '1',
		marginLeft: '10px',
		'& p': {
			marginBottom: 2,
			lineHeight: '16px',
			display: '-webkit-box',
			overflow: 'hidden',
			maxHeight: '59px',
			textOverflow: 'ellipsis',
			WebkitLineClamp: '2',
			WebkitBoxOrient: 'vertical',
		},
		'& span': {
			fontSize: '11px',
			// fontStyle: 'italic',
			color: '#90949c',
		},
		'& strong': {
			color: theme.palette.primary[900],
		},
		'& i': {
			fontSize: '14px',
			marginRight: '5px',
			color: theme.palette.primary[900],
		},
	},
	control: {
		marginLeft: '20px',
		'@media (max-width: 370px)': {
			'& $control': {
				marginLeft: 5,
			},
		},
	},
	content: {
		flex: 1,
		display: 'flex',
		alignItems: 'center',
		color: '#000 !important',
	},
});

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			deleteRelationship,
			updateRelationship,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class AddFriendReq extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		relationshipData: PropTypes.object,
		loading: PropTypes.bool,
		// store
		// store: PropTypes.shape({
		// 	modal: PropTypes.object.isRequired,
		// }).isRequired,
		// action
		action: PropTypes.shape({
			deleteRelationship: PropTypes.func.isRequired,
			updateRelationship: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		relationshipData: {},
		loading: false,
	}

	state = {
		loading: false,
	}

	handleAccept = () => {
		this.setState({
			loading: true,
		});

		this.props.action.updateRelationship({
			id: this.props.relationshipData.id,
			updatedAt: new Date(),
			status: 'accepted',
		}, () => {
			this.setState({
				loading: false,
			});
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	handleDecline = () => {
		this.setState({
			loading: true,
		});
		this.props.action.deleteRelationship({
			id: this.props.relationshipData.id,
		}, () => {
			this.setState({
				loading: false,
			});
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	render() {
		const { classes, loading, relationshipData = {} } = this.props;
		const { creator = {} } = relationshipData;

		if (loading) {
			return (
				<div className={classes.item}>
					<div className="loading-block" style={{ width: 50, height: 50, borderRadius: 50, margin: 0 }} />
					<div className={classes.text}>
						<div className="loading-block" />
						<div className="loading-block" />
						<div className="loading-block" style={{ width: '50%' }} />
					</div>
				</div>
			);
		}

		return (
			<div
				className={classes.item}
			>
				<AvatarBlock
					userData={creator}
					date={relationshipData.createdAt}
					size={40}
				/>
				<div className={classes.control}>
					<Button loading={this.state.loading} style={{ marginRight: 5 }} size="small" type="primary" onClick={this.handleAccept}>Chấp nhận</Button>
					<Button loading={this.state.loading} size="small" onClick={this.handleDecline}>Xóa yêu cầu</Button>
				</div>
			</div>
		);
	}
}
