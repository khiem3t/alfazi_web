/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-02 00:26:56
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Form, Input, Button, Modal } from 'antd';

import AuthStorage from 'src/utils/AuthStorage';

// import { classOptions, subjectOptions } from 'src/constants/selectOption';

import { createReport } from 'src/redux/actions/report';
import CheckLogin from 'src/components/Form/CheckLogin';

function mapStateToProps(/* state */) {
	return {
		store: {
			// modal: state.modal,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			createReport,
		}, dispatch),
	};
};

@Form.create()
@connect(mapStateToProps, mapDispatchToProps)
export default class ModalReport extends PureComponent {
	static propTypes = {
		form: PropTypes.object.isRequired,
		questionData: PropTypes.object,
		onRefresh: PropTypes.func,
		onCancel: PropTypes.func,
		visible: PropTypes.bool.isRequired,

		// action
		action: PropTypes.shape({
			createReport: PropTypes.func.isRequired,
		}).isRequired,

		type: PropTypes.string.isRequired,
		answerData: PropTypes.object,
		replyData: PropTypes.object,
		userData: PropTypes.object,
	}

	static defaultProps = {
		onRefresh: f => f,
		onCancel: f => f,

		questionData: undefined,
		answerData: undefined,
		userData: undefined,
		replyData: undefined,
	}

	state = {
		loading: false,
	}

	handleSubmit = () => {
		this.props.form.validateFields((err, values) => {
			if (!err) {
				const { reason } = values;
				const { type, questionData, answerData, userData, replyData } = this.props;
				let dataReport = {};
				let receiverId;
				let questionId;
				let answerId;
				let replyId;

				if (type === 'question') {
					dataReport = questionData;
					questionId = questionData.id;
					receiverId = questionData.creatorId;
				} else if (type === 'answer') {
					dataReport = answerData;
					answerId = answerData.id;
					questionId = answerData.questionId;
					receiverId = answerData.creatorId;
				} else if (type === 'reply') {
					dataReport = replyData;
					replyId = replyData.id;
					answerId = replyData.answerId;
					questionId = replyData.questionId;
					receiverId = replyData.creatorId;
				} else {
					dataReport = userData;
					receiverId = userData.id;
				}

				this.setState({ loading: true });
				this.props.action.createReport({ reason, type, receiverId, replyId, answerId, questionId, dataReport, creatorId: AuthStorage.userId }, () => {
					this.setState({ loading: false }, this.props.onCancel);
				});
			}
		});
	}

	render() {
		const { form: { getFieldDecorator }, visible, ...rest } = this.props;

		return (
			<Modal
				title="Báo cáo vị phạm"
				visible={visible}
				footer={null}
				maskClosable={!this.state.loading}
				closable={!this.state.loading}
				{...rest}
			>
				<Form layout="horizontal">
					<Form.Item>
						{getFieldDecorator('reason', {
							rules: [{ required: true, message: 'Hãy nhập nội dung lý do.' }],
						})(
							<Input.TextArea
								rows={6}
								placeholder="Lý do vi phạm"
							/>,
						)}
					</Form.Item>
					<div className="text-right">
						<CheckLogin onClick={this.handleSubmit}>
							<Button size="large" type="primary" htmlType="submit" loading={this.state.loading}>
								Báo cáo
							</Button>
						</CheckLogin>
					</div>
				</Form>
			</Modal>
		);
	}
}

