/* eslint jsx-a11y/no-noninteractive-element-interactions: 0 */
/* --------------------------------------------------------
* Author Nguyễn Đăng Anh Thi
* Email nguyendanganhthi247@gmail.com
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Modal, Select, Divider, Input } from 'antd';
import withStyles from 'src/theme/jss/withStyles';
import Math from 'src/components/MathTex/Math';
import renderMathInElement from 'src/components/MathTex/lib/auto-render';
import mathExpressions from 'src/components/MathTex/mathExpression';


const { TextArea } = Input;
const Option = Select.Option;

const styleSheet = (/* theme */) => ({
	keyboard: {
		'& .katex': {
			fontSize: '1.2em',
			fontWeight: '600',
		},
		boxShadow: '0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12)',
		border: '1px solid #f9f9f9',
		display: 'inline-block',
		WebkitBorderRadius: '5px',
		cursor: 'pointer',
		margin: '10px',
		padding: '10px',
		'&:hover': {
			position: 'relative',
			top: '1px',
			left: '1px',
			borderColor: '#e5e5e5',
			cursor: 'pointer',
		},
	},
	answerHeader: {
		marginTop: '20px',
	},
	renderingBox: {
		marginTop: '10px',
		padding: '20px',
		fontSize: '17px',
		whiteSpace: 'pre-line',
		boxShadow: '0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12)',
		maxHeight: '100px',
		overflowY: 'scroll',
	},
});

@withStyles(styleSheet)
export default class MathEditor extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		onChange: PropTypes.func.isRequired,
		content: PropTypes.string.isRequired,
		updateContentWithMathExpressions: PropTypes.func.isRequired,
	}

	static defaultProps = {}

	constructor(props) {
		super(props);
		this.state = {
			selectedExpressions: Object.values(mathExpressions)[0],
		};
		this.handleSelectChange = this.handleSelectChange.bind(this);
	}

	componentDidUpdate() {
		if (this.renderingBox) {
			renderMathInElement(
				this.renderingBox,
				{
					delimiters: [
						{ left: '$', right: '$', display: false },
					],
				},
			);
		}
	}


	handleSelectChange(option) {
		this.setState({
			selectedExpressions: mathExpressions[option],
		});
	}

	handleMathKeyboardClick(key) {
		this.props.updateContentWithMathExpressions(this.state.selectedExpressions[key]);
	}

	render() {
		const { classes, content, onChange } = this.props;
		const mathExpressionOptions = Object.keys(mathExpressions);
		// where user can select math or chemistry expression

		return (
			<div>
				<Modal
					title=""
					mask={false}
					bodyStyle={{ height: '80vh' }}
					width="80vw"
					className="ignore-react-onclickoutside"
					{...this.props}
				>
					<Select
						style={{ width: 200 }}
						defaultValue={mathExpressionOptions[0]}
						onChange={this.handleSelectChange}
					>
						{
							mathExpressionOptions.map(title => (
								<Option value={title} key={title}>{title}</Option>
							))
						}
					</Select>
					<ul style={{ padding: 0 }}>
						{
							Object.keys(this.state.selectedExpressions).map(key => (
								<li
									className={classes.keyboard}
									key={key}
									onClick={() => this.handleMathKeyboardClick(key)}
								>
									<Math math={this.state.selectedExpressions[key]} />
								</li>
							))
						}
					</ul>
					<TextArea value={content} onChange={onChange} className={classes.textarea} rows={3} />
					<h3 className={classes.answerHeader}>Trả lời</h3>
					<Divider style={{ width: '10%', margin: 0 }} />
					{ content &&
						<div
							ref={node => { this.renderingBox = node; }}
							className={classes.renderingBox}
						>
							{ content }
						</div>
					}
				</Modal>
			</div>
		);
	}
}
