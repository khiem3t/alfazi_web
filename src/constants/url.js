/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-09 15:44:20
*------------------------------------------------------- */

export default {
	API_URL: process.env.API_URL || 'http://localhost:3002/api/v1',
	WEB_URL: process.env.WEB_URL || 'http://localhost:3001',
};

