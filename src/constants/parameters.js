/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-01 00:23:35
*------------------------------------------------------- */

export const MINIMUM_TIME_TO_UNLOCK = 15; // minutes

export const QUESTION_FEE = 6; // coins
export const QUESTION_FEE_REFUND = 1; // coins

export const QUESTION_CREATE_REWARD_EXP = 2; // exp

export const ANSWER_CREATE_REWARD_EXP = 5; // exp

export const ANSWER_LIKE_REWARD_EXP = 5; // exp

export const ANSWER_PICK_REWARD_EXP = 10; // exp
export const ANSWER_PICK_REWARD_COIN = 4; // coins
