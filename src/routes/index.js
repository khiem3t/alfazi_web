/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-12 01:26:53
*------------------------------------------------------- */

const routes = module.exports = require('next-routes')(); // eslint-disable-line

routes.add({ pattern: '/profile/edit/:id', page: 'profile' });
routes.add({ pattern: '/profile/:id', page: 'profile' });

routes.add({ pattern: '/question/:id', page: 'question' });

routes.add({ pattern: '/blog/post/:slug', page: 'blog/post' });

