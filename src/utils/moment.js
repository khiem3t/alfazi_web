import moment from 'moment';

moment.locale('vi');

moment.prototype.formatCustom = function (date) { // eslint-disable-line
	const diff = moment().diff(this, 'days');
	if (diff < 5) {
		return this.fromNow();
	}
	return this.format('D [Tháng] M [lúc] HH:mm');
};

export default moment;
