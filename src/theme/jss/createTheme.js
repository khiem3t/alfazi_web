/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-12 11:04:40
*------------------------------------------------------- */
export default () => ({
	palette: {
		primary: {
			'50': 'rgba(20, 97, 89, 0.15)',
			'100': '#B2DFDB',
			'200': '#80CBC4',
			'300': '#4DB6AC',
			'400': '#26A69A',
			'500': '#009688',
			'600': '#00897B',
			'700': '#00796B',
			'800': '#00695C',
			'900': '#136259',
		},
		text: {
			primary: 'rgba(0, 0, 0, 0.87)',
			secondary: 'rgba(0, 0, 0, 0.54)',
			disabled: 'rgba(0, 0, 0, 0.38)',
			hint: 'rgba(0, 0, 0, 0.38)',
			icon: 'rgba(0, 0, 0, 0.38)',
			divider: 'rgba(0, 0, 0, 0.12)',
			lightDivider: 'rgba(0, 0, 0, 0.075)',
			white: 'rgba(255,255,255, 1);',
			light: 'rgba(0, 0, 0, 0.25)',
		},
	},
	breakpoints: {
		up: {
			sm: '@media (min-width: 576px)',
			md: '@media (min-width: 768px)',
			lg: '@media (min-width: 992px)',
			xl: '@media (min-width: 1200px)',
		},
		down: {
			xs: '@media (max-width: 575.98px)',
			sm: '@media (max-width: 767.98px)',
			md: '@media (max-width: 991.98px)',
			lg: '@media (max-width: 1199.98px)',
		},
		only: {
			xs: '@media (max-width: 575.98px)',
			sm: '@media (min-width: 576px) and (max-width: 767.98px)',
			md: '@media (min-width: 768px) and (max-width: 991.98px)',
			lg: '@media (min-width: 992px) and (max-width: 1199.98px)',
			xl: '@media (min-width: 1200px)',
		},
	},
});
