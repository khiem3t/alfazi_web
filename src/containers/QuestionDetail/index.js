/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 01:34:48
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Sticky from 'react-stickynode';
// import moment from 'src/utils/moment';

import Title from 'src/components/Layout/Title';
import Head from 'next/head';

import withStyles from 'src/theme/jss/withStyles';

import { Row, Col } from 'antd';

import AuthStorage from 'src/utils/AuthStorage';

import Container from 'src/components/Layout/Container';
import RelativeQuestion from 'src/components/Widget/RelativeQuestion';
import AnswerList from 'src/components/Answer/List';

import { getQuestionData } from 'src/redux/actions/question';

import URL from 'src/constants/url';

import Content from './Content';

const styleSheet = (/* theme */) => ({
	root: {
		minHeight: 'calc(100vh - 415px)',
	},
	content: {
		display: 'flex',
		border: '1px solid #e8e8e8',
	},
	questionRelative: {
		marginTop: 20,
	},
	moreComment: {
		textAlign: 'center',
		padding: '20px 0',
	},
	notFound: {
		fontSize: '20px',
		padding: '100px',
		textAlign: 'center',
		color: 'gray',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			questionData: state.question.questionView,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getQuestionData,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class QuestionDetail extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionId: PropTypes.string.isRequired,
		// store
		store: PropTypes.shape({
			questionData: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getQuestionData: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		loading: true,
		where: {
			status: 'active',
			questionId: this.props.questionId,
		},
	}

	componentDidMount() {
		if (this.props.questionId) {
			this.props.action.getQuestionData({ id: this.props.questionId, filter: this.filter }, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.questionId && this.props.questionId !== nextProps.questionId) {
			this.setState({
				loading: true,
				where: {
					status: 'active',
					questionId: nextProps.questionId,
				},
			});
			nextProps.action.getQuestionData({ id: nextProps.questionId, filter: this.filter }, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	filter = {
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
			{
				relation: 'pick',
			},
			{
				relation: 'saves',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						creatorId: AuthStorage.userId,
					},
				},
			},
		],
		counts: ['saves', 'reports'],
		where: {
			status: 'active',
		},
	}

	handleRefresh = () => {
		// this.forceUpdate();
	}

	render() {
		const { classes, questionId, store: { questionData } } = this.props;

		const { creator = {} } = questionData;

		if (questionData.status === 'deleted') {
			return (
				<div className={classes.notFound}>
					Câu hỏi này đã bị xóa.
				</div>
			);
		}

		if (questionData.status === 'hidden' && questionData.creatorId !== AuthStorage.userId) {
			return (
				<div className={classes.notFound}>
					Câu hỏi này không còn hoạt động nữa
				</div>
			);
		}

		const questionHasPick = questionData.pick && !!questionData.pick.id;

		return (
			<Container className={classes.root}>
				<Title>{questionData.content}</Title>
				<Head>
					<meta
						name="description"
						content={questionData.content}
					/>
					{/* Twitter */}
					<meta name="twitter:title" content={`${creator.fullName} hỏi về chủ đề ${questionData.subject} ${questionData.class} | Alfazi`} />
					<meta
						name="twitter:description"
						content={questionData.content}
					/>
					<meta name="twitter:image" content={questionData.images && questionData.images.length > 0 ? questionData.images[0] : (URL.WEB_URL + '/static/assets/images/logo/256x256.png')} />
					{/* Facebook */}
					<meta property="og:url" content={URL.WEB_URL + '/question/' + questionData.id} />
					<meta property="og:title" content={`${creator.fullName} hỏi về chủ đề ${questionData.subject} ${questionData.class} | Alfazi`} />
					<meta
						property="og:description"
						content={questionData.content}
					/>
					<meta property="og:image" content={questionData.images && questionData.images.length > 0 ? questionData.images[0] : (URL.WEB_URL + '/static/assets/images/logo/256x256.png')} />
				</Head>
				<Row gutter={20}>
					<Col xs={24} lg={16}>
						<Content
							loading={this.state.loading}
							questionData={questionData}
						/>
						{
							questionId &&
							<AnswerList
								where={this.state.where}
								questionId={questionId}
								questionData={questionData}
								questionHasPick={questionHasPick}
								onRefresh={this.handleRefresh}
							/>
						}
					</Col>
					<div id="question-cropper" />
					<Col xs={24} lg={8} >
						<Sticky top={50} bottomBoundary="#content" className="static-xs-down">
							<div className={classes.questionRelative}>
								<RelativeQuestion />
							</div>
						</Sticky>
					</Col>
				</Row>
			</Container>
		);
	}
}
