/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-03 01:34:48
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';
import Router from 'next/router';

import { Tag } from 'antd';

import AvatarBlock from 'src/components/User/AvatarBlock';
import Latex from 'react-latex';
import ImageLightBoxContainer from 'src/components/LightBox/ImageLightBoxContainer';
import RibbonQuestion from 'src/components/Question/Ribbon';

const styleSheet = (theme) => ({
	content: {
		position: 'relative',
		padding: 20,
		background: '#fff',
		marginTop: 5,
		[theme.breakpoints.up.lg]: {
			marginTop: 20,
		},
		// borderBottom: '1px solid #e8e8e8',
	},
	textWrapper: {
		overflowX: 'auto',
		width: '100%',
	},
	tag: {
		marginTop: 30,
	},
	tagItem: {
		margin: '5px 5px 0 0',
	},
	imgWrapper: {
		overflowX: 'auto',
		marginTop: 20,
	},
	img: {
		display: 'flex',
		'& img': {
			height: 200,
			width: 'auto',
			marginRight: 3,
		},
	},
	avatarWrap: {
		display: 'flex',
		marginBottom: '20px',
		alignItems: 'self-start',
	},
	info: {
		marginLeft: '10px',
		flex: 1,
		'& h3': {
			margin: 0,
		},
		'& span': {
			margin: 0,
			fontStyle: 'italic',
		},
	},
	ribbon: {
		float: 'right',
	},
});

@withStyles(styleSheet)
export default class QuestionContent extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		questionData: PropTypes.object,
		loading: PropTypes.bool,
	}

	static defaultProps = {
		loading: false,
		questionData: {},
	}

	render() {
		const { classes, questionData = {}, loading } = this.props;
		const { creator = {} } = questionData;

		if (loading) {
			return (
				<div className={classes.content}>
					<div className={classes.avatarWrap}>
						<div className="loading-block" style={{ width: 50, height: 50, borderRadius: 50 }} />
						<div className={classes.info}>
							<div>
								<div className="loading-block" style={{ width: 200 }} />
							</div>
							<div className="loading-block" style={{ width: 100 }} />
						</div>
					</div>
					<h3>
						<div className="loading-block" />
						<div className="loading-block" />
						<div className="loading-block" />
						<div className="loading-block" />
						<div className="loading-block" />
						<div className="loading-block" />
						<div className="loading-block" style={{ width: '50%' }} />
					</h3>
				</div>
			);
		}

		return (
			<div className={classes.content}>
				<div className={classes.ribbon}>
					<RibbonQuestion type={questionData.type} />

				</div>
				<div className={classes.avatarWrap}>
					<AvatarBlock
						userData={creator}
						date={questionData.createdAt}
					/>
				</div>
				<div className={classes.textWrapper}>

					<h3 className="pre-wrap">
						<Latex>
							{questionData.content}
						</Latex>

					</h3>
				</div>
				<div className={classes.tag}>
					{
						questionData.subject &&
						<Tag className={classes.tagItem} color="blue" onClick={() => Router.push(`/?subject=${questionData.subject}`)}>{questionData.subject}</Tag>
					}
					{
						questionData.class &&
						<Tag className={classes.tagItem} color="volcano" onClick={() => Router.push(`/?class=${questionData.class}`)}>{questionData.class}</Tag>
					}
					{
						questionData.hashtag &&
						questionData.hashtag.map((el, i) => {
							return <Tag className={classes.tagItem} key={i} color="purple" onClick={() => Router.push(`/?hashtag=${el}`)}>{el}</Tag>;
						})
					}
				</div>
				<div className={classes.imgWrapper}>
					<ImageLightBoxContainer
						className={classes.img}
						images={questionData.images}
					/>
				</div>
			</div>
		);
	}
}
