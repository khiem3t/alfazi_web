/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-23 14:44:53
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Row, Col, Button, Icon, Modal } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import { receivedAchievement } from 'src/redux/actions/achievement';

const styleSheet = (theme) => ({
	root: {
		background: '#fff',
		marginBottom: 10,
		padding: '20px',
	},
	value: {
		fontSize: 18,
		color: theme.palette.primary[900],
		fontWeight: '500',
		padding: '5px 0',
		display: 'block',
	},
	received: {
		color: '#52c41a',
	},
	highlight: {
		backgroundColor: theme.palette.primary[50],
	},
});

function mapStateToProps(state) {
	return {
		store: {
			auth: state.auth,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			receivedAchievement,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class AchievementItem extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		achievementData: PropTypes.object,
		loading: PropTypes.bool,
		onRefresh: PropTypes.func,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			receivedAchievement: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		achievementData: {},
		loading: false,
		onRefresh: f => f,
	}

	handleReceivedAchievement = () => {
		const { achievementData } = this.props;

		const { achievementsUnlock = [{}] } = achievementData;

		if (achievementsUnlock[0] && achievementsUnlock[0].status === 'notReceived') {
			this.props.action.receivedAchievement({ id: achievementsUnlock[0].id }, () => {
				this.props.onRefresh();
			});
		}
	}

	handleOpenInstruction = (message) => {
		Modal.info({
			title: 'Cách nhận được xu',
			content: (
				<div>
					<p>{message}</p>
				</div>
			),
			onOk() { },
		});
	}

	render() {
		const { classes, achievementData, loading, store: { auth = {} } } = this.props;

		const { achievementsUnlock = [] } = achievementData;

		if (loading) {
			return (
				<div className={classes.root}>
					<Row gutter={20} type="flex" align="middle">
						<Col className="text-center-xs-down" xs={24} sm={16}>
							<div className="loading-block" style={{ height: 18, width: '40%' }} />
							<div className="loading-block" />
							<div className="loading-block" style={{ width: '70%' }} />
						</Col>
					</Row>
				</div>
			);
		}

		const Action = () => {
			if (!achievementsUnlock[0]) {
				return <a onClick={() => this.handleOpenInstruction(achievementData.instruction)}>Xem hướng dẫn</a>;
			}

			if (achievementsUnlock[0].status === 'notReceived') {
				return <Button type="primary" onClick={this.handleReceivedAchievement}>Nhận thưởng</Button>;
			} else if (achievementsUnlock[0].status === 'received' && achievementData.type === 'attendance' && auth.attendance && !auth.attendance.loggedIn) {
				return <a onClick={() => this.handleOpenInstruction(achievementData.instruction)}>Xem hướng dẫn</a>;
			}
			return (
				<div className={classes.received}>
					<Icon type="check" /> Đã nhận
				</div>
			);
		}

		return (
			<div className={classes.root}>
				<Row gutter={20} type="flex" align="middle">
					<Col className="text-center-xs-down" xs={24} sm={16}>
						<h3>{achievementData.name}</h3>
						{achievementData.desc}
					</Col>
					<Col className="text-center-xs-down" xs={24} sm={4}>
						<span className={classes.value}>+{achievementData.amount} xu</span>
					</Col>
					<Col className="text-center" xs={24} sm={4}>
						{
							achievementData.type === 'referral' &&
							<h4>{achievementsUnlock.length || 0}/5</h4>
						}
						<Action />
					</Col>
				</Row>
			</div>
		);
	}
}

