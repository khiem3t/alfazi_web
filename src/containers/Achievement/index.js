/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-22 10:15:52
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import Container from 'src/components/Layout/Container';

import { getAchievementList } from 'src/redux/actions/achievement';

import Item from './Item';

const styleSheet = (/* theme */) => ({
	root: {
		marginTop: 30,
	},
});

function mapStateToProps(state) {
	return {
		store: {
			achievementList: state.achievement.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getAchievementList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class Achievement extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		// store
		store: PropTypes.shape({
			achievementList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getAchievementList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		loading: true,
	}

	componentDidMount() {
		this.props.action.getAchievementList({
			firstLoad: true,
			filter: this.filter,
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	filter = {
		order: 'name ASC',
		include: [
			{
				relation: 'achievementsUnlock',
				scope: {
					order: 'status',
					limit: 5,
					where: {
						userId: AuthStorage.userId,
					},
				},
			},
		],
	}

	handleRefresh = () => {
		this.setState({
			loading: true,
		});
		this.props.action.getAchievementList({
			firstLoad: true,
			filter: this.filter,
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	render() {
		const { classes, store: { achievementList } } = this.props;

		const list = achievementList.data.sort((a, b) => {
			return b.achievementsUnlock.length - a.achievementsUnlock.length;
		});

		return (
			<Container className={classes.root}>
				{
					this.state.loading ?
						[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map((ach, i) => {
							return <Item key={i} loading />;
						}) :
						list.map((ach) => {
							return <Item key={ach.type} achievementData={ach} onRefresh={this.handleRefresh} />;
						})
				}
			</Container>
		);
	}
}
