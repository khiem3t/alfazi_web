/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 14:12:00
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import UserItem from 'src/components/User/Item';

import { getUserList } from 'src/redux/actions/user';

const styleSheet = (/* theme */) => ({
	root: {
		background: '#fff',
		padding: '20px 20px 10px',
	},
	notFound: {
		padding: '50px 0',
		background: '#fff',
		textAlign: 'center',
		marginBottom: '20px',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			userList: state.user.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getUserList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class UserListSearch extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		where: PropTypes.object,
		searchText: PropTypes.string,
		// store
		store: PropTypes.shape({
			userList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getUserList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		where: {},
		searchText: '',
	}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		this.props.action.getUserList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.searchText !== this.props.searchText) {
			this.setState({
				loading: true,
			});
			this.filter.skip = 0;
			this.filter.page = 1;
			this.filter.where.or = [
				{ fullName: { regexp: '/' + nextProps.searchText + '/i' } },
				{ address: { regexp: '/' + nextProps.searchText + '/i' } },
				{ province: { regexp: '/' + nextProps.searchText + '/i' } },
				{ class: { regexp: '/' + nextProps.searchText + '/i' } },
				{ school: { regexp: '/' + nextProps.searchText + '/i' } },
			];
			nextProps.action.getUserList({ filter: this.filter, firstLoad: true }, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	filter = {
		limit: 5,
		skip: 0,
		page: 1,
		order: 'createdAt DESC',
		include: [
			{
				relation: 'friendsSent',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						friendId: AuthStorage.userId,
					},
				},
			},
			{
				relation: 'friendsReceived',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						creatorId: AuthStorage.userId,
					},
				},
			},
		],
		// counts: ['questions', 'friendsSent', 'friendsReceived'],
		where: {
			status: 'active',
			id: { nin: [AuthStorage.userId] },
			or: [
				{ fullName: { regexp: '/' + this.props.searchText + '/i' } },
				{ address: { regexp: '/' + this.props.searchText + '/i' } },
				{ province: { regexp: '/' + this.props.searchText + '/i' } },
				{ class: { regexp: '/' + this.props.searchText + '/i' } },
				{ school: { regexp: '/' + this.props.searchText + '/i' } },
			],
		},
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getUserList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	render() {
		const { classes, store: { userList } } = this.props;

		if (userList.data.length === 0 && !this.state.loading) {
			return null;
		}

		return (
			<div className={classes.root}>
				<h3 style={{ marginBottom: 20 }} className="text-center-sm-down">
					Danh sách người dùng
					{
						userList.total > 0 && <span style={{ marginLeft: 10 }}>({userList.total})</span>
					}
				</h3>
				{
					userList.data.length === 0 && !this.state.loading ?
						<div className={classes.notFound}>
							Không tìm thấy người dùng phù hợp.
						</div> :
						<div style={{ borderTop: '1px solid #e8e8e8' }}>
							{
								this.state.loading ?
									<div>
										<UserItem loading />
										<UserItem loading />
									</div> :
									userList.data.map((user) => {
										return (
											<UserItem key={user.id} userData={user} />
										);
									})
							}
							{
								this.state.loadingMore &&
								<div>
									<UserItem loading />
									<UserItem loading />
								</div>
							}
							{
								!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < userList.total &&
								<div className="text-center">
									<a type="primary" onClick={this.handleViewMore}>Xem thêm</a>
								</div>
							}
						</div>
				}
			</div>
		);
	}
}
