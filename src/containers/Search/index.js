/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-28 10:22:16
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import withStyles from 'src/theme/jss/withStyles';

import { Row, Col } from 'antd';

import Container from 'src/components/Layout/Container';

import ListQuestion from './Question';
import ListUser from './User';
import ListBlog from './Blog';

const styleSheet = (/* theme */) => ({
	root: {
		marginTop: 20,
	},
});

@withStyles(styleSheet)
export default class Search extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		searchText: PropTypes.string.isRequired,
	}

	static defaultProps = {}

	render() {
		const { classes, searchText } = this.props;

		return (
			<Container className={classes.root}>
				<h2 className="text-center">Tìm kiếm với từ khóa '<strong>{searchText}</strong>'</h2>
				<Row gutter={10}>
					<Col xs={24} lg={{ offset: 6, span: 12 }}>
						<ListUser
							searchText={searchText}
						/>
						<ListBlog
							searchText={searchText}
						/>
						<ListQuestion
							searchText={searchText}
						/>
					</Col>
				</Row>
			</Container>
		);
	}
}
