/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-20 17:12:47
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'src/utils/moment';

import { Link } from 'src/routes';

import withStyles from 'src/theme/jss/withStyles';

import { getPostList } from 'src/redux/actions/blogPost';

import Item from './Item';

const styleSheet = (/* theme */) => ({
	root: {
		marginTop: 20,
		background: '#fff',
		padding: '20px 20px 10px',
	},
	itemHighlight: {
		backgroundRepeat: 'no-repeat',
		backgroundColor: '#e9ebee',
		backgroundSize: 'cover',
		height: 200,
		marginBottom: '10px',
		padding: '15px',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'flex-end',
		position: 'relative',
		'& h3': {
			color: '#fff',
		},
		'& span': {
			color: '#fff',
			fontSize: 12,
		},
		'&:before': {
			content: '""',
			position: 'absolute',
			top: '0',
			right: '0',
			left: '0',
			bottom: '0',
			background: 'rgba(0, 0, 0, 0.21)',
		},
	},
	inner: {
		position: 'relative',
		zIndex: 1,
	},
	title: {
		marginBottom: '10px',
		position: 'relative',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			list: state.blogPost.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getPostList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class BlogPostSearch extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		searchText: PropTypes.string,
		// store
		store: PropTypes.shape({
			list: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getPostList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		searchText: undefined,
	}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		this.props.action.getPostList({ firstLoad: true, filter: this.filter }, () => {
			this.setState({
				loading: false,
			});
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.searchText !== this.props.searchText) {
			this.setState({
				loading: true,
			});
			this.filter.skip = 0;
			this.filter.page = 1;
			this.filter.where.or = [
				{ title: { regexp: '/' + nextProps.searchText + '/i' } },
				{ brief: { regexp: '/' + nextProps.searchText + '/i' } },
				{ content: { regexp: '/' + nextProps.searchText + '/i' } },
				{ tags: { regexp: '/' + nextProps.searchText + '/i' } },
			];
			nextProps.action.getPostList({ filter: this.filter, firstLoad: true }, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	filter = {
		limit: 5,
		skip: 0,
		page: 1,
		order: 'publishedDate DESC',
		// include: 'category',
		fields: { content: false },
		where: {
			state: 'published',
			publishedDate: { lte: new Date() },
			or: [
				{ title: { regexp: '/' + this.props.searchText + '/i' } },
				{ brief: { regexp: '/' + this.props.searchText + '/i' } },
				{ content: { regexp: '/' + this.props.searchText + '/i' } },
				{ tags: { regexp: '/' + this.props.searchText + '/i' } },
			],
		},
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getPostList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	render() {
		const { classes, store: { list = { data: [] } } } = this.props;
		if (list.data.length === 0 && !this.state.loading) {
			return null;
		}
		return (
			<div className={classes.root}>
				<h3 className={classes.title + ' text-center-sm-down'}>
					Bài viết ({list.total})
				</h3>
				{
					this.state.loading ?
						[0, 0, 0, 0, 0].map((post, i) => {
							if (i === 0) {
								return (
									<div key={i} className={classes.itemHighlight}>
										<div className={classes.inner}>
											<div className="loading-block" />
											<div className="loading-block" style={{ width: '60%' }} />
											<div className="loading-block" style={{ width: '40%' }} />
										</div>
									</div>
								);
							}
							return <Item key={i} loading />;
						}) :
						list.data.map((post, i) => {
							if (i === 0) {
								return (
									<Link key={post.id} route={'/blog/post/' + post.slug}>
										<a>
											<div className={classes.itemHighlight} style={{ backgroundImage: 'url("' + (post.image && post.image.secure_url) + '")' }}>
												<div className={classes.inner}>
													<h3>{post.title}</h3>
													<span>{moment(post.publishedDate).format('LL')}</span>
												</div>
											</div>
										</a>
									</Link>
								);
							}
							return <Item key={post.id} postData={post} />;
						})
				}
				{
					this.state.loadingMore &&
					<div>
						<Item loading />
						<Item loading />
					</div>
				}
				{
					!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < list.total &&
					<div className="text-center">
						<a type="primary" onClick={this.handleViewMore}>Xem thêm</a>
					</div>
				}
			</div>
		);
	}
}

