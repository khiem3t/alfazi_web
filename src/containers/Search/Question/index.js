/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 11:58:01
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Button } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import QuestionCard from 'src/components/Question/Card';
import AuthStorage from 'src/utils/AuthStorage';

import { getQuestionList } from 'src/redux/actions/question';

import QuestionListLoading from './Loading';

const styleSheet = (/* theme */) => ({
	list: {
		marginTop: 20,
	},
	notFound: {
		padding: '50px 0',
		background: '#fff',
		textAlign: 'center',
		marginBottom: '20px',
	},
});

const mapStateToProps = (state) => {
	return {
		store: {
			auth: state.auth,
			questionList: state.question.questionList,
		},
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getQuestionList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class QuestionList extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		searchText: PropTypes.string,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
			questionList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getQuestionList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		searchText: '',
	}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		this.props.action.getQuestionList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.searchText !== this.props.searchText) {
			this.setState({
				loading: true,
			});
			this.filter.skip = 0;
			this.filter.page = 1;
			this.filter.where.or = [
				{ content: { regexp: '/' + nextProps.searchText + '/i' } },
				{ hashTag: { regexp: '/' + nextProps.searchText + '/i' } },
				{ subject: { regexp: '/' + nextProps.searchText + '/i' } },
				{ class: { regexp: '/' + nextProps.searchText + '/i' } },
			];
			nextProps.action.getQuestionList({ filter: this.filter, firstLoad: true }, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	filter = {
		limit: 12,
		skip: 0,
		page: 1,
		order: 'updatedAt DESC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
			{
				relation: 'saves',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						creatorId: AuthStorage.userId,
					},
				},
			},
		],
		counts: ['answers', 'saves'],
		where: {
			status: 'active',
			or: [
				{ content: { regexp: '/' + this.props.searchText + '/i' } },
				{ hashTag: { regexp: '/' + this.props.searchText + '/i' } },
				{ subject: { regexp: '/' + this.props.searchText + '/i' } },
				{ class: { regexp: '/' + this.props.searchText + '/i' } },
			],
		},
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getQuestionList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	render() {
		const { classes, store: { questionList = { data: [] } } } = this.props;

		if (questionList.total === 0 && !this.state.loading) {
			return null;
		}

		return (
			<div className={classes.list}>
				<h3 className="text-center-sm-down">Câu hỏi ({questionList.total})</h3>
				{
					questionList.total === 0 && !this.state.loading &&
					<div className={classes.notFound}>
						Không tìm thấy câu hỏi nào.
					</div>
				}

				{
					this.state.loading ?
						<QuestionListLoading /> :
						questionList.data.map((question) => {
							return (
								<QuestionCard key={question.id} questionData={question} />
							);
						})
				}
				{
					this.state.loadingMore && <QuestionListLoading />
				}
				{
					!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < questionList.total &&
					<div className="text-center">
						<Button type="primary" style={{ width: 300, margin: '0 auto' }} onClick={this.handleViewMore}>Xem thêm</Button>
					</div>
				}
			</div>
		);
	}
}
