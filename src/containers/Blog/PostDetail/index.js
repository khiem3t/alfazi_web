/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-19 16:13:46
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Sticky from 'react-stickynode';
import moment from 'src/utils/moment';
import Head from 'next/head';
import Title from 'src/components/Layout/Title';

import Link from 'next/link';

import withStyles from 'src/theme/jss/withStyles';

import { Tag, Icon, Row, Col, Breadcrumb } from 'antd';

import Container from 'src/components/Layout/Container';
import ShareList from 'src/components/Form/ShareList';
import BlogPostTop from 'src/components/Widget/BlogPostTop';
import BlogPostRelative from 'src/components/Widget/BlogPostRelative';

import LINK from 'src/constants/url';

import { getPostData } from 'src/redux/actions/blogPost';

const styleSheet = (/* theme */) => ({
	// '@global body': {
	// 	background: '#fff',
	// },
	root: {
		marginTop: 20,
	},
	stats: {
		display: 'flex',
		alignItems: 'center',
		marginBottom: '30px',
	},
	statsItem: {
		display: 'flex',
		alignItems: 'center',
		fontSize: '12px',
		color: '#676767',
		marginRight: 20,
		'& i': {
			fontSize: '14px',
			marginRight: '3px',
		},
	},
	main: {
		background: '#fff',
		marginTop: '25px',
		marginBottom: '20px',
		padding: 15,
	},
	img: {
		marginBottom: '40px',
		'& img': {
			width: '100%',
		},
	},
	content: {
		'& img': {
			maxWidth: '100%',
		},
		'& p.MsoListParagraph': {
			textIndent: '0 !important',
		},
	},
	action: {
		marginTop: '40px',
		// marginBottom: '40px',
		padding: '20px 0 10px',
		display: 'flex',
		justifyContent: 'space-between',
		borderTop: '1px solid #e6e6e6e6',
		'& span': {
			fontSize: 18,
		},
	},
	tags: {
		display: 'flex',
		alignItems: 'center',
		flex: '1',
		overflow: 'scroll',
		margin: '0 10px',
	},
	breadcrumb: {
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		textOverflow: 'ellipsis',
		'& span': {
			overflow: 'hidden',
			whiteSpace: 'nowrap',
		},
	},
	notFound: {
		fontSize: '20px',
		padding: '100px 15px',
		textAlign: 'center',
		color: 'gray',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			postData: state.blogPost.view,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getPostData,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class PostDetail extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		slug: PropTypes.string.isRequired,
		// store
		store: PropTypes.shape({
			postData: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getPostData: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	// state = {
	// 	loading: true,
	// }

	componentDidMount() {
		this.props.action.getPostData({ filter: { where: { slug: this.props.slug }, include: 'category' } }, () => {
			// this.setState({
			// 	loading: false,
			// });
		}, () => {
			// this.setState({
			// 	loading: false,
			// });
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.slug !== this.props.slug) {
			nextProps.action.getPostData({ filter: { where: { slug: nextProps.slug }, include: 'category' } }, () => {
				// this.setState({
				// 	loading: false,
				// });
			}, () => {
				// this.setState({
				// 	loading: false,
				// });
			});
		}
	}


	render() {
		const { classes, store: { postData = { } } } = this.props;
		const { category = {} } = postData;
		const shareUrl = LINK.WEB_URL + '/blog/post/' + postData.slug;
		// const title = postData.brief;

		if (!postData.loading && !postData.id) {
			return (
				<div className={classes.notFound}>
					Bài viết không tồn tại.
				</div>
			);
		}

		if (postData.loading) {
			return (
				<Container className={classes.root}>
					<Row gutter={24}>
						<Col md={16}>
							<div className="loading-block" />
							<div className={classes.main}>
								<div className="loading-block" style={{ height: 25 }} />
								<div className="loading-block" style={{ height: 25, width: '60%', marginBottom: 40 }} />

								<div className={classes.img}>
									<div className="loading-block" style={{ height: 300 }} />
								</div>

								<div className={classes.content}>
									<div className="loading-block" />
									<div className="loading-block" />
									<div className="loading-block" />
									<div className="loading-block" />
									<div className="loading-block" />
									<div className="loading-block" />
									<div className="loading-block" style={{ width: '60%', marginBottom: 100 }} />
								</div>
							</div>
						</Col>
						<Col md={8}>
							<Sticky top={70} bottomBoundary="#content">
								<BlogPostTop loading={postData.loading} />
							</Sticky>
						</Col>
					</Row>
				</Container>
			);
		}

		return (
			<Container className={classes.root}>
				<Title>{postData.title}</Title>
				<Head>
					<meta
						name="description"
						content={postData.brief}
					/>
					{/* Twitter */}
					<meta name="twitter:title" content={postData.title + ' | Alfazi'} />
					<meta
						name="twitter:description"
						content={postData.brief}
					/>
					<meta name="twitter:image" content={postData.image && postData.image.secure_url} />
					{/* Facebook */}
					<meta property="og:url" content={shareUrl} />
					<meta property="og:title" content={postData.title + ' | Alfazi'} />
					<meta
						property="og:description"
						content={postData.brief}
					/>
					<meta property="og:image" content={postData.image && postData.image.secure_url} />
				</Head>
				<Row gutter={24}>
					<Col md={16}>
						<Breadcrumb className={classes.breadcrumb}>
							<Breadcrumb.Item>Home</Breadcrumb.Item>
							<Breadcrumb.Item><Link href="/blog" ><a>Blog</a></Link></Breadcrumb.Item>
							<Breadcrumb.Item><Link href={'/blog?categoryId=' + category.id} ><a>{category.name}</a></Link></Breadcrumb.Item>
							<Breadcrumb.Item>{postData.title}</Breadcrumb.Item>
						</Breadcrumb>
						<div className={classes.main}>
							<h1>{postData.title}</h1>
							<div className={classes.stats}>
								<div className={classes.statsItem}>
									<Icon type="clock-circle-o" />
									{moment(postData.publishedDate).format('LL')}
								</div>
								<div className={classes.statsItem}>
									<Icon type="eye-o" />
									{postData.viewsCount || 0}
								</div>
							</div>

							<div className={classes.img}>
								<img src={postData.image && postData.image.secure_url} alt="" />
							</div>

							<div className={classes.content} dangerouslySetInnerHTML={{ __html: postData.content }} />
							<div className={classes.action}>
								<span>Tags:</span>
								<div className={classes.tags}>
									{
										postData.tags && postData.tags.split(',').map((tag) => {
											return <Tag key={tag} color="green">{tag}</Tag>;
										})
									}
								</div>
								<div className={classes.share}>
									<ShareList shareUrl={shareUrl} />
								</div>
							</div>
						</div>
						{
							postData.id &&
							<BlogPostRelative categoryId={postData.categoryId} postCurrentId={postData.id} />
						}
					</Col>
					<Col md={8}>
						<Sticky top={70} bottomBoundary="#content">
							{
								postData.id &&
								<BlogPostTop postCurrentId={postData.id} />
							}
						</Sticky>
					</Col>
				</Row>
			</Container>
		);
	}
}
