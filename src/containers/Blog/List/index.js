/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-19 15:07:14
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Tabs, Pagination } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import BlogList from 'src/components/Post/List';

import { getCategoryList } from 'src/redux/actions/blogCategory';
import { getPostList } from 'src/redux/actions/blogPost';

import Router from 'next/router';

import Banner from './Banner';

const styleSheet = (/* theme */) => ({
	root: {},
	tab: {
		textAlign: 'center',
		marginTop: '20px',
	},
});

function mapStateToProps(state) {
	return {
		store: {
			blogCategoryList: state.blogCategory.list,
			blogPostList: state.blogPost.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getCategoryList,
			getPostList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class Blog extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		categoryId: PropTypes.string,
		// store
		store: PropTypes.shape({
			blogCategoryList: PropTypes.object.isRequired,
			blogPostList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getCategoryList: PropTypes.func.isRequired,
			getPostList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {
		categoryId: 'all',
	}

	state = {
		tab: this.props.categoryId,
		loading: true,
	}

	componentDidMount() {
		this.props.action.getCategoryList({ firstLoad: true });
		if (this.props.categoryId !== 'all') {
			this.filter.where.categoryId = this.props.categoryId;
		}
		this.props.action.getPostList({ firstLoad: true, filter: this.filter }, () => {
			this.setState({
				loading: false,
			});
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.categoryId !== this.props.categoryId) {
			if (nextProps.categoryId !== 'all') {
				this.filter.where.categoryId = nextProps.categoryId;
			} else {
				delete this.filter.where.categoryId;
			}
			this.filter.skip = 0;
			this.filter.page = 1;
			this.setState({
				loading: true,
			});
			nextProps.action.getPostList({ firstLoad: true, filter: this.filter }, () => {
				this.setState({
					loading: false,
				});
			}, () => {
				this.setState({
					loading: false,
				});
			});
		}
	}

	filter = {
		limit: 12,
		skip: 0,
		page: 1,
		order: 'publishedDate DESC',
		include: 'category',
		fields: { content: false },
		where: {
			state: 'published',
			publishedDate: { lte: new Date() },
		},
	}


	handleChangeTab = (tab) => {
		Router.push('/blog?categoryId=' + tab);

		this.setState({
			tab,
		});
	}

	handleChangePagination = (page) => {
		this.filter.skip = (page - 1) * this.filter.limit;
		this.filter.page = page;
		this.setState({
			loading: true,
		});
		this.props.action.getPostList({ firstLoad: true, filter: this.filter }, () => {
			this.setState({
				loading: false,
			});
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	render() {
		const { classes, store: { blogCategoryList, blogPostList } } = this.props;

		return (
			<div className={classes.root}>
				<Banner />
				<Tabs className={classes.tab} activeKey={this.state.tab} onChange={this.handleChangeTab}>
					<Tabs.TabPane tab="ALL" key="all" />
					{
						blogCategoryList.data.map((cate) => {
							return <Tabs.TabPane tab={cate.name} key={cate.id} />;
						})
					}
				</Tabs>
				<BlogList dataList={blogPostList.data} loading={this.state.loading} />
				{
					blogPostList.total > 0 &&
					<div className="text-center">
						<Pagination current={this.filter.page} total={blogPostList.total} pageSize={this.filter.limit} onChange={this.handleChangePagination} />
					</div>
				}

			</div>
		);
	}
}
