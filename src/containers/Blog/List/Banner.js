/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 11:17:21
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import Container from 'src/components/Layout/Container';

const styleSheet = (theme) => ({
	root: {
		background: 'url("/static/assets/images/Home/banner.jpg") no-repeat',
		height: 300,
		width: '100%',
		backgroundSize: 'cover',
		position: 'relative',
		'&:before': {
			content: '""',
			position: 'absolute',
			background: 'rgba(19, 97, 89, 0.92)',
			top: '0',
			right: '0',
			left: '0',
			bottom: '0',
			width: '100%',
			height: '100%',
		},
	},
	text: {
		position: 'absolute',
		zIndex: 1,
		top: '0',
		right: '0',
		left: '0',
		bottom: '0',
		width: '100%',
		height: '100%',
	},
	textWrapper: {
		padding: 15,
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		height: '100%',
		'& h2': {
			fontSize: '35px',
			color: '#fff',
		},
		'& p': {
			color: '#fff',
			fontSize: '20px',
		},
		'& strong': {
			color: '#fff',
			fontSize: '30px',
		},
		[theme.breakpoints.up.md]: {
			padding: 0,
		},
	},
});

const HomeBanner = (props) => {
	const { classes } = props;
	return (
		<div className={classes.root}>
			<div className={classes.text}>
				<Container className={classes.textWrapper}>
					<h2>Học tập và chia sẻ</h2>
					<p>
						Với hơn <strong>150 000</strong> người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức.
						Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam.
					</p>
				</Container>
			</div>
		</div>
	);
};

HomeBanner.propTypes = {
	classes: PropTypes.object.isRequired,
};

HomeBanner.defaultProps = {
	// classes: {},
};

export default withStyles(styleSheet)(HomeBanner);
