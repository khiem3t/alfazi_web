/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 13:42:08
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import { Checkbox, Radio } from 'antd';

import Sticky from 'react-stickynode';

import { classOptions, subjectOptions } from 'src/constants/selectOption';

import Tags from './Tags';

const CheckboxGroup = Checkbox.Group;
const styleSheet = (/* theme */) => ({
	root: {
		marginTop: 20,
	},
	listWrapper: {},
	list: {
		listStyle: 'none',
		padding: 0,
		marginBottom: '1em',
		'& > label': {
			display: 'block',
			padding: 5,
		},
	},
});

@withStyles(styleSheet)
export default class SideBarLeft extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		query: PropTypes.object,
		loading: PropTypes.bool,
		onChangeFilter: PropTypes.func.isRequired,
	}

	static defaultProps = {
		query: {
			class: [],
			subject: [],
		},
		loading: false,
	}

	handleChangeFilter = (value, key) => {
		this.props.onChangeFilter(value, key);
	}

	handleChangeType = (e) => {
		this.props.onChangeFilter(e.target.value, 'type');
	}

	render() {
		const { classes, query, loading } = this.props;

		if (loading) {
			return (
				<Sticky top={50} bottomBoundary="#content">
					<div className={classes.root}>
						<div className={classes.listWrapper}>
							<div className="loading-block" style={{ height: 20, marginBottom: 5, width: '70%' }} />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
						</div>
					</div>
					<div className={classes.root}>
						<div className={classes.listWrapper}>
							<div className="loading-block" style={{ height: 20, marginBottom: 5, width: '70%' }} />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
							<div className="loading-block" />
						</div>
					</div>
				</Sticky>
			);
		}
		return (
			<Sticky top={50} bottomBoundary="#content">
				<div className={classes.root}>
					<Tags query={query} />
					<div className={classes.listWrapper}>
						<h4 className={classes.title}>Loại</h4>
						<Radio.Group className={classes.list} onChange={this.handleChangeType} value={query.type || ''}>
							<Radio value="">Tất cả</Radio>
							<Radio value="free">Câu hỏi miễn phí</Radio>
							<Radio value="coin">Câu hỏi xu</Radio>
							<Radio value="Chưa có trả lời">Câu hỏi chưa có trả lời</Radio>
							<Radio value="Của bạn bè">Câu hỏi của bạn bè</Radio>
						</Radio.Group>
					</div>
					<div className={classes.listWrapper}>
						<h4 className={classes.title}>Môn học</h4>
						<CheckboxGroup value={query.subject ? query.subject.split(',') : []} className={classes.list} options={subjectOptions} onChange={(value) => this.handleChangeFilter(value, 'subject')} />
					</div>
					<div className={classes.listWrapper}>
						<h4 className={classes.title}>Lớp học</h4>
						<CheckboxGroup value={query.class ? query.class.split(',') : []} className={classes.list} options={classOptions} onChange={(value) => this.handleChangeFilter(value, 'class')} />
					</div>
				</div>
			</Sticky>
		);
	}
}
