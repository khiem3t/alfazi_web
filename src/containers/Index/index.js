/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-01-11 14:30:56
*------------------------------------------------------- */

import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Row, Col, Button } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import Router from 'next/router';

import AuthStorage from 'src/utils/AuthStorage';

import QuestionList from 'src/components/Question/List';
import Pin from 'src/components/Question/Pin';
import Container from 'src/components/Layout/Container';
import BtnAddQuestion from 'src/components/Question/BtnAdd';

import { getQuestionList, getPinList } from 'src/redux/actions/question';

import Banner from './Banner';
import SideBarRight from './SideBarRight';
import SideBarLeft from './SideBarLeft';
import MobileFilter from './MobileFilter';

const styleSheet = (theme) => ({
	tab: {
		marginTop: 5,
		[theme.breakpoints.up.lg]: {
			marginTop: 20,
		},
	},
});

const mapStateToProps = (state) => {
	return {
		store: {
			auth: state.auth,
			questionList: state.question.questionList,
			pinList: state.question.pinList,
			newQuestion: state.question.newQuestion,
		},
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getQuestionList,
			getPinList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class IndexPage extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		query: PropTypes.object,
		asPath: PropTypes.string,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
			questionList: PropTypes.object.isRequired,
			pinList: PropTypes.object.isRequired,
			newQuestion: PropTypes.number.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getQuestionList: PropTypes.func.isRequired,
			getPinList: PropTypes.func.isRequired,
		}).isRequired,

	}

	static defaultProps = {
		query: {},
		asPath: '',
	}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		// this._buildQuery();
		this._getData();
	}

	componentWillReceiveProps(nextProps) {
		this.query = nextProps.query;
		if (nextProps.asPath !== this.props.asPath) {
			this._getData();
		}
	}

	filter = {
		limit: 12,
		skip: 0,
		page: 1,
		order: 'updatedAt DESC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
			{
				relation: 'saves',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						creatorId: AuthStorage.userId,
					},
				},
			},
		],
		counts: ['answers', 'saves', 'reports'],
		where: {
			status: 'active',
			type: this.props.query.type || 'free',
		},
	}
	filterPin = {
		limit: 12,
		skip: 0,
		page: 1,
		order: 'updatedAt DESC',
		include: [
			{
				relation: 'creator',
				scope: {
					fields: ['id', 'username', 'avatar', 'fullName', 'role'],
				},
			},
			{
				relation: 'saves',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						creatorId: AuthStorage.userId,
					},
				},
			},
		],
		counts: ['answers', 'saves', 'reports'],
		where: {
			and: [{ pin: true }, { status: 'active' }],
		},
	}

	query = this.props.query

	_buildQuery = () => {
		if (Object.keys(this.query).length > 0) {
			let str = '/';

			Object.keys(this.query).forEach((key, i) => {
				if (this.query[key]) {
					if (Object.keys(this.query).length === i + 1) {
						if (i === 0) {
							str += `?${key}=${this.query[key]}`;
						} else {
							str += `${key}=${this.query[key]}`;
						}
					} else {
						if (i === 0) {
							str += `?${key}=${this.query[key]}&`;
						} else {
							str += `${key}=${this.query[key]}&`;
						}
					}
				}
			});

			Router.push(str);
		} else {
			Router.push('/');
		}
	}

	_getData = () => {
		this.setState({
			loading: true,
		});

		this.filter.where = { status: 'active' };

		if (Object.keys(this.query).length > 0) {
			Object.keys(this.query).forEach((key) => {
				if (this.query[key]) {
					if (key === 'class' || key === 'subject') {
						this.filter.where[key] = { inq: this.query[key].split(',') };
					} else {
						if (this.query[key] === 'Chưa có trả lời') {
							this.filter.where.answersCount = 0;
						} else if (this.query[key] === 'Của bạn bè') {
							const { friendsReceived = [], friendsSent = [] } = this.props.store.auth;

							const friendIdList = [
								...friendsReceived.map((el) => {
									return el.creatorId;
								}),
								...friendsSent.map((el) => {
									return el.friendId;
								}),
							];

							if (friendIdList.length > 0) {
								this.filter.where.creatorId = { inq: friendIdList };
							}
						} else {
							this.filter.where[key] = this.query[key];
						}
					}
				}
			});
		}

		this.filter.skip = 0;
		this.filter.page = 1;

		this.props.action.getQuestionList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
		this.props.action.getPinList({ filter: this.filterPin, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	handleRefresh = () => {
		this.setState({
			loading: true,
		});

		this.filter.page = 1;
		this.filter.skip = 0;

		this.props.action.getQuestionList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getQuestionList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	handleChangeFilter = (value, key) => {
		if (key === 'subject' || key === 'class') {
			this.query[key] = value.join(',');
		} else {
			this.query[key] = value;
		}

		this._buildQuery();
	}

	render() {
		const { store: { auth, newQuestion, questionList = { data: [] }, pinList = { data: [] } }, classes } = this.props;

		return (
			<div>
				<Banner />
				<Container>
					<Row gutter={20}>
						<Col xs={4} md={6} lg={5} className="hidden-sm-down">
							<SideBarLeft onChangeFilter={this.handleChangeFilter} query={this.query} />
						</Col>
						<Col xs={24} md={18} lg={12} className={classes.tab}>
							<MobileFilter>
								<SideBarLeft onChangeFilter={this.handleChangeFilter} query={this.query} />
							</MobileFilter>
							{
								!this.state.loading &&
								<Fragment>
									<BtnAddQuestion
										type={this.query.type}
										onRefresh={this.handleRefresh}
									/>
									{
										newQuestion > 0 &&
										<Button
											style={{
												marginBottom: '10px',
												width: '100%',
											}}
											type="primary"
											onClick={this.handleRefresh}
										>
											+{newQuestion} câu hỏi mới
										</Button>
									}

								</Fragment>
							}


							<Pin
								questionList={pinList}
								loading={this.state.loading}
								loadingMore={this.state.loadingMore}
							/>

							<QuestionList
								questionList={questionList}
								loading={this.state.loading}
								loadingMore={this.state.loadingMore}
								pin
							/>

							{
								!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < questionList.total &&
								<div className="text-center">
									<Button style={{ width: 300, margin: '0 auto' }} onClick={this.handleViewMore}>Xem thêm</Button>
								</div>
							}
						</Col>
						<div id="index-cropper" />
						<Col xs={6} lg={7} className="hidden-md-down">
							<SideBarRight userData={auth} />
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
