/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-18 16:00:02
*------------------------------------------------------- */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import { Modal, Button } from 'antd';

import FaSliders from 'react-icons/lib/fa/sliders';

import Router from 'next/router';

const styleSheet = (/* theme */) => ({
	root: {
		marginBottom: 5,
	},
	header: {
		background: '#fff',
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '0 10px 0 15px',
		'& h3': {
			margin: 0,
		},
	},
	tags: {
		background: '#fff',
		padding: '10px 15px',
		margin: '0',
		borderTop: '1px solid #e8e8e8',
	},
	btn: {
		fontSize: '18px',
		padding: '10px',
	},
	list: {
		listStyle: 'none',
		padding: 0,
		marginBottom: '1em',
		'& > label': {
			display: 'block',
			padding: 5,
		},
	},
	body: {
		padding: '1px 21px',
	},
	action: {
		textAlign: 'right',
		borderTop: '1px solid #e8e8e8',
		padding: '10px 20px',
	},
});

@withStyles(styleSheet)
export default class MobileFilter extends PureComponent {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		children: PropTypes.node.isRequired,
	}

	static defaultProps = {
	}

	state = {
		open: false,
	}

	handleOpenFilter = () => {
		this.setState({
			open: !this.state.open,
		});
	}

	handleClear = () => {
		Router.push('/');
		this.setState({
			open: false,
		});
	}

	render() {
		const { classes, children } = this.props;

		return (
			<div className={classes.root + ' hidden-md-up'}>
				<div className={classes.header}>
					<h3>Bộ lọc</h3>
					<a onClick={this.handleOpenFilter} className={classes.btn}>
						<FaSliders />
					</a>
				</div>
				<Modal
					title={null}
					style={{ top: 20 }}
					bodyStyle={{ padding: 0 }}
					visible={this.state.open}
					footer={null}
					onCancel={this.handleOpenFilter}
					closable={false}
				>
					<div className={classes.body}>
						{
							children
						}
					</div>
					<div className={classes.action}>
						<Button onClick={this.handleClear}>Xóa tất cả</Button>
						<Button onClick={this.handleOpenFilter} style={{ marginLeft: 10 }} type="primary">Xong</Button>
					</div>
				</Modal>
			</div>
		);
	}
}
