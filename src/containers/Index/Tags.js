/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-11 15:51:27
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';

import Router from 'next/router';

import { Tag } from 'antd';

const styleSheet = (theme) => ({
	root: {
		marginTop: 5,
		marginBottom: 20,
		[theme.breakpoints.up.lg]: {
			marginTop: 20,
		},
	},
	tagItem: {
		marginBottom: 4,
	},
	action: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: '10px',
		'& h4': {
			margin: '0',
		},
	},
});

const buildQuery = (query) => {
	if (Object.keys(query).length > 0) {
		let str = '/';

		Object.keys(query).forEach((key, i) => {
			if (query[key]) {
				if (Object.keys(query).length === i + 1) {
					if (i === 0) {
						str += `?${key}=${query[key]}`;
					} else {
						str += `${key}=${query[key]}`;
					}
				} else {
					if (i === 0) {
						str += `?${key}=${query[key]}&`;
					} else {
						str += `${key}=${query[key]}&`;
					}
				}
			}
		});

		Router.push(str);
	} else {
		Router.push('/');
	}
};

const Tags = (props) => {
	const { classes, query, className, showTitle } = props;

	if (Object.keys(query).length === 0) {
		return null;
	}

	const handleClose = (key, tag) => {
		if (key === 'subject' || key === 'class') {
			const list = query[key] ? query[key].split(',') : [];

			const index = list.findIndex(el => {
				return el === tag;
			});

			if (index >= 0) {
				list.splice(index, 1);
			}

			if (list.length > 0) {
				query[key] = list.join(',');
			} else {
				delete query[key];
			}
		} else {
			delete query[key];
		}

		buildQuery(query);
	};


	return (
		<div className={classes.root + ' ' + className}>
			{
				showTitle &&
				<div className={classes.action}>
					<h4 className={classes.title}>Lọc theo:</h4>
					<a onClick={() => Router.push('/')} style={{ fontSize: 12 }}>
						Xóa tất cả
					</a>
				</div>
			}

			{
				Object.keys(query).map((key) => {
					if (key === 'subject' || key === 'class') {
						return query[key].split(',').map((el) => {
							return (
								<Tag
									key={el}
									className={classes.tagItem}
									closable
									onClose={() => handleClose(key, el)}
								>
									<a onClick={() => Router.push(`/?${key}=${el}`)}>
										{el}
									</a>
								</Tag>
							);
						});
					}
					return (
						<Tag
							key={query[key]}
							className={classes.tagItem}
							closable
							onClose={() => handleClose(key, query[key])}
						>
							<a onClick={() => Router.push(`/?${key}=${query[key]}`)}>
								{query[key]}
							</a>
						</Tag>
					);
				})
			}
		</div>
	);
};

Tags.propTypes = {
	classes: PropTypes.object.isRequired,
	query: PropTypes.object,
	className: PropTypes.string,
	showTitle: PropTypes.bool,
};

Tags.defaultProps = {
	query: {},
	className: '',
	showTitle: true,
};

export default withStyles(styleSheet)(Tags);
