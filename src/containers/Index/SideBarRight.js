/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 13:42:08
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import Ranking from 'src/components/Widget/Ranking';
import TopQuestion from 'src/components/Widget/TopQuestion';
import UserCard from 'src/components/User/Card';

const styleSheet = (/* theme */) => ({
	root: {},
});

const SideBarRight = (props) => {
	const { classes, userData } = props;

	return (
		<Sticky top={50} bottomBoundary="#content">
			<div className={classes.root}>
				{
					AuthStorage.loggedIn &&
					<UserCard userData={userData} showProgressLevel />
				}
				<Ranking />
				<TopQuestion />
			</div>
		</Sticky>
	);
};

SideBarRight.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object.isRequired,
};

SideBarRight.defaultProps = {
	// classes: {},
};

export default withStyles(styleSheet)(SideBarRight);
