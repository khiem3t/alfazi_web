/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-06 14:28:42
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Row, Col, Tooltip } from 'antd';

import { Link } from 'src/routes';
import moment from 'moment';

import withStyles from 'src/theme/jss/withStyles';

const styleSheet = (theme) => ({
	root: {
		background: '#fff',
		marginBottom: 10,
		padding: '20px',
	},
	value: {
		fontSize: 18,
		color: theme.palette.primary[900],
		fontWeight: '500',
		padding: '5px 0',
		display: 'block',
	},
	subtract: {
		color: 'red',
	},
	received: {
		color: '#52c41a',
	},
	highlight: {
		backgroundColor: theme.palette.primary[50],
	},
	date: {
		fontSize: '12px',
		fontStyle: 'italic',
		cursor: 'pointer',
	},
});

@withStyles(styleSheet)
export default class AchievementItem extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		transactionCoinsData: PropTypes.object,
		loading: PropTypes.bool,
	}

	static defaultProps = {
		transactionCoinsData: {},
		loading: false,
	}

	_renderContent = () => {
		const { transactionCoinsData = {}, classes } = this.props;
		const { data = {}, question = {} } = transactionCoinsData;

		if (transactionCoinsData.type === 'answerPickCreated') {
			return (
				<div>
					<Link route={'/question/' + question.id}>Câu trả lời</Link> của bạn được người đặt câu hỏi ( Bằng Xu ) chọn là chính xác.
					<br />
					<Tooltip title={moment(transactionCoinsData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
						<span className={classes.date}>{moment(transactionCoinsData.createdAt).formatCustom()}</span>
					</Tooltip>
				</div>
			);
		}
		if (transactionCoinsData.type === 'answerPickRefund') {
			return (
				<div>
					Bạn được hoàn trả khi chọn câu trả lời đúng cho <Link route={'/question/' + question.id}>câu hỏi xu</Link> của mình
					<br />
					<Tooltip title={moment(transactionCoinsData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
						<span className={classes.date}>{moment(transactionCoinsData.createdAt).formatCustom()}</span>
					</Tooltip>
				</div>
			);
		}
		if (transactionCoinsData.type === 'receivedAchievement') {
			return (
				<div>
					Bạn nhận thưởng khi đạt được thành tựu.
					{
						data.achievement && (
							<div>
								<h3 style={{ marginBottom: 0 }}>{data.achievement.name}</h3>
								{data.achievement.desc}
							</div>
						)
					}
					<br />
					<Tooltip title={moment(transactionCoinsData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
						<span className={classes.date}>{moment(transactionCoinsData.createdAt).formatCustom()}</span>
					</Tooltip>
				</div>
			);
		}
		if (transactionCoinsData.type === 'changeQuestionTypeToCoin') {
			return (
				<div>
					Bạn đã đổi <Link route={'/question/' + question.id}>câu hỏi</Link> từ câu hỏi miễn phí sang câu hỏi xu.
					<br />
					<Tooltip title={moment(transactionCoinsData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
						<span className={classes.date}>{moment(transactionCoinsData.createdAt).formatCustom()}</span>
					</Tooltip>
				</div>
			);
		}
		if (transactionCoinsData.type === 'questionCreated') {
			return (
				<div>
					Bạn đã tạo <Link route={'/question/' + question.id}>câu hỏi</Link> bằng xu.
					<br />
					<Tooltip title={moment(transactionCoinsData.createdAt).format('[Ngày] D [Tháng] M [Năm] YYYY [Lúc] HH:mm')}>
						<span className={classes.date}>{moment(transactionCoinsData.createdAt).formatCustom()}</span>
					</Tooltip>

				</div>
			);
		}

		return transactionCoinsData.type;
	}

	render() {
		const { classes, transactionCoinsData, loading } = this.props;

		if (loading) {
			return (
				<div className={classes.root}>
					<Row gutter={20} type="flex" align="middle">
						<Col className="text-center-xs-down" xs={24} sm={16}>
							<div className="loading-block" style={{ height: 18, width: '40%' }} />
							<div className="loading-block" />
							<div className="loading-block" style={{ width: '70%' }} />
						</Col>
					</Row>
				</div>
			);
		}

		return (
			<div className={classes.root}>
				<Row gutter={20} type="flex" align="middle">
					<Col className="text-center-xs-down" xs={24} sm={18}>
						{this._renderContent()}
					</Col>
					<Col className="text-center-xs-down text-right" xs={24} sm={4}>
						<span className={classes.value + ' ' + (transactionCoinsData.amount < 0 && classes.subtract)}>{transactionCoinsData.amount < 0 ? transactionCoinsData.amount : ('+' + transactionCoinsData.amount)} Xu</span>
					</Col>
				</Row>
			</div>
		);
	}
}

