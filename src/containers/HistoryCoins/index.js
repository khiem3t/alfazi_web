/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-22 10:15:52
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import Container from 'src/components/Layout/Container';

import { getTransactionCoinsList } from 'src/redux/actions/transactionCoins';

import Item from './Item';

const styleSheet = (/* theme */) => ({
	root: {
		marginTop: 30,
	},
});

function mapStateToProps(state) {
	return {
		store: {
			transactionCoinsList: state.transactionCoins.list,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getTransactionCoinsList,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class Achievement extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		// store
		store: PropTypes.shape({
			transactionCoinsList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getTransactionCoinsList: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		this.props.action.getTransactionCoinsList({
			firstLoad: true,
			filter: this.filter,
		}, () => {
			this.setState({
				loading: false,
			});
		});
	}

	filter = {
		limit: 12,
		skip: 0,
		page: 1,
		include: [
			{
				relation: 'question',
			},
		],
		where: {
			creatorId: AuthStorage.userId,
		},
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getTransactionCoinsList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}


	render() {
		const { classes, store: { transactionCoinsList } } = this.props;

		if (!this.state.loading && transactionCoinsList.total === 0) {
			return (
				<div
					style={{
						textAlign: 'center',
						padding: '100px 20px',
					}}
				>
					Không có lịch sử nào.
				</div>
			);
		}

		return (
			<Container className={classes.root}>
				{
					this.state.loading ?
						[1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map((tran, i) => {
							return <Item key={i} loading />;
						}) :
						transactionCoinsList.data.map((tran) => {
							return <Item key={tran.id} transactionCoinsData={tran} />;
						})
				}
				{
					this.state.loadingMore && [1, 1].map((tran, i) => {
						return <Item key={i} loading />;
					})
				}
				{
					!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < transactionCoinsList.total &&
					<div className="text-center">
						<a onClick={this.handleViewMore}>Xem thêm lịch sử</a>
					</div>
				}
			</Container>
		);
	}
}
