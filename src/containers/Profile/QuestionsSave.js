/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-04-11 17:48:52
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Button } from 'antd';

// import withStyles from 'src/theme/jss/withStyles';

import AuthStorage from 'src/utils/AuthStorage';

import QuestionList from 'src/components/Question/List';

import { getSavedQuestionList } from 'src/redux/actions/questionSave';

// const styleSheet = (theme) => ({
// });

const mapStateToProps = (state) => {
	return {
		store: {
			auth: state.auth,
			questionSaveList: state.questionSave.list,
		},
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getSavedQuestionList,
		}, dispatch),
	};
};

// @withStyles(styleSheet)
@connect(mapStateToProps, mapDispatchToProps)
export default class IndexPage extends Component {
	static propTypes = {
		// classes: PropTypes.object.isRequired,
		userId: PropTypes.string.isRequired,
		// store
		store: PropTypes.shape({
			auth: PropTypes.object.isRequired,
			questionSaveList: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getSavedQuestionList: PropTypes.func.isRequired,
		}).isRequired,

	}

	static defaultProps = {
		// asPath: '',
	}

	state = {
		loading: true,
		loadingMore: false,
	}

	componentDidMount() {
		this._getData();
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.userId !== nextProps.userId) {
			this.filter.where = {
				creatorId: nextProps.userId,
			};
			this._getData();
		}
	}

	filter = {
		limit: 12,
		skip: 0,
		page: 1,
		order: 'createdAt DESC',
		include: [
			{
				relation: 'question',
				scope: {
					include: [
						{
							relation: 'creator',
							scope: {
								fields: ['id', 'username', 'avatar', 'fullName', 'role'],
							},
						},
						{
							relation: 'saves',
							scope: {
								limit: 1,
								skip: 0,
								where: {
									creatorId: AuthStorage.userId,
								},
							},
						},
					],
					counts: ['answers', 'saves', 'reports'],
				},
			},
		],
		where: {
			creatorId: this.props.userId,
		},
	}

	_getData = () => {
		this.setState({
			loading: true,
		});

		this.filter.skip = 0;
		this.filter.page = 1;

		this.props.action.getSavedQuestionList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	handleRefresh = () => {
		this.setState({
			loading: true,
		});

		this.filter.page = 1;
		this.filter.skip = 0;

		this.props.action.getSavedQuestionList({ filter: this.filter, firstLoad: true }, () => {
			this.setState({
				loading: false,
			});
		});
	}

	handleViewMore = () => {
		this.setState({
			loadingMore: true,
		});

		this.filter.skip = this.filter.limit * this.filter.page;
		this.props.action.getSavedQuestionList({ filter: this.filter }, () => {
			this.filter.page = this.filter.page + 1;
			this.setState({
				loadingMore: false,
			});
		}, () => {
			this.setState({
				loadingMore: false,
			});
		});
	}

	render() {
		const { store: { questionSaveList = { data: [] } } } = this.props;

		const questionList = questionSaveList.data.map((el) => {
			return el.question || {};
		});

		return (
			<div>
				<QuestionList
					questionList={{ ...questionSaveList, data: questionList }}
					loading={this.state.loading}
					loadingMore={this.state.loadingMore}
				/>

				{
					!this.state.loading && !this.state.loadingMore && this.filter.limit * this.filter.page < questionSaveList.total &&
					<div className="text-center">
						<Button type="primary" style={{ width: 300, margin: '0 auto' }} onClick={this.handleViewMore}>Xem thêm</Button>
					</div>
				}
			</div>
		);
	}
}
