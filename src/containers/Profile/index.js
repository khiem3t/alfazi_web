/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 08:23:18
*------------------------------------------------------- */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Sticky from 'react-stickynode';
import moment from 'src/utils/moment';

import Title from 'src/components/Layout/Title';
import Head from 'next/head';

import { Row, Col } from 'antd';

import withStyles from 'src/theme/jss/withStyles';
import withSizes from 'src/theme/jss/withSizes';

import Container from 'src/components/Layout/Container';
import ListFriend from 'src/components/User/ListFriend';
import TrackingList from 'src/components/Tracking/List';

import { getUserData } from 'src/redux/actions/user';

import AuthStorage from 'src/utils/AuthStorage';

import URL from 'src/constants/url';

import QuestionList from './Questions';
import QuestionSaveList from './QuestionsSave';
import Banner from './Banner';
import Intro from './Intro';

const styleSheet = (theme) => ({
	root: {
		marginTop: 5,
		[theme.breakpoints.up.md]: {
			marginTop: 20,
		},
	},
});

function mapStateToProps(state) {
	return {
		store: {
			userView: state.user.view,
			auth: state.auth,
		},
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		action: bindActionCreators({
			getUserData,
		}, dispatch),
	};
};

@withStyles(styleSheet)
@withSizes
@connect(mapStateToProps, mapDispatchToProps)
export default class Profile extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		userId: PropTypes.string.isRequired,
		breakpoints: PropTypes.string.isRequired,
		// store
		store: PropTypes.shape({
			userView: PropTypes.object.isRequired,
			auth: PropTypes.object.isRequired,
		}).isRequired,
		// action
		action: PropTypes.shape({
			getUserData: PropTypes.func.isRequired,
		}).isRequired,
	}

	static defaultProps = {}

	state = {
		loadingUser: false,
		tab: 'question',
	}

	componentDidMount() {
		if (this.props.userId !== AuthStorage.userId) {
			this.setState({ // eslint-disable-line
				loadingUser: true,
			});
			this.props.action.getUserData({ id: this.props.userId, filter: this.filterUser }, () => {
				this.setState({
					loadingUser: false,
				});
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.userId !== nextProps.userId) {
			this.setState({ // eslint-disable-line
				loadingUser: true,
				tab: 'question',
			});
			nextProps.action.getUserData({ id: nextProps.userId, filter: this.filterUser }, () => {
				this.setState({
					loadingUser: false,
				});
			});
		}
	}

	filterUser = {
		include: [
			{
				relation: 'badgesUnlock',
				scope: {
					include: 'badge',
				},
			},
			{
				relation: 'friendsSent',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						friendId: AuthStorage.userId,
					},
				},
			},
			{
				relation: 'friendsReceived',
				scope: {
					limit: 1,
					skip: 0,
					where: {
						creatorId: AuthStorage.userId,
					},
				},
			},
		],
		counts: ['questions', 'questionsSave', 'friendsSent', 'friendsReceived'],
		where: {
			status: 'active',
		},
	}

	handleChangTab = (tab) => {
		this.setState({
			tab,
		});
	}

	render() {
		const { store: { userView, auth }, classes, userId, breakpoints = 'lg' } = this.props;

		const userData = AuthStorage.userId === userId ? auth : userView;

		return (
			<div className={classes.a}>
				<Title>{`${userData.fullName} - Hồ sơ người dùng`}</Title>
				<Head>
					<meta
						name="description"
						content={`
							${userData.level === 0 ? '- Thành viên mới' : ' - Thành viên cấp ' + userData.level}

							- Điểm tích lũy: ${userData.pointsCount || 0}

							- Xu tích lũy: ${userData.coinsCount || 0}

							- Tham gia ngày: ${moment(userData.createdAt).format('LL')}

							${userData.desc || 'Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam.'}
						`}
					/>
					{/* Twitter */}
					<meta name="twitter:title" content={`${userData.fullName} - Hồ sơ người dùng | Alfazi`} />
					<meta
						name="twitter:description"
						content={`
							${userData.level === 0 ? '- Thành viên mới' : ' - Thành viên cấp ' + userData.level}

							- Điểm tích lũy: ${userData.pointsCount || 0}

							- Xu tích lũy: ${userData.coinsCount || 0}

							- Tham gia ngày: ${moment(userData.createdAt).format('LL')}

							${userData.desc || 'Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam.'}
						`}
					/>
					<meta name="twitter:image" content={userData.avatar || (URL.WEB_URL + '/static/assets/images/logo/256x256.png')} />
					{/* Facebook */}
					<meta property="og:url" content={URL.WEB_URL + '/profile/' + userId} />
					<meta property="og:title" content={`${userData.fullName} - Hồ sơ người dùng | Alfazi`} />
					<meta
						property="og:description"
						content={`
							${userData.level === 0 ? '- Thành viên mới' : ' - Thành viên cấp ' + userData.level}

							- Điểm tích lũy: ${userData.pointsCount || 0}

							- Xu tích lũy: ${userData.coinsCount || 0}

							- Tham gia ngày: ${moment(userData.createdAt).format('LL')}

							${userData.desc || 'Với hơn 150 000 người tham gia và đến với trang Alfazi để được hỏi đáp và chia sẻ kiến thức. Hãy tham gia vào cộng động chia sẻ kiến thức phổ thông lớn nhất Việt Nam.'}
						`}
					/>
					<meta property="og:image" content={userData.avatar || (URL.WEB_URL + '/static/assets/images/logo/256x256.png')} />
				</Head>

				<Banner
					onChangeTab={this.handleChangTab}
					userData={userData}
					loading={this.state.loadingUser}
				/>
				<Container className={classes.root}>
					<Row gutter={20}>
						{
							(breakpoints !== 'sm' && breakpoints !== 'xs') &&
							<Col xs={24} md={10} lg={8} className="hidden-sm-down">
								<Sticky top={70} bottomBoundary="#content">
									<Intro
										userData={userData}
										loading={this.state.loadingUser}
									/>
								</Sticky>
							</Col>
						}
						<Col xs={24} md={14} lg={16}>
							{
								this.state.tab === 'question' &&
								<QuestionList
									userId={userId}
								/>
							}
							{
								this.state.tab === 'questionSave' &&
								<QuestionSaveList
									userId={userId}
								/>
							}
							{
								this.state.tab === 'info' &&
								<Intro
									userData={userData}
									loading={this.state.loadingUser}
								/>
							}
							{
								this.state.tab === 'friend' &&
								<ListFriend userId={userId} />
							}
							{
								this.state.tab === 'history' &&
								<TrackingList />
							}
						</Col>
					</Row>
					<Row>
						<Col>
							<div id="index-cropper" />
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
