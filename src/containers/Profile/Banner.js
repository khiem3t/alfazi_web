/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-01 11:17:21
*------------------------------------------------------- */

import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'src/theme/jss/withStyles';
import withSizes from 'src/theme/jss/withSizes';

import { Tabs, Button, Menu, Dropdown } from 'antd';

import AuthStorage from 'src/utils/AuthStorage';

import Container from 'src/components/Layout/Container';
import Avatar from 'src/components/Stuff/Avatar';
import BtnAddFriend from 'src/components/User/BtnAddFriend';
import ModalReport from 'src/components/Modals/Report';
import BtnEditProfile from 'src/components/User/BtnEditProfile';

import FaFlag from 'react-icons/lib/fa/flag';
import MdKeyboardControl from 'react-icons/lib/md/keyboard-control';
import FaUserSecret from 'react-icons/lib/fa/user-secret';

const styleSheet = (theme) => ({
	root: {
		position: 'relative',
	},
	cover: {
		backgroundColor: theme.palette.primary[900] + ' !important',
		background: 'url("/static/assets/images/cover/cover-default-big.jpg") no-repeat',
		backgroundSize: 'cover !important',
		backgroundPosition: 'center center !important',
		height: 300,
		width: '100%',
		position: 'relative',
		[theme.breakpoints.down.sm]: {
			height: 350,
		},
		'&:before': {
			content: '""',
			position: 'absolute',
			background: 'rgba(0, 0, 0, 0.4)',
			top: '0',
			right: '0',
			left: '0',
			bottom: '0',
			width: '100%',
			height: '100%',
		},
	},
	tabWrapper: {
		background: '#fff',
		borderBottom: '1px solid #e8e8e8',
	},
	user: {
		zIndex: '1',
		textAlign: 'center',
		display: 'flex',
		alignItems: 'flex-end',
		[theme.breakpoints.down.sm]: {
			flexDirection: 'column',
			justifyContent: 'center',
			alignItems: 'center',
		},
	},
	name: {
		marginLeft: '15px',
		marginBottom: '15px',
		color: '#fff',
		textAlign: 'left',
		[theme.breakpoints.down.sm]: {
			margin: '10px 0',
			textAlign: 'center',
		},
		'& h1': {
			color: '#fff',
			marginBottom: 0,
		},
		'& span': {
			display: 'flex',
		},
	},
	avatar: {
		padding: '5px',
		background: '#fff',
		border: '1px solid #e8e8e8',
		display: 'flex',
		[theme.breakpoints.up.md]: {
			marginBottom: '-40px',
		},
	},
	tab: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'flex-end',
		'& .ant-tabs': {
			flex: '1',
			textAlign: 'right',
		},
		'& .ant-tabs .ant-tabs-bar': {
			margin: '0',
			borderBottom: 0,
			'& .ant-tabs-tab': {
				padding: '15px 16px',
			},
		},
		'& .ant-tabs-nav-wrap': {
			marginBottom: 0,
		},
	},
	btnMore: {
		padding: '15px 0 15px 40px',
		cursor: 'pointer',
	},
	info: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'flex-end',
		height: '100%',
		[theme.breakpoints.down.sm]: {
			flexDirection: 'column',
			justifyContent: 'center',
			alignItems: 'center',
		},
	},
	action: {
		marginBottom: '15px',
		display: 'flex',
		alignItems: 'center',
	},
	actionMore: {
		marginLeft: '5px',
		fontSize: '22px',
		padding: '5px',
		lineHeight: '17px',
	},
	number: {
		color: '#868585',
		// fontStyle: 'italic',
	},
	iconAdmin: {
		fontSize: 20,
		marginRight: 5,
	},
});

// const menuMore = (
// 	<Menu>
// 		<Menu.Item>
// 			Câu hỏi đã lưu
// 		</Menu.Item>
// 		<Menu.Item>
// 			Câu hỏi đang theo dõi
// 		</Menu.Item>
// 	</Menu>
// );

@withStyles(styleSheet)
@withSizes
export default class HomeBanner extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		breakpoints: PropTypes.string.isRequired,
		onChangeTab: PropTypes.func,
		userData: PropTypes.object.isRequired,
		loading: PropTypes.bool,
	}

	static defaultProps = {
		onChangeTab: f => f,
		loading: false,
	}

	state = {
		openModalReport: false,
	}

	handleSelectMenu = ({ key }) => {
		if (key === 'report') {
			this.setState({ openModalReport: true });
		}
	}

	handleCloseModal = () => {
		this.setState({ openModalReport: false });
	}

	render() {
		const { classes, onChangeTab, loading, userData = {}, breakpoints } = this.props;

		const { friendsReceived = [], friendsSent = [] } = userData;

		const relationship = [...friendsReceived, ...friendsSent];

		const menuAction = (
			<Menu onClick={this.handleSelectMenu}>
				<Menu.Item key="report">
					<FaFlag style={{ marginRight: 10 }} /> Báo cáo vi phạm
				</Menu.Item>
				{/* <Menu.Item key="2">
			<Icon type="minus-circle-o" style={{ marginRight: 10 }} /> Chặn
		</Menu.Item> */}
			</Menu>
		);

		if (loading) {
			return (
				<div className={classes.root}>
					<div className={classes.cover}>
						<Container className={classes.info}>
							<div className={classes.user}>
								<div className={classes.avatar}>
									<div className="loading-block" style={{ width: 150, height: 150, marginRight: 0 }} />
								</div>
								<div className="loading-block" style={{ width: 150, height: 30, marginLeft: 10, marginBottom: 10, marginTop: 10 }} />
							</div>
						</Container>
					</div>
					<div className={classes.tabWrapper} style={{ height: 50 }} />
				</div>
			);
		}

		return (
			<div className={classes.root}>
				<div className={classes.cover} style={{ backgroundImage: `url("${userData.cover ? userData.cover : '/static/assets/images/cover/cover-default.jpg'}")` }}>
					<Container className={classes.info}>
						<div className={classes.user}>
							<div className={classes.avatar}>
								<Avatar size={150} borderRadius="0" src={userData.avatar} name={userData.fullName} />
							</div>
							<div className={classes.name}>
								<h1>{userData.fullName}</h1>
								{
									userData.role === 'admin' ? // eslint-disable-line
										<span><FaUserSecret className={classes.iconAdmin} /> Quản trị viên</span> :
										userData.role === 'mod' ? // eslint-disable-line
											<span><FaUserSecret className={classes.iconAdmin} /> Điều hành viên</span> : // eslint-disable-line
											<span>{userData.level === 0 ? 'Thành viên mới' : 'Thành viên cấp ' + userData.level}</span>
								}
							</div>
						</div>
						<div className={classes.action}>
							{
								AuthStorage.userId === userData.id ?
									<BtnEditProfile userData={userData} /> :
									<Fragment>
										<BtnAddFriend friendId={userData.id} relationshipData={relationship[0]} />
										<Dropdown onClick={f => f} overlay={menuAction} trigger={['click']}>
											<Button style={{ marginLeft: 8 }} className={classes.actionMore}>
												<MdKeyboardControl />
											</Button>
										</Dropdown>
									</Fragment>
							}
						</div>
					</Container>
				</div>
				<div className={classes.tabWrapper}>
					<Container className={classes.tab}>
						<Tabs onChange={(tab) => onChangeTab(tab)} className="text-center-sm-down">
							<Tabs.TabPane tab="Câu hỏi" key="question" />
							<Tabs.TabPane tab="Câu hỏi đã lưu" key="questionSave" />
							{
								(breakpoints === 'sm' || breakpoints === 'xs') &&
								<Tabs.TabPane tab="Thông tin" key="info" />
							}
							<Tabs.TabPane tab="Bạn bè" key="friend" />
							{
								AuthStorage.userId === userData.id &&
								<Tabs.TabPane tab="Nhật ký hoạt động" key="history" />
							}
						</Tabs>
						{/* <Dropdown overlay={menuMore} trigger={['click']}>
							<div className={classes.btnMore}>
								Thêm <Icon type="down" />
							</div>
						</Dropdown> */}
					</Container>
				</div>
				{
					this.state.openModalReport &&
					<ModalReport
						visible={this.state.openModalReport}
						onCancel={this.handleCloseModal}
						destroyOnClose
						type="user"
						userData={userData}
					/>
				}
			</div>
		);
	}
}
