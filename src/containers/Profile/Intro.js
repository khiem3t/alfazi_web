/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-03-05 12:18:03
*------------------------------------------------------- */

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'src/utils/moment';

import AuthStorage from 'src/utils/AuthStorage';

import { Icon, Tooltip } from 'antd';

import withStyles from 'src/theme/jss/withStyles';

import Avatar from 'src/components/Stuff/Avatar';
import StatsBlock from 'src/components/User/StatsBlock';

const styleSheet = (theme) => ({
	root: {
		background: '#fff',
		padding: 20,
		minHeight: 'calc(100vh - 450px)',
	},
	item: {
		display: 'flex',
		alignItems: 'flex-start',
		borderBottom: '1px solid #e8e8e8',
		padding: '15px',
		'&:last-child': {
			borderBottom: 0,
		},
	},
	value: {
		marginLeft: '10px',
		fontWeight: '500',
		flex: 1,
		color: theme.palette.primary[900],
	},
	desc: {
		display: 'flex',
		alignItems: 'flex-end',
		margin: '30px 0 40px',
	},
	avatar: {
		zIndex: '5',
		position: 'relative',
		marginBottom: '-15px',
	},
	dialog: {
		color: 'black',
		padding: '10px 20px',
		position: 'relative',
		background: '#E5E5EA',
		borderRadius: '25px',
		width: '100%',
		textTransform: 'capitalize',
		marginLeft: '10px',
		flex: '1',
		'&:before': {
			content: '""',
			position: 'absolute',
			zIndex: '2',
			bottom: '-2px',
			left: '-7px',
			height: '20px',
			borderLeft: '20px solid #E5E5EA',
			borderBottomRightRadius: '16px 14px',
			WebkitTransform: 'translate(0, -2px)',
		},

		'&:after': {
			content: '""',
			position: 'absolute',
			zIndex: '3',
			bottom: '-2px',
			left: '4px',
			width: '26px',
			height: '20px',
			background: 'white',
			borderBottomRightRadius: '10px',
			WebkitTransform: 'translate(-30px, -2px)',
		},
	},
	stats: {
		display: 'flex',
		padding: '15px 10px',
		marginTop: 20,
	},
	statsItem: {
		flex: '1',
		textAlign: 'center',
		padding: '0 5px',
	},
	text: {},
	number: {
		fontSize: '26px',
		fontWeight: '500',
		color: theme.palette.primary[900],
	},
	level: {
		marginBottom: '30px',
	},
});

const Intro = (props) => {
	const { classes, userData, loading } = props;

	if (loading) {
		return (
			<div className={classes.root}>
				<div className="loading-block" style={{ width: '60%', height: 25, marginBottom: 10 }} />
				<div className={classes.desc}>
					<div className="loading-block" />
					<div className="loading-block" />
					<div className="loading-block" style={{ width: '80%' }} />
				</div>
				<div className={classes.item} style={{ marginTop: 20 }}>
					<div className={classes.label} style={{ flex: 1 }}>
						<div className="loading-block" />
					</div>
					<div className={classes.value} style={{ flex: 2 }}>
						<div className="loading-block" />
					</div>
				</div>
				<div className={classes.item}>
					<div className={classes.label} style={{ flex: 1 }}>
						<div className="loading-block" />
					</div>
					<div className={classes.value} style={{ flex: 2 }}>
						<div className="loading-block" />
					</div>
				</div>
				<div className={classes.item}>
					<div className={classes.label} style={{ flex: 1 }}>
						<div className="loading-block" />
					</div>
					<div className={classes.value} style={{ flex: 2 }}>
						<div className="loading-block" />
					</div>
				</div>
				<div className={classes.item}>
					<div className={classes.label} style={{ flex: 1 }}>
						<div className="loading-block" />
					</div>
					<div className={classes.value} style={{ flex: 2 }}>
						<div className="loading-block" />
					</div>
				</div>
				<div className={classes.item}>
					<div className={classes.label} style={{ flex: 1 }}>
						<div className="loading-block" />
					</div>
					<div className={classes.value} style={{ flex: 2 }}>
						<div className="loading-block" />
					</div>
				</div>
				<div className={classes.item}>
					<div className={classes.label} style={{ flex: 1 }}>
						<div className="loading-block" />
					</div>
					<div className={classes.value} style={{ flex: 2 }}>
						<div className="loading-block" />
					</div>
				</div>
				<div className={classes.item}>
					<div className={classes.label} style={{ flex: 1 }}>
						<div className="loading-block" />
					</div>
					<div className={classes.value} style={{ flex: 2 }}>
						<div className="loading-block" />
					</div>
				</div>
			</div>
		);
	}

	return (
		<div className={classes.root}>
			<div className={classes.level}>
				<StatsBlock userData={userData} />
			</div>
			<h4>Thông tin</h4>
			{
				userData.desc &&
				<div className={classes.desc}>
					<Avatar src={userData.avatar} name={userData.fullName} className={classes.avatar} />
					<div className={classes.dialog}>
						{userData.desc}
					</div>
				</div>
			}

			<div className={classes.item}>
				<div className={classes.label}>
					<Icon type="calendar" /> Tham gia ngày:
				</div>
				<div className={classes.value}>
					{moment(userData.createdAt).format('LL')}
				</div>

			</div>
			{
				userData.gender &&
				<div className={classes.item}>
					<div className={classes.label}>
						<Icon type="woman" /> Giới tính:
					</div>
					<div className={classes.value}>
						{userData.gender === 'male' ? 'Nam' : 'Nữ'}
					</div>
				</div>
			}
			{
				userData.phone && userData.id === AuthStorage.userId &&
				<div className={classes.item}>
					<div className={classes.label}>
						<Icon type="phone" /> Số điện thoại:
					</div>
					<div className={classes.value}>
						{userData.phone}
					</div>
					<div className={classes.help}>
						<Tooltip title="Chỉ có bạn mới có thể nhìn thấy thông tin này">
							<Icon type="lock" />
						</Tooltip>
					</div>
				</div>
			}
			{
				userData.email && userData.id === AuthStorage.userId &&
				<div className={classes.item}>
					<div className={classes.label}>
						<Icon type="mail" /> Email:
					</div>
					<div className={classes.value}>
						{userData.email}
					</div>
					<div className={classes.help}>
						<Tooltip title="Chỉ có bạn mới có thể nhìn thấy thông tin này">
							<Icon type="lock" />
						</Tooltip>
					</div>
				</div>
			}

			{
				userData.birthDate &&
				<div className={classes.item}>
					<div className={classes.label}>
						<Icon type="calendar" /> Ngày sinh:
					</div>
					<div className={classes.value}>
						{moment(userData.birthDate).format('LL')}
					</div>

				</div>
			}
			{
				userData.province &&
				<div className={classes.item}>
					<div className={classes.label}>
						<Icon type="cloud-o" /> Tình thành:
					</div>
					<div className={classes.value}>
						{userData.province}
					</div>

				</div>
			}
			{
				userData.school &&
				<div className={classes.item}>
					<div className={classes.label}>
						<Icon type="solution" /> Trường:
					</div>
					<div className={classes.value}>
						{userData.school}
					</div>

				</div>
			}
			{
				userData.class &&
				<div className={classes.item}>
					<div className={classes.label}>
						<Icon type="idcard" /> Lớp:
					</div>
					<div className={classes.value}>
						{userData.class}
					</div>
				</div>
			}
			{
				userData.address &&
				<div className={classes.item}>
					<div className={classes.label}>
						<Icon type="environment-o" /> Địa chỉ:
					</div>
					<div className={classes.value}>
						{userData.address}
					</div>
				</div>
			}
		</div>
	);
};

Intro.propTypes = {
	classes: PropTypes.object.isRequired,
	userData: PropTypes.object.isRequired,
	loading: PropTypes.bool,
};

Intro.defaultProps = {
	loading: false,
};

export default withStyles(styleSheet)(Intro);
