/* --------------------------------------------------------
* Author Khiem
* Email khiem3t@gmail.com

*
* Created: 2018-02-12 01:16:17
*------------------------------------------------------- */
function disableCacheDirectory(config) {
	config.module.rules
		.filter(({ loader }) => loader === 'babel-loader')
		.map(l => (l.options.cacheDirectory = false)); // eslint-disable-line
}

module.exports = {
	webpack: (config, { dev }) => {
		disableCacheDirectory(config);

		if (dev) {
			// 'eval-source-map'
			config.devtool = 'cheap-module-source-map'; // eslint-disable-line
		}

		config.module.rules.push(
			{
				test: /\.less$/,
				use: [{
					loader: 'emit-file-loader',
					options: {
						name: 'dist/[path][name].[ext]',
					},
				}, {
					loader: 'babel-loader',
				}, {
					loader: 'raw-loader',
				}, {
					loader: 'less-loader',
					options:{
						javascriptEnabled: true
					}
				}],
				include: /theme/,
			},
		);

		return config;
	},
};
